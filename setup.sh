# setupATLAS
# source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
########### v 1 ##############
#localSetupROOT 6.10.04-x86_64-slc6-gcc62-opt
########### v 2 ##############
# localSetupROOT 6.14.04-x86_64-slc6-gcc62-opt
# lsetup "cmake 3.11.0"
lsetup "cmake 3.14.0"
lsetup "gcc gcc620_x86_64_slc6"
# lsetup "boost boost-1.66.0-python2.7-x86_64-slc6-gcc62"
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"
##############################
# localSetupPython 2.7.4-x86_64-slc6-gcc48
lsetup "python 2.7.4-x86_64-slc6-gcc48"
export ROOTFOLDERPATH=`pwd`
export ROOTDATAPATH="${ROOTFOLDERPATH}/data"
export ROOTCFGPATH="${ROOTFOLDERPATH}/cfg"

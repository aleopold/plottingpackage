

void getTreeValues() {
  TFile* file = TFile::Open("/eos/user/a/aleopold/hgtd/results/merged/Jets.20585471.MERGED.root");


  TTreeReader reader("JetTree", file);

  TTreeReaderValue<int> m_tree_eventnumber(reader, "m_tree_eventnumber");

  while (reader.Next()) {
    cout << *m_tree_eventnumber << '\n';
  }
}


// bool eventPassesVBFSelection(
//     const std::vector<xAOD::Jet*>& pt_ordered_jets,
//     const xAOD::JetContainer* truth_jet_container, double mjj_cut /*GeV*/) {
//   //all jets given to the function were already checked to pass a RpT>0.15 cut
//
//   if(pt_ordered_jets.size() != 2) {return false;}
//   auto leading = pt_ordered_jets.at(0);
//   auto subleading = pt_ordered_jets.at(1);
//   //leading track >70GeV, subleading jet >50GeV
//   if(leading->pt()/1.e3 < 70.) {return false;}
//   if(subleading->pt()/1.e3 < 50.) {return false;}
//
//   //delta Eta > 3
//   double delta_eta = fabs(leading->eta() - subleading->eta());
//   if (delta_eta <=3 ) {return false;}
//
//   //check that leading is HS and subleading is PU
//   auto leading_category = determineJetCategory(leading, truth_jet_container);
//   if (leading_category != JetCategory::HS) {return false;}
//   auto subleading_category = determineJetCategory(subleading, truth_jet_container);
//   if (subleading_category == JetCategory::HS or subleading_category == JetCategory::NEITHER) {return false;}
//
//   //PU jet needs to be in hgtd acceptance
//   double abs_eta_sl = fabs(subleading->eta());
//   if(abs_eta_sl<2.4 or abs_eta_sl>4.0) {return false;}
//
//   return true;
// }

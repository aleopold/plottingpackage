#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

TString g_input_file_path_hits = "";
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
TString dataset_name = "VBFinv, mu=200";
TString descr = "pileupRejectionTruthT0_highpt";
bool print_png = true;
TString work_status = "Internal";
bool g_do_ratioplot = true;
double x_eff_min = 0.80;
double legend_text_size = 0.01;
bool g_use_logscale = false;

TFile* g_file_pileup = nullptr;
TFile* g_file_hits = nullptr;

void plot3p6(TString name1, TString name2,  TString label, TString plot_name = "TDRplot3p6") {
  auto hist_primes = (TH1F*) g_file_hits->Get(name1);
  if (hist_primes) {cout << "plot3p6: can't get histo\n";}
  // hist_primes->SetLineColor(kBlue);
  hist_primes->SetLineColor(kGreen+2);
  hist_primes->SetLineStyle(1);
  hist_primes->GetXaxis()->SetTitle("Time of Arrival - t_{0} [ns]");
  hist_primes->GetYaxis()->SetTitle("# Entries");
  auto hist_seconds = (TH1F*) g_file_hits->Get(name2);
  if (hist_primes) {cout << "plot3p6: can't get histo\n";}
  // hist_seconds->SetLineColor(kBlue);
  hist_seconds->SetLineColor(kGreen+2);
  hist_seconds->SetLineStyle(2);

  auto canvas = new TCanvas();
  canvas->SetLogy();

  auto axishisto = new TH1F("", "", 10, -1.5, 5.0);
  axishisto->GetYaxis()->SetRangeUser(1, 1.e6);
  axishisto->GetXaxis()->SetTitle("Time of Arrival - t_{0} [ns]");
  axishisto->GetYaxis()->SetTitle("# Entries");
  axishisto->Draw();

  hist_primes->Draw("same");
  hist_seconds->Draw("same");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.31, 0.88, text_color, work_status.Data());
  // atlas::myText(0.19, 0.83, text_color, "single #mu, p_{T} = 45 GeV, <#mu>=0");
  atlas::myText(0.19, 0.83, text_color, "single #pi, p_{T} = 20 GeV, <#mu>=0");
  atlas::myText(0.19, 0.77, text_color, "Timing scenario initial");
  atlas::myText(0.19, 0.72, text_color, label);

  TLegend* legend = new TLegend(0.7, 0.8, 0.95, 0.9);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.03);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->AddEntry(hist_primes, "Primaries", "l");
  legend->AddEntry(hist_seconds, "Secondaries", "l");
  legend->Draw("same");

  gPad->RedrawAxis();

  TString plot_name_string = Form("%s/%s.pdf",  output_plot_path.Data(), plot_name.Data());
  canvas->Print(plot_name_string);
  plot_name_string.Form("%s/%s.png",  output_plot_path.Data(), plot_name.Data());
  if (print_png) canvas->Print(plot_name_string);

  delete canvas;
  delete legend;
}


void tdrPlots() {
  TEnv env("pileup.cfg");

  g_input_file_path_hits = env.GetValue("input_file_path_hits", "default");
  g_do_ratioplot = env.GetValue("do_ratioplot", false);
  x_eff_min = env.GetValue("x_eff_min", 0.8);
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");
  g_use_logscale = env.GetValue("use_log_scale", false);

  g_file_hits = TFile::Open(g_input_file_path_hits, "READ");

  SetAtlasStyle();

  // plot3p6("m_hist_toa_tof_primaries", "m_hist_toa_tof_secondaries", "all layers", "TDRplot3p6_muon_alllayers");
  //
  // plot3p6("m_hist_toa_tof_primaries_layer_0", "m_hist_toa_tof_secondaries_layer_0", "layer 0", "TDRplot3p6_muon_layer0");
  //
  // plot3p6("m_hist_toa_tof_primaries_layer_3", "m_hist_toa_tof_secondaries_layer_3", "layer 3", "TDRplot3p6_muon_layer3");

  plot3p6("m_hist_toa_tof_primaries", "m_hist_toa_tof_secondaries", "all layers", "TDRplot3p6_pion20_alllayers");

  plot3p6("m_hist_toa_tof_primaries_layer_0", "m_hist_toa_tof_secondaries_layer_0", "layer 0", "TDRplot3p6_pion20_layer0");

  plot3p6("m_hist_toa_tof_primaries_layer_3", "m_hist_toa_tof_secondaries_layer_3", "layer 3", "TDRplot3p6_pion20_layer3");

  g_file_hits->Close();
}

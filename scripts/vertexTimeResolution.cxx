#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

#include "preparePaths.cxx"

TString g_input_file_path = "/afs/cern.ch/user/t/tawang/public/PileupRejection_100k.root";
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
TString dataset_name = "VBFinv, mu=200";
TString descr = "pileupRejectionTruthT0_highpt";
bool print_png = true;
TString work_status = "Internal";
bool g_do_ratioplot = true;
double x_eff_min = 0.80;
double legend_text_size = 0.01;
bool g_use_logscale = false;

const double m_td_fitmin = -0.05;
const double m_td_fitmax = 0.05;

TFile* g_file = nullptr;

TString g_config_file_name = "pileup.cfg";

double* fitGaussianCore(TH1F* hist) {
  // double m_td_fitmin = -0.05;
  // double m_td_fitmax = 0.05;
  TF1 *func = new TF1("func","gaus(0)",m_td_fitmin-1., m_td_fitmax+1.);

  hist->Fit(func, "0", "", m_td_fitmin, m_td_fitmax);
  // hist->Fit(func, "", "", m_td_fitmin, m_td_fitmax);

  double mu = func->GetParameter(1);
  double sig = func->GetParameter(2);

  double* params = new double[2];
  params[0] = mu;
  params[1] = sig;
  delete func;
  return params;
}

TF1* fitGaussianCoreFunction(TH1F* hist) {
  // double m_td_fitmin = -0.05;
  // double m_td_fitmax = 0.05;
  TF1 *func = new TF1("func","gaus(0)",m_td_fitmin-1., m_td_fitmax+1.);

  hist->Fit(func, "0", "", m_td_fitmin, m_td_fitmax);
  // hist->Fit(func, "", "", m_td_fitmin, m_td_fitmax);

  return func;
}


void plotting(TString histoname, TString postfix, TString label, bool do_fit = true) {
  TH1F* hist = (TH1F*) g_file->Get(histoname);
  if (not hist) {
    cout << "hist not found" << endl;
    return;
  }
  double* params = fitGaussianCore(hist);

  hist->Scale(1./hist->Integral(0, hist->GetNbinsX()+1));

  TF1* fit = nullptr;
  if (do_fit) {
    fit = fitGaussianCoreFunction(hist);
  }

  double bin_left = hist->GetXaxis()->FindBin(-2*params[1]);
  double bin_right = hist->GetXaxis()->FindBin(2*params[1]);
  double outliers = hist->Integral(0, bin_left) + hist->Integral(bin_right, hist->GetNbinsX()+1);
  double total = hist->Integral(0,hist->GetNbinsX()+1);

  //outliers 100ps window
  bin_left = hist->GetXaxis()->FindBin(-0.1);
  bin_right = hist->GetXaxis()->FindBin(0.1);
  double outliers100ps = hist->Integral(0, bin_left) + hist->Integral(bin_right, hist->GetNbinsX()+1);
  outliers100ps = outliers100ps/total;

  //----- P L O T T I N G -----


  TCanvas* canvas = new TCanvas();
  // hist->GetXaxis()->SetTitle("t_{VX}^{reco} - t_{VX}^{truth} [ns]");
  hist->GetXaxis()->SetTitle("t_{track}^{reco} - t_{track}^{truth} [ns]");
  hist->GetYaxis()->SetTitle("Arbitrary Units / 4 ps");

  if (g_use_logscale) {
    canvas->SetLogy();
    hist->SetAxisRange(0.00001, 0.4, "Y");
  } else {
    hist->SetAxisRange(0., 0.2, "Y");
  }


  if (do_fit) {
    fit->SetNpx(10000);
  }

  double marker_size = 0.8;

  hist->SetMarkerSize(marker_size);
  hist->SetMarkerStyle(20);
  hist->SetMarkerColor(kBlue);
  hist->Draw();
  if (do_fit) {
    fit->SetLineWidth(1);
    fit->SetLineColor(kBlue);
    fit->Draw("same");
  }


  TLegend* legend = new TLegend(0.65, 0.5, 0.90, 0.94);
  legend->SetTextSize(0.028);
  legend->SetBorderSize(0);
  legend->SetFillStyle(0);
  //#splitline{}{|t_{reco}-t_{truth}| > 2#times#sigma_{t}}
  if(do_fit) {
    TString legend_entry = "#splitline{#sigma_{t} = %.0f ps}{|t_{reco} - t_{truth}| > 2#times#sigma_{t}: %.1f %}";
    legend->AddEntry(fit, Form(legend_entry, 1, params[1]*1000, outliers/total*100.), "l");
  }

  //add up everything outside 100ps difference



  legend->Draw("same");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.89, text_color);
  atlas::myText(0.31, 0.89, text_color, work_status.Data());
  TString dataset_buff = "VBFinv, mu=200, Step 3.1";
  dataset_buff.ReplaceAll("mu", "<#mu>");
  dataset_buff.ReplaceAll("_", ", ");
  atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  atlas::myText(0.19, 0.79, text_color, dataset_buff.Data(), 0.04);
  // myText(0.19, 0.79, text_color, "VBFinv, #langle#mu#rangle=200, ITk Layout", 0.04);
  atlas::myText(0.19, 0.74, text_color, "p_{T}^{track} > 1.0 GeV", 0.035);
  atlas::myText(0.19, 0.69, text_color, "Timing scenario \"Initial\"", 0.035);
  atlas::myText(0.19, 0.64, text_color, label.Data(), 0.035);


  // TString outlierinfo = Form("fraction outside #pm 0.06ns: %.1f %%", outliers100ps);
  // atlas::myText(0.6, 0.89, text_color, outlierinfo.Data(), 0.035);


  //////// get the corresponding efficiency
  // TString eff_hist_name = histoname.ReplaceAll("_res_", "_has_time_");
  // auto eff_hist = (TH1F*) g_file->Get(eff_hist_name);
  // float total_entries = eff_hist->Integral();
  // float good_entries = eff_hist->GetBinContent(2);
  // atlas::myText(0.65, 0.85, text_color, Form("#epsilon = %.2f %%", 100*good_entries/total_entries), 0.04);

  TString plot_name;
  if (g_use_logscale) {
    plot_name = Form("%s/VxTimeResPlotLog_%s.pdf",  output_plot_path.Data(), postfix.Data());
  } else {
    plot_name = Form("%s/VxTimeResPlot_%s.pdf",  output_plot_path.Data(), postfix.Data());
  }
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (print_png) canvas->Print(plot_name);

  delete canvas;
  delete legend;
  if(do_fit) {
    delete fit;
  }

}

void vertexTimeResolution() {
  TEnv env(g_config_file_name);

  // g_input_file_path = env.GetValue("bdt_control_input_file_path", "default");
  // g_input_file_path = "/eos/user/a/aleopold/hgtd/results/merged/AccessorPerformance.20501250.MERGED.root";
  g_input_file_path = "/afs/cern.ch/work/a/aleopold/private/hgtd/newAnalysis/run/TrackTimeAccessors_Output_test.root";
  g_do_ratioplot = env.GetValue("do_ratioplot", false);
  x_eff_min = env.GetValue("x_eff_min", 0.8);
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");
  g_use_logscale = env.GetValue("use_log_scale", false);

  g_file = TFile::Open(g_input_file_path, "READ");

  SetAtlasStyle();

  preparePlottingDir(g_config_file_name);

  // plotting("m_hist_vertex_time_res_default", "DefaultVxTime_PF", "DefaultVxTime_PF", false);

  plotting("PFTQv05S_999_0.105_1.5_0.0/ALL_m_hist_time_res", "TimeResSmeared", "TimeResSmeared");

  plotting("PFTQv05S_999_0.109_1.5_0.01/ALL_m_hist_time_res", "TimeResSmeared2", "TimeResSmeared2");

  plotting("PFTQv05S_999_0.120_1.5_0.02/ALL_m_hist_time_res", "TimeResSmeared3", "TimeResSmeared3");

  plotting("PFTQv05S_999_0.138_1.5_0.03/ALL_m_hist_time_res", "TimeResSmeared4", "TimeResSmeared4");

  plotting("PFTQv05S_999_0.160_1.5_0.04/ALL_m_hist_time_res", "TimeResSmeared5", "TimeResSmeared5");

  plotting("PFTQv05S_999_0.183_1.5_0.05/ALL_m_hist_time_res", "TimeResSmeared6", "TimeResSmeared6");

  plotting("PFTQv05S_999_0.208_1.5_0.06/ALL_m_hist_time_res", "TimeResSmeared7", "TimeResSmeared7");

  plotting("PFTQv05S_999_0.235_1.5_0.07/ALL_m_hist_time_res", "TimeResSmeared8", "TimeResSmeared8");

  plotting("PFTQv05S_999_0.262_1.5_0.08/ALL_m_hist_time_res", "TimeResSmeared9", "TimeResSmeared9");

  plotting("PFTQv05/ALL_m_hist_time_res", "TimeResPFTQv05", "TimeResPFTQv05");


  // plotting("m_hist_vertex_time_res_v02", "TCv02_PFORv02");
  // plotting("m_hist_vertex_time_res_v02_truth", "TCv02_TruthTime");
  // plotting("m_hist_vertex_time_res_v03", "TCv03_PFORv02");
  // plotting("m_hist_vertex_time_res_v04", "TCv04PT2p0_PFORv02");
  // plotting("m_hist_vertex_time_res_v05", "TCv04PT1p5_PFORv02");
  // plotting("m_hist_vertex_time_res_v06", "TCv05PT1p5_PFORv02");
  // plotting("m_hist_vertex_time_res_hscluster", "TC_VX_time");

  // plotting("hist_res_bdt_selected_t0", "BDT_time", "BDT t0", true);
  //
  // for (int i=0; i<20; i++) {
  //   double cutval = 0.025*i;
  //   TString name = Form("hist_res_bdt_selected_t0_%.3f", cutval);
  //   TString plotname = Form("BDT_time_%.3f", cutval);
  //   plotting(name, plotname, plotname);
  // }

  // plotting("hist_res_bdt_selected_eventt0", "BDT_t0", "BDT_t0");
  //
  // plotting("hist_res_type1", "BDT_t0_type1", "Best HS cluster");
  // plotting("hist_res_type2", "BDT_t0_type2", ">50% HS cluster", false);
  // plotting("hist_res_type3", "BDT_t0_type3", "=50% HS cluster");
  // plotting("hist_res_type4", "BDT_t0_type4", "<50% HS cluster");
  // plotting("hist_res_type5", "BDT_t0_type5", "PU cluster", false);


  g_file->Close();
}

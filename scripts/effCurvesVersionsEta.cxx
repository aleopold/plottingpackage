#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

#include "preparePaths.cxx"

TString g_input_file_path = "default";
TString g_output_plot_path = "default";
int g_skip = 0;
TString g_dataset_name = "default";
TString g_dataset_description = "default";
bool g_print_png = false;
TString g_work_status = "default";
bool g_use_log_scale = false;
bool g_do_zoom = false;
TString g_descr = "default";

TFile* g_file = nullptr;

// TString g_config_file_name = "muon45_mu0.cfg";
// TString g_config_file_name = "pion0p1to5p0_mu0.cfg";
TString g_config_file_name = "VBFinv_mu200.cfg";

TString selected_tracks = "ALL";

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
////////////          H E L P E R          ////////////

TH1F* turnGraphIntoHist(TGraph* graph, TString name, TString label, int bins, double xmin, double xmax, int color);

///////////////////////////////////////////////////////


void efficiencyAndMistagLineDeltaT(TString accessor, TString track_type, TString vs_x, TString vs_x_bin, TString xlabel) {

  TString eff_name = Form("%s/%s_m_eff_vs_%s", accessor.Data(), track_type.Data(), vs_x.Data());
  auto eff = (TEfficiency*) g_file->Get(eff_name);
  if (eff==nullptr) {
    cout << "s_m_eff_vs_ histogram not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph = eff->CreateGraph();
  graph->SetMarkerStyle(21);
  graph->SetMarkerSize(1.);
  Color_t marker_color = kBlack;
  graph->SetMarkerColor(marker_color);
  graph->SetLineColor(marker_color);
  graph->SetLineWidth(4);
  // graph->GetYaxis()->SetRangeUser(0.,1.35);

  TString eff_name2 = Form("%s/%s_m_eff_passes_delta_t_cut_vs_%s", accessor.Data(), track_type.Data(), vs_x_bin.Data());
  auto eff2 = (TEfficiency*) g_file->Get(eff_name2);
  if (eff2==nullptr) {
    cout << "s_m_eff_passes_delta_t_cut_vs_ histogram not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph2 = eff2->CreateGraph();
  graph2->SetMarkerStyle(21);
  graph2->SetMarkerSize(1.);
  Color_t marker_color2 = kBlue;
  graph2->SetMarkerColor(marker_color2);
  graph2->SetLineColor(marker_color2);
  graph2->SetLineWidth(4);
  // graph2->GetYaxis()->SetRangeUser(0.,1.35);

  TString eff_name3 = Form("%s/%s_m_eff_mistag_delta_t_cut_vs_%s", accessor.Data(), track_type.Data(), vs_x_bin.Data());
  auto eff3 = (TEfficiency*) g_file->Get(eff_name3);
  if (eff3==nullptr) {
    cout << "s_m_eff_mistag_delta_t_cut_vs_ histogram not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph3 = eff3->CreateGraph();
  graph3->SetMarkerStyle(21);
  graph3->SetMarkerSize(1.);
  Color_t marker_color3 = kRed;
  graph3->SetMarkerColor(marker_color3);
  graph3->SetLineColor(marker_color3);
  graph3->SetLineWidth(4);
  // graph3->GetYaxis()->SetRangeUser(0.,1.35);

  //plotting part
  TCanvas *canvas = new TCanvas();
  // gPad->SetTickx(0);
  // canvas->SetTicks(1,1);

  // auto hist = new TH1F("axishist", ";truth #it{p}_{#it{T}} [GeV];track-time association efficiency", 10, 2.4, 4.0);
  auto hist = new TH1F("axishist", Form(";%s;time association rate", xlabel.Data()), 10, 2.4, 4.0);
  hist->SetAxisRange(0., 1.4, "Y");
  hist->Draw();

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.89, text_color);
  atlas::myText(0.31, 0.89, text_color, g_work_status.Data());

  graph->Draw("same P");
  graph2->Draw("same P");
  graph3->Draw("same P");


  TString dataset_descr = "VBF H #rightarrow invisible, #LT#mu#GT=200 ";
  if (g_config_file_name == "muon45_mu0.cfg") {
    // dataset_descr = "Single muon, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0";
    dataset_descr = "#mu^{+}, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0 ";
  } else if (g_config_file_name == "pion0p1to5p0_mu0.cfg") {
    // dataset_descr = "Single pion, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0";
    dataset_descr = "#pi^{+}, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0 ";
  }
  // atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  atlas::myText(0.19, 0.84, text_color, dataset_descr.Data(), 0.035);
  // myText(0.19, 0.79, text_color, "VBFinv, #langle#mu#rangle=200 ", 0.04);
  atlas::myText(0.19, 0.79, text_color, "p_{T}^{track} > 1.0 GeV", 0.035);
  atlas::myText(0.19, 0.74, text_color, "Timing scenario \"Initial\"", 0.035);

  auto legend = new TLegend(0.62, 0.7, 0.9, 0.92);
  legend->SetFillStyle(0);
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.035);
  legend->AddEntry(graph, "Total", "lp");
  legend->AddEntry(graph2, "Correctly reconstructed", "lp");
  legend->AddEntry(graph3, "Misassignment", "lp");

  legend->Draw("same");

  gPad->RedrawAxis();

  TString plot_name = Form("%s/efficiencyAndMistagLineDeltaT%s_%s_%s_%s.pdf",  g_output_plot_path.Data(), vs_x.Data(), accessor.Data(),  g_dataset_name.Data(), vs_x_bin.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (g_print_png) canvas->Print(plot_name);

  delete canvas;
  delete hist;
  delete graph;
}


void efficiencyAndMistagLinePrimeFractionGT50(TString accessor, TString track_type, TString vs_x, TString vs_x_bin, TString xlabel) {

  TString eff_name = Form("%s/%s_m_eff_vs_%s", accessor.Data(), track_type.Data(), vs_x.Data());
  auto eff = (TEfficiency*) g_file->Get(eff_name);
  if (eff==nullptr) {
    cout << "s_m_eff_vs_ histogram not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph_total = eff->CreateGraph();
  graph_total->SetMarkerStyle(21);
  graph_total->SetMarkerSize(1.);
  Color_t marker_color = kBlack;
  graph_total->SetMarkerColor(marker_color);
  graph_total->SetLineColor(marker_color);
  graph_total->SetLineWidth(4);

  TString eff_name2 = Form("%s/%s_m_eff_gt50pcprimes_vs_%s", accessor.Data(), track_type.Data(), vs_x.Data());
  auto eff2 = (TEfficiency*) g_file->Get(eff_name2);
  if (eff2==nullptr) {
    cout << "histogram " << eff_name2 <<  " not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph2 = eff2->CreateGraph();
  graph2->SetMarkerStyle(21);
  graph2->SetMarkerSize(1.);
  Color_t marker_color2 = kBlue;
  graph2->SetMarkerColor(marker_color2);
  graph2->SetLineColor(marker_color2);
  graph2->SetLineWidth(4);
  // graph2->GetYaxis()->SetRangeUser(0.,1.35);

  TString eff_name3 = Form("%s/%s_m_eff_gt50pcprimes_vs_%s_mistag", accessor.Data(), track_type.Data(), vs_x.Data());
  auto eff3 = (TEfficiency*) g_file->Get(eff_name3);
  if (eff3==nullptr) {
    cout << "s_m_eff_gt50pcprimes_vs__mistag histogram not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph3 = eff3->CreateGraph();
  graph3->SetMarkerStyle(21);
  graph3->SetMarkerSize(1.);
  Color_t marker_color3 = kRed;
  graph3->SetMarkerColor(marker_color3);
  graph3->SetLineColor(marker_color3);
  graph3->SetLineWidth(4);


  TCanvas *canvas = new TCanvas();

  auto hist = new TH1F("axishist", Form(";%s; time association rate", xlabel.Data()), 10, 2.4, 4.0);
  hist->SetAxisRange(0., 1.4, "Y");
  hist->Draw();

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.89, text_color);
  atlas::myText(0.31, 0.89, text_color, g_work_status.Data());

  // graph_total->Draw("same P");
  graph2->Draw("same P");
  graph3->Draw("same P");


  TString dataset_descr = "VBF H #rightarrow invisible, #LT#mu#GT=200 ";
  if (g_config_file_name == "muon45_mu0.cfg") {
    // dataset_descr = "Single muon, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0";
    dataset_descr = "#mu^{+}, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0 ";
  } else if (g_config_file_name == "pion0p1to5p0_mu0.cfg") {
    // dataset_descr = "Single pion, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0";
    dataset_descr = "#pi^{+}, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0 ";
  }
  // atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  atlas::myText(0.19, 0.84, text_color, dataset_descr.Data(), 0.035);
  // myText(0.19, 0.79, text_color, "VBFinv, #langle#mu#rangle=200 ", 0.04);
  atlas::myText(0.19, 0.79, text_color, "p_{T}^{track} > 1.0 GeV", 0.035);
  atlas::myText(0.19, 0.74, text_color, "Timing scenario \"Initial\"", 0.035);

  auto legend = new TLegend(0.62, 0.7, 0.9, 0.92);
  legend->SetFillStyle(0);
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.035);
  // legend->AddEntry(graph_total, "Total", "lp");
  legend->AddEntry(graph2, "Correctly reconstructed", "lp");
  legend->AddEntry(graph3, "Misassignment", "lp");

  legend->Draw("same");

  gPad->RedrawAxis();

  TString plot_name = Form("%s/efficiencyAndMistagLinePrimeFractionGT50Vs%s_%s_%s_%s.pdf",  g_output_plot_path.Data(), vs_x.Data(), accessor.Data(), track_type.Data(), g_dataset_name.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (g_print_png) canvas->Print(plot_name);

}

void efficiencyAndMistagLinePrimeFractionGE50(TString accessor, TString track_type, TString vs_x, TString vs_x_bin, TString xlabel) {

  TString eff_name = Form("%s/%s_m_eff_vs_%s", accessor.Data(), track_type.Data(), vs_x.Data());
  auto eff = (TEfficiency*) g_file->Get(eff_name);
  if (eff==nullptr) {
    cout << "s_m_eff_vs_ histogram not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph_total = eff->CreateGraph();
  graph_total->SetMarkerStyle(21);
  graph_total->SetMarkerSize(1.);
  Color_t marker_color = kBlack;
  graph_total->SetMarkerColor(marker_color);
  graph_total->SetLineColor(marker_color);
  graph_total->SetLineWidth(4);

  TString eff_name2 = Form("%s/%s_m_eff_ge50pcprimes_vs_%s", accessor.Data(), track_type.Data(), vs_x.Data());
  auto eff2 = (TEfficiency*) g_file->Get(eff_name2);
  if (eff2==nullptr) {
    cout << "histogram " << eff_name2 <<  " not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph2 = eff2->CreateGraph();
  graph2->SetMarkerStyle(21);
  graph2->SetMarkerSize(1.);
  Color_t marker_color2 = kBlue;
  graph2->SetMarkerColor(marker_color2);
  graph2->SetLineColor(marker_color2);
  graph2->SetLineWidth(4);
  // graph2->GetYaxis()->SetRangeUser(0.,1.35);

  TString eff_name3 = Form("%s/%s_m_eff_ge50pcprimes_vs_%s_mistag", accessor.Data(), track_type.Data(), vs_x.Data());
  auto eff3 = (TEfficiency*) g_file->Get(eff_name3);
  if (eff3==nullptr) {
    cout << "s_m_eff_gt50pcprimes_vs__mistag histogram not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph3 = eff3->CreateGraph();
  graph3->SetMarkerStyle(21);
  graph3->SetMarkerSize(1.);
  Color_t marker_color3 = kRed;
  graph3->SetMarkerColor(marker_color3);
  graph3->SetLineColor(marker_color3);
  graph3->SetLineWidth(4);


  TCanvas *canvas = new TCanvas();

  auto hist = new TH1F("axishist", Form(";%s; time association rate", xlabel.Data()), 10, 2.4, 4.0);
  hist->SetAxisRange(0., 1.4, "Y");
  hist->Draw();

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.89, text_color);
  atlas::myText(0.31, 0.89, text_color, g_work_status.Data());

  graph_total->Draw("same P");
  graph2->Draw("same P");
  graph3->Draw("same P");


  TString dataset_descr = "VBF H #rightarrow invisible, #LT#mu#GT=200 ";
  if (g_config_file_name == "muon45_mu0.cfg") {
    // dataset_descr = "Single muon, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0";
    dataset_descr = "#mu^{+}, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0 ";
  } else if (g_config_file_name == "pion0p1to5p0_mu0.cfg") {
    // dataset_descr = "Single pion, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0";
    dataset_descr = "#pi^{+}, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0 ";
  }
  // atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  atlas::myText(0.19, 0.84, text_color, dataset_descr.Data(), 0.035);
  // myText(0.19, 0.79, text_color, "VBFinv, #langle#mu#rangle=200 ", 0.04);
  atlas::myText(0.19, 0.79, text_color, "p_{T}^{track} > 1.0 GeV", 0.035);
  atlas::myText(0.19, 0.74, text_color, "Timing scenario \"Initial\"", 0.035);

  auto legend = new TLegend(0.62, 0.7, 0.9, 0.92);
  legend->SetFillStyle(0);
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.035);
  legend->AddEntry(graph_total, "Total", "lp");
  legend->AddEntry(graph2, "Correctly reconstructed", "lp");
  legend->AddEntry(graph3, "Misassignment", "lp");

  legend->Draw("same");

  gPad->RedrawAxis();

  TString plot_name = Form("%s/efficiencyAndMistagLinePrimeFractionGE50Vs%s_%s_%s.pdf",  g_output_plot_path.Data(), vs_x.Data(), accessor.Data(),  g_dataset_name.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (g_print_png) canvas->Print(plot_name);

}

void efficiencyStackPlotAllCases(TString accessor, TString track_type, TString vs_x, TString vs_x_bin, TString xlabel) {

  TString eff_name = Form("%s/%s_m_eff_vs_%s", accessor.Data(), track_type.Data(), vs_x.Data());
  auto eff = (TEfficiency*) g_file->Get(eff_name);
  if (eff==nullptr) {
    cout << "s_m_eff_vs_ histogram not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph_total = eff->CreateGraph();
  graph_total->SetMarkerStyle(21);
  graph_total->SetMarkerSize(0);
  Color_t marker_color = kBlack;
  graph_total->SetMarkerColor(marker_color);
  graph_total->SetLineColor(marker_color);
  graph_total->SetLineWidth(2);

  TString name_template = "%s/%s_m_eff_vs_%s_primesfrac%s";

  std::vector<TString> primes_fractions = {"AllPrimes", "MoreThanHalfPrimes", "HalfPrimesHasPrimes", "LessThanHalfPrimes","NoPrimesNoPossiblePrimes", "NoPrimes1PossiblePrimes", "NoPrimes2PossiblePrimes", "NoPrimes3PossiblePrimes", "NoPrimes4PossiblePrimes"};

  std::vector<TString> labels = {"Prime Frac. = 1", "0.5 < Prime Frac. < 1", "Prime Frac. = 0.5", "0 < Prime Frac. < 0.5", "Misassignment", "Confusion"};

  // std::vector<Color_t> colors = {kTeal+4, kTeal+2, kTeal, kTeal-9, kMagenta, kRed, kRed-7, kRed+2, kRed+3};
  std::vector<Color_t> colors = {kTeal+4, kTeal+2, kTeal, kTeal-9, kMagenta, kRed, kRed, kRed, kRed};

  std::vector<TEfficiency*> effs;
  std::vector<TH1F*> hists;

  for(int i=0; i< primes_fractions.size(); i++) {
    TString plotname = Form(name_template, accessor.Data(), track_type.Data(), vs_x.Data(), primes_fractions.at(i).Data());
    auto eff = (TEfficiency*) g_file->Get(plotname);
    if (eff == nullptr) {
      std::cout << "efficiencyAndMistagLinePrimeFractionGT50 ERROR\n";
      std::cout << "could not find " << plotname << '\n';
      return;
    }
    eff->SetFillColor(colors.at(i));
    effs.push_back(eff);
  }

  THStack* stack_hist = new THStack("stack_hist",";|#eta|; time association rate");

  for (int i=0; i<effs.size(); i++) {
    auto graph = effs.at(i)->CreateGraph();
    TH1F* hist = turnGraphIntoHist(graph, graph->GetName()+TString::Format("h%i", i), "", 16, 2.4, 4.0, effs.at(i)->GetFillColor());
    hist->SetFillColor(colors.at(i));
    hists.push_back(hist);
    stack_hist->Add(hist);
  }

  TCanvas* canvas = new TCanvas();
  canvas->SetTicks(1,1);

  auto hist = new TH1F("axishist", Form(";%s; time association rate", xlabel.Data()), 10, 2.4, 4.0);
  hist->SetAxisRange(0., 1.4, "Y");
  hist->Draw();

  stack_hist->Draw("same");

  graph_total->Draw("same p");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.89, text_color);
  atlas::myText(0.31, 0.89, text_color, g_work_status.Data());

  TString dataset_descr = "VBF H #rightarrow invisible, #LT#mu#GT=200 ";
  if (g_config_file_name == "muon45_mu0.cfg") {
    // dataset_descr = "Single muon, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0";
    dataset_descr = "#mu^{+}, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0 ";
  } else if (g_config_file_name == "pion0p1to5p0_mu0.cfg") {
    // dataset_descr = "Single pion, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0";
    dataset_descr = "#pi^{+}, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0 ";
  }
  // atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  atlas::myText(0.19, 0.84, text_color, dataset_descr.Data(), 0.035);
  // myText(0.19, 0.79, text_color, "VBFinv, #langle#mu#rangle=200 ", 0.04);
  atlas::myText(0.19, 0.79, text_color, "p_{T}^{track} > 1.0 GeV", 0.035);
  atlas::myText(0.19, 0.74, text_color, "Timing scenario \"Initial\"", 0.035);

  auto legend = new TLegend(0.6, 0.73, 0.85, 0.92);

  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.025);

  legend->AddEntry(graph_total, "Total", "lp");
  for (int i=0; i<labels.size(); i++) {
    legend->AddEntry(hists.at(i), labels.at(i), "f");
  }
  // legend->AddEntry(hists.at(0), "Segment found", "f");
  // legend->AddEntry(hists.at(1), "No hits in cone", "f");
  // legend->AddEntry(hists.at(2), "No seed found", "f");
  // legend->AddEntry(hists.at(3), "No working fit", "f");
  // legend->AddEntry(hists.at(4), "No segment passed cut", "f");

  legend->Draw("same");

  gPad->RedrawAxis();

  TString plot_name = Form("%s/efficiencyStackPlotAllCases_%s_%s_%s_%s.pdf",  g_output_plot_path.Data(), track_type.Data(), vs_x.Data(), accessor.Data(),  g_dataset_name.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (g_print_png) canvas->Print(plot_name);

}



void efficiencyStackPlotGoodBad(TString accessor, TString track_type, TString vs_x, TString vs_x_bin, TString xlabel) {

  std::vector<TObject*> cleanup;

  TString eff_name = Form("%s/%s_m_eff_vs_%s", accessor.Data(), track_type.Data(), vs_x.Data());
  auto eff = (TEfficiency*) g_file->Get(eff_name);
  if (eff==nullptr) {
    cout << "s_m_eff_vs_ histogram not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph_total = eff->CreateGraph();
  cleanup.push_back(graph_total);
  graph_total->SetMarkerStyle(21);
  graph_total->SetMarkerSize(1.);
  Color_t marker_color = kBlack;
  graph_total->SetMarkerColor(marker_color);
  graph_total->SetLineColor(marker_color);
  graph_total->SetLineWidth(4);

  TString name_template = "%s/%s_m_eff_vs_%s_primesfrac%s";

  std::vector<TString> primes_fractions_good = {"AllPrimes", "MoreThanHalfPrimes"};

  std::vector<TString> primes_fractions_bad = {"HalfPrimesHasPrimes", "LessThanHalfPrimes","NoPrimesNoPossiblePrimes", "NoPrimes1PossiblePrimes", "NoPrimes2PossiblePrimes", "NoPrimes3PossiblePrimes", "NoPrimes4PossiblePrimes"};

  std::vector<Color_t> colors = {kTeal+4, kMagenta};
  std::vector<TString> labels = {"Correctly reconstructed", "Misassigned"};

  // TEfficiency* good_eff = new TEfficiency("good_eff", Form(";%s;", vs_x.Data()), 16, 2.4, 4.0);
  // cleanup.push_back(good_eff);
  // TEfficiency* bad_eff = new TEfficiency("bad_eff", Form(";%s;", vs_x.Data()), 16, 2.4, 4.0);
  TH1F* good_eff = new TH1F("good_eff", Form(";%s;", vs_x.Data()), 16, 2.4, 4.0);
  cleanup.push_back(good_eff);
  TH1F* bad_eff = new TH1F("bad_eff", Form(";%s;", vs_x.Data()), 16, 2.4, 4.0);

  cleanup.push_back(bad_eff);
  // std::vector<TEfficiency*> effs;
  std::vector<TH1F*> effs;
  std::vector<TH1F*> hists;

  for(int i=0; i< primes_fractions_good.size(); i++) {
    TString plotname = Form(name_template, accessor.Data(), track_type.Data(), vs_x.Data(), primes_fractions_good.at(i).Data());
    auto eff = (TEfficiency*) g_file->Get(plotname);
    if (eff == nullptr) {
      std::cout << "efficiencyStackPlotGoodBad ERROR\n";
      std::cout << "could not find " << plotname << '\n';
      return;
    }
    // good_eff->Add(*eff);
    TGraphAsymmErrors* graph = eff->CreateGraph();
    TH1F* hist = turnGraphIntoHist(graph, graph->GetName()+TString::Format("goodh%i", i), "", 16, 2.4, 4.0, colors.at(0));
    cleanup.push_back(hist);
    good_eff->Add(hist);
  }
  good_eff->SetFillColor(colors.at(0));
  // std::cout << "good total eff: " << good_eff->GetEfficiency(3) << '\n';

  for(int i=0; i< primes_fractions_bad.size(); i++) {
    TString plotname = Form(name_template, accessor.Data(), track_type.Data(), vs_x.Data(), primes_fractions_bad.at(i).Data());
    auto eff = (TEfficiency*) g_file->Get(plotname);
    if (eff == nullptr) {
      std::cout << "efficiencyStackPlotGoodBad ERROR\n";
      std::cout << "could not find " << plotname << '\n';
      return;
    }
    // bad_eff->Add(*eff);
    TGraphAsymmErrors* graph = eff->CreateGraph();
    TH1F* hist = turnGraphIntoHist(graph, graph->GetName()+TString::Format("badh%i", i), "", 16, 2.4, 4.0, colors.at(1));
    cleanup.push_back(hist);
    bad_eff->Add(hist);
  }
  bad_eff->SetFillColor(colors.at(1));
  // std::cout << "bad total eff: " << bad_eff->GetEfficiency(3) << '\n';

  effs.push_back(good_eff);
  effs.push_back(bad_eff);

  THStack* stack_hist = new THStack("stack_hist",";|#eta|; time association rate");
  cleanup.push_back(stack_hist);

  for (int i=0; i<effs.size(); i++) {
    // auto graph = effs.at(i)->CreateGraph();
    // TH1F* hist = turnGraphIntoHist(graph, graph->GetName()+TString::Format("h%i", i), "", 16, 2.4, 4.0, effs.at(i)->GetFillColor());
    // cleanup.push_back(hist);
    // hist->SetFillColor(colors.at(i));
    // hists.push_back(hist);
    // stack_hist->Add(hist);
    hists.push_back(effs.at(i));
    stack_hist->Add(effs.at(i));
  }

  TCanvas* canvas = new TCanvas();
  cleanup.push_back(canvas);
  canvas->SetTicks(1,1);

  auto hist = new TH1F("axishist", Form(";%s; time association rate", xlabel.Data()), 10, 2.4, 4.0);
  cleanup.push_back(hist);
  hist->SetAxisRange(0., 1.4, "Y");
  hist->Draw();

  stack_hist->Draw("same");

  graph_total->Draw("same P");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.89, text_color);
  atlas::myText(0.31, 0.89, text_color, g_work_status.Data());

  TString dataset_descr = "VBF H #rightarrow invisible, #LT#mu#GT=200 ";
  if (g_config_file_name == "muon45_mu0.cfg") {
    // dataset_descr = "Single muon, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0";
    dataset_descr = "#mu^{+}, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0 ";
  } else if (g_config_file_name == "pion0p1to5p0_mu0.cfg") {
    // dataset_descr = "Single pion, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0";
    dataset_descr = "#pi^{+}, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0 ";
  }
  // atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  atlas::myText(0.19, 0.84, text_color, dataset_descr.Data(), 0.035);
  // myText(0.19, 0.79, text_color, "VBFinv, #langle#mu#rangle=200 ", 0.04);
  atlas::myText(0.19, 0.79, text_color, "p_{T}^{track} > 1.0 GeV", 0.035);
  atlas::myText(0.19, 0.74, text_color, "Timing scenario \"Initial\"", 0.035);

  auto legend = new TLegend(0.6, 0.73, 0.85, 0.92);
  cleanup.push_back(legend);

  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.035);

  for (int i=0; i<hists.size(); i++) {
    legend->AddEntry(hists.at(i), labels.at(i), "f");
  }
  // legend->AddEntry(hists.at(0), "Segment found", "f");
  // legend->AddEntry(hists.at(1), "No hits in cone", "f");
  // legend->AddEntry(hists.at(2), "No seed found", "f");
  // legend->AddEntry(hists.at(3), "No working fit", "f");
  // legend->AddEntry(hists.at(4), "No segment passed cut", "f");

  legend->Draw("same");

  gPad->RedrawAxis();

  TString plot_name = Form("%s/efficiencyStackPlotGoodBad_%s_%s_%s_%s.pdf",  g_output_plot_path.Data(), track_type.Data(), vs_x.Data(), accessor.Data(),  g_dataset_name.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (g_print_png) canvas->Print(plot_name);

  std::for_each(cleanup.begin(), cleanup.end(), [](TObject* obj) {delete obj;});

}


void effCurvesVersionsEta(TString config_file_name = "") {
  if (config_file_name != "") {
    g_config_file_name = config_file_name;
  }
  TEnv env(g_config_file_name);

  g_input_file_path = env.GetValue("input_file_path_TTI", "default");
  g_output_plot_path = env.GetValue("output_plot_path", "default");
  g_dataset_name = env.GetValue("dataset_name", "default");
  g_dataset_description = env.GetValue("dataset_description", "My Sample");
  g_print_png = env.GetValue("print_png", false);
  g_do_zoom = env.GetValue("do_zoom", false);
  g_use_log_scale = env.GetValue("use_log_scale", false);
  g_work_status = env.GetValue("work_status", "default");

  g_file = TFile::Open(g_input_file_path, "READ");

  SetAtlasStyle();

  preparePlottingDir(g_config_file_name);



  // efficiencyAndMistagLineDeltaT("ProgressiveFilter_1", selected_tracks, "eta", "eta_0", "|#eta|");
  // efficiencyAndMistagLineDeltaT("ProgressiveFilter_1", selected_tracks, "eta", "eta_1", "|#eta|");
  // efficiencyAndMistagLineDeltaT("ProgressiveFilter_1", selected_tracks, "eta", "eta_2", "|#eta|");
  // // truth #it{p}_{#it{T}} [GeV]
  // efficiencyAndMistagLineDeltaT("PFORv02", selected_tracks, "eta", "eta_0", "|#eta|");
  // efficiencyAndMistagLineDeltaT("PFORv03", selected_tracks, "eta", "eta_0", "|#eta|");
  // efficiencyAndMistagLineDeltaT("PFTQv01", selected_tracks, "eta", "eta_0", "|#eta|");
  //
  // efficiencyAndMistagLineDeltaT("PFORv02", selected_tracks, "eta", "eta_1", "|#eta|");
  // efficiencyAndMistagLineDeltaT("PFORv03", selected_tracks, "eta", "eta_1", "|#eta|");
  // efficiencyAndMistagLineDeltaT("PFTQv01", selected_tracks, "eta", "eta_1", "|#eta|");
  //
  // efficiencyAndMistagLineDeltaT("PFORv02", selected_tracks, "eta", "eta_2", "|#eta|");
  // efficiencyAndMistagLineDeltaT("PFORv03", selected_tracks, "eta", "eta_2", "|#eta|");
  // efficiencyAndMistagLineDeltaT("PFTQv01", selected_tracks, "eta", "eta_2", "|#eta|");
  //////////////
  // efficiencyAndMistagLinePrimeFractionGT50("ProgressiveFilter_1", selected_tracks, "eta", "eta_0", "|#eta|");

  // efficiencyAndMistagLinePrimeFractionGT50("PFTQv03", selected_tracks, "eta", "eta_0", "|#eta|");

  // efficiencyAndMistagLinePrimeFractionGT50("PFTQv03_1", selected_tracks, "eta", "eta_0", "|#eta|");

  // efficiencyAndMistagLinePrimeFractionGT50("PFTQv05", selected_tracks, "eta", "eta_0", "|#eta|");
  ////////////////
  // efficiencyAndMistagLinePrimeFractionGE50("ProgressiveFilter_1", selected_tracks, "eta", "eta_0", "|#eta|");
  // efficiencyAndMistagLinePrimeFractionGE50("PFORv03", selected_tracks, "eta", "eta_0", "|#eta|");
  // efficiencyAndMistagLinePrimeFractionGE50("PFTQv01", selected_tracks, "eta", "eta_0", "|#eta|");
  ////////////////
  // efficiencyStackPlotAllCases("ProgressiveFilter_1", selected_tracks, "eta", "eta_0", "|#eta|");

  // efficiencyStackPlotAllCases("PFTQv03", selected_tracks, "eta", "eta_0", "|#eta|");
  // efficiencyStackPlotAllCases("PFTQv03_1", selected_tracks, "eta", "eta_0", "|#eta|");

  // efficiencyStackPlotAllCases("PFTQv04", selected_tracks, "eta", "eta_0", "|#eta|");

  // efficiencyStackPlotAllCases("PFTQv05", selected_tracks, "eta", "eta_0", "|#eta|");

  ////////////////
  // efficiencyStackPlotGoodBad("ProgressiveFilter_1", selected_tracks, "eta", "eta_0", "|#eta|");
  //
  // efficiencyStackPlotGoodBad("PFORv03", selected_tracks, "eta", "eta_0", "|#eta|");
  //
  // efficiencyStackPlotGoodBad("PFTQv01", selected_tracks, "eta", "eta_0", "|#eta|");


  //TDR plots
  efficiencyAndMistagLinePrimeFractionGT50("ProgressiveFilter_1", selected_tracks, "eta", "eta_0", "|#eta|");
  efficiencyAndMistagLinePrimeFractionGT50("PFTQv05", selected_tracks, "eta", "eta_0", "|#eta|");
  efficiencyStackPlotAllCases("ProgressiveFilter_1", selected_tracks, "eta", "eta_0", "|#eta|");
  efficiencyStackPlotAllCases("PFTQv05", selected_tracks, "eta", "eta_0", "|#eta|");


  g_file->Close();
}


////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////


TH1F* turnGraphIntoHist(TGraph* graph, TString name, TString label, int bins, double xmin, double xmax, int color) {
  TH1F* hist = new TH1F(name, label, bins, xmin, xmax);
  for (int point=0; point<graph->GetN(); point++) {
    double x,y;
    graph->GetPoint(point, x, y);
    int bin_n = hist->FindFixBin(x);
    hist->SetBinContent(bin_n, y);
  }
  hist->SetFillColor(color);
  hist->SetLineWidth(0);
  hist->SetMarkerSize(0);
  hist->SetMarkerColor(color);
  return hist;
}

#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

#include "preparePaths.cxx"

// TString g_config_file_name = "muon45_mu0.cfg";
// TString g_config_file_name = "pion0p1to5p0_mu0.cfg";
TString g_config_file_name = "VBFinv_mu200.cfg";

double m_td_fitmin = -0.035;
double m_td_fitmax = 0.035;


// double m_td_fitmin = -0.05;
// double m_td_fitmax = 0.05;

TString work_status = "Internal";
TString output_plot_path = "~/www/hgtd/meeting_20191219/";
TString path = "../data/AccessorPerformanc.20068742.MERGED.truthEvent.root";
TString g_dataset_name = "default";

double* fitGaussianCore(TH1F* hist) {
  // double m_td_fitmin = -0.05;
  // double m_td_fitmax = 0.05;
  TF1 *func = new TF1("func","gaus(0)",m_td_fitmin-1., m_td_fitmax+1.);

  hist->Fit(func, "0", "", m_td_fitmin, m_td_fitmax);
  // hist->Fit(func, "", "", m_td_fitmin, m_td_fitmax);

  double mu = func->GetParameter(1);
  double sig = func->GetParameter(2);

  double* params = new double[2];
  params[0] = mu;
  params[1] = sig;
  delete func;
  return params;
}

TF1* fitGaussianCoreFunction(TH1F* hist) {
  // double m_td_fitmin = -0.05;
  // double m_td_fitmax = 0.05;
  TF1 *func = new TF1("func","gaus(0)",m_td_fitmin-1., m_td_fitmax+1.);

  hist->Fit(func, "0", "", m_td_fitmin, m_td_fitmax);
  // hist->Fit(func, "", "", m_td_fitmin, m_td_fitmax);

  return func;
}

void plotting(bool logscale, std::array<TString, 4> histo_names, TString postfix, TString associator) {
  // TString work_status = "Internal";
  // // TString output_plot_path = "~/www/hgtd/request_20190319/";
  // // TString output_plot_path = "~/www/hgtd/meeting_20191104/";
  // TString output_plot_path = "~/www/hgtd/meeting_20191219/";
  // // TString postfix = "20191027";
  //
  // /////////  O P E N   R O O T F I L E  /////////
  // // TString path = "../data/AccessorPerformanc.19966362.MERGED.root";
  // // TString path = "/afs/cern.ch/work/a/aleopold/private/hgtd/newAnalysis/run/TrackTimeAccessors_Output_test.root";
  // TString path = "../data/AccessorPerformanc.20068742.MERGED.truthEvent.root";

  TFile* f = TFile::Open(path, "READ");
  if (not f) {
    cout << "file not found" << endl;
    return;
  }
  /////////

  TString hist1_name = histo_names.at(0);
  TH1F* hist1 = (TH1F*) f->Get(hist1_name);
  if (not hist1) {
    cout << "hist1 not found" << endl;
    return;
  }
  double* params1 = fitGaussianCore(hist1);

  TString hist2_name = histo_names.at(1);
  TH1F* hist2 = (TH1F*) f->Get(hist2_name);
  if (not hist2) {
    cout << "hist2 not found" << endl;
    return;
  }
  double* params2 = fitGaussianCore(hist2);

  m_td_fitmin = -0.035;
  m_td_fitmax = 0.035;

  TString hist3_name = histo_names.at(2);
  TH1F* hist3 = (TH1F*) f->Get(hist3_name);
  if (not hist3) {
    cout << "hist3 not found" << endl;
    return;
  }
  double* params3 = fitGaussianCore(hist3);

  m_td_fitmin = -0.055;
  m_td_fitmax = 0.055;

  TString hist4_name = histo_names.at(3);
  TH1F* hist4 = (TH1F*) f->Get(hist4_name);
  if (not hist4) {
    cout << "hist4 not found" << endl;
    return;
  }
  double* params4 = fitGaussianCore(hist4);

  double total = hist1->Integral(0, hist1->GetNbinsX()+1);
  total += hist2->Integral(0, hist2->GetNbinsX()+1);
  total += hist3->Integral(0, hist3->GetNbinsX()+1);
  total += hist4->Integral(0, hist4->GetNbinsX()+1);

  hist1->Scale(1./total);
  hist2->Scale(1./total);
  hist3->Scale(1./total);
  hist4->Scale(1./total);


  TF1* fit1 = fitGaussianCoreFunction(hist1);
  TF1* fit2 = fitGaussianCoreFunction(hist2);
  m_td_fitmin = -0.035;
  m_td_fitmax = 0.035;
  TF1* fit3 = fitGaussianCoreFunction(hist3);
  m_td_fitmin = -0.055;
  m_td_fitmax = 0.055;
  TF1* fit4 = fitGaussianCoreFunction(hist4);

  double bin_left1 = hist1->GetXaxis()->FindBin(-2*params1[1]);
  double bin_right1 = hist1->GetXaxis()->FindBin(2*params1[1]);
  double outliers1 = hist1->Integral(0, bin_left1) + hist1->Integral(bin_right1, hist1->GetNbinsX()+1);
  double total1 = hist1->Integral(0,hist1->GetNbinsX()+1);

  double bin_left2 = hist2->GetXaxis()->FindBin(-2*params2[1]);
  double bin_right2 = hist2->GetXaxis()->FindBin(2*params2[1]);
  double outliers2 = hist2->Integral(0, bin_left2) + hist2->Integral(bin_right2, hist2->GetNbinsX()+1);
  double total2 = hist2->Integral(0,hist2->GetNbinsX()+1);

  double bin_left3 = hist3->GetXaxis()->FindBin(-2*params3[1]);
  double bin_right3 = hist3->GetXaxis()->FindBin(2*params3[1]);
  double outliers3 = hist3->Integral(0, bin_left3) + hist3->Integral(bin_right3, hist3->GetNbinsX()+1);
  double total3 = hist3->Integral(0,hist3->GetNbinsX()+1);

  double bin_left4 = hist4->GetXaxis()->FindBin(-2*params4[1]);
  double bin_right4 = hist4->GetXaxis()->FindBin(2*params4[1]);
  double outliers4 = hist4->Integral(0, bin_left4) + hist4->Integral(bin_right4, hist4->GetNbinsX()+1);
  double total4 = hist4->Integral(0,hist4->GetNbinsX()+1);

  //----- P L O T T I N G -----


  TCanvas* canvas = new TCanvas();
  hist1->GetXaxis()->SetTitle("t_{reco} - t_{truth} [ns]");
  hist1->GetYaxis()->SetTitle("Arbitrary Units / 4 ps");

  if (logscale) {
    canvas->SetLogy();
    // hist1->SetAxisRange(0.00001, 0.11, "Y");
    // int factor = 3;
    // if (g_config_file_name == "muon45_mu0.cfg") {
    //   factor = 15;
    // } else if (g_config_file_name == "pion0p1to5p0_mu0.cfg") {
    //   factor = 15;
    // } else if (g_config_file_name == "VBFinv_mu200.cfg") {
    //   factor = 15;
    // }
    // hist1->SetMaximum( max ( max ( factor*hist1->GetMaximum(), factor*hist2->GetMaximum()), max ( factor*hist3->GetMaximum(), factor*hist4->GetMaximum() ) )  );

    hist1->SetAxisRange(1.e-5, 0.4, "Y"); //for mu0
    // hist1->SetAxisRange(1.e-4, 0.3, "Y"); //for m200
  } else {
    hist1->SetAxisRange(0., 0.043, "Y");
  }

  cout << "npx" << fit1->GetNpx() << endl; //SetNpx
  fit1->SetNpx(10000);
  fit2->SetNpx(10000);
  fit3->SetNpx(10000);
  fit4->SetNpx(10000);

  double marker_size = 0.8;

  hist1->SetMarkerSize(marker_size);
  hist1->SetMarkerStyle(20);
  hist1->SetMarkerColor(kRed);
  hist1->Draw();
  fit1->SetLineWidth(1);
  fit1->SetLineColor(kRed);
  fit1->Draw("same");

  hist2->SetMarkerSize(marker_size);
  hist2->SetMarkerStyle(21);
  hist2->SetMarkerColor(kMagenta);
  hist2->Draw("same");
  fit2->SetLineWidth(1);
  fit2->SetLineColor(kMagenta);
  fit2->Draw("same");

  hist3->SetMarkerSize(marker_size);
  hist3->SetMarkerStyle(22);
  hist3->SetMarkerColor(kAzure);
  hist3->Draw("same");
  fit3->SetLineWidth(1);
  fit3->SetLineColor(kAzure);
  fit3->Draw("same");

  hist4->SetMarkerSize(marker_size);
  hist4->SetMarkerStyle(23);
  hist4->SetMarkerColor(kTeal+4);
  hist4->Draw("same");
  fit4->SetLineWidth(1);
  fit4->SetLineColor(kTeal+4);
  fit4->Draw("same");

  // TLegend* legend = new TLegend(0.68, 0.9, 0.88, 0.7);
  TLegend* legend = new TLegend(0.65, 0.5, 0.90, 0.94);
  legend->SetTextSize(0.028);
  legend->SetBorderSize(0);
  legend->SetFillStyle(0);
  //#splitline{}{|t_{reco}-t_{truth}| > 2#times#sigma_{t}}
  TString legend_entry = "#splitline{N_{hits} = %i, #sigma_{t} = %.0f ps}{|t_{reco} - t_{truth}| > 2#times#sigma_{t}: %.1f %}";

  legend->AddEntry(hist1, Form(legend_entry, 1, params1[1]*1000, outliers1/total1*100.));
  legend->AddEntry(hist2, Form(legend_entry, 2, params2[1]*1000, outliers2/total2*100.));
  legend->AddEntry(hist3, Form(legend_entry, 3, params3[1]*1000, outliers3/total3*100.));
  legend->AddEntry(hist4, Form(legend_entry, 4, params4[1]*1000, outliers4/total4*100.));

  legend->Draw("same");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.89, text_color);
  atlas::myText(0.31, 0.89, text_color, work_status.Data());

  TString dataset_descr = "VBF H #rightarrow invisible, #LT#mu#GT=200 ";
  if (g_config_file_name == "muon45_mu0.cfg") {
    // dataset_descr = "Single muon, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0";
    dataset_descr = "#mu^{+}, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0 ";
  } else if (g_config_file_name == "pion0p1to5p0_mu0.cfg") {
    // dataset_descr = "Single pion, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0";
    dataset_descr = "#pi^{+}, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0 ";
  } else if (g_config_file_name == "VBFinv_mu200.cfg") {
    dataset_descr = "VBF H #rightarrow invisible, #LT#mu#GT=200 ";
  }
  // atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  atlas::myText(0.19, 0.84, text_color, dataset_descr.Data(), 0.035);
  // myText(0.19, 0.79, text_color, "VBFinv, #langle#mu#rangle=200 ", 0.04);
  atlas::myText(0.19, 0.79, text_color, "p_{T}^{track} > 1.0 GeV", 0.035);
  atlas::myText(0.19, 0.74, text_color, "Timing scenario \"Initial\"", 0.035);
  // atlas::myText(0.19, 0.69, text_color, associator.Data(), 0.035);

  // myText(0.19, 0.69, text_color, "Timing scenario \"1ab^{-1}\"", 0.04);
  gPad->RedrawAxis();

  // canvas->Print("Plots/timeResComb.pdf");
  TString plot_name;
  if (logscale) {
    // plot_name = Form("%s/timeResCombLogz0.pdf",  output_plot_path.Data());
    plot_name = Form("%s/timeResLog_%s_%s.pdf",  output_plot_path.Data(), g_dataset_name.Data(), postfix.Data());
  } else {
    plot_name = Form("%s/timeRes_%s_%s.pdf",  output_plot_path.Data(), g_dataset_name.Data(),  postfix.Data());
  }
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (true) canvas->Print(plot_name);

  f->Close();
  delete canvas;
  delete legend;
  delete fit1;
}

void timeResolutionPlot(TString config_file_name = "") {
  if (config_file_name != "") {
    g_config_file_name = config_file_name;
  }
  TEnv env(g_config_file_name);

  path = env.GetValue("input_file_path_TTI", "default");
  output_plot_path = env.GetValue("output_plot_path", "default");
  work_status = env.GetValue("work_status", "default");
  g_dataset_name = env.GetValue("dataset_name", "default");

  SetAtlasStyle();

  preparePlottingDir(g_config_file_name);

  //all
  // std::vector<TString> algo_types = {"ProgressiveFilter", "ProgressiveFilter_1", "SegmentFinding", "PFORv01", "TruthTrackTimePrimariesAcc", "PFORv02", "PFORv02_1.0_0.07", "PFORv02_0.5_0.07", "PFORv03"};
  //
  // std::vector<TString> track_types = {"HS", "PU", "ALL", "HSpT5", "goodZ0", "HSgoodZ0", "assocToPVX", "HSassocToPVX", "HSassocToPVXgoodZ0", "HSgoodZ0LastHit"};

  //relevant for TDR
  // std::vector<TString> algo_types = {"ProgressiveFilter_1", "PFTQv03", "PFTQv04"};
  std::vector<TString> algo_types = {"ProgressiveFilter_1", "PFTQv05"};
  // std::vector<TString> algo_types = {"ProgressiveFilter", "ProgressiveFilter_1"};


  // std::vector<TString> track_types = {"HS", "HSgoodZ0", "goodZ0"};
  // std::vector<TString> track_types = {"ALL", "HS"};
  std::vector<TString> track_types = {"ALL"};

  for(const auto& algo_type : algo_types) {
    for(const auto& track_type : track_types) {
      plotting(true,
        {Form("%s/%s_m_hist_time_res_nhits_1", algo_type.Data(), track_type.Data()),
        Form("%s/%s_m_hist_time_res_nhits_2", algo_type.Data(), track_type.Data()),
        Form("%s/%s_m_hist_time_res_nhits_3", algo_type.Data(), track_type.Data()),
        Form("%s/%s_m_hist_time_res_nhits_4", algo_type.Data(), track_type.Data())},
        Form("%s_%s",  algo_type.Data(), track_type.Data()),
        Form("%s_%s",  algo_type.Data(), track_type.Data()));

      // plotting(true,
      //   {Form("%s/%s_m_hist_time_res_primaries_nhits_1", algo_type.Data(), track_type.Data()),
      //   Form("%s/%s_m_hist_time_res_primaries_nhits_2", algo_type.Data(), track_type.Data()),
      //   Form("%s/%s_m_hist_time_res_primaries_nhits_3", algo_type.Data(), track_type.Data()),
      //   Form("%s/%s_m_hist_time_res_primaries_nhits_4", algo_type.Data(), track_type.Data())},
      //   Form("%s_%s",  algo_type.Data(), track_type.Data()),
      //   Form("%s_%s",  algo_type.Data(), track_type.Data()));
    } //LOOP track_types
  }//LOOP algo_types

}

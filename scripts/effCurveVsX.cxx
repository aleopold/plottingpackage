#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

// TString g_config_file_name = "muon45_mu0.cfg";
TString g_config_file_name = "pion0p1to5p0_mu0.cfg";

double m_td_fitmin = -0.055;
double m_td_fitmax = 0.055;


TString work_status = "Internal";
TString output_plot_path = "~/www/hgtd/meeting_20191219/";
TString path = "../data/AccessorPerformanc.20068742.MERGED.truthEvent.root";
TString g_dataset_name = "default";
TString g_dataset_description = "default";
bool print_png;

TFile* g_file = nullptr;

void plot() {

  auto eff = (TEfficiency*) g_file->Get("ProgressiveFilter/HS_m_eff_vs_truthpt");
  if (eff==nullptr) {
    cout << "nAverageHitsPerTrack histogram not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph = eff->CreateGraph();
  graph->SetMarkerStyle(21);
  graph->SetMarkerSize(1.);
  Color_t marker_color = kBlack;
  graph->SetMarkerColor(marker_color);
  graph->SetLineColor(marker_color);
  graph->SetLineWidth(4);
  graph->GetYaxis()->SetRangeUser(0.,1.35);

  //plotting part
  TCanvas *canvas = new TCanvas();
  // gPad->SetTickx(0);
  // canvas->SetTicks(1,1);

  auto hist = new TH1F("axishist", ";truth #it{p}_{#it{T}} [GeV];track-time association efficiency", 9, 0.5, 5);
  hist->SetAxisRange(0., 1.4, "Y");
  hist->Draw();

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.89, text_color);
  atlas::myText(0.31, 0.89, text_color, work_status.Data());

  graph->Draw("same P");

  TString dataset_descr = "VBFinv, #LT#mu#GT=200, ITk Layout";
  if (g_config_file_name == "muon45_mu0.cfg") {
    // dataset_descr = "Single muon, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0";
    dataset_descr = "#mu^{+}, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0, ITk Layout";
  } else if (g_config_file_name == "pion0p1to5p0_mu0.cfg") {
    // dataset_descr = "Single pion, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0";
    dataset_descr = "#pi^{+}, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0, ITk Layout";
  }
  // atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  atlas::myText(0.19, 0.84, text_color, dataset_descr.Data(), 0.035);
  // myText(0.19, 0.79, text_color, "VBFinv, #langle#mu#rangle=200, ITk Layout", 0.04);
  atlas::myText(0.19, 0.79, text_color, "p_{T}^{track} > 1.0 GeV", 0.035);
  atlas::myText(0.19, 0.74, text_color, "Timing scenario \"Initial\"", 0.035);

  // hist->Draw();
  // eff->Draw("same");
  // graph->Draw("same P");

  gPad->RedrawAxis();

  TString plot_name = Form("%s/Progressive_eff_vs_truthpt_%s.pdf",  output_plot_path.Data(), g_dataset_name.Data());
  canvas->Print(plot_name);
  plot_name.Form("%s/Progressive_eff_vs_truthpt_%s.png",  output_plot_path.Data(), g_dataset_name.Data());
  if (print_png) canvas->Print(plot_name);

  delete canvas;
  delete hist;
  delete graph;
}

void effCurveVsX() {
  std::cout << "[effCurveVsX] start" << '\n';
  TEnv env(g_config_file_name);

  TString g_input_file_path = env.GetValue("input_file_path_TTI", "default");
  output_plot_path = env.GetValue("output_plot_path", "default");
  g_dataset_name = env.GetValue("dataset_name", "default");
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");
  g_dataset_description = env.GetValue("dataset_description", "default");

  g_file = TFile::Open(g_input_file_path, "READ");
  SetAtlasStyle();

  plot();

  g_file->Close();
}

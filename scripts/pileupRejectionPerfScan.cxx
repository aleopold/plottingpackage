#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

TString g_input_file_path = "/afs/cern.ch/user/t/tawang/public/PileupRejection_100k.root";
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
TString dataset_name = "VBFinv, mu=200";
TString descr = "";
TString dataset_description = "";
bool print_png = true;
TString work_status = "Internal";
bool g_do_ratioplot = true;
double x_eff_min = 0.80;
double legend_text_size = 0.02;

const static int m_n_eff_steps = 10;
//mistagging 0% to 50% in 5% steps
const static int m_n_mis_steps = 10;
//resolution from 5 ps to 80 ps in 5 ps steps
const static int m_n_res_steps = 15;

TFile* g_file = nullptr;

TEfficiency* getEfficiencyObject(TString name);
TGraph* getRatioGraph(TGraph* numerator, TGraph* denominator);
double findCutForGivenEff(TGraph* graph, double eff);
float getImprovement(TString reference_name, TString test_name, TString pt_range, double hs_eff, double time_eff, double time_pur);

void scanOverRange(int resolution_bin, float resolution, TString pt_range, double hs_eff) {

  auto hist = new TH2D("hist", ";efficiency;purity", m_n_eff_steps, 0, m_n_eff_steps, m_n_mis_steps, 0, m_n_mis_steps);



  float set_eff = 1.0; float set_eff_step = 0.05;
  for (int eff_i=0; eff_i<m_n_eff_steps; eff_i++) {
    hist->GetXaxis()->SetBinLabel(eff_i+1,Form("%.1f %%", set_eff*100));
    set_eff -= set_eff_step;
  }
  float set_mis = 0.0; float set_mis_step = 0.05;
  for (int mis_i=0; mis_i<m_n_mis_steps; mis_i++) {
    hist->GetYaxis()->SetBinLabel(mis_i+1,Form("%.1f %%", (1.0-set_mis)*100));
    set_mis += set_mis_step;
  }

  for (int eff_i=0; eff_i<m_n_eff_steps; eff_i++) {
    for (int mis_i=0; mis_i<m_n_mis_steps; mis_i++) {
      // for (int res_i=0; res_i<m_n_res_steps; res_i++) {
        std::string test_name = "SubjetRpTScan_" + std::to_string(eff_i) + "_" + std::to_string(mis_i) + "_" + std::to_string(resolution_bin);
        float impr = getImprovement("StandardRpT", TString(test_name), pt_range, hs_eff, 0, 0);
      // }
        hist->SetBinContent(eff_i+1, mis_i+1, impr);
    }
  }

  TCanvas *canvas = new TCanvas();
  hist->Draw("col text");

  Color_t text_color = kWhite;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.315, 0.88, text_color, work_status.Data(), 0.05);
  atlas::myText(0.19, 0.84, text_color, Form("resolution: %2.0f ps, %.0f %% HS efficiency", resolution*1000, hs_eff*100), 0.03);
  if (pt_range == "low") {
    atlas::myText(0.19, 0.80, text_color, "30 GeV < p_{T}^{jet} < 50 GeV", 0.03);
  } else {
    atlas::myText(0.19, 0.80, text_color, "50 GeV < p_{T}^{jet} < 80 GeV", 0.03);
  }


  TString plot_name = Form("%s/SubjetRpTPerfScan_%s_%s_pt_%s%i.pdf",  output_plot_path.Data(), dataset_name.Data(), pt_range.Data(), descr.Data(), resolution_bin);
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (print_png) canvas->Print(plot_name);

  delete canvas;
  delete hist;
}

float getImprovement(TString reference_name, TString test_name, TString pt_range, double hs_eff, double time_eff, double time_pur) {
  // get the PU rejection at the set efficiency for new and old
  TString template_name = "m_eff_%s_%s_pt_%s";
  auto eff_old_hs = getEfficiencyObject(Form(template_name, "hs", pt_range.Data(), reference_name.Data()));
  auto eff_old_hs_graph = eff_old_hs->CreateGraph();
  double hs_old_cutval = findCutForGivenEff(eff_old_hs_graph, hs_eff);

  auto eff_old_pu = getEfficiencyObject(Form(template_name, "pu", pt_range.Data(), reference_name.Data()));
  auto eff_old_pu_graph = eff_old_pu->CreateGraph();
  double pu_eff_val_old = eff_old_pu_graph->Eval(hs_old_cutval);

  auto eff_new_hs = getEfficiencyObject(Form(template_name, "hs", pt_range.Data(), test_name.Data()));
  auto eff_new_hs_graph = eff_new_hs->CreateGraph();
  double hs_new_cutval = findCutForGivenEff(eff_new_hs_graph, hs_eff);

  auto eff_new_pu = getEfficiencyObject(Form(template_name, "pu", pt_range.Data(), test_name.Data()));
  auto eff_new_pu_graph = eff_new_pu->CreateGraph();
  double pu_eff_val_new = eff_new_pu_graph->Eval(hs_new_cutval);

  // cout<<pu_eff_val_old << '\n';
  // cout<<pu_eff_val_new << '\n';
  // cout<<1./pu_eff_val_old << '\n';
  // cout<<1./pu_eff_val_new << '\n';
  //
  // cout<<(1./pu_eff_val_new)/(1./pu_eff_val_old) << '\n';
  //
  // cout<<((1./pu_eff_val_new)/(1./pu_eff_val_old) - 1.)*100 << "%" << '\n';

  return ((1./pu_eff_val_new)/(1./pu_eff_val_old) - 1.)*100;

}

void pileupRejectionPerfScan() {


  TEnv env("pileup.cfg");

  g_input_file_path = env.GetValue("input_file_path", "default");
  g_do_ratioplot = env.GetValue("do_ratioplot", false);
  x_eff_min = env.GetValue("x_eff_min", 0.8);
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  dataset_description = env.GetValue("dataset_description", "default");
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");

  g_file = TFile::Open(g_input_file_path, "READ");


  SetAtlasStyle();

  gStyle->SetPaintTextFormat("2.1f %%");
  // getImprovement("StandardRpT", "SubJetRpTReco", "low", 0.85,0, 0);

  float set_res = 0.005; float set_res_step = 0.005;
  for (int res_i=0; res_i<m_n_res_steps; res_i++) {
    scanOverRange(res_i, set_res, "low", 0.85);
    scanOverRange(res_i, set_res, "high", 0.85);
    set_res += set_res_step;
  }

}

///////////////

TEfficiency* getEfficiencyObject(TString name) {
  auto eff = (TEfficiency*) g_file->Get(name);
  if (eff == nullptr) {
    cout << "[getEfficiencyObject]: " << name << " does not exist, aborting \n";
  }
  return eff;
}

double findCutForGivenEff(TGraph* graph, double eff) {
  //from min value
  int steps = 1000;
  double xmin = graph->GetXaxis()->GetXmin();
  double xmax = graph->GetXaxis()->GetXmax();
  double stepsize = (xmax - xmin)/(double)steps;

  for(int i=0; i<steps; i++) {
    double x = xmin + ((double)i)*stepsize;
    double y = graph->Eval(x);
    if (y<=eff) {
      // cout << "found cut value: " << x << " at eff " << y << '\n';
      return x;
    }
  }
  cout << "WARNING, EFF DOES NOT FALL BELOW DEFINED VALUE!!" << '\n';
  cout << "lowest value: " << graph->Eval(xmax) << '\n';
  return -1;
}

#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

TString g_input_file_path = "/afs/cern.ch/work/a/aleopold/private/hgtd/analysis_pkg/run/JetControlPlots_Output_10k.root";
// bool g_do_ratioplot = true;
// double x_eff_min = 0.80;
TString g_output_plot_path = "default";
// int skip = 0;
// TString dataset_name = "VBFinv, mu=200";
// TString descr = "pileupRejectionVsDensityFixedHSEff";
bool g_print_png = true;
// TString work_status = "Internal";

TFile* g_file = nullptr;

void plotGraph(int pt_bin) {
  auto canvas = new TCanvas();
  auto axes_hist = new TH1F("axes_hist", ";|#eta|; jet response", 8, 0, 4.0);
  axes_hist->GetYaxis()->SetRangeUser(0., 2.5);
  axes_hist->Draw();

  TGraphErrors* graph = new TGraphErrors();

  for (int eta_bin=0; eta_bin<8; eta_bin++) {
    TString histname = Form("m_hist_hs_jet_pt_resolution_pt_%i_eta_%i", pt_bin, eta_bin);
    auto hist = (TH1F*) g_file->Get(histname);
    float mean = hist->GetMean();
    float error = hist->GetStdDev();
    cout << "x " << 0.25 + 0.5 * eta_bin << ", y " << mean << '\n';
    graph->SetPoint(eta_bin, 0.25 + 0.5 * eta_bin, mean);
    graph->SetPointError(eta_bin, 0, error);
  }

  graph->SetMarkerColor(kBlack);
  graph->SetLineColor(kBlack);

  graph->Draw("PL same");

  TString plotname = Form("%s/JetResponse_ptbin_%i.pdf", g_output_plot_path.Data(), pt_bin);
  canvas->Print(plotname);
  if (g_print_png) {
    plotname.ReplaceAll(".pdf", ".png");
    canvas->Print(plotname);
  }

  delete canvas;
  delete axes_hist;
  delete graph;
}

void jetResponse() {

  TEnv env("pileup.cfg");

  // g_input_file_path = env.GetValue("input_file_path", "default");
  // g_do_ratioplot = env.GetValue("do_ratioplot", false);
  // x_eff_min = env.GetValue("x_eff_min", 0.8);
  g_output_plot_path = env.GetValue("output_plot_path", "default");
  // dataset_name = env.GetValue("dataset_name", "default");
  // dataset_description = env.GetValue("dataset_name", "My Sample");
  g_print_png = env.GetValue("print_png", false);
  // work_status = env.GetValue("work_status", "default");

  g_file = TFile::Open(g_input_file_path, "READ");

  plotGraph(0);
  plotGraph(1);
  plotGraph(2);
  plotGraph(3);

  g_file->Close();

}

// "m_hist_hs_jet_pt_resolution_pt_%i_eta_%i"

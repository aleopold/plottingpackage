#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

TString g_input_file_path = "/afs/cern.ch/work/a/aleopold/private/hgtd/newAnalysis/run/TrackTimeAccessors_Output_test.root";
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
int skip = 0;
TString dataset_name = "VBFinv, mu=200";
TString dataset_description = "";
bool print_png = true;
TString work_status = "Internal";
bool g_use_log_scale = false;
bool do_zoom = false;
// TString descr = "OutlierStackPlotGoodZ0_pforv02";
TString descr = "OutlierStackPlotAllZ0_PF";
// TString descr = "OutlierStackPlotBadZ0";

// TString g_config_file_name = "muon45_mu0.cfg";
// TString g_config_file_name = "pion0p1to5p0_mu0.cfg";
TString g_config_file_name = "pileup.cfg";

TFile* file = nullptr;

void plot(TString accessor, TString track_type) {

  // std::vector<Color_t> colors = {kCyan+3, kCyan+2, kCyan+1, kRed-2, kPink-4};
  std::vector<Color_t> colors = {kCyan+2, kRed-2, kPink-4};
  std::vector<int> styles = {3144, 3344, 1001};

  std::vector<TH1F*> hists;


  auto hist1 = (TH1F*) file->Get("hist_res_type1");
  // hists.push_back(hist1);
  auto hist2 = (TH1F*) file->Get("hist_res_type2");
  hist1->Add(hist2);
  // hists.push_back(hist2);
  auto hist3 = (TH1F*) file->Get("hist_res_type3");
  hist1->Add(hist3);
  // hists.push_back(hist3);
  hists.push_back(hist1);
  auto hist4 = (TH1F*) file->Get("hist_res_type4");
  hists.push_back(hist4);
  auto hist5 = (TH1F*) file->Get("hist_res_type5");
  hists.push_back(hist5);


  for (size_t i=0; i<hists.size(); i++) {
    hists.at(i)->SetFillColor(colors.at(i));
    hists.at(i)->SetFillStyle(styles.at(i));
    hists.at(i)->SetLineWidth(0.);
  }

  gROOT->ForceStyle();
  //scale to total n
  double total = 0;
  for (auto& hist : hists) {
    total += hist->Integral(0, hist->GetNbinsX()+1);
    cout << "total: " << total << '\n';
  }
  for (auto& hist : hists) {
    hist->Scale(1./total);
  }


  // auto overall_hist = new TH1F("overall_hist", ";t0_{reco} - t0_{truth} [ns]; number of tracks", 200, -.4, 0.4);
  // for (const auto& h : hists) {
  //   overall_hist->Add(h);
  // }

  THStack* stack_hist = new THStack("stack_hist",";t_{0}^{reco} - t_{0}^{truth} [ns]; a.u.");

  //std::vector<int> colors = {42, 30, 46, 38, 28};
  int ntotal = 0;
  for (size_t i=0; i<hists.size(); i++) {
    hists.at(i)->SetFillColor(colors.at(i));
    hists.at(i)->SetLineWidth(0.);
    ntotal += hists.at(i)->Integral(0, hists.at(i)->GetNbinsX()+1);
    cout << "fraction: " << hists.at(i)->Integral(0, hists.at(i)->GetNbinsX()+1) << '\n';
    stack_hist->Add(hists.at(i));
  }

  stack_hist->SetMaximum(stack_hist->GetMaximum()*1.7);
  if (g_use_log_scale) {
    // stack_hist->SetMaximum(stack_hist->GetMaximum()*4.0);
    // stack_hist->SetAxisRange(1.e-4, 0.3, "Y"); //for m200
    // stack_hist->SetAxisRange(0.00001, 0.4, "Y");
    // hstack_hists->GetYaxis()->SetLimits(1.e-4, 0.3;
  }
  if(do_zoom) {
    stack_hist->SetMaximum(100);
  }

  TCanvas* canvas = new TCanvas();
  // canvas->SetGrayscale();
  canvas->SetTicks(1,1);
  if (g_use_log_scale) {
    canvas->SetLogy();
  }

  gROOT->ForceStyle();

  auto hist = new TH1F("axishist", ";t0_{reco} - t0_{truth} [ns]; a.u.", 10, -0.4, 0.4);
  // hist->SetAxisRange(1., 1.e6, "Y");
  hist->SetAxisRange(0.00001, 0.7, "Y");
  if (not g_use_log_scale) {
    hist->SetAxisRange(0, 0.1, "Y");
  }
  //
  hist->Draw();

  stack_hist->Draw("hist same");
  if (g_use_log_scale) {
    // stack_hist->SetMaximum(stack_hist->GetMaximum()*4.0);
    // stack_hist->SetAxisRange(1.e-4, 0.3, "Y"); //for m200
    // stack_hist->GetYaxis()->SetLimits(1.e-4, 0.3);
  }

  // overall_hist->SetMarkerColor(kBlack);
  // overall_hist->SetMarkerStyle(20);
  // overall_hist->SetMarkerSize(0.5);
  // overall_hist->Draw("same p");
  // overall_hist->Draw();

  canvas->Update();

  // cout << "total " << ntotal << '\n';
  // for (size_t i=0; i<hists.size(); i++) {
  //   cout << "hist " << i << " " <<  (float) hists.at(i)->Integral(0, hists.at(i)->GetNbinsX()+1)/(float) ntotal << '\n';
  //   cout << "hist " << i << " " <<  (float) hists.at(i)->Integral(0, hists.at(i)->GetNbinsX()+1)/(float) 1 << '\n';
  // }

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.31, 0.88, text_color, work_status);

  TString buffer_dataset_description = dataset_description;
  buffer_dataset_description.ReplaceAll("$", "#");
  atlas::myText(0.19, 0.82, text_color, buffer_dataset_description.Data(), 0.05);
  // TString dataset_descr = "VBF H#rightarrow invisible, #LT#mu#GT=200, ITk Layout";
  // if (g_config_file_name == "muon45_mu0.cfg") {
  //   // dataset_descr = "Single muon, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0";
  //   dataset_descr = "#mu^{+}, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0, ITk Layout";
  // } else if (g_config_file_name == "pion0p1to5p0_mu0.cfg") {
  //   // dataset_descr = "Single pion, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0";
  //   dataset_descr = "#pi^{+}, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0, ITk Layout";
  // } else if (g_config_file_name == "VBFinv_mu200.cfg" or g_config_file_name == "pileup.cfg") {
  //   dataset_descr = "VBF H #rightarrow invisible, #LT#mu#GT=200, ITk Layout";
  // }
  // atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  // atlas::myText(0.19, 0.84, text_color, dataset_descr.Data(), 0.035);

  // auto legend = new TLegend(0.47, 0.65, 0.85, 0.92);

  auto legend = new TLegend(0.72, 0.65, 0.9, 0.92);

  legend->SetTextFont(42);
  // legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.025);


  // // ntotal = 1.;
  // legend->AddEntry(hists.at(0), "Best HS cluster", "f");
  // legend->AddEntry(hists.at(1), ">50% HS cluster", "f");
  // legend->AddEntry(hists.at(2), "=50% HS cluster", "f");
  // legend->AddEntry(hists.at(3), "<50% HS cluster", "f");
  // legend->AddEntry(hists.at(4), "PU cluster", "f");
  legend->AddEntry(hists.at(0), "#geq 50% HS tracks", "f");
  legend->AddEntry(hists.at(1), "<50% HS tracks", "f");
  legend->AddEntry(hists.at(2), "PU tracks", "f");

  legend->Draw("same");

  gPad->RedrawAxis();

  TString plot_name = Form("%s/%s_%s_%s_%s.pdf",  output_plot_path.Data(), dataset_name.ReplaceAll(" ", "").ReplaceAll(",", "_").Data(), descr.Data(), accessor.Data(), track_type.Data());
  if (g_use_log_scale) {
    plot_name.ReplaceAll(".pdf", "_logscale.pdf");
  }
  canvas->Print(plot_name);
  if (print_png) canvas->Print(plot_name.ReplaceAll(".pdf", ".png"));

  // std::for_each(cleanup.begin(), cleanup.end(), [](TObject* obj) {delete obj;});

}


void t0StackPlot() {

  TEnv env(g_config_file_name);

  g_input_file_path = env.GetValue("bdt_control_input_file_path", "default");
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  dataset_description = env.GetValue("dataset_description", "My Sample");
  print_png = env.GetValue("print_png", false);
  do_zoom = env.GetValue("do_zoom", false);
  g_use_log_scale = env.GetValue("use_log_scale", false);
  work_status = env.GetValue("work_status", "default");

  file = TFile::Open(g_input_file_path, "READ");

  SetAtlasStyle();

  plot("hi", "ho");



}

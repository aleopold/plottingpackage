

void trainBDT() {

  TMVA::Tools::Instance();
  auto inputFile = TFile::Open("prepareTreeForBDT.root");
  auto outputFile = TFile::Open("TMVAOutputCV.root", "RECREATE");

  TMVA::Factory factory("TMVAClassification", outputFile,
                        "!V:ROC:!Correlations:!Silent:Color:!DrawProgressBar:"
                        "AnalysisType=Classification");

  TMVA::DataLoader loader("dataset");

  loader.AddVariable("m_delta_z");
  loader.AddVariable("m_z_sigma");
  loader.AddVariable("m_q_over_p");
  loader.AddVariable("m_q_over_p_sigma");
  loader.AddVariable("m_delta_z_resunits");
  loader.AddVariable("m_cluster_sumpt2");
  loader.AddVariable("m_d0");
  loader.AddVariable("m_d0_sigma");
  loader.AddVariable("m_number_of_tracks");
  loader.AddVariable("m_z0_stddev");
  loader.AddVariable("m_z0_mean");
  loader.AddVariable("m_closest_vx_is_pvx");
  loader.AddVariable("m_distance_pu_vx");
  loader.AddVariable("m_n_pu_vertices");
  loader.AddVariable("m_pt_mod");

  TTree *tsignal, *tbackground;
  inputFile->GetObject("SignalTree", tsignal);
  inputFile->GetObject("BackgroundTree", tbackground);

  TCut mycuts = "";
  TCut mycutb = "";
  // TCut mycuts = "m_number_of_tracks>=3";
  // TCut mycutb = "m_number_of_tracks>=3";

  loader.AddSignalTree(tsignal, 1.0);         // signal weight  = 1
  loader.AddBackgroundTree(tbackground, 1.0); // background weight = 1
  // loader.PrepareTrainingAndTestTree(mycuts, mycutb,
  // "nTrain_Signal=2000:nTrain_Background=2000:SplitMode=Random:NormMode=NumEvents:!V"
  // );
  loader.PrepareTrainingAndTestTree(mycuts, mycutb, "random");

  // USED FOR MAIN RESULTS
  // factory.BookMethod(&loader,TMVA::Types::kBDT, "BDT",
  // "!V:NTrees=40:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20"
  // );

  factory.BookMethod(
      &loader, TMVA::Types::kBDT, "BDT",
      "!V:NTrees=400:MinNodeSize=2.5%:MaxDepth=4:BoostType=AdaBoost:"
      "AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType="
      "GiniIndex:nCuts=20");

  factory.TrainAllMethods();

  factory.TestAllMethods();
  factory.EvaluateAllMethods();

  outputFile->Close();

  auto c1 = factory.GetROCCurve(&loader);
  c1->Draw();
  if (!gROOT->IsBatch())
    TMVA::TMVAGui("TMVAOutputCV.root");
}

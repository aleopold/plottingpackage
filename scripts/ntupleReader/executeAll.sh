root -l -q -b prepareTreeForBDT.cxx
root -l -q -b trainBDT.cxx
root -l -q -b puRejROC.cxx > /dev/null 2>&1 &
root -l -q -b evaluateBDT.cxx > /dev/null 2>&1 &


#path="/Users/aleopold/ATLAS/hgtd/data/vbf_mu200"


# path="/Users/aleopold/ATLAS/hgtd/ntuples/user.aleopold.hgtd_VBFinv_mu200_step3p1_TimeClusterNtuples.20200129_0019_PileupRejectionNtuple/"
### VBFinv
# path="/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_TimeClusterNtuples.20200129_0019_PileupRejectionNtuple/"
# ls -d $path/*.root   > INPUT_VBFinv.txt
### VBFinv 20200228
# path="/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_PUNtuples_newvars_PFTQv05_pT1p0.20200226_1917_PileupRejectionNtuple/"
# ls -d $path/*.root   > INPUT_VBFinv_20200228.txt
### dijet
# path="/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_JZ2W_mu200_step3p1_TimeClusterNtuples.20200130_0103_PileupRejectionNtuple/"
# ls -d $path/*.root   > INPUT_JZ2W.txt

# path="/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_JZ3W_mu200_step3p1_TimeClusterNtuples.20200204_1412_PileupRejectionNtuple/"
# ls -d $path/*.root   > INPUT_JZ3W.txt
### ttbar
# path="/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_ttbar_mu200_step3p1_TimeClusterNtuples.20200130_0112_PileupRejectionNtuple/"
# ls -d $path/*.root   > INPUT_ttbar.txt

# path="/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_PUNtuples_newvars_PFTQv05S_999_0.183_1.5_0.05_pT1p0.20200228_1036_PileupRejectionNtuple"
# ls -d $path/*.root   > INPUT_Smeared.txt

### VBFinv from Tao
# path="/eos/user/a/aleopold/hgtd/ntuples/fromTao/pileup_ntuple_2020_2_7/"
# ls -d $path/*.root   > INPUT_VBFinvTao2.txt

# path="/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_ttbar_mu200_step3p1_TimeClusterNtuples.20200130_0112_PileupRejectionNtuple/"
# ls -d $path/*.root  >> INPUT_combined.txt
# path="/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_TimeClusterNtuples.20200129_0019_PileupRejectionNtuple/"
# ls -d $path/*.root  >> INPUT_combined.txt

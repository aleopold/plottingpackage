
// std::map<int, float> getTruthMap(std::string path_to_rootfile) {
//   auto file = TFile::Open(path_to_rootfile.c_str());
//   TTreeReader reader("HSTime", file);
//
//   TTreeReaderValue<int> m_eventnumber(reader, "m_eventnumber");
//   TTreeReaderValue<bool> m_hast0(reader, "m_hast0");
//   TTreeReaderValue<float> m_truth_t0(reader, "m_truth_t0");
//
//   std::map<int, float> event_t0;
//
//   while(reader.Next()) {
//     if(*m_hast0) {
//       event_t0[*m_eventnumber] = *m_truth_t0;
//     }
//   }
//   file->Close();
//   return event_t0;
// }


std::map<int, float> getMap(std::string path_to_rootfile) {
  auto file = TFile::Open(path_to_rootfile.c_str());
  TTreeReader reader("HSTime", file);

  TTreeReaderValue<int> m_eventnumber(reader, "m_eventnumber");
  TTreeReaderValue<bool> m_hast0(reader, "m_hast0");
  TTreeReaderValue<float> m_t0(reader, "m_t0");

  std::map<int, float> event_t0;

  while(reader.Next()) {
    if(*m_hast0) {
      event_t0[*m_eventnumber] = *m_t0;
    }
  }
  file->Close();
  return event_t0;
}

void exampleLoad() {

  auto my_map = getMap("hardScatterTime.root");

  cout << my_map.size() << '\n';

  auto iterator = my_map.find(398080);
  if (iterator != my_map.end()) {
    cout << "event 398080 has reconstructed t0\n";
    cout << "t0: " << my_map[398080] << '\n';
  } else {
    cout << "no time for event 398080\n";
  }

}


#include "getTChain.cxx"

struct EventData {
  int eventnumber = -1;
  int n_tracks = 0;
  int n_hs_tracks = 0;
  int n_hs_tracks_w_time = 0;
  int n_hs_tracks_w_good_time = 0;
  float local_vx_density;
  //jets
  int n_fwd_pu_jets = 0;
  int n_fwd_hs_jets = 0;
  int n_fwd_hard_pu_jets = 0;
  int n_fwd_hard_hs_jets = 0;
};

void evaluateProblematicEvents(){

  TMVA::Tools::Instance();

  TMVA::Reader *tmva_reader = new TMVA::Reader( "!Color:!Silent" );

  float m_delta_z;
  float m_z_sigma;
  float m_q_over_p;
  float m_q_over_p_sigma;
  float m_d0;
  float m_d0_sigma;
  float m_delta_z_resunits;
  float m_cluster_sumpt2;
  float m_number_of_tracks;

  tmva_reader->AddVariable("m_delta_z", &m_delta_z );
  tmva_reader->AddVariable("m_z_sigma", &m_z_sigma );
  tmva_reader->AddVariable("m_q_over_p", &m_q_over_p );
  tmva_reader->AddVariable("m_q_over_p_sigma", &m_q_over_p_sigma );
  tmva_reader->AddVariable("m_delta_z_resunits", &m_delta_z_resunits );
  tmva_reader->AddVariable("m_cluster_sumpt2", &m_cluster_sumpt2 );
  tmva_reader->AddVariable("m_d0", &m_d0 );
  tmva_reader->AddVariable("m_d0_sigma", &m_d0_sigma );
  // tmva_reader->AddVariable("m_number_of_tracks", &m_number_of_tracks );

  TString weightfile = "dataset/weights/TMVAClassification_BDT.weights.xml";
  tmva_reader->BookMVA("BDT", weightfile);



  // TString path = "/afs/cern.ch/work/a/aleopold/private/hgtd/analysis_pkg/run/PileupRejectionNtuple_20k.root";
  // auto g_file = TFile::Open(path, "READ");
  // TTreeReader reader("JetTree", g_file);

  std::vector <TString> list_of_files = getListOfFiles("INPUT.txt", -1);
  int n_files_to_skip = 40;
  int start_pos_for_erase = list_of_files.size() - n_files_to_skip;
  list_of_files.erase(list_of_files.begin() + start_pos_for_erase, list_of_files.end());
  if (list_of_files.size() == 0) {
    cout << "ERROR: getListOfFiles returned no files to be read" << endl;
    return;
  }
  cout << list_of_files.size() << " files left for evaluation\n";
  TChain* g_file = getChain(list_of_files, "JetTree", -1);
  TTreeReader reader(g_file);

  TTreeReaderValue<int> m_tree_eventnumber(reader, "m_tree_eventnumber");
  TTreeReaderValue<double> m_tree_truth_t0(reader, "m_tree_truth_t0");
  TTreeReaderValue<double> m_tree_param_vx_density(reader, "m_tree_param_vx_density");
  TTreeReaderValue<double> m_tree_local_vx_density(reader, "m_tree_local_vx_density");
  TTreeReaderValue<double> m_tree_local_truthvx_density(reader, "m_tree_local_truthvx_density");
  TTreeReaderValue<double> m_tree_deltaz_vx_truthvx(reader, "m_tree_deltaz_vx_truthvx");
  //
  TTreeReaderValue<int> m_tree_n_hgtd_tracks(reader, "m_tree_n_hgtd_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks(reader, "m_tree_n_hgtd_hs_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_time(reader, "m_tree_n_hgtd_hs_tracks_with_time");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_good_time(reader, "m_tree_n_hgtd_hs_tracks_with_good_time");

  TTreeReaderArray<double> m_tree_cluster_time(reader, "m_tree_cluster_time");
  TTreeReaderArray<double> m_tree_cluster_time_res(reader, "m_tree_cluster_time_res");
  TTreeReaderArray<double> m_tree_cluster_z0(reader, "m_tree_cluster_z0");
  TTreeReaderArray<double> m_tree_cluster_z0_sigma(reader, "m_tree_cluster_z0_sigma");
  TTreeReaderArray<double> m_tree_cluster_q_over_p(reader, "m_tree_cluster_q_over_p");
  TTreeReaderArray<double> m_tree_cluster_q_over_p_sigma(reader, "m_tree_cluster_q_over_p_sigma");
  TTreeReaderArray<double> m_tree_cluster_d0(reader, "m_tree_cluster_d0");
  TTreeReaderArray<double> m_tree_cluster_d0_sigma(reader, "m_tree_cluster_d0_sigma");
  TTreeReaderArray<double> m_tree_cluster_hs_fraction(reader, "m_tree_cluster_hs_fraction");
  TTreeReaderArray<double> m_tree_cluster_delta_z(reader, "m_tree_cluster_delta_z");
  TTreeReaderArray<double> m_tree_cluster_delta_z_resunit(reader, "m_tree_cluster_delta_z_resunit");
  TTreeReaderArray<double> m_tree_cluster_sumpt2(reader, "m_tree_cluster_sumpt2");
  TTreeReaderArray<double> m_tree_cluster_ntracks(reader, "m_tree_cluster_ntracks");

  TTreeReaderValue<int> m_tree_jet_category(reader, "m_tree_jet_category");
  TTreeReaderValue<double> m_tree_jet_pt(reader, "m_tree_jet_pt");
  TTreeReaderValue<double> m_tree_jet_eta(reader, "m_tree_jet_eta");
  TTreeReaderValue<double> m_tree_jet_rpt(reader, "m_tree_jet_rpt");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_truth_time(reader, "m_tree_jet_hgtd_rpt_trutht0_truth_time");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_reco_time(reader, "m_tree_jet_hgtd_rpt_trutht0_reco_time");
  TTreeReaderValue<double> m_tree_jet_subjet_rpt(reader, "m_tree_jet_subjet_rpt");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times_truth_trk(reader, "m_tree_cluster_ntracks");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times(reader, "m_tree_jet_hgtd_rpt_cluster_times");

  /////////////////////////////////////////////
  ///////////// output histograms /////////////
  std::vector<TObject*> write;

  cout << "reading event data" << '\n';

  double jeteta_min = 2.4;
  double jeteta_max = 4.0;
  double min_bdt_cut_val = 0.2;


  /////////////////////////////////////////////
  // pre-store per event data

  // std::map<int, EventData> event_data;
  // int curr_evnt_nr = -1;
  // while (reader.Next()) {
  //   int event_number = *m_tree_eventnumber;
  //
  // }//LOOP over TChain
  //
  // reader.Restart();

  /////////////////////////////////////////////
  /////////////////////////////////////////////
  //get the events, where a PU cluster is chosen

  std::vector<int> problematic_events;

  int current_eventnumber = -1;
  while (reader.Next()) {
    //get the clusters only once per event
    int event_number = *m_tree_eventnumber;
    if (event_number == current_eventnumber) {continue;}
    current_eventnumber = event_number;

    int n_clusters = m_tree_cluster_hs_fraction.GetSize();

    //find the BDT choice
    bool cluster_accepted = false;
    int cluster_position = -1;
    double max_bdt_value = -1;

    for (int i=0; i<n_clusters; i++) {
      m_delta_z = m_tree_cluster_delta_z.At(i);
      m_z_sigma = m_tree_cluster_z0_sigma.At(i);
      m_q_over_p = m_tree_cluster_q_over_p.At(i);
      m_q_over_p_sigma = m_tree_cluster_q_over_p_sigma.At(i);
      m_delta_z_resunits = m_tree_cluster_delta_z_resunit.At(i);
      m_cluster_sumpt2 = m_tree_cluster_sumpt2.At(i);
      m_d0 = m_tree_cluster_d0.At(i);
      m_d0_sigma = m_tree_cluster_d0_sigma.At(i);
      m_number_of_tracks = m_tree_cluster_ntracks.At(i);

      // cout << "BDT out: " << tmva_reader->EvaluateMVA("BDT") << '\n';
      float bdt_output = tmva_reader->EvaluateMVA("BDT");

      if (bdt_output>min_bdt_cut_val and bdt_output>max_bdt_value and m_number_of_tracks>=3) {
        max_bdt_value = bdt_output;
        cluster_position = i;
        cluster_accepted = true;
      }
    }//LOOP over clusters in event

    if (cluster_accepted) {
      //check if it is a pure PU cluster
      if (m_tree_cluster_hs_fraction.At(cluster_position) < 0.01) {
        problematic_events.push_back(event_number);
        continue;
      }
    }
  }//LOOP over ntuple

  //remove duplicated entries from event number vector
  sort(problematic_events.begin(), problematic_events.end());
  problematic_events.erase( unique( problematic_events.begin(), problematic_events.end() ), problematic_events.end() );

  cout << "problematic_events: " << problematic_events.size() << '\n';

  /////////////////////////////////////////////
  /////////////////////////////////////////////

  reader.Restart();

  // struct EventData {
  //   int eventnumber = -1;
  //   int n_tracks = 0;
  //   int n_hs_tracks = 0;
  //   int n_hs_tracks_w_time = 0;
  //   int n_hs_tracks_w_good_time = 0;
  //   float local_vx_density;
  //   //jets
  //   int n_fwd_pu_jets = 0;
  //   int n_fwd_hs_jets = 0;
  //   int n_fwd_hard_pu_jets = 0;
  //   int n_fwd_hard_hs_jets = 0;
  // };

  std::map<int, EventData> event_data;

  while (reader.Next()) {
    int event_number = *m_tree_eventnumber;

    //jet
    double eta = *m_tree_jet_eta;
    double abs_eta = fabs(eta);
    if(abs_eta<jeteta_min or abs_eta>jeteta_max) {continue;}

    double pt = *m_tree_jet_pt;
    if(pt<30.) {continue;}

    //apply rpt cut on accepted jets
    double rpt = *m_tree_jet_rpt;
    if(rpt<0.17) {continue;}

    int category = *m_tree_jet_category;
    bool ishs = (category==0);
    bool ispu = (category>0 and category<4);

    if (ishs) {
      event_data[event_number].n_fwd_hs_jets++;
    } else if (ispu) {
      event_data[event_number].n_fwd_pu_jets++;
    }
    // if(pt<50.) {continue;}
    // if (ishs) {
    //   event_data[event_number].n_fwd_hard_hs_jets++;
    // } else if (ispu) {
    //   event_data[event_number].n_fwd_hard_pu_jets++;
    // }
  }//LOOP over TChain

  //get kinematic distributions in these problematic events
  auto hist_n_hs_jets = new TH1F("hist_n_hs_jets", ";n jets; a.u.", 10, 0, 10);
  write.push_back(hist_n_hs_jets);
  auto hist_n_pu_jets = new TH1F("hist_n_pu_jets", ";n jets; a.u.", 10, 0, 10);
  write.push_back(hist_n_pu_jets);
  // auto hist_n_pu_jets = new TH1F("hist_n_pu_jets", ";n jets; a.u.", 10, 0, 10);
  // write.push_back(hist_n_pu_jets);
  for(const auto& event : event_data) {
    int n_fwd_hs_jets = event.second.n_fwd_hs_jets;
    // int n_fwd_hard_hs_jets = event.second.n_fwd_hard_hs_jets;
    int n_fwd_pu_jets = event.second.n_fwd_pu_jets;
    // int n_fwd_hard_pu_jets = event.second.n_fwd_hard_pu_jets;

    hist_n_hs_jets->Fill(n_fwd_hs_jets);
    hist_n_pu_jets->Fill(n_fwd_pu_jets);

  }

  auto rootfile = TFile::Open("ProblematicEventsOutput.root", "RECREATE");
  for (const auto& el : write) {
    el->Write();
  }
  rootfile->Close();
  cout << "writing ProblematicEventsOutput.root\n";
}

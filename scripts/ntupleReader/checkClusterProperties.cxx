struct EventData {
  // int n_fwd_jets;
  int n_tracks;
  int n_hs_tracks;
  int n_hs_tracks_w_time;
  int n_hs_tracks_w_good_time;

  float local_vx_density;
};


void checkClusterProperties() {

  TString path = "/afs/cern.ch/work/a/aleopold/private/hgtd/analysis_pkg/run/PileupRejectionNtuple_20k.root";

  auto g_file = TFile::Open(path, "READ");

  TTreeReader reader("JetTree", g_file);

  TTreeReaderValue<int> m_tree_eventnumber(reader, "m_tree_eventnumber");
  // TTreeReaderValue<double> m_tree_truth_t0(reader, "m_tree_truth_t0");
  // TTreeReaderValue<double> m_tree_param_vx_density(reader, "m_tree_param_vx_density");
  TTreeReaderValue<double> m_tree_local_vx_density(reader, "m_tree_local_vx_density");
  // TTreeReaderValue<double> m_tree_local_truthvx_density(reader, "m_tree_local_truthvx_density");
  // TTreeReaderValue<double> m_tree_deltaz_vx_truthvx(reader, "m_tree_deltaz_vx_truthvx");
  //
  TTreeReaderValue<int> m_tree_n_hgtd_tracks(reader, "m_tree_n_hgtd_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks(reader, "m_tree_n_hgtd_hs_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_time(reader, "m_tree_n_hgtd_hs_tracks_with_time");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_good_time(reader, "m_tree_n_hgtd_hs_tracks_with_good_time");

  TTreeReaderArray<double> m_tree_cluster_time(reader, "m_tree_cluster_time");
  TTreeReaderArray<double> m_tree_cluster_time_res(reader, "m_tree_cluster_time_res");
  TTreeReaderArray<double> m_tree_cluster_z0(reader, "m_tree_cluster_z0");
  TTreeReaderArray<double> m_tree_cluster_z0_sigma(reader, "m_tree_cluster_z0_sigma");
  TTreeReaderArray<double> m_tree_cluster_q_over_p(reader, "m_tree_cluster_q_over_p");
  TTreeReaderArray<double> m_tree_cluster_q_over_p_sigma(reader, "m_tree_cluster_q_over_p_sigma");
  TTreeReaderArray<double> m_tree_cluster_d0(reader, "m_tree_cluster_d0");
  TTreeReaderArray<double> m_tree_cluster_d0_sigma(reader, "m_tree_cluster_d0_sigma");
  TTreeReaderArray<double> m_tree_cluster_hs_fraction(reader, "m_tree_cluster_hs_fraction");
  TTreeReaderArray<double> m_tree_cluster_delta_z(reader, "m_tree_cluster_delta_z");
  TTreeReaderArray<double> m_tree_cluster_delta_z_resunit(reader, "m_tree_cluster_delta_z_resunit");
  TTreeReaderArray<double> m_tree_cluster_sumpt2(reader, "m_tree_cluster_sumpt2");
  TTreeReaderArray<double> m_tree_cluster_ntracks(reader, "m_tree_cluster_ntracks");

  TTreeReaderValue<int> m_tree_jet_category(reader, "m_tree_jet_category");
  TTreeReaderValue<double> m_tree_jet_pt(reader, "m_tree_jet_pt");
  TTreeReaderValue<double> m_tree_jet_eta(reader, "m_tree_jet_eta");
  TTreeReaderValue<double> m_tree_jet_rpt(reader, "m_tree_jet_rpt");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_truth_time(reader, "m_tree_jet_hgtd_rpt_trutht0_truth_time");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_reco_time(reader, "m_tree_jet_hgtd_rpt_trutht0_reco_time");
  TTreeReaderValue<double> m_tree_jet_subjet_rpt(reader, "m_tree_jet_subjet_rpt");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times_truth_trk(reader, "m_tree_cluster_ntracks");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times(reader, "m_tree_jet_hgtd_rpt_cluster_times");

  auto hist_pileup_density = new TH1F("hist_pileup_density", ";PU VX denisty;a.u.", 30, 0, 5);

  //get event infor for each event
  std::map<int, EventData> eventdatamap;
  int current_eventnumber = -1;
  while (reader.Next()) {
    int event_number = *m_tree_eventnumber;
    if (event_number == current_eventnumber) {continue;}
    current_eventnumber = event_number;
    EventData evnt_data;
    evnt_data.n_tracks = *m_tree_n_hgtd_tracks;
    evnt_data.n_hs_tracks = *m_tree_n_hgtd_hs_tracks;
    evnt_data.n_hs_tracks_w_time = *m_tree_n_hgtd_hs_tracks_with_time;
    evnt_data.n_hs_tracks_w_good_time = *m_tree_n_hgtd_hs_tracks_with_good_time;
    evnt_data.local_vx_density = *m_tree_local_vx_density;
    hist_pileup_density->Fill(*m_tree_local_vx_density);
    eventdatamap[event_number] = evnt_data;

  }

  auto rootfile = TFile::Open("clusterAndEvntProperties.root", "RECREATE");
  hist_pileup_density->Write();
  rootfile->Close();

  reader.Restart();

  // now check if the not-working of the TCRpT has
  // something to do with the event properties
  while (reader.Next()) {
    int event_number = *m_tree_eventnumber;
    double pt = *m_tree_jet_pt;
    if(pt<30.) {continue;}
    double std_rpt = *m_tree_jet_rpt;
    if (std_rpt<0.2) {continue;}

    int category = *m_tree_jet_category;
    //only PU jet
    if (category<1 or category>3) {continue;}
    //get the best truth cluster position
    double current_fraction = 0;
    int current_position = -1;
    for (int i=0; i<m_tree_cluster_hs_fraction.GetSize(); i++) {
      if (m_tree_cluster_hs_fraction.At(i)>0.5 and m_tree_cluster_hs_fraction.At(i) > current_fraction) {
        current_fraction = m_tree_cluster_hs_fraction.At(i);
        current_position = i;
      }
    }
    if (current_position<0) {continue;}
    //check if PU jet with RpT > 0.1 after clusterT0
    double btc_rpt = m_tree_jet_hgtd_rpt_cluster_times.At(current_position);
    // if(btc_rpt>0.1) {
    //   cout << "<<<<< RpT: " << btc_rpt << '\n';
    //   cout << eventdatamap[event_number].n_tracks << '\n';
    //   cout << eventdatamap[event_number].n_hs_tracks << '\n';
    //   cout << eventdatamap[event_number].n_hs_tracks_w_time << '\n';
    //   cout << eventdatamap[event_number].n_hs_tracks_w_good_time << '\n';
    // }
    if(btc_rpt>0.2) {
      cout << "<<<<< accepted \n";
    } else {
      cout << "<<<<< REJECTED \n";
    }
    cout << "RpT: " << std_rpt << '\n';
    cout << "TCRpT: " << btc_rpt << '\n';
    cout << "n trks: " << eventdatamap[event_number].n_tracks << '\n';
    cout << "n hs trks: " << eventdatamap[event_number].n_hs_tracks << '\n';
    cout << "n hs trks w/ time: " << eventdatamap[event_number].n_hs_tracks_w_time << '\n';
    cout << "n hs trks w/ good time: " << eventdatamap[event_number].n_hs_tracks_w_good_time << '\n';
    cout << "local VXd: " <<  eventdatamap[event_number].local_vx_density << '\n';
  }
}

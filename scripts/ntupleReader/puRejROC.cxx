#include "getTChain.cxx"
#include "prepareRocCurves.cxx"

struct EventData {
  int eventnumber = -1;
  int n_tracks = 0;
  int n_hs_tracks = 0;
  int n_hs_tracks_w_time = 0;
  int n_hs_tracks_w_good_time = 0;

  float local_vx_density;

  // jets
  int n_fwd_pu_jets = 0;
  int n_fwd_hs_jets = 0;
  int n_fwd_hard_pu_jets = 0;
  int n_fwd_hard_hs_jets = 0;

  int n_fwd_hard_jets = 0;
  bool hasHighRpTJets = false;
};

struct BDTData {
  vector<int> index;
  vector<float> score;
  vector<float> hs_frac;
};

void puRejROC() {
  //////////  load BDT from XML //////////
  TMVA::Tools::Instance();

  TMVA::Reader *tmva_reader = new TMVA::Reader("!Color:!Silent");

  float m_delta_z;
  float m_z_sigma;
  float m_q_over_p;
  float m_q_over_p_sigma;
  float m_d0;
  float m_d0_sigma;
  float m_delta_z_resunits;
  float m_cluster_sumpt2;
  float m_number_of_tracks;
  float m_z0_stddev;
  float m_z0_mean;
  float m_closest_vx_is_pvx;
  float m_distance_pu_vx;
  float m_n_pu_vertices;
  float m_pt_mod;

  tmva_reader->AddVariable("m_delta_z", &m_delta_z);
  tmva_reader->AddVariable("m_z_sigma", &m_z_sigma);
  tmva_reader->AddVariable("m_q_over_p", &m_q_over_p);
  tmva_reader->AddVariable("m_q_over_p_sigma", &m_q_over_p_sigma);
  tmva_reader->AddVariable("m_delta_z_resunits", &m_delta_z_resunits);
  tmva_reader->AddVariable("m_cluster_sumpt2", &m_cluster_sumpt2);
  tmva_reader->AddVariable("m_d0", &m_d0);
  tmva_reader->AddVariable("m_d0_sigma", &m_d0_sigma);
  tmva_reader->AddVariable("m_number_of_tracks", &m_number_of_tracks);
  tmva_reader->AddVariable("m_z0_stddev", &m_z0_stddev);
  tmva_reader->AddVariable("m_z0_mean", &m_z0_mean);
  tmva_reader->AddVariable("m_closest_vx_is_pvx", &m_closest_vx_is_pvx);
  tmva_reader->AddVariable("m_distance_pu_vx", &m_distance_pu_vx);
  tmva_reader->AddVariable("m_n_pu_vertices", &m_n_pu_vertices);
  tmva_reader->AddVariable("m_pt_mod", &m_pt_mod);

  TString weightfile = "dataset/weights/TMVAClassification_BDT.weights.xml";
  tmva_reader->BookMVA("BDT", weightfile);
  ////////////////////////////////////////
  cout << "BDT loaded\n";

  initRpTCuts();

  std::vector<TString> list_of_files =
      getListOfFiles("INPUT.txt", -1); // this is defined in GetTChain.h
  // skip the files used for training
  // int n_files_to_skip = 196;
  int n_files_to_skip = 30;
  // int n_files_to_skip = 198; // with new vars files, PFTQv05
  // int n_files_to_skip = 200; //FILES FROM TAO
  int end_pos_for_erase = list_of_files.size() - n_files_to_skip;
  list_of_files.erase(list_of_files.begin(),
                      list_of_files.end() - end_pos_for_erase);

  cout << list_of_files.size() << " files left for evaluation\n";

  if (list_of_files.size() == 0) {
    cout << "ERROR: getListOfFiles returned no files to be read" << endl;
    return 1;
  }
  TChain *g_file = getChain(list_of_files, "JetTree", -1);
  std::vector<TObject *> write;

  std::array<TEfficiency *, 4> m_eff_std_rpt;
  setupRocCurvePlots(m_eff_std_rpt, "StandardRpT", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_std_rpt_variablebin;
  setupRocCurvePlotsVariableBins(m_eff_std_rpt_variablebin, "StandardRpT",
                                 "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_subjet_rpt;
  setupRocCurvePlots(m_eff_subjet_rpt, "SubJetRpT", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_subjet_rpt_variablebin;
  setupRocCurvePlotsVariableBins(m_eff_subjet_rpt_variablebin, "SubJetRpT",
                                 "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_trutht0_truthtrktime_rpt;
  setupRocCurvePlots(m_eff_trutht0_truthtrktime_rpt, "TruthT0TruthTrkTime",
                     "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_trutht0_truthtrktime_rpt_variablebin;
  setupRocCurvePlotsVariableBins(m_eff_trutht0_truthtrktime_rpt_variablebin,
                                 "TruthT0TruthTrkTime", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_trutht0_recotrktime_rpt;
  setupRocCurvePlots(m_eff_trutht0_recotrktime_rpt, "TruthT0RecoTrkTime",
                     "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_trutht0_recotrktime_rpt_75eff;
  setupRocCurvePlots(m_eff_trutht0_recotrktime_rpt_75eff,
                     "TruthT0RecoTrkTime75eff", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_smearedtrutht0_recotrktime_rpt_75eff;
  setupRocCurvePlots(m_eff_smearedtrutht0_recotrktime_rpt_75eff,
                     "SmearedTruthT0RecoTrkTime75eff", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_trutht0_recotrktime_rpt_variablebin;
  setupRocCurvePlotsVariableBins(m_eff_trutht0_recotrktime_rpt_variablebin,
                                 "TruthT0RecoTrkTime", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_bestTruthHSCluster_rpt;
  setupRocCurvePlots(m_eff_bestTruthHSCluster_rpt, "BestTruthCluster",
                     "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_bestTruthHSClusterSubjetRpT;
  setupRocCurvePlots(m_eff_bestTruthHSClusterSubjetRpT,
                     "BestTruthClusterrSubjetRpT", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_BDTClusterOrStdRpT;
  setupRocCurvePlots(m_eff_BDTClusterOrStdRpT, "BDTClusterOrStdRpT", "RpT cut",
                     write);

  std::array<TEfficiency *, 4> m_eff_BDTClusterOrStdRpT_variablebin;
  setupRocCurvePlotsVariableBins(m_eff_BDTClusterOrStdRpT_variablebin,
                                 "BDTClusterOrStdRpT", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_BDTClusterOrSubjetRpT;
  setupRocCurvePlots(m_eff_BDTClusterOrSubjetRpT, "BDTClusterOrSubjetRpT",
                     "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_BDTClusterOrSubjetRpT_variablebin;
  setupRocCurvePlotsVariableBins(m_eff_BDTClusterOrSubjetRpT_variablebin,
                                 "BDTClusterOrSubjetRpT", "RpT cut", write);

  std::array<TEfficiency *, 2> m_eff_StandardRpTAllPt;
  setupRocCurvePlotsCombined(m_eff_StandardRpTAllPt, "StandardRpTAllPt",
                             "RpT cut", write);

  std::array<TEfficiency *, 2> m_eff_TruthRpTAllPt;
  setupRocCurvePlotsCombined(m_eff_TruthRpTAllPt, "TruthRpTAllPt", "RpT cut",
                             write);

  std::array<TEfficiency *, 2> m_eff_BDTClusterOrSubjetRpTAllPt;
  setupRocCurvePlotsCombined(m_eff_BDTClusterOrSubjetRpTAllPt,
                             "BDTClusterOrSubjetRpTAllPt", "RpT cut", write);

  auto m_hist_jetcategory =
      new TH1F("m_hist_jetcategory", ";category;a.u.", 5, 0, 5);
  write.push_back(m_hist_jetcategory);

  // number of PU jets vs eta for stdRpT and BDTRpt
  auto m_hist_jeteta_rpt = new TH1F("m_hist_jeteta_rpt", ";|#eta|; a.u.", 4,
                                    2.4, 4.0); // cut of 0.2 for std rpt
  write.push_back(m_hist_jeteta_rpt);

  auto m_hist_jeteta_rpt_highpt =
      new TH1F("m_hist_jeteta_rpt_highpt", ";|#eta|; a.u.", 4, 2.4,
               4.0); // cut of 0.2 for std rpt
  write.push_back(m_hist_jeteta_rpt_highpt);

  auto m_hist_jeteta_btrpt =
      new TH1F("m_hist_jeteta_btrpt", ";|#eta|; a.u.", 4, 2.4, 4.0); // 0.17
  write.push_back(m_hist_jeteta_btrpt);

  auto m_hist_jeteta_btrpt_highpt = new TH1F(
      "m_hist_jeteta_btrpt_highpt", ";|#eta|; a.u.", 4, 2.4, 4.0); // 0.17
  write.push_back(m_hist_jeteta_btrpt_highpt);

  auto m_hist_jeteta_rpt_1bin =
      new TH1F("m_hist_jeteta_rpt_1bin", ";|#eta|; a.u.", 1, 2.4,
               4.0); // cut of 0.2 for std rpt
  write.push_back(m_hist_jeteta_rpt_1bin);
  auto m_hist_jeteta_btrpt_1bin = new TH1F(
      "m_hist_jeteta_btrpt_1bin", ";|#eta|; a.u.", 1, 2.4, 4.0); // 0.17
  write.push_back(m_hist_jeteta_btrpt_1bin);

  // make ROC curve for all 5 (or 6) cases
  std::array<TEfficiency *, 4> m_eff_std_rpt_type1;
  setupRocCurvePlots(m_eff_std_rpt_type1, "StdRpTtype1", "RpT cut", write);
  std::array<TEfficiency *, 4> m_eff_bdt_rpt_type1;
  setupRocCurvePlots(m_eff_bdt_rpt_type1, "BDTRpTtype1", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_std_rpt_type2;
  setupRocCurvePlots(m_eff_std_rpt_type2, "StdRpTtype2", "RpT cut", write);
  std::array<TEfficiency *, 4> m_eff_bdt_rpt_type2;
  setupRocCurvePlots(m_eff_bdt_rpt_type2, "BDTRpTtype2", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_std_rpt_type3;
  setupRocCurvePlots(m_eff_std_rpt_type3, "StdRpTtype3", "RpT cut", write);
  std::array<TEfficiency *, 4> m_eff_bdt_rpt_type3;
  setupRocCurvePlots(m_eff_bdt_rpt_type3, "BDTRpTtype3", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_std_rpt_type4;
  setupRocCurvePlots(m_eff_std_rpt_type4, "StdRpTtype4", "RpT cut", write);
  std::array<TEfficiency *, 4> m_eff_bdt_rpt_type4;
  setupRocCurvePlots(m_eff_bdt_rpt_type4, "BDTRpTtype4", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_std_rpt_type5;
  setupRocCurvePlots(m_eff_std_rpt_type5, "StdRpTtype5", "RpT cut", write);
  std::array<TEfficiency *, 4> m_eff_bdt_rpt_type5;
  setupRocCurvePlots(m_eff_bdt_rpt_type5, "BDTRpTtype5", "RpT cut", write);

  // bdt if there is no perfect choice
  std::array<TEfficiency *, 4> m_eff_no_perfect_butbdt;
  setupRocCurvePlots(m_eff_no_perfect_butbdt, "NoPerfectButBDT", "RpT cut",
                     write);
  std::array<TEfficiency *, 4> m_eff_no_perfect_butbdt_stdcomparison;
  setupRocCurvePlots(m_eff_no_perfect_butbdt_stdcomparison,
                     "NoPerfectButBDTStdComparison", "RpT cut", write);

  // bdt if ther is a perfect choice
  std::array<TEfficiency *, 4> m_eff_yes_perfect_butbdt;
  setupRocCurvePlots(m_eff_yes_perfect_butbdt, "YesPerfectButBDT", "RpT cut",
                     write);
  std::array<TEfficiency *, 4> m_eff_yes_perfect_butbdt_stdcomparison;
  setupRocCurvePlots(m_eff_yes_perfect_butbdt_stdcomparison,
                     "YesPerfectButBDTStdComparison", "RpT cut", write);
  std::array<TEfficiency *, 4> m_eff_yes_perfect_butbdt_perfectcomparison;
  setupRocCurvePlots(m_eff_yes_perfect_butbdt_perfectcomparison,
                     "YesPerfectButBDTPerfectComparison", "RpT cut", write);

  const static int n_density_nbins = 5;
  const float density_min = 0.0;
  const float density_step = 0.6;
  const float density_max = density_min + n_density_nbins * density_step;
  // ROC curves in bins of pile-up local density
  std::array<std::array<TEfficiency *, 4>, n_density_nbins>
      m_eff_loc_density_stdrpt;
  std::array<std::array<TEfficiency *, 4>, n_density_nbins>
      m_eff_loc_density_bdtrpt;
  std::string plotname("");
  for (int i = 0; i < n_density_nbins; i++) {
    plotname = "StandardRpT_density_bin" + std::to_string(i);
    setupRocCurvePlotsVariableBins(m_eff_loc_density_stdrpt.at(i), plotname,
                                   "RpT cut", write);
    plotname = "BDTClusterOrSubjetRpT_density_bin" + std::to_string(i);
    setupRocCurvePlotsVariableBins(m_eff_loc_density_bdtrpt.at(i), plotname,
                                   "RpT cut", write);
  }

  // ROC curves in bins of pile-up parametrised density

  auto hist_hs_pt = new TH1F("hist_hs_pt", ";pT [GeV];a.u.", 100, 0, 200);
  write.push_back(hist_hs_pt);
  auto hist_hs_eta = new TH1F("hist_hs_eta", ";|#eta|;a.u.", 16, 2.4, 4.0);
  write.push_back(hist_hs_eta);
  auto hist_pu_pt = new TH1F("hist_pu_pt", ";pT [GeV];a.u.", 100, 0, 200);
  write.push_back(hist_pu_pt);
  auto hist_pu_eta = new TH1F("hist_pu_eta", ";|#eta|;a.u.", 16, 2.4, 4.0);
  write.push_back(hist_pu_eta);

  auto hist_rpt_hs = new TH1F("hist_rpt_hs", ";RpT;a.u.", 50, 0, 1.5);
  write.push_back(hist_rpt_hs);
  auto hist_rpt_pu = new TH1F("hist_rpt_pu", ";RpT;a.u.", 50, 0, 1.5);
  write.push_back(hist_rpt_pu);
  auto hist_hsct0rpt_hs = new TH1F("hist_hsct0rpt_hs", ";RpT;a.u.", 50, 0, 1.5);
  write.push_back(hist_hsct0rpt_hs);
  auto hist_hsct0rpt_pu = new TH1F("hist_hsct0rpt_pu", ";RpT;a.u.", 50, 0, 1.5);
  write.push_back(hist_hsct0rpt_pu);
  auto hist_deltaRpT_hs =
      new TH1F("hist_deltaRpT_hs", ";#DeltaRpT;a.u.", 75, -1.5, 1.5);
  write.push_back(hist_deltaRpT_hs);
  auto hist_deltaRpT_pu =
      new TH1F("hist_deltaRpT_pu", ";#DeltaRpT;a.u.", 75, -1.5, 1.5);
  write.push_back(hist_deltaRpT_pu);

  auto hist_loc_pu_dens_2p0 =
      new TH1F("hist_loc_pu_dens_2p0", ";local PU density; a.u.", 20, 0, 6);
  write.push_back(hist_loc_pu_dens_2p0);
  auto hist_loc_pu_dens_0p1 =
      new TH1F("hist_loc_pu_dens_0p1", ";local PU density; a.u.", 20, 0, 6);
  write.push_back(hist_loc_pu_dens_0p1);
  auto hist_loc_truth_pu_dens_2p0 = new TH1F(
      "hist_loc_truth_pu_dens_2p0", ";local PU density; a.u.", 20, 0, 6);
  write.push_back(hist_loc_truth_pu_dens_2p0);
  auto hist_loc_truth_pu_dens_0p1 = new TH1F(
      "hist_loc_truth_pu_dens_0p1", ";local PU density; a.u.", 20, 0, 6);
  write.push_back(hist_loc_truth_pu_dens_0p1);

  // pu rejection vs eta
  std::array<TEfficiency *, n_eta_bins> m_eff_hs_rpt_low_pt;
  std::array<TEfficiency *, n_eta_bins> m_eff_hs_rpt_high_pt;
  std::array<TEfficiency *, n_eta_bins> m_eff_pu_rpt_low_pt;
  std::array<TEfficiency *, n_eta_bins> m_eff_pu_rpt_high_pt;
  prepareEffScanVsEta(m_eff_hs_rpt_low_pt, m_eff_hs_rpt_high_pt,
                      m_eff_pu_rpt_low_pt, m_eff_pu_rpt_high_pt, write, "std");

  std::array<TEfficiency *, n_eta_bins> m_eff_hs_bdtrpt_low_pt;
  std::array<TEfficiency *, n_eta_bins> m_eff_hs_bdtrpt_high_pt;
  std::array<TEfficiency *, n_eta_bins> m_eff_pu_bdtrpt_low_pt;
  std::array<TEfficiency *, n_eta_bins> m_eff_pu_bdtrpt_high_pt;
  prepareEffScanVsEta(m_eff_hs_bdtrpt_low_pt, m_eff_hs_bdtrpt_high_pt,
                      m_eff_pu_bdtrpt_low_pt, m_eff_pu_bdtrpt_high_pt, write,
                      "bdt");

  TTreeReader reader(g_file);

  TTreeReaderValue<int> m_tree_eventnumber(reader, "m_tree_eventnumber");
  // TTreeReaderValue<double> m_tree_truth_t0(reader, "m_tree_truth_t0");
  // TTreeReaderValue<double> m_tree_param_vx_density(reader,
  // "m_tree_param_vx_density");
  TTreeReaderValue<double> m_tree_local_vx_density(reader,
                                                   "m_tree_local_vx_density");
  TTreeReaderValue<double> m_tree_local_truthvx_density(
      reader, "m_tree_local_truthvx_density");
  TTreeReaderValue<double> m_tree_deltaz_vx_truthvx(reader,
                                                    "m_tree_deltaz_vx_truthvx");

  TTreeReaderValue<int> m_tree_n_hgtd_tracks(reader, "m_tree_n_hgtd_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks(reader,
                                                "m_tree_n_hgtd_hs_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_time(
      reader, "m_tree_n_hgtd_hs_tracks_with_time");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_good_time(
      reader, "m_tree_n_hgtd_hs_tracks_with_good_time");

  TTreeReaderArray<double> m_tree_cluster_time(reader, "m_tree_cluster_time");
  TTreeReaderArray<double> m_tree_cluster_time_res(reader,
                                                   "m_tree_cluster_time_res");
  TTreeReaderArray<double> m_tree_cluster_z0(reader, "m_tree_cluster_z0");
  TTreeReaderArray<double> m_tree_cluster_z0_sigma(reader,
                                                   "m_tree_cluster_z0_sigma");
  TTreeReaderArray<double> m_tree_cluster_q_over_p(reader,
                                                   "m_tree_cluster_q_over_p");
  TTreeReaderArray<double> m_tree_cluster_q_over_p_sigma(
      reader, "m_tree_cluster_q_over_p_sigma");
  TTreeReaderArray<double> m_tree_cluster_d0(reader, "m_tree_cluster_d0");
  TTreeReaderArray<double> m_tree_cluster_d0_sigma(reader,
                                                   "m_tree_cluster_d0_sigma");
  TTreeReaderArray<double> m_tree_cluster_hs_fraction(
      reader, "m_tree_cluster_hs_fraction");
  TTreeReaderArray<double> m_tree_cluster_delta_z(reader,
                                                  "m_tree_cluster_delta_z");
  TTreeReaderArray<double> m_tree_cluster_delta_z_resunit(
      reader, "m_tree_cluster_delta_z_resunit");
  TTreeReaderArray<double> m_tree_cluster_sumpt2(reader,
                                                 "m_tree_cluster_sumpt2");
  TTreeReaderArray<double> m_tree_cluster_ntracks(reader,
                                                  "m_tree_cluster_ntracks");
  TTreeReaderArray<double> m_tree_cluster_z0_stddev(reader,
                                                    "m_tree_cluster_z0_stddev");
  TTreeReaderArray<double> m_tree_cluster_z0_mean(reader,
                                                  "m_tree_cluster_z0_mean");
  TTreeReaderArray<double> m_tree_cluster_closest_vx_is_pvx(
      reader, "m_tree_cluster_closest_vx_is_pvx");
  TTreeReaderArray<double> m_tree_cluster_distance_pu_vx(
      reader, "m_tree_cluster_distance_pu_vx");
  TTreeReaderArray<double> m_tree_cluster_n_pu_vertices(
      reader, "m_tree_cluster_n_pu_vertices");
  TTreeReaderArray<double> m_tree_cluster_pt_mod(reader,
                                                 "m_tree_cluster_pt_mod");

  TTreeReaderValue<int> m_tree_jet_category(reader, "m_tree_jet_category");
  TTreeReaderValue<double> m_tree_jet_pt(reader, "m_tree_jet_pt");
  TTreeReaderValue<double> m_tree_jet_eta(reader, "m_tree_jet_eta");
  TTreeReaderValue<double> m_tree_jet_rpt(reader, "m_tree_jet_rpt");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_truth_time(
      reader, "m_tree_jet_hgtd_rpt_trutht0_truth_time");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_reco_time(
      reader, "m_tree_jet_hgtd_rpt_trutht0_reco_time");
  // TTreeReaderValue<double> m_tree_jet_hgtd_rpt_smearedt0_reco_time(reader,
  // "m_tree_jet_hgtd_rpt_smearedt0_reco_time");
  TTreeReaderValue<double> m_tree_jet_subjet_rpt(reader,
                                                 "m_tree_jet_subjet_rpt");
  // TTreeReaderArray<double>
  // m_tree_jet_hgtd_rpt_cluster_times_truth_trk(reader,
  // "m_tree_cluster_ntracks");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times(
      reader, "m_tree_jet_hgtd_rpt_cluster_times");

  TRandom3 m_rand;

  double jeteta_min = 2.4;
  double jeteta_max = 4.0;

  // double min_bdt_cut_val = 0.1;
  // double min_bdt_cut_val = -0.24;

  double min_bdt_cut_val = -0.2;
  /////////////////////////////////////////////
  cout << "reading event data" << '\n';
  std::map<int, EventData> event_data;
  while (reader.Next()) {
    // use only jets over 30GeV
    if (*m_tree_jet_pt < 50.) {
      continue;
    }
    double eta = *m_tree_jet_eta;
    double abs_eta = fabs(eta);
    if (abs_eta < jeteta_min or abs_eta > jeteta_max) {
      continue;
    }
    int event_number = *m_tree_eventnumber;
    event_data[event_number].n_fwd_hard_jets++;

  } // LOOP over TChain
  reader.Restart();
  /////////////////////////////////////////////

  /////////////////////////////////////////////
  // read in the lGBM output scores
  // cout << "reading lGBM scores" << '\n';
  // auto score_file = TFile::Open("clusterScores.root");
  // TTreeReader score_reader("ClusterTree", score_file);
  // TTreeReaderValue<int> m_score_eventnumber(score_reader, "m_eventnumber");
  // TTreeReaderValue<int> m_score_index(score_reader, "m_cluster_index");
  // TTreeReaderValue<double> m_score_hsfraction(score_reader, "m_hs_fraction");
  // TTreeReaderValue<double> m_score_bdtout(score_reader, "m_bdt_output");
  //
  // std::map<int, BDTData> lgbm_data;
  //
  // while (score_reader.Next()) {
  //   lgbm_data[*m_score_eventnumber].index.push_back(*m_score_index);
  //   lgbm_data[*m_score_eventnumber].score.push_back(*m_score_bdtout);
  //   lgbm_data[*m_score_eventnumber].hs_frac.push_back(*m_score_hsfraction);
  // }
  // cout << "map length: " << lgbm_data.size() << '\n';
  /////////////////////////////////////////////

  auto m_denisty_bfh =
      new TH1F("m_denisty_bfh", "", n_density_nbins, density_min, density_max);

  cout << "starting loop\n";
  while (reader.Next()) {

    ///////////////// access evnt  info /////////////////
    int event_number = *m_tree_eventnumber;
    bool dangerous = false;
    if (event_data[event_number].n_fwd_hard_jets == 1) {
      dangerous = true;
      // cout << "changed the cut!!\n";
    }
    /////////////////////////////////////////////////////

    // if (*m_tree_deltaz_vx_truthvx>=0.1) {continue;}
    double pt = *m_tree_jet_pt;
    // use only jets over 30GeV
    if (pt < 30.) {
      continue;
    }
    double eta = *m_tree_jet_eta;
    double abs_eta = fabs(eta);
    if (abs_eta < jeteta_min or abs_eta > jeteta_max) {
      continue;
    }
    // which bin does the local density of this event belong to
    float vx_density = *m_tree_local_truthvx_density;
    int vx_density_bin = m_denisty_bfh->FindBin(vx_density) - 1;
    if (vx_density_bin > n_density_nbins - 1) {
      vx_density_bin = n_density_nbins - 1;
    }

    // retrieve the ITk only RpT variable for this jet
    double std_rpt = *m_tree_jet_rpt;
    int category = *m_tree_jet_category;
    m_hist_jetcategory->Fill(category);
    // this is how the jet categories are defined
    bool ishs = (category == 0);
    bool ispu = (category > 0 and category < 4);
    // bool ispu = (category>0);
    fillRpTEffCurves(std_rpt, m_eff_std_rpt, category, pt);
    fillRpTEffCurves(std_rpt, m_eff_std_rpt_variablebin, category, pt);
    fillRpTEffCurves(std_rpt, m_eff_loc_density_stdrpt.at(vx_density_bin),
                     category, pt);

    fillRpTEffCurvesCombined(std_rpt, m_eff_StandardRpTAllPt, category);

    fillEtaPURej(std_rpt, m_eff_hs_rpt_low_pt, m_eff_hs_rpt_high_pt,
                 m_eff_pu_rpt_low_pt, m_eff_pu_rpt_high_pt, category, pt,
                 abs_eta);
    /////////////////
    // curves with truth information
    double truth_t0_truth_trk_rpt = *m_tree_jet_hgtd_rpt_trutht0_truth_time;
    fillRpTEffCurves(truth_t0_truth_trk_rpt, m_eff_trutht0_truthtrktime_rpt,
                     category, pt);
    fillRpTEffCurves(truth_t0_truth_trk_rpt,
                     m_eff_trutht0_truthtrktime_rpt_variablebin, category, pt);
    double truth_t0_reco_trk_rpt = *m_tree_jet_hgtd_rpt_trutht0_reco_time;
    fillRpTEffCurves(truth_t0_reco_trk_rpt, m_eff_trutht0_recotrktime_rpt,
                     category, pt);
    fillRpTEffCurves(truth_t0_reco_trk_rpt,
                     m_eff_trutht0_recotrktime_rpt_variablebin, category, pt);

    // double smeared_truth_t0_reco_trk_rpt =
    // *m_tree_jet_hgtd_rpt_smearedt0_reco_time;
    double smeared_truth_t0_reco_trk_rpt = -1;

    /////////////////

    // cout << "get truth cluster\n";
    // get the cluster with the highest HS fraction, but > 0.5
    double current_fraction = 0;
    int truth_cluster_position = -1;
    int n_clusters = m_tree_cluster_hs_fraction.GetSize();
    for (int i = 0; i < n_clusters; i++) {
      if (m_tree_cluster_hs_fraction.At(i) > 0.5 and
          m_tree_cluster_hs_fraction.At(i) > current_fraction and
          m_tree_cluster_ntracks.At(i) >= 3) {
        current_fraction = m_tree_cluster_hs_fraction.At(i);
        truth_cluster_position = i;
      }
    }
    //
    double btc_rpt = -1;
    double subjetRpT = *m_tree_jet_subjet_rpt;
    fillRpTEffCurves(subjetRpT, m_eff_subjet_rpt, category, pt);
    fillRpTEffCurves(subjetRpT, m_eff_subjet_rpt_variablebin, category, pt);

    if (truth_cluster_position > 0) {
      btc_rpt = m_tree_jet_hgtd_rpt_cluster_times.At(truth_cluster_position);
      fillRpTEffCurves(btc_rpt, m_eff_bestTruthHSCluster_rpt, category, pt);
      fillRpTEffCurves(btc_rpt, m_eff_bestTruthHSClusterSubjetRpT, category,
                       pt);
      fillRpTEffCurvesCombined(btc_rpt, m_eff_TruthRpTAllPt, category);

    } else {
      fillRpTEffCurves(std_rpt, m_eff_bestTruthHSCluster_rpt, category, pt);
      fillRpTEffCurves(subjetRpT, m_eff_bestTruthHSClusterSubjetRpT, category,
                       pt);
      fillRpTEffCurvesCombined(std_rpt, m_eff_TruthRpTAllPt, category);
    }

    if (ispu and std_rpt > 0.2) {
      if (pt < 50.) {
        m_hist_jeteta_rpt->Fill(abs_eta);
        m_hist_jeteta_rpt_1bin->Fill(abs_eta);
      }
      if (pt > 50. and std_rpt > 0.26) {
        m_hist_jeteta_rpt_highpt->Fill(abs_eta);
      }
    }

    // cout << "get BDT cluster\n";
    ////////////////////////////////////////
    // get the cluster from BDT evaluation
    bool cluster_accepted = false;
    int cluster_position = -1;
    double max_bdt_value = -1;
    for (int i = 0; i < n_clusters; i++) {
      m_delta_z = m_tree_cluster_delta_z.At(i);
      m_z_sigma = m_tree_cluster_z0_sigma.At(i);
      m_q_over_p = m_tree_cluster_q_over_p.At(i);
      m_q_over_p_sigma = m_tree_cluster_q_over_p_sigma.At(i);
      m_delta_z_resunits = m_tree_cluster_delta_z_resunit.At(i);
      m_cluster_sumpt2 = m_tree_cluster_sumpt2.At(i);
      m_d0 = m_tree_cluster_d0.At(i);
      m_d0_sigma = m_tree_cluster_d0_sigma.At(i);
      m_number_of_tracks = m_tree_cluster_ntracks.At(i);
      m_z0_stddev = m_tree_cluster_z0_stddev.At(i);
      m_z0_mean = m_tree_cluster_z0_mean.At(i);
      m_closest_vx_is_pvx = m_tree_cluster_closest_vx_is_pvx.At(i);
      m_distance_pu_vx = m_tree_cluster_distance_pu_vx.At(i);
      m_n_pu_vertices = m_tree_cluster_n_pu_vertices.At(i);
      m_pt_mod = m_tree_cluster_pt_mod.At(i);

      float bdt_output = tmva_reader->EvaluateMVA("BDT");

      if (bdt_output > min_bdt_cut_val and bdt_output > max_bdt_value and
          m_number_of_tracks >= 3) {
        // cout << "found good cluster at " << i << " with BDT op " <<
        // bdt_output << '\n';
        max_bdt_value = bdt_output;
        cluster_position = i;
        cluster_accepted = true;
      }
    } // LOOP over clusters in event
    ////////////////// //!!!!!! NOTE TRUTH !!!!!!
    ////////////////// if (cluster_accepted) {
    //////////////////   if
    ///(m_tree_cluster_hs_fraction.At(cluster_position)<0.5) {
    //////////////////     cluster_accepted = truth_cluster_position >= 0;
    //////////////////     cluster_position = truth_cluster_position;
    //////////////////   }
    ////////////////// }

    // truth study, check how eff drops in events where no cluster found
    if (cluster_accepted) {
      fillRpTEffCurves(truth_t0_reco_trk_rpt,
                       m_eff_trutht0_recotrktime_rpt_75eff, category, pt);
      fillRpTEffCurves(smeared_truth_t0_reco_trk_rpt,
                       m_eff_smearedtrutht0_recotrktime_rpt_75eff, category,
                       pt);
    } else {
      fillRpTEffCurves(std_rpt, m_eff_trutht0_recotrktime_rpt_75eff, category,
                       pt);
      fillRpTEffCurves(std_rpt, m_eff_smearedtrutht0_recotrktime_rpt_75eff,
                       category, pt);
    }

    ////////////////////////////////////////
    // cout << "and into the histos\n";
    if (cluster_accepted) {

      // cout << "curr i " << cluster_position << '\n';
      // cout << "max bdt val " << max_bdt_value << '\n';
      double bdt_selected_cluster_rpt =
          m_tree_jet_hgtd_rpt_cluster_times.At(cluster_position);
      fillRpTEffCurves(bdt_selected_cluster_rpt, m_eff_BDTClusterOrStdRpT,
                       category, pt);
      fillRpTEffCurves(bdt_selected_cluster_rpt,
                       m_eff_BDTClusterOrStdRpT_variablebin, category, pt);

      fillRpTEffCurves(bdt_selected_cluster_rpt, m_eff_BDTClusterOrSubjetRpT,
                       category, pt);
      fillRpTEffCurves(bdt_selected_cluster_rpt,
                       m_eff_BDTClusterOrSubjetRpT_variablebin, category, pt);
      fillRpTEffCurves(bdt_selected_cluster_rpt,
                       m_eff_loc_density_bdtrpt.at(vx_density_bin), category,
                       pt);

      // eta bins
      fillEtaPURej(bdt_selected_cluster_rpt, m_eff_hs_bdtrpt_low_pt,
                   m_eff_hs_bdtrpt_high_pt, m_eff_pu_bdtrpt_low_pt,
                   m_eff_pu_bdtrpt_high_pt, category, pt, abs_eta);

      fillRpTEffCurvesCombined(bdt_selected_cluster_rpt,
                               m_eff_BDTClusterOrSubjetRpTAllPt, category);

      /// check what happens when the BDT accepts something but it is not >50%
      /// HS
      if (truth_cluster_position < 0) {
        fillRpTEffCurves(bdt_selected_cluster_rpt, m_eff_no_perfect_butbdt,
                         category, pt);
        fillRpTEffCurves(std_rpt, m_eff_no_perfect_butbdt_stdcomparison,
                         category, pt);
      }

      if (truth_cluster_position >= 0) {
        fillRpTEffCurves(bdt_selected_cluster_rpt, m_eff_yes_perfect_butbdt,
                         category, pt);
        fillRpTEffCurves(std_rpt, m_eff_yes_perfect_butbdt_stdcomparison,
                         category, pt);
        fillRpTEffCurves(btc_rpt, m_eff_yes_perfect_butbdt_perfectcomparison,
                         category, pt);
      }

      if (ispu and bdt_selected_cluster_rpt > 0.17) {
        if (pt < 50.) {
          m_hist_jeteta_btrpt->Fill(abs_eta);
          m_hist_jeteta_btrpt_1bin->Fill(abs_eta);
        }
        if (pt > 50. and bdt_selected_cluster_rpt > 0.23) {
          m_hist_jeteta_btrpt_highpt->Fill(abs_eta);
        }
      }

      // split in types
      if (cluster_position == truth_cluster_position) {
        fillRpTEffCurves(std_rpt, m_eff_std_rpt_type1, category, pt);
        fillRpTEffCurves(bdt_selected_cluster_rpt, m_eff_bdt_rpt_type1,
                         category, pt);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) > 0.5) {
        fillRpTEffCurves(std_rpt, m_eff_std_rpt_type2, category, pt);
        fillRpTEffCurves(bdt_selected_cluster_rpt, m_eff_bdt_rpt_type2,
                         category, pt);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) == 0.5) {
        fillRpTEffCurves(std_rpt, m_eff_std_rpt_type3, category, pt);
        fillRpTEffCurves(bdt_selected_cluster_rpt, m_eff_bdt_rpt_type3,
                         category, pt);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) < 0.5 and
                 m_tree_cluster_hs_fraction.At(cluster_position) > 0.0) {
        fillRpTEffCurves(std_rpt, m_eff_std_rpt_type4, category, pt);
        fillRpTEffCurves(bdt_selected_cluster_rpt, m_eff_bdt_rpt_type4,
                         category, pt);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) == 0.0) {
        fillRpTEffCurves(std_rpt, m_eff_std_rpt_type5, category, pt);
        fillRpTEffCurves(bdt_selected_cluster_rpt, m_eff_bdt_rpt_type5,
                         category, pt);
      }
    } else {
      fillRpTEffCurves(std_rpt, m_eff_BDTClusterOrStdRpT, category, pt);
      fillRpTEffCurves(std_rpt, m_eff_BDTClusterOrStdRpT_variablebin, category,
                       pt);
      fillRpTEffCurves(subjetRpT, m_eff_BDTClusterOrSubjetRpT, category, pt);
      fillRpTEffCurves(subjetRpT, m_eff_BDTClusterOrSubjetRpT_variablebin,
                       category, pt);
      fillRpTEffCurves(subjetRpT, m_eff_loc_density_bdtrpt.at(vx_density_bin),
                       category, pt);

      // eta bins
      fillEtaPURej(subjetRpT, m_eff_hs_bdtrpt_low_pt, m_eff_hs_bdtrpt_high_pt,
                   m_eff_pu_bdtrpt_low_pt, m_eff_pu_bdtrpt_high_pt, category,
                   pt, abs_eta);

      fillRpTEffCurvesCombined(subjetRpT, m_eff_BDTClusterOrSubjetRpTAllPt,
                               category);

      if (ispu and subjetRpT > 0.19) {
        m_hist_jeteta_btrpt->Fill(abs_eta);
        m_hist_jeteta_btrpt_1bin->Fill(abs_eta);
        if (pt > 50.) {
          m_hist_jeteta_btrpt_highpt->Fill(abs_eta);
        }
      }
    }

    // cout << "and into the histos hs/pu\n";
    if (ishs) {
      hist_hs_pt->Fill(pt);
      hist_hs_eta->Fill(abs_eta);
      hist_rpt_hs->Fill(std_rpt);
      hist_hsct0rpt_hs->Fill(btc_rpt);
      if (btc_rpt > 0) {
        hist_deltaRpT_hs->Fill(std_rpt - btc_rpt);
      }
    } else if (ispu) {
      hist_pu_pt->Fill(pt);
      hist_pu_eta->Fill(abs_eta);
      hist_rpt_pu->Fill(std_rpt);
      hist_hsct0rpt_pu->Fill(btc_rpt);
      if (btc_rpt > 0) {
        hist_deltaRpT_pu->Fill(std_rpt - btc_rpt);
      }
    }
    // cout << "and into the histos end\n";

  } // LOOP over jets

  auto rootfile = TFile::Open("controlplots.root", "RECREATE");
  for (const auto &el : write) {
    el->Write();
  }
  rootfile->Close();
  cout << "writing controlplots.root\n";
}

#include "getTChain.cxx"


void rptNtupleReader() {

  std::vector <TString> list_of_files = getListOfFiles("INPUT.txt", -1); //this is defined in GetTChain.h
  if (list_of_files.size() == 0) {
    cout << "ERROR: getListOfFiles returned no files to be read" << endl;
    return 1;
  }
  TChain * g_file = getChain(list_of_files, "JetTree", -1);
  TTreeReader reader(g_file);

  TTreeReaderValue<int> m_tree_eventnumber(reader, "m_tree_eventnumber");
  TTreeReaderValue<double> m_tree_truth_t0(reader, "m_tree_truth_t0");
  TTreeReaderValue<double> m_tree_param_vx_density(reader, "m_tree_param_vx_density");
  TTreeReaderValue<double> m_tree_local_vx_density(reader, "m_tree_local_vx_density");
  TTreeReaderValue<double> m_tree_local_truthvx_density(reader, "m_tree_local_truthvx_density");
  TTreeReaderValue<double> m_tree_deltaz_vx_truthvx(reader, "m_tree_deltaz_vx_truthvx");

  TTreeReaderValue<int> m_tree_n_hgtd_tracks(reader, "m_tree_n_hgtd_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks(reader, "m_tree_n_hgtd_hs_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_time(reader, "m_tree_n_hgtd_hs_tracks_with_time");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_good_time(reader, "m_tree_n_hgtd_hs_tracks_with_good_time");

  TTreeReaderArray<double> m_tree_cluster_time(reader, "m_tree_cluster_time");
  TTreeReaderArray<double> m_tree_cluster_time_res(reader, "m_tree_cluster_time_res");
  TTreeReaderArray<double> m_tree_cluster_z0(reader, "m_tree_cluster_z0");
  TTreeReaderArray<double> m_tree_cluster_z0_sigma(reader, "m_tree_cluster_z0_sigma");
  TTreeReaderArray<double> m_tree_cluster_q_over_p(reader, "m_tree_cluster_q_over_p");
  TTreeReaderArray<double> m_tree_cluster_q_over_p_sigma(reader, "m_tree_cluster_q_over_p_sigma");
  TTreeReaderArray<double> m_tree_cluster_d0(reader, "m_tree_cluster_d0");
  TTreeReaderArray<double> m_tree_cluster_d0_sigma(reader, "m_tree_cluster_d0_sigma");
  TTreeReaderArray<double> m_tree_cluster_hs_fraction(reader, "m_tree_cluster_hs_fraction");
  TTreeReaderArray<double> m_tree_cluster_delta_z(reader, "m_tree_cluster_delta_z");
  TTreeReaderArray<double> m_tree_cluster_delta_z_resunit(reader, "m_tree_cluster_delta_z_resunit");
  TTreeReaderArray<double> m_tree_cluster_sumpt2(reader, "m_tree_cluster_sumpt2");
  TTreeReaderArray<double> m_tree_cluster_ntracks(reader, "m_tree_cluster_ntracks");

  TTreeReaderValue<int> m_tree_jet_category(reader, "m_tree_jet_category");
  TTreeReaderValue<double> m_tree_jet_pt(reader, "m_tree_jet_pt");
  TTreeReaderValue<double> m_tree_jet_eta(reader, "m_tree_jet_eta");
  TTreeReaderValue<double> m_tree_jet_rpt(reader, "m_tree_jet_rpt");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_truth_time(reader, "m_tree_jet_hgtd_rpt_trutht0_truth_time");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_reco_time(reader, "m_tree_jet_hgtd_rpt_trutht0_reco_time");
  TTreeReaderValue<double> m_tree_jet_subjet_rpt(reader, "m_tree_jet_subjet_rpt");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times_truth_trk(reader, "m_tree_cluster_ntracks");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times(reader, "m_tree_jet_hgtd_rpt_cluster_times");

  while (reader.Next()) {
    int event_number = *m_tree_eventnumber;
    cout << event_number << '\n';
  }
}

#include "getTChain.cxx"

void prepareTreeForBDT() {

  int n_files_for_training = -1;
  // int n_files_for_training = 30; // with new vars files, PFTQv05

  std::vector<TString> list_of_files = getListOfFiles(
      "INPUT.txt", n_files_for_training); // this is defined in GetTChain.h
  if (list_of_files.size() == 0) {
    cout << "ERROR: getListOfFiles returned no files to be read" << endl;
    return;
  }

  // int n_files_to_skip = 90;
  // int end_pos_for_erase = list_of_files.size() - n_files_to_skip;
  // list_of_files.erase(list_of_files.begin(),
  // list_of_files.end()-end_pos_for_erase);

  TChain *g_file = getChain(list_of_files, "JetTree", n_files_for_training);

  TTreeReader reader(g_file);

  TTreeReaderValue<int> m_tree_eventnumber(reader, "m_tree_eventnumber");
  // TTreeReaderValue<double> m_tree_truth_t0(reader, "m_tree_truth_t0");
  // TTreeReaderValue<double> m_tree_param_vx_density(reader,
  // "m_tree_param_vx_density"); TTreeReaderValue<double>
  // m_tree_local_vx_density(reader, "m_tree_local_vx_density");
  // TTreeReaderValue<double> m_tree_local_truthvx_density(reader,
  // "m_tree_local_truthvx_density"); TTreeReaderValue<double>
  // m_tree_deltaz_vx_truthvx(reader, "m_tree_deltaz_vx_truthvx");

  // TTreeReaderValue<int> m_tree_n_hgtd_tracks(reader, "m_tree_n_hgtd_tracks");
  // TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks(reader,
  // "m_tree_n_hgtd_hs_tracks"); TTreeReaderValue<int>
  // m_tree_n_hgtd_hs_tracks_with_time(reader,
  // "m_tree_n_hgtd_hs_tracks_with_time"); TTreeReaderValue<int>
  // m_tree_n_hgtd_hs_tracks_with_good_time(reader,
  // "m_tree_n_hgtd_hs_tracks_with_good_time");

  TTreeReaderArray<double> m_tree_cluster_time(reader, "m_tree_cluster_time");
  TTreeReaderArray<double> m_tree_cluster_time_res(reader,
                                                   "m_tree_cluster_time_res");
  TTreeReaderArray<double> m_tree_cluster_z0(reader, "m_tree_cluster_z0");
  TTreeReaderArray<double> m_tree_cluster_z0_sigma(reader,
                                                   "m_tree_cluster_z0_sigma");
  TTreeReaderArray<double> m_tree_cluster_q_over_p(reader,
                                                   "m_tree_cluster_q_over_p");
  TTreeReaderArray<double> m_tree_cluster_q_over_p_sigma(
      reader, "m_tree_cluster_q_over_p_sigma");
  TTreeReaderArray<double> m_tree_cluster_d0(reader, "m_tree_cluster_d0");
  TTreeReaderArray<double> m_tree_cluster_d0_sigma(reader,
                                                   "m_tree_cluster_d0_sigma");
  TTreeReaderArray<double> m_tree_cluster_hs_fraction(
      reader, "m_tree_cluster_hs_fraction");
  TTreeReaderArray<double> m_tree_cluster_delta_z(reader,
                                                  "m_tree_cluster_delta_z");
  TTreeReaderArray<double> m_tree_cluster_delta_z_resunit(
      reader, "m_tree_cluster_delta_z_resunit");
  TTreeReaderArray<double> m_tree_cluster_sumpt2(reader,
                                                 "m_tree_cluster_sumpt2");
  TTreeReaderArray<double> m_tree_cluster_ntracks(reader,
                                                  "m_tree_cluster_ntracks");
  TTreeReaderArray<double> m_tree_cluster_z0_stddev(reader,
                                                    "m_tree_cluster_z0_stddev");
  TTreeReaderArray<double> m_tree_cluster_z0_mean(reader,
                                                  "m_tree_cluster_z0_mean");
  TTreeReaderArray<double> m_tree_cluster_closest_vx_is_pvx(
      reader, "m_tree_cluster_closest_vx_is_pvx");
  TTreeReaderArray<double> m_tree_cluster_distance_pu_vx(
      reader, "m_tree_cluster_distance_pu_vx");
  TTreeReaderArray<double> m_tree_cluster_n_pu_vertices(
      reader, "m_tree_cluster_n_pu_vertices");
  TTreeReaderArray<double> m_tree_cluster_pt_mod(reader,
                                                 "m_tree_cluster_pt_mod");

  // TTreeReaderValue<int> m_tree_jet_category(reader, "m_tree_jet_category");
  // TTreeReaderValue<double> m_tree_jet_pt(reader, "m_tree_jet_pt");
  // TTreeReaderValue<double> m_tree_jet_eta(reader, "m_tree_jet_eta");
  // TTreeReaderValue<double> m_tree_jet_rpt(reader, "m_tree_jet_rpt");
  // TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_truth_time(reader,
  // "m_tree_jet_hgtd_rpt_trutht0_truth_time"); TTreeReaderValue<double>
  // m_tree_jet_hgtd_rpt_trutht0_reco_time(reader,
  // "m_tree_jet_hgtd_rpt_trutht0_reco_time"); TTreeReaderValue<double>
  // m_tree_jet_subjet_rpt(reader, "m_tree_jet_subjet_rpt");
  // TTreeReaderArray<double>
  // m_tree_jet_hgtd_rpt_cluster_times_truth_trk(reader,
  // "m_tree_cluster_ntracks"); TTreeReaderArray<double>
  // m_tree_jet_hgtd_rpt_cluster_times(reader,
  // "m_tree_jet_hgtd_rpt_cluster_times");

  TTree *m_signal_tree = nullptr;
  TTree *m_background_tree = nullptr;

  double m_hs_fraction;
  int m_is_hs_cluster;
  // double m_delta_t;
  double m_delta_z;
  double m_z_sigma;
  double m_q_over_p;
  double m_q_over_p_sigma;
  double m_d0;
  double m_d0_sigma;
  double m_delta_z_resunits;
  double m_cluster_sumpt2;
  double m_number_of_tracks;
  double m_z0_stddev;
  double m_z0_mean;
  double m_closest_vx_is_pvx;
  double m_distance_pu_vx;
  double m_n_pu_vertices;
  double m_pt_mod;

  // set up the output trees
  auto rootfile = TFile::Open("prepareTreeForBDT.root", "RECREATE");

  m_signal_tree = new TTree("SignalTree", "SignalTree");
  // m_signal_tree->Branch("m_hs_fraction", &m_hs_fraction);
  // m_signal_tree->Branch("m_delta_t", &m_delta_t);
  m_signal_tree->Branch("m_delta_z", &m_delta_z);
  m_signal_tree->Branch("m_z_sigma", &m_z_sigma);
  m_signal_tree->Branch("m_q_over_p", &m_q_over_p);
  m_signal_tree->Branch("m_q_over_p_sigma", &m_q_over_p_sigma);
  m_signal_tree->Branch("m_d0", &m_d0);
  m_signal_tree->Branch("m_d0_sigma", &m_d0_sigma);
  m_signal_tree->Branch("m_delta_z_resunits", &m_delta_z_resunits);
  m_signal_tree->Branch("m_cluster_sumpt2", &m_cluster_sumpt2);
  m_signal_tree->Branch("m_number_of_tracks", &m_number_of_tracks);
  m_signal_tree->Branch("m_z0_stddev", &m_z0_stddev);
  m_signal_tree->Branch("m_z0_mean", &m_z0_mean);
  m_signal_tree->Branch("m_closest_vx_is_pvx", &m_closest_vx_is_pvx);
  m_signal_tree->Branch("m_distance_pu_vx", &m_distance_pu_vx);
  m_signal_tree->Branch("m_n_pu_vertices", &m_n_pu_vertices);
  m_signal_tree->Branch("m_pt_mod", &m_pt_mod);

  m_background_tree = new TTree("BackgroundTree", "BackgroundTree");
  // m_background_tree->Branch("m_hs_fraction", &m_hs_fraction);
  // m_background_tree->Branch("m_delta_t", &m_delta_t);
  m_background_tree->Branch("m_delta_z", &m_delta_z);
  m_background_tree->Branch("m_z_sigma", &m_z_sigma);
  m_background_tree->Branch("m_q_over_p", &m_q_over_p);
  m_background_tree->Branch("m_q_over_p_sigma", &m_q_over_p_sigma);
  m_background_tree->Branch("m_d0", &m_d0);
  m_background_tree->Branch("m_d0_sigma", &m_d0_sigma);
  m_background_tree->Branch("m_delta_z_resunits", &m_delta_z_resunits);
  m_background_tree->Branch("m_cluster_sumpt2", &m_cluster_sumpt2);
  m_background_tree->Branch("m_number_of_tracks", &m_number_of_tracks);
  m_background_tree->Branch("m_z0_stddev", &m_z0_stddev);
  m_background_tree->Branch("m_z0_mean", &m_z0_mean);
  m_background_tree->Branch("m_closest_vx_is_pvx", &m_closest_vx_is_pvx);
  m_background_tree->Branch("m_distance_pu_vx", &m_distance_pu_vx);
  m_background_tree->Branch("m_n_pu_vertices", &m_n_pu_vertices);
  m_background_tree->Branch("m_pt_mod", &m_pt_mod);

  int current_eventnumber = -1;
  while (reader.Next()) {
    // get the clusters only once per event
    int event_number = *m_tree_eventnumber;
    if (event_number == current_eventnumber) {
      continue;
    }
    current_eventnumber = event_number;
    // cout << "EVNT " << current_eventnumber << '\n';
    /////////

    // write out info for each cluster
    for (int i = 0; i < m_tree_cluster_hs_fraction.GetSize(); i++) {
      // cout << "frac " << m_tree_cluster_hs_fraction.At(i) << '\n';
      // cout << "time " << m_tree_cluster_time.At(i) << '\n';
      // cout << "dz " << m_tree_cluster_delta_z.At(i) << '\n';
      // cout << "qp " << m_tree_cluster_q_over_p.At(i) << '\n';
      // cout << "n " << m_tree_cluster_ntracks.At(i) << '\n';
      m_delta_z = m_tree_cluster_delta_z.At(i);
      m_z_sigma = m_tree_cluster_z0_sigma.At(i);
      m_q_over_p = m_tree_cluster_q_over_p.At(i);
      m_q_over_p_sigma = m_tree_cluster_q_over_p_sigma.At(i);
      m_d0 = m_tree_cluster_d0.At(i);
      m_d0_sigma = m_tree_cluster_d0_sigma.At(i);
      m_delta_z_resunits = m_tree_cluster_delta_z_resunit.At(i);
      m_cluster_sumpt2 = m_tree_cluster_sumpt2.At(i);
      m_number_of_tracks = m_tree_cluster_ntracks.At(i);
      m_z0_stddev = m_tree_cluster_z0_stddev.At(i);
      m_z0_mean = m_tree_cluster_z0_mean.At(i);
      m_closest_vx_is_pvx = m_tree_cluster_closest_vx_is_pvx.At(i);
      m_distance_pu_vx = m_tree_cluster_distance_pu_vx.At(i);
      m_n_pu_vertices = m_tree_cluster_n_pu_vertices.At(i);
      m_pt_mod = m_tree_cluster_pt_mod.At(i);

      // if(m_number_of_tracks<3) {continue;}

      if (m_tree_cluster_hs_fraction.At(i) > 0.5) {
        // signal
        // cout << "signal" << '\n';
        m_signal_tree->Fill();
      } else {
        // background
        // cout << "background" << '\n';
        m_background_tree->Fill();
      }
    } // LOOP over clusters in event
  }   // LOOP over ntuple

  // m_signal_tree->Write();
  // m_background_tree->Write();
  rootfile->Write();
  rootfile->Close();

  cout << "written to prepareTreeForBDT.root\n";
}

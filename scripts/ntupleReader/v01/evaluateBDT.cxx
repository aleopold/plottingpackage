
#include "getTChain.cxx"

struct EventData {
  int eventnumber = -1;
  int n_tracks = 0;
  int n_hs_tracks = 0;
  int n_hs_tracks_w_time = 0;
  int n_hs_tracks_w_good_time = 0;

  float local_vx_density;

  // jets
  int n_fwd_pu_jets = 0;
  int n_fwd_hs_jets = 0;
  int n_fwd_hard_pu_jets = 0;
  int n_fwd_hard_hs_jets = 0;
};

void evaluateBDT() {

  TMVA::Tools::Instance();

  TMVA::Reader *tmva_reader = new TMVA::Reader("!Color:!Silent");

  float m_delta_z;
  float m_z_sigma;
  float m_q_over_p;
  float m_q_over_p_sigma;
  float m_d0;
  float m_d0_sigma;
  float m_delta_z_resunits;
  float m_cluster_sumpt2;
  float m_number_of_tracks;
  float m_z0_stddev;
  float m_z0_mean;
  float m_closest_vx_is_pvx;
  float m_distance_pu_vx;
  float m_n_pu_vertices;
  float m_pt_mod;

  tmva_reader->AddVariable("m_delta_z", &m_delta_z);
  tmva_reader->AddVariable("m_z_sigma", &m_z_sigma);
  tmva_reader->AddVariable("m_q_over_p", &m_q_over_p);
  tmva_reader->AddVariable("m_q_over_p_sigma", &m_q_over_p_sigma);
  tmva_reader->AddVariable("m_delta_z_resunits", &m_delta_z_resunits);
  tmva_reader->AddVariable("m_cluster_sumpt2", &m_cluster_sumpt2);
  tmva_reader->AddVariable("m_d0", &m_d0);
  tmva_reader->AddVariable("m_d0_sigma", &m_d0_sigma);
  // tmva_reader->AddVariable("m_number_of_tracks", &m_number_of_tracks );
  // tmva_reader->AddVariable("m_z0_stddev", &m_z0_stddev );
  // tmva_reader->AddVariable("m_closest_vx_is_pvx", &m_closest_vx_is_pvx );
  // tmva_reader->AddVariable("m_distance_pu_vx", &m_distance_pu_vx );
  // tmva_reader->AddVariable("m_n_pu_vertices", &m_n_pu_vertices );
  // tmva_reader->AddVariable("m_pt_mod", &m_pt_mod );

  TString weightfile = "dataset/weights/TMVAClassification_BDT.weights.xml";
  tmva_reader->BookMVA("BDT", weightfile);

  // TString path =
  // "/afs/cern.ch/work/a/aleopold/private/hgtd/analysis_pkg/run/PileupRejectionNtuple_20k.root";
  // auto g_file = TFile::Open(path, "READ");
  // TTreeReader reader("JetTree", g_file);

  std::vector<TString> list_of_files = getListOfFiles("INPUT.txt", -1);
  // int n_files_to_skip = 40;
  // int n_files_to_skip = 198; // with new vars files, PFTQv05
  int n_files_to_skip = 38; // 175 total,PFTQv04_PFTQv04
  int start_pos_for_erase = list_of_files.size() - n_files_to_skip;
  list_of_files.erase(list_of_files.begin() + start_pos_for_erase,
                      list_of_files.end());
  if (list_of_files.size() == 0) {
    cout << "ERROR: getListOfFiles returned no files to be read" << endl;
    return;
  }
  cout << list_of_files.size() << " files left for evaluation\n";
  TChain *g_file = getChain(list_of_files, "JetTree", -1);
  TTreeReader reader(g_file);

  TTreeReaderValue<int> m_tree_eventnumber(reader, "m_tree_eventnumber");
  TTreeReaderValue<double> m_tree_truth_t0(reader, "m_tree_truth_t0");
  TTreeReaderValue<double> m_tree_param_vx_density(reader,
                                                   "m_tree_param_vx_density");
  TTreeReaderValue<double> m_tree_local_vx_density(reader,
                                                   "m_tree_local_vx_density");
  TTreeReaderValue<double> m_tree_local_truthvx_density(
      reader, "m_tree_local_truthvx_density");
  TTreeReaderValue<double> m_tree_deltaz_vx_truthvx(reader,
                                                    "m_tree_deltaz_vx_truthvx");
  //
  TTreeReaderValue<int> m_tree_n_hgtd_tracks(reader, "m_tree_n_hgtd_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks(reader,
                                                "m_tree_n_hgtd_hs_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_time(
      reader, "m_tree_n_hgtd_hs_tracks_with_time");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_good_time(
      reader, "m_tree_n_hgtd_hs_tracks_with_good_time");

  TTreeReaderArray<double> m_tree_cluster_time(reader, "m_tree_cluster_time");
  TTreeReaderArray<double> m_tree_cluster_time_res(reader,
                                                   "m_tree_cluster_time_res");
  TTreeReaderArray<double> m_tree_cluster_z0(reader, "m_tree_cluster_z0");
  TTreeReaderArray<double> m_tree_cluster_z0_sigma(reader,
                                                   "m_tree_cluster_z0_sigma");
  TTreeReaderArray<double> m_tree_cluster_q_over_p(reader,
                                                   "m_tree_cluster_q_over_p");
  TTreeReaderArray<double> m_tree_cluster_q_over_p_sigma(
      reader, "m_tree_cluster_q_over_p_sigma");
  TTreeReaderArray<double> m_tree_cluster_d0(reader, "m_tree_cluster_d0");
  TTreeReaderArray<double> m_tree_cluster_d0_sigma(reader,
                                                   "m_tree_cluster_d0_sigma");
  TTreeReaderArray<double> m_tree_cluster_hs_fraction(
      reader, "m_tree_cluster_hs_fraction");
  TTreeReaderArray<double> m_tree_cluster_delta_z(reader,
                                                  "m_tree_cluster_delta_z");
  TTreeReaderArray<double> m_tree_cluster_delta_z_resunit(
      reader, "m_tree_cluster_delta_z_resunit");
  TTreeReaderArray<double> m_tree_cluster_sumpt2(reader,
                                                 "m_tree_cluster_sumpt2");
  TTreeReaderArray<double> m_tree_cluster_ntracks(reader,
                                                  "m_tree_cluster_ntracks");
  // TTreeReaderArray<double> m_tree_cluster_z0_stddev(reader,
  // "m_tree_cluster_z0_stddev"); TTreeReaderArray<double>
  // m_tree_cluster_z0_mean(reader, "m_tree_cluster_z0_mean");
  // TTreeReaderArray<double> m_tree_cluster_closest_vx_is_pvx(reader,
  // "m_tree_cluster_closest_vx_is_pvx"); TTreeReaderArray<double>
  // m_tree_cluster_distance_pu_vx(reader, "m_tree_cluster_distance_pu_vx");
  // TTreeReaderArray<double> m_tree_cluster_n_pu_vertices(reader,
  // "m_tree_cluster_n_pu_vertices"); TTreeReaderArray<double>
  // m_tree_cluster_pt_mod(reader, "m_tree_cluster_pt_mod");

  TTreeReaderValue<int> m_tree_jet_category(reader, "m_tree_jet_category");
  TTreeReaderValue<double> m_tree_jet_pt(reader, "m_tree_jet_pt");
  TTreeReaderValue<double> m_tree_jet_eta(reader, "m_tree_jet_eta");
  TTreeReaderValue<double> m_tree_jet_rpt(reader, "m_tree_jet_rpt");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_truth_time(
      reader, "m_tree_jet_hgtd_rpt_trutht0_truth_time");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_reco_time(
      reader, "m_tree_jet_hgtd_rpt_trutht0_reco_time");
  TTreeReaderValue<double> m_tree_jet_subjet_rpt(reader,
                                                 "m_tree_jet_subjet_rpt");
  // TTreeReaderArray<double>
  // m_tree_jet_hgtd_rpt_cluster_times_truth_trk(reader,
  // "m_tree_cluster_ntracks");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times(
      reader, "m_tree_jet_hgtd_rpt_cluster_times");

  /////////////////////////////////////////////
  ///////////// output histograms /////////////
  std::vector<TObject *> write;

  auto hist_bdt_out_sig =
      new TH1F("hist_bdt_out_sig", ";BDT output;a.u.", 40, -1, 1);
  write.push_back(hist_bdt_out_sig);
  auto hist_bdt_out_bg =
      new TH1F("hist_bdt_out_bg", ";BDT output;a.u.", 40, -1, 1);
  write.push_back(hist_bdt_out_bg);

  vector<double> bdt_cutvalues;
  vector<TH1F *> hist_res_bdt_selected_t0_vec;
  vector<TH1F *> hist_has_time_bdt_selected_t0_vec;
  for (int i = 0; i < 20; i++) {
    double cutval = 0.025 * i;
    bdt_cutvalues.push_back(cutval);
    auto hist_res_bdt_selected_t0 =
        new TH1F(Form("hist_res_bdt_selected_t0_%.3f", cutval),
                 ";t_reco - t_truth;a.u.", 200, -0.4, 0.4);
    write.push_back(hist_res_bdt_selected_t0);
    hist_res_bdt_selected_t0_vec.push_back(hist_res_bdt_selected_t0);
    auto hist_has_time_bdt_selected_t0 =
        new TH1F(Form("hist_has_time_bdt_selected_t0_%.3f", cutval),
                 ";has time;a.u.", 2, 0, 2);
    write.push_back(hist_has_time_bdt_selected_t0);
    hist_has_time_bdt_selected_t0_vec.push_back(hist_has_time_bdt_selected_t0);
  }

  auto hist_res_bdt_selected_eventt0 =
      new TH1F("hist_res_bdt_selected_eventt0", ";t_reco - t_truth; a.u.", 200,
               -0.4, 0.4);
  write.push_back(hist_res_bdt_selected_eventt0);
  auto hist_has_time_bdt_selected_eventt0 = new TH1F(
      "hist_has_time_bdt_selected_eventt0", ";has time; a.u.", 2, 0, 2);
  write.push_back(hist_has_time_bdt_selected_eventt0);

  // what does the BDT compared to the "best" truth selection
  auto hist_same_cluster =
      new TH1F("hist_same_cluster", ";found same cluster;a.u.", 2, 0, 2);
  write.push_back(hist_same_cluster);
  // what is the HS fraction in the chsoen cluster?
  auto hist_hs_fraction_chosen_cluster = new TH1F(
      "hist_hs_fraction_chosen_cluster", ";HS track fraction;a.u.", 20, 0, 1);
  write.push_back(hist_hs_fraction_chosen_cluster);
  // what kind of cluster is chosen?
  // auto hist_chosen_cluster_type = new TH1F("hist_chosen_cluster_type",
  // ";cluster type;a.u.", 6, 0, 6);
  // hist_chosen_cluster_type->GetXaxis()->SetBinLabel(1,"Best HS cluster");
  // hist_chosen_cluster_type->GetXaxis()->SetBinLabel(2,">50% HS cluster");
  // hist_chosen_cluster_type->GetXaxis()->SetBinLabel(3,"=50% HS cluster");
  // hist_chosen_cluster_type->GetXaxis()->SetBinLabel(4,"<50% HS cluster");
  // hist_chosen_cluster_type->GetXaxis()->SetBinLabel(5,"PU cluster");
  // hist_chosen_cluster_type->GetXaxis()->SetBinLabel(6,"no cluster");
  // write.push_back(hist_chosen_cluster_type);

  auto hist_chosen_cluster_type =
      new TH1F("hist_chosen_cluster_type", ";cluster type;a.u.", 4, 0, 4);
  hist_chosen_cluster_type->GetXaxis()->SetBinLabel(1, "#geq 50% HS tracks");
  hist_chosen_cluster_type->GetXaxis()->SetBinLabel(2, "<50% HS tracks");
  hist_chosen_cluster_type->GetXaxis()->SetBinLabel(3, "PU tracks");
  hist_chosen_cluster_type->GetXaxis()->SetBinLabel(4, "no cluster chosen");
  write.push_back(hist_chosen_cluster_type);

  auto hist_chosen_cluster_type_detail = new TH1F(
      "hist_chosen_cluster_type_detail", ";cluster type;a.u.", 5, 0, 5);
  hist_chosen_cluster_type_detail->GetXaxis()->SetBinLabel(
      1, "#geq 50% HS tracks");
  hist_chosen_cluster_type_detail->GetXaxis()->SetBinLabel(2, "<50% HS tracks");
  hist_chosen_cluster_type_detail->GetXaxis()->SetBinLabel(3, "PU tracks");
  hist_chosen_cluster_type_detail->GetXaxis()->SetBinLabel(
      4, "no cluster chosen - bdt output did not pass cut");
  hist_chosen_cluster_type_detail->GetXaxis()->SetBinLabel(
      5, "no cluster chosen - not enough tracks");
  write.push_back(hist_chosen_cluster_type_detail);

  auto hist_chosen_cluster_typ_fwdPUjets = new TH1F(
      "hist_chosen_cluster_typ_fwdPUjets", ";cluster type;a.u.", 4, 0, 4);
  hist_chosen_cluster_typ_fwdPUjets->GetXaxis()->SetBinLabel(
      1, "#geq 50% HS tracks");
  hist_chosen_cluster_typ_fwdPUjets->GetXaxis()->SetBinLabel(2,
                                                             "<50% HS tracks");
  hist_chosen_cluster_typ_fwdPUjets->GetXaxis()->SetBinLabel(3, "PU tracks");
  hist_chosen_cluster_typ_fwdPUjets->GetXaxis()->SetBinLabel(
      4, "no cluster chosen");
  write.push_back(hist_chosen_cluster_typ_fwdPUjets);

  auto hist_res_type1 =
      new TH1F("hist_res_type1", ";t_reco - t_truth; a.u.", 200, -0.4, 0.4);
  write.push_back(hist_res_type1);
  auto hist_res_type2 =
      new TH1F("hist_res_type2", ";t_reco - t_truth; a.u.", 200, -0.4, 0.4);
  write.push_back(hist_res_type2);
  auto hist_res_type3 =
      new TH1F("hist_res_type3", ";t_reco - t_truth; a.u.", 200, -0.4, 0.4);
  write.push_back(hist_res_type3);
  auto hist_res_type4 =
      new TH1F("hist_res_type4", ";t_reco - t_truth; a.u.", 200, -0.4, 0.4);
  write.push_back(hist_res_type4);
  auto hist_res_type5 =
      new TH1F("hist_res_type5", ";t_reco - t_truth; a.u.", 200, -0.4, 0.4);
  write.push_back(hist_res_type5);

  auto hist_ntracks_type1 =
      new TH1F("hist_ntracks_type1", ";n tracks; a.u.", 20, 0, 20);
  write.push_back(hist_ntracks_type1);
  auto hist_ntracks_type2 =
      new TH1F("hist_ntracks_type2", ";n tracks; a.u.", 20, 0, 20);
  write.push_back(hist_ntracks_type2);
  auto hist_ntracks_type3 =
      new TH1F("hist_ntracks_type3", ";n tracks; a.u.", 20, 0, 20);
  write.push_back(hist_ntracks_type3);
  auto hist_ntracks_type4 =
      new TH1F("hist_ntracks_type4", ";n tracks; a.u.", 20, 0, 20);
  write.push_back(hist_ntracks_type4);
  auto hist_ntracks_type5 =
      new TH1F("hist_ntracks_type5", ";n tracks; a.u.", 20, 0, 20);
  write.push_back(hist_ntracks_type5);

  auto hist_no_perfect_butbdt =
      new TH1F("hist_no_perfect_butbdt", ";type; a.u.", 5, 0, 5);
  hist_no_perfect_butbdt->GetXaxis()->SetBinLabel(1, "Best HS cluster");
  hist_no_perfect_butbdt->GetXaxis()->SetBinLabel(2, ">50% HS cluster");
  hist_no_perfect_butbdt->GetXaxis()->SetBinLabel(3, "=50% HS cluster");
  hist_no_perfect_butbdt->GetXaxis()->SetBinLabel(4, "<50% HS cluster");
  hist_no_perfect_butbdt->GetXaxis()->SetBinLabel(5, "PU cluster");
  write.push_back(hist_no_perfect_butbdt);

  auto hist_compare_bdt_with_perfect_choice =
      new TH1F("hist_compare_bdt_with_perfect_choice", ";type; a.u.", 5, 0, 5);
  hist_compare_bdt_with_perfect_choice->GetXaxis()->SetBinLabel(
      1, "Best HS cluster");
  hist_compare_bdt_with_perfect_choice->GetXaxis()->SetBinLabel(
      2, ">50% HS cluster");
  hist_compare_bdt_with_perfect_choice->GetXaxis()->SetBinLabel(
      3, "=50% HS cluster");
  hist_compare_bdt_with_perfect_choice->GetXaxis()->SetBinLabel(
      4, "<50% HS cluster");
  hist_compare_bdt_with_perfect_choice->GetXaxis()->SetBinLabel(5,
                                                                "PU cluster");
  write.push_back(hist_compare_bdt_with_perfect_choice);

  // how often is there a perfect cluster and BDT uses non?
  auto hist_perfect_no_bdt = new TH1F(
      "hist_perfect_no_bdt", ";BDT accepts no perfect cluster; a.u.", 2, 0, 2);
  write.push_back(hist_perfect_no_bdt);

  auto hist_loc_pu_dens_2p0 =
      new TH1F("hist_loc_pu_dens_2p0", ";local PU density; a.u.", 20, 0, 6);
  write.push_back(hist_loc_pu_dens_2p0);
  auto hist_loc_pu_dens_0p1 =
      new TH1F("hist_loc_pu_dens_0p1", ";local PU density; a.u.", 20, 0, 6);
  write.push_back(hist_loc_pu_dens_0p1);
  auto hist_loc_truth_pu_dens_2p0 = new TH1F(
      "hist_loc_truth_pu_dens_2p0", ";local PU density; a.u.", 20, 0, 6);
  write.push_back(hist_loc_truth_pu_dens_2p0);
  auto hist_loc_truth_pu_dens_0p1 = new TH1F(
      "hist_loc_truth_pu_dens_0p1", ";local PU density; a.u.", 20, 0, 6);
  write.push_back(hist_loc_truth_pu_dens_0p1);

  // what happens in the events where the BDT chooses only PU tracks?
  auto hist_n_tracks_pu_cluster =
      new TH1F("hist_n_tracks_pu_cluster",
               ";n VX assoc. tracks in HGTD acceptance; a.u.", 40, 0, 40);
  write.push_back(hist_n_tracks_pu_cluster);
  auto hist_n_hstracks_pu_cluster =
      new TH1F("hist_n_hstracks_pu_cluster",
               ";n VX assoc. HS tracks in HGTD acceptance; a.u.", 40, 0, 40);
  write.push_back(hist_n_hstracks_pu_cluster);
  auto hist_n_hstracks_wtime_pu_cluster = new TH1F(
      "hist_n_hstracks_wtime_pu_cluster",
      ";n VX assoc. HS tracks w/ time in HGTD acceptance; a.u.", 40, 0, 40);
  write.push_back(hist_n_hstracks_wtime_pu_cluster);
  auto hist_n_hstracks_wgoodtime_pu_cluster =
      new TH1F("hist_n_hstracks_wgoodtime_pu_cluster",
               ";n VX assoc. HS tracks w/ good time in HGTD acceptance; a.u.",
               40, 0, 40);
  write.push_back(hist_n_hstracks_wgoodtime_pu_cluster);

  auto hist_n_tracks_hs_cluster =
      new TH1F("hist_n_tracks_hs_cluster",
               ";n VX assoc. tracks in HGTD acceptance; a.u.", 40, 0, 40);
  write.push_back(hist_n_tracks_hs_cluster);
  auto hist_n_hstracks_hs_cluster =
      new TH1F("hist_n_hstracks_hs_cluster",
               ";n VX assoc. HS tracks in HGTD acceptance; a.u.", 40, 0, 40);
  write.push_back(hist_n_hstracks_hs_cluster);
  auto hist_n_hstracks_wtime_hs_cluster = new TH1F(
      "hist_n_hstracks_wtime_hs_cluster",
      ";n VX assoc. HS tracks w/ time in HGTD acceptance; a.u.", 40, 0, 40);
  write.push_back(hist_n_hstracks_wtime_hs_cluster);
  auto hist_n_hstracks_wgoodtime_hs_cluster =
      new TH1F("hist_n_hstracks_wgoodtime_hs_cluster",
               ";n VX assoc. HS tracks w/ good time in HGTD acceptance; a.u.",
               40, 0, 40);
  write.push_back(hist_n_hstracks_wgoodtime_hs_cluster);

  // how many HS clusters do I have in events where a PU one was chosen?
  auto hist_n_goodclusters_pu_chosen = new TH1F(
      "hist_n_goodclusters_pu_chosen", ";n good clusters; a.u.", 10, 0, 10);
  write.push_back(hist_n_goodclusters_pu_chosen);
  auto hist_n_goodclusters_hs_chosen = new TH1F(
      "hist_n_goodclusters_hs_chosen", ";n good clusters; a.u.", 10, 0, 10);
  write.push_back(hist_n_goodclusters_hs_chosen);

  // hs tracks with time in events with

  auto hist2d_nhsjets_vs_npujets =
      new TH2D("hist2d_nhsjets_vs_npujets", ";n HS jets; n PU jets", 10, 0, 10,
               10, 0, 10);
  write.push_back(hist2d_nhsjets_vs_npujets);

  /////////////////////////////////////////////
  // extract per event information
  // struct EventData {
  //   int eventnumber;
  //   int n_tracks;
  //   int n_hs_tracks;
  //   int n_hs_tracks_w_time;
  //   int n_hs_tracks_w_good_time;
  //
  //   float local_vx_density;
  //
  //   //jets
  //   int n_fwd_pu_jets;
  //   int n_fwd_hs_jets;
  //   int n_fwd_hard_pu_jets;
  //   int n_fwd_hard_hs_jets;
  // };

  cout << "reading event data" << '\n';

  double jeteta_min = 2.4;
  double jeteta_max = 4.0;

  std::map<int, EventData> event_data;
  int curr_evnt_nr = -1;
  while (reader.Next()) {
    int event_number = *m_tree_eventnumber;
    // if (event_number == curr_evnt_nr) {continue;}
    // curr_evnt_nr = event_number;

    event_data[event_number].n_tracks = *m_tree_n_hgtd_tracks;

    // jet
    double eta = *m_tree_jet_eta;
    double abs_eta = fabs(eta);
    if (abs_eta < jeteta_min or abs_eta > jeteta_max) {
      continue;
    }

    double pt = *m_tree_jet_pt;
    int category = *m_tree_jet_category;
    bool ishs = (category == 0);
    bool ispu = (category > 0 and category < 4);

    if (pt < 30.) {
      continue;
    }
    if (ishs) {
      event_data[event_number].n_fwd_hs_jets++;
    } else if (ispu) {
      event_data[event_number].n_fwd_pu_jets++;
    }
    if (pt < 50.) {
      continue;
    }
    if (ishs) {
      event_data[event_number].n_fwd_hard_hs_jets++;
    } else if (ispu) {
      event_data[event_number].n_fwd_hard_pu_jets++;
    }

  } // LOOP over TChain

  // cout << "number of events: " << event_data.size() << '\n';
  // for (const auto& el : event_data) {
  //   cout << el.second.n_fwd_hard_pu_jets << '\n';
  // }
  //
  // return;

  reader.Restart();

  /////////////////////////////////////////////

  // double min_bdt_cut_val = 0.1;
  double min_bdt_cut_val = 0.2;

  cout << "starting loop" << '\n';

  int current_eventnumber = -1;
  while (reader.Next()) {
    // get the clusters only once per event
    int event_number = *m_tree_eventnumber;
    if (event_number == current_eventnumber) {
      continue;
    }
    current_eventnumber = event_number;

    double loc_vx_dens = *m_tree_local_vx_density;
    double loc_truthvx_dens = *m_tree_local_truthvx_density;

    hist_loc_pu_dens_2p0->Fill(loc_vx_dens);
    hist_loc_truth_pu_dens_2p0->Fill(loc_truthvx_dens);
    if (*m_tree_deltaz_vx_truthvx < 0.1) {
      hist_loc_pu_dens_0p1->Fill(loc_vx_dens);
      hist_loc_truth_pu_dens_0p1->Fill(loc_truthvx_dens);
    }

    int n_fwd_hard_hs_jets = event_data[event_number].n_fwd_hard_hs_jets;
    int n_fwd_hard_pu_jets = event_data[event_number].n_fwd_hard_pu_jets;

    if (not(n_fwd_hard_hs_jets == 0 and n_fwd_hard_pu_jets == 0)) {
      hist2d_nhsjets_vs_npujets->Fill(n_fwd_hard_hs_jets, n_fwd_hard_pu_jets);
    }

    double truth_t0 = *m_tree_truth_t0;

    int n_clusters = m_tree_cluster_hs_fraction.GetSize();

    // find the BDT choice
    bool cluster_accepted = false;
    int cluster_position = -1;
    double max_bdt_value = -1;
    // find the truth best cluster
    bool truth_cluster_accepted = false;
    int truth_cluster_position = -1;
    double truth_highest_hs_fraction = -1;
    bool no_cluster_passes_cut = true;
    bool no_cluster_has_3tracks = true;

    int n_good_clusters = 0;
    for (int i = 0; i < n_clusters; i++) {
      // write out info for each cluster, if bdt accepts it and what time it has
      vector<bool> accepted_cluster(bdt_cutvalues.size(), false);
      vector<double> accepted_time(bdt_cutvalues.size(), -1);
      vector<double> accepted_bdt_output(bdt_cutvalues.size(), -1);
      m_delta_z = m_tree_cluster_delta_z.At(i);
      m_z_sigma = m_tree_cluster_z0_sigma.At(i);
      m_q_over_p = m_tree_cluster_q_over_p.At(i);
      m_q_over_p_sigma = m_tree_cluster_q_over_p_sigma.At(i);
      m_delta_z_resunits = m_tree_cluster_delta_z_resunit.At(i);
      m_cluster_sumpt2 = m_tree_cluster_sumpt2.At(i);
      m_d0 = m_tree_cluster_d0.At(i);
      m_d0_sigma = m_tree_cluster_d0_sigma.At(i);
      m_number_of_tracks = m_tree_cluster_ntracks.At(i);
      // m_z0_stddev = m_tree_cluster_z0_stddev.At(i);
      // m_z0_mean = m_tree_cluster_z0_mean.At(i);
      // m_closest_vx_is_pvx = m_tree_cluster_closest_vx_is_pvx.At(i);
      // m_distance_pu_vx = m_tree_cluster_distance_pu_vx.At(i);
      // m_n_pu_vertices = m_tree_cluster_n_pu_vertices.At(i);
      // m_pt_mod = m_tree_cluster_pt_mod.At(i);

      // cout << "BDT out: " << tmva_reader->EvaluateMVA("BDT") << '\n';
      float bdt_output = tmva_reader->EvaluateMVA("BDT");
      if (m_tree_cluster_hs_fraction.At(i) > 0.5) {
        if (m_number_of_tracks >= 3) {
          n_good_clusters++;
        }
        hist_bdt_out_sig->Fill(bdt_output);
      } else {
        hist_bdt_out_bg->Fill(bdt_output);
      }

      if (m_tree_cluster_hs_fraction.At(i) > 0.5 and
          m_tree_cluster_hs_fraction.At(i) > truth_highest_hs_fraction and
          m_number_of_tracks >= 3) {
        truth_cluster_accepted = true;
        truth_cluster_position = i;
        truth_highest_hs_fraction = m_tree_cluster_hs_fraction.At(i);
      }

      for (int j = 0; j < bdt_cutvalues.size(); j++) {
        if (bdt_output > bdt_cutvalues.at(j) and
            bdt_output > accepted_bdt_output.at(j)) {
          accepted_bdt_output.at(j) = bdt_output;
          accepted_time.at(j) = m_tree_cluster_time.At(i);
          accepted_cluster.at(j) = true;
        }
      }
      for (int j = 0; j < bdt_cutvalues.size(); j++) {
        hist_has_time_bdt_selected_t0_vec.at(j)->Fill(accepted_cluster.at(j));
        if (accepted_cluster.at(j)) {
          hist_res_bdt_selected_t0_vec.at(j)->Fill(accepted_time.at(j) -
                                                   truth_t0);
        }
      }
      // get the one best cluster from BDT
      // if (bdt_output>min_bdt_cut_val and bdt_output>max_bdt_value and
      // m_number_of_tracks>=3) {
      //   // cout << "found good cluster at " << i << " with BDT op " <<
      //   bdt_output << '\n'; max_bdt_value = bdt_output; cluster_position = i;
      //   cluster_accepted = true;
      // }
      if (bdt_output > min_bdt_cut_val and bdt_output > max_bdt_value) {
        no_cluster_passes_cut = false;
        if (m_number_of_tracks >= 3) {
          max_bdt_value = bdt_output;
          cluster_position = i;
          cluster_accepted = true;
          no_cluster_has_3tracks = false;
        }
      }
    } // LOOP over clusters in event

    double time_res = -999;
    if (cluster_accepted) {
      time_res = m_tree_cluster_time.At(cluster_position) - truth_t0;
    }

    ///////////////////////////
    // check comparison BDT and truth cluster selection
    if (cluster_accepted and truth_cluster_accepted) {
      hist_same_cluster->Fill(cluster_position == truth_cluster_position);
    }
    if (cluster_accepted) {
      hist_hs_fraction_chosen_cluster->Fill(
          m_tree_cluster_hs_fraction.At(cluster_position));
      // types of clusters
      if (cluster_position == truth_cluster_position) {
        hist_chosen_cluster_type->Fill(0.5);
        hist_chosen_cluster_type_detail->Fill(0.5);
        hist_res_type1->Fill(time_res);
        hist_ntracks_type1->Fill(m_tree_cluster_ntracks.At(cluster_position));
        if (n_fwd_hard_pu_jets >= 1 and n_fwd_hard_hs_jets == 0) {
          hist_chosen_cluster_typ_fwdPUjets->Fill(0.5);
        }
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) > 0.5) {
        hist_chosen_cluster_type->Fill(0.5);
        hist_chosen_cluster_type_detail->Fill(0.5);
        hist_res_type2->Fill(time_res);
        hist_ntracks_type2->Fill(m_tree_cluster_ntracks.At(cluster_position));
        if (n_fwd_hard_pu_jets >= 1 and n_fwd_hard_hs_jets == 0) {
          hist_chosen_cluster_typ_fwdPUjets->Fill(0.5);
        }
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) == 0.5) {
        hist_chosen_cluster_type->Fill(0.5);
        hist_chosen_cluster_type_detail->Fill(0.5);
        hist_res_type3->Fill(time_res);
        hist_ntracks_type3->Fill(m_tree_cluster_ntracks.At(cluster_position));
        if (n_fwd_hard_pu_jets >= 1 and n_fwd_hard_hs_jets == 0) {
          hist_chosen_cluster_typ_fwdPUjets->Fill(0.5);
        }
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) < 0.5 and
                 m_tree_cluster_hs_fraction.At(cluster_position) > 0.0) {
        hist_chosen_cluster_type->Fill(1.5);
        hist_chosen_cluster_type_detail->Fill(1.5);
        hist_res_type4->Fill(time_res);
        hist_ntracks_type4->Fill(m_tree_cluster_ntracks.At(cluster_position));
        if (n_fwd_hard_pu_jets >= 1 and n_fwd_hard_hs_jets == 0) {
          hist_chosen_cluster_typ_fwdPUjets->Fill(1.5);
        }
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) == 0.0) {
        hist_chosen_cluster_type->Fill(2.5);
        hist_chosen_cluster_type_detail->Fill(2.5);
        hist_res_type5->Fill(time_res);
        hist_ntracks_type5->Fill(m_tree_cluster_ntracks.At(cluster_position));
        if (n_fwd_hard_pu_jets >= 1 and n_fwd_hard_hs_jets == 0) {
          hist_chosen_cluster_typ_fwdPUjets->Fill(2.5);
        }
      }
    } else {
      hist_chosen_cluster_type->Fill(3.5);
      if (n_fwd_hard_pu_jets >= 1 and n_fwd_hard_hs_jets == 0) {
        hist_chosen_cluster_typ_fwdPUjets->Fill(3.5);
      }

      if (no_cluster_passes_cut) {
        hist_chosen_cluster_type_detail->Fill(3.5);
      }

      if (not no_cluster_passes_cut and no_cluster_has_3tracks) {
        hist_chosen_cluster_type_detail->Fill(4.5);
      }
    }

    if (cluster_accepted and not truth_cluster_accepted) {
      if (cluster_position == truth_cluster_position) {
        hist_no_perfect_butbdt->Fill(0.5);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) > 0.5) {
        hist_no_perfect_butbdt->Fill(1.5);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) == 0.5) {
        hist_no_perfect_butbdt->Fill(2.5);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) < 0.5 and
                 m_tree_cluster_hs_fraction.At(cluster_position) > 0.0) {
        hist_no_perfect_butbdt->Fill(3.5);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) == 0.0) {
        hist_no_perfect_butbdt->Fill(4.5);
      }
    }

    if (cluster_accepted and truth_cluster_accepted) {
      if (cluster_position == truth_cluster_position) {
        hist_compare_bdt_with_perfect_choice->Fill(0.5);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) > 0.5) {
        hist_compare_bdt_with_perfect_choice->Fill(1.5);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) == 0.5) {
        hist_compare_bdt_with_perfect_choice->Fill(2.5);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) < 0.5 and
                 m_tree_cluster_hs_fraction.At(cluster_position) > 0.0) {
        hist_compare_bdt_with_perfect_choice->Fill(3.5);
      } else if (m_tree_cluster_hs_fraction.At(cluster_position) == 0.0) {
        hist_compare_bdt_with_perfect_choice->Fill(4.5);
      }
    }

    // what happens in events where the BDT chooses PU clusters
    int n_tracks = *m_tree_n_hgtd_tracks;
    int n_hs_tracks = *m_tree_n_hgtd_hs_tracks;
    int n_hs_timed_tracks = *m_tree_n_hgtd_hs_tracks_with_time;
    int n_hs_goodtimed_tracks = *m_tree_n_hgtd_hs_tracks_with_good_time;

    if (cluster_accepted) {
      if (m_tree_cluster_hs_fraction.At(cluster_position) < 0.01) {
        hist_n_tracks_pu_cluster->Fill(n_tracks);
        hist_n_hstracks_pu_cluster->Fill(n_hs_tracks);
        hist_n_hstracks_wtime_pu_cluster->Fill(n_hs_timed_tracks);
        hist_n_hstracks_wgoodtime_pu_cluster->Fill(n_hs_goodtimed_tracks);
        hist_n_goodclusters_pu_chosen->Fill(n_good_clusters);
      } else {
        hist_n_tracks_hs_cluster->Fill(n_tracks);
        hist_n_hstracks_hs_cluster->Fill(n_hs_tracks);
        hist_n_hstracks_wtime_hs_cluster->Fill(n_hs_timed_tracks);
        hist_n_hstracks_wgoodtime_hs_cluster->Fill(n_hs_goodtimed_tracks);
        hist_n_goodclusters_hs_chosen->Fill(n_good_clusters);
      }
    }

    hist_perfect_no_bdt->Fill(not cluster_accepted and truth_cluster_accepted);

    ///////////////////////////

    hist_has_time_bdt_selected_eventt0->Fill(cluster_accepted);
    if (cluster_accepted) {
      hist_res_bdt_selected_eventt0->Fill(time_res);
    }

  } // LOOP over ntuple

  auto rootfile = TFile::Open("BDToutputStuff.root", "RECREATE");
  for (const auto &el : write) {
    el->Write();
  }
  rootfile->Close();
  cout << "writing BDToutputStuff.root\n";
}

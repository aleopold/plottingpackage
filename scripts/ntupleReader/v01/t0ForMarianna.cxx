
#include "getTChain.cxx"

struct EventData {
  int eventnumber = -1;
  int n_tracks = 0;
  int n_hs_tracks = 0;
  int n_hs_tracks_w_time = 0;
  int n_hs_tracks_w_good_time = 0;

  float local_vx_density;

  // jets
  int n_fwd_pu_jets = 0;
  int n_fwd_hs_jets = 0;
  int n_fwd_hard_pu_jets = 0;
  int n_fwd_hard_hs_jets = 0;
};

void t0ForMarianna() {

  TMVA::Tools::Instance();

  TMVA::Reader *tmva_reader = new TMVA::Reader("!Color:!Silent");

  float m_delta_z;
  float m_z_sigma;
  float m_q_over_p;
  float m_q_over_p_sigma;
  float m_d0;
  float m_d0_sigma;
  float m_delta_z_resunits;
  float m_cluster_sumpt2;
  float m_number_of_tracks;

  tmva_reader->AddVariable("m_delta_z", &m_delta_z);
  tmva_reader->AddVariable("m_z_sigma", &m_z_sigma);
  tmva_reader->AddVariable("m_q_over_p", &m_q_over_p);
  tmva_reader->AddVariable("m_q_over_p_sigma", &m_q_over_p_sigma);
  tmva_reader->AddVariable("m_delta_z_resunits", &m_delta_z_resunits);
  tmva_reader->AddVariable("m_cluster_sumpt2", &m_cluster_sumpt2);
  tmva_reader->AddVariable("m_d0", &m_d0);
  tmva_reader->AddVariable("m_d0_sigma", &m_d0_sigma);
  // tmva_reader->AddVariable("m_number_of_tracks", &m_number_of_tracks );

  TString weightfile = "dataset/weights/TMVAClassification_BDT.weights.xml";
  tmva_reader->BookMVA("BDT", weightfile);

  // TString path =
  // "/afs/cern.ch/work/a/aleopold/private/hgtd/analysis_pkg/run/PileupRejectionNtuple_20k.root";
  // auto g_file = TFile::Open(path, "READ");
  // TTreeReader reader("JetTree", g_file);

  std::vector<TString> list_of_files = getListOfFiles("INPUT.txt", -1);
  int n_files_to_skip = 0;
  int start_pos_for_erase = list_of_files.size() - n_files_to_skip;
  list_of_files.erase(list_of_files.begin() + start_pos_for_erase,
                      list_of_files.end());
  if (list_of_files.size() == 0) {
    cout << "ERROR: getListOfFiles returned no files to be read" << endl;
    return;
  }
  cout << list_of_files.size() << " files left for evaluation\n";
  TChain *g_file = getChain(list_of_files, "JetTree", -1);
  TTreeReader reader(g_file);

  TTreeReaderValue<int> m_tree_eventnumber(reader, "m_tree_eventnumber");
  TTreeReaderValue<double> m_tree_truth_t0(reader, "m_tree_truth_t0");
  TTreeReaderValue<double> m_tree_param_vx_density(reader,
                                                   "m_tree_param_vx_density");
  TTreeReaderValue<double> m_tree_local_vx_density(reader,
                                                   "m_tree_local_vx_density");
  TTreeReaderValue<double> m_tree_local_truthvx_density(
      reader, "m_tree_local_truthvx_density");
  TTreeReaderValue<double> m_tree_deltaz_vx_truthvx(reader,
                                                    "m_tree_deltaz_vx_truthvx");
  //
  TTreeReaderValue<int> m_tree_n_hgtd_tracks(reader, "m_tree_n_hgtd_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks(reader,
                                                "m_tree_n_hgtd_hs_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_time(
      reader, "m_tree_n_hgtd_hs_tracks_with_time");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_good_time(
      reader, "m_tree_n_hgtd_hs_tracks_with_good_time");

  TTreeReaderArray<double> m_tree_cluster_time(reader, "m_tree_cluster_time");
  TTreeReaderArray<double> m_tree_cluster_time_res(reader,
                                                   "m_tree_cluster_time_res");
  TTreeReaderArray<double> m_tree_cluster_z0(reader, "m_tree_cluster_z0");
  TTreeReaderArray<double> m_tree_cluster_z0_sigma(reader,
                                                   "m_tree_cluster_z0_sigma");
  TTreeReaderArray<double> m_tree_cluster_q_over_p(reader,
                                                   "m_tree_cluster_q_over_p");
  TTreeReaderArray<double> m_tree_cluster_q_over_p_sigma(
      reader, "m_tree_cluster_q_over_p_sigma");
  TTreeReaderArray<double> m_tree_cluster_d0(reader, "m_tree_cluster_d0");
  TTreeReaderArray<double> m_tree_cluster_d0_sigma(reader,
                                                   "m_tree_cluster_d0_sigma");
  TTreeReaderArray<double> m_tree_cluster_hs_fraction(
      reader, "m_tree_cluster_hs_fraction");
  TTreeReaderArray<double> m_tree_cluster_delta_z(reader,
                                                  "m_tree_cluster_delta_z");
  TTreeReaderArray<double> m_tree_cluster_delta_z_resunit(
      reader, "m_tree_cluster_delta_z_resunit");
  TTreeReaderArray<double> m_tree_cluster_sumpt2(reader,
                                                 "m_tree_cluster_sumpt2");
  TTreeReaderArray<double> m_tree_cluster_ntracks(reader,
                                                  "m_tree_cluster_ntracks");

  TTreeReaderValue<int> m_tree_jet_category(reader, "m_tree_jet_category");
  TTreeReaderValue<double> m_tree_jet_pt(reader, "m_tree_jet_pt");
  TTreeReaderValue<double> m_tree_jet_eta(reader, "m_tree_jet_eta");
  TTreeReaderValue<double> m_tree_jet_rpt(reader, "m_tree_jet_rpt");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_truth_time(
      reader, "m_tree_jet_hgtd_rpt_trutht0_truth_time");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_reco_time(
      reader, "m_tree_jet_hgtd_rpt_trutht0_reco_time");
  TTreeReaderValue<double> m_tree_jet_subjet_rpt(reader,
                                                 "m_tree_jet_subjet_rpt");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times_truth_trk(
      reader, "m_tree_cluster_ntracks");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times(
      reader, "m_tree_jet_hgtd_rpt_cluster_times");

  /////////////////////////////////////////////
  ///////////// output histograms /////////////
  std::vector<TObject *> write;

  int m_eventnumber;
  bool m_hast0;
  float m_t0, m_truth_t0;

  TTree *tree = new TTree("HSTime", "HSTime");
  tree->Branch("m_eventnumber", &m_eventnumber);
  tree->Branch("m_hast0", &m_hast0);
  tree->Branch("m_t0", &m_t0);
  tree->Branch("m_truth_t0", &m_truth_t0);

  write.push_back(tree);

  /////////////////////////////////////////////
  // extract per event information
  // struct EventData {
  //   int eventnumber;
  //   int n_tracks;
  //   int n_hs_tracks;
  //   int n_hs_tracks_w_time;
  //   int n_hs_tracks_w_good_time;
  //
  //   float local_vx_density;
  //
  //   //jets
  //   int n_fwd_pu_jets;
  //   int n_fwd_hs_jets;
  //   int n_fwd_hard_pu_jets;
  //   int n_fwd_hard_hs_jets;
  // };

  cout << "reading event data" << '\n';

  double jeteta_min = 2.4;
  double jeteta_max = 4.0;

  // double min_bdt_cut_val = 0.1;
  double min_bdt_cut_val = 0.2;

  cout << "starting loop" << '\n';

  int current_eventnumber = -1;
  while (reader.Next()) {
    ////////////////
    // get the clusters only once per event
    m_eventnumber = *m_tree_eventnumber;
    if (m_eventnumber == current_eventnumber) {
      continue;
    }
    current_eventnumber = m_eventnumber;
    ////////////////

    m_truth_t0 = *m_tree_truth_t0;

    int n_clusters = m_tree_cluster_hs_fraction.GetSize();

    // find the BDT choice
    float max_bdt_value = -1.;
    m_t0 = -999;
    m_hast0 = false;

    int n_good_clusters = 0;
    for (int i = 0; i < n_clusters; i++) {
      // write out info for each cluster, if bdt accepts it and what time it has

      m_delta_z = m_tree_cluster_delta_z.At(i);
      m_z_sigma = m_tree_cluster_z0_sigma.At(i);
      m_q_over_p = m_tree_cluster_q_over_p.At(i);
      m_q_over_p_sigma = m_tree_cluster_q_over_p_sigma.At(i);
      m_delta_z_resunits = m_tree_cluster_delta_z_resunit.At(i);
      m_cluster_sumpt2 = m_tree_cluster_sumpt2.At(i);
      m_d0 = m_tree_cluster_d0.At(i);
      m_d0_sigma = m_tree_cluster_d0_sigma.At(i);
      m_number_of_tracks = m_tree_cluster_ntracks.At(i);

      // cout << "BDT out: " << tmva_reader->EvaluateMVA("BDT") << '\n';
      float bdt_output = tmva_reader->EvaluateMVA("BDT");

      // get the one best cluster from BDT
      if (bdt_output > min_bdt_cut_val and bdt_output > max_bdt_value and
          m_number_of_tracks >= 3) {
        // cout << "found good cluster at " << i << " with BDT op " <<
        // bdt_output << '\n';
        max_bdt_value = bdt_output;

        m_hast0 = true;
        m_t0 = m_tree_cluster_time.At(i);
      }
    } // LOOP over clusters in event

    tree->Fill();

  } // LOOP over ntuple

  auto rootfile = TFile::Open("hardScatterTime.root", "RECREATE");
  for (const auto &el : write) {
    el->Write();
  }
  rootfile->Close();
  cout << "writing hardScatterTime.root\n";
}


int m_n_rpt_bins = 100;
double m_rpt_stepsize = 0.01;
double m_rpt_offset = 0.001;
std::vector<double> m_eff_rpt_cut;
TH1F* m_bin_finder = nullptr;

double m_jet_pt_low_cut = 30.;
double m_jet_pt_high_cut = 50.;
double m_jet_pt_max_cut = 1.e30;


static const int n_eta_bins = 10;

int g_eta_bins = 10;


void initRpTCuts() {
  m_eff_rpt_cut.push_back(m_rpt_offset);
  for (int i=1; i<m_n_rpt_bins; i++) {
    m_eff_rpt_cut.push_back((double) i * m_rpt_stepsize);
  }

  m_bin_finder = new TH1F("binfinderhist", "", g_eta_bins, 0.0, 4.0);
}

// double m_jet_pt_low_cut = 30.;
// double m_jet_pt_high_cut = 50.;
// double m_jet_pt_max_cut = 1000.;

// double m_jet_pt_low_cut = 30.;
// double m_jet_pt_high_cut = 45.;
// double m_jet_pt_max_cut = 60.;



void setupRocCurvePlots(std::array<TEfficiency*, 4>& arr, const std::string & name, const std::string & x_label, std::vector<TObject*>& write) {
  std::string plot_name = "m_eff_hs_low_pt_" + name;
  std::string label = ";" + x_label + ";HS efficiency";

  arr.at(0) = new TEfficiency(plot_name.c_str(), label.c_str(), m_n_rpt_bins, 0, (double)m_n_rpt_bins*m_rpt_stepsize);

  plot_name = "m_eff_hs_high_pt_" + name;
  arr.at(1) = new TEfficiency(plot_name.c_str(), label.c_str(), m_n_rpt_bins, 0, (double)m_n_rpt_bins*m_rpt_stepsize);

  label = ";" + x_label + ";PU efficiency";

  plot_name = "m_eff_pu_low_pt_" + name;
  arr.at(2) = new TEfficiency(plot_name.c_str(), label.c_str(), m_n_rpt_bins, 0, (double)m_n_rpt_bins*m_rpt_stepsize);

  plot_name = "m_eff_pu_high_pt_" + name;
  arr.at(3) = new TEfficiency(plot_name.c_str(), label.c_str(), m_n_rpt_bins, 0, (double)m_n_rpt_bins*m_rpt_stepsize);
  for (int i=0; i<4; i++) {
    write.push_back(arr.at(i));
  }
}

void setupRocCurvePlotsVariableBins(std::array<TEfficiency*, 4>& arr, const std::string & name, const std::string & x_label, std::vector<TObject*>& write) {
  std::string plot_name = "m_eff_hs_low_pt_" + name + "VarBin";
  std::string label = ";" + x_label + ";HS efficiency";

  // double bins[] = {0.001, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5};
  // int n_bins = sizeof(bins)/sizeof(double) - 1;
  double bins[] = {0.01, 0.02, 0.03, 0.05, 0.07, 0.09, 0.11, 0.13, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5, 0.6};
  int n_bins = sizeof(bins)/sizeof(double) - 1;

  arr.at(0) = new TEfficiency(plot_name.c_str(), label.c_str(), n_bins, bins);

  plot_name = "m_eff_hs_high_pt_" + name + "VarBin";
  arr.at(1) = new TEfficiency(plot_name.c_str(), label.c_str(), n_bins, bins);

  label = ";" + x_label + ";PU efficiency";

  plot_name = "m_eff_pu_low_pt_" + name + "VarBin";
  arr.at(2) = new TEfficiency(plot_name.c_str(), label.c_str(), n_bins, bins);

  plot_name = "m_eff_pu_high_pt_" + name + "VarBin";
  arr.at(3) = new TEfficiency(plot_name.c_str(), label.c_str(), n_bins, bins);
  for (int i=0; i<4; i++) {
    write.push_back(arr.at(i));
  }
}

void setupRocCurvePlotsCombined(std::array<TEfficiency*, 2>& arr, const std::string & name, const std::string & x_label, std::vector<TObject*>& write) {
  std::string plot_name = "m_eff_hs_" + name;
  std::string label = ";" + x_label + ";HS efficiency";

  arr.at(0) = new TEfficiency(plot_name.c_str(), label.c_str(), m_n_rpt_bins, 0, (double)m_n_rpt_bins*m_rpt_stepsize);

  label = ";" + x_label + ";PU efficiency";

  plot_name = "m_eff_pu_" + name;
  arr.at(1) = new TEfficiency(plot_name.c_str(), label.c_str(), m_n_rpt_bins, 0, (double)m_n_rpt_bins*m_rpt_stepsize);

  for (int i=0; i<2; i++) {
    write.push_back(arr.at(i));
  }
}


void fillRpTEffCurves(float rpt, std::array<TEfficiency*, 4>& eff, int category, double pt) {

  bool passed_cut = false;
  for (auto curr_cut : m_eff_rpt_cut) {
    passed_cut = (rpt >= curr_cut);
    if (category == 0) {
      //split in low and high pt
      if (pt>m_jet_pt_low_cut and pt<m_jet_pt_high_cut) {
        eff.at(0)->Fill(passed_cut, curr_cut);
      } if (pt>=m_jet_pt_high_cut and pt<=m_jet_pt_max_cut) {
        eff.at(1)->Fill(passed_cut, curr_cut);
      }
    // } else if (category >0) { //DON'T USE THIS
    } else if (category == 1 or category == 2 or category == 3) {
      if (pt>m_jet_pt_low_cut and pt<m_jet_pt_high_cut) {
        eff.at(2)->Fill(passed_cut, curr_cut);
      } if (pt>=m_jet_pt_high_cut and pt<=m_jet_pt_max_cut) {
        eff.at(3)->Fill(passed_cut, curr_cut);
      }
    }
  }//LOOP over cut values
}

void fillRpTEffCurvesCombined(float rpt, std::array<TEfficiency*, 2>& eff, int category) {

  bool passed_cut = false;
  for (auto curr_cut : m_eff_rpt_cut) {
    passed_cut = (rpt >= curr_cut);
    if (category == 0) {
      eff.at(0)->Fill(passed_cut, curr_cut);
    } else if (category == 1 or category == 2 or category == 3) {
      eff.at(1)->Fill(passed_cut, curr_cut);
    }
  }//LOOP over cut values
}

void prepareEffScanVsEta(
std::array<TEfficiency*, n_eta_bins>& m_eff_hs_rpt_param_low_pt,
std::array<TEfficiency*, n_eta_bins>& m_eff_hs_rpt_param_high_pt,
std::array<TEfficiency*, n_eta_bins>& m_eff_pu_rpt_param_low_pt,
std::array<TEfficiency*, n_eta_bins>& m_eff_pu_rpt_param_high_pt,
std::vector<TObject*>& write, std::string prefix) {

  int m_n_rpt_bins = 100;
  double m_rpt_stepsize = 0.01;
  double m_rpt_offset = 0.001;

  std::string plot_name("");

  for (int i=0; i<n_eta_bins; i++) {
    plot_name = prefix + "m_eff_hs_rpt_low_pt_etabin_" + std::to_string(i);
    m_eff_hs_rpt_param_low_pt.at(i) = new TEfficiency(
      plot_name.c_str(),
      ";RpT cut;HS efficiency",
      m_n_rpt_bins,
      0,
      (double)m_n_rpt_bins*m_rpt_stepsize);
    write.push_back(m_eff_hs_rpt_param_low_pt.at(i));

    plot_name = prefix + "m_eff_hs_rpt_high_pt_etabin_" + std::to_string(i);
    m_eff_hs_rpt_param_high_pt.at(i) = new TEfficiency(
      plot_name.c_str(),
      ";RpT cut;HS efficiency",
      m_n_rpt_bins,
      0,
      (double)m_n_rpt_bins*m_rpt_stepsize);
    write.push_back(m_eff_hs_rpt_param_high_pt.at(i));

    plot_name = prefix + "m_eff_pu_rpt_low_pt_etabin_" + std::to_string(i);
    m_eff_pu_rpt_param_low_pt.at(i) = new TEfficiency(
      plot_name.c_str(),
      ";RpT cut;PU efficiency",
      m_n_rpt_bins,
      0,
      (double)m_n_rpt_bins*m_rpt_stepsize);
    write.push_back(m_eff_pu_rpt_param_low_pt.at(i));


    plot_name = prefix + "m_eff_pu_rpt_high_pt_etabin_" + std::to_string(i);
    m_eff_pu_rpt_param_high_pt.at(i) = new TEfficiency(
      plot_name.c_str(),
      ";RpT cut;PU efficiency",
      m_n_rpt_bins,
      0,
      (double)m_n_rpt_bins*m_rpt_stepsize);
    write.push_back(m_eff_pu_rpt_param_high_pt.at(i));

  }
}

void fillEtaPURej(float rpt,
std::array<TEfficiency*, n_eta_bins>& m_eff_hs_rpt_param_low_pt,
std::array<TEfficiency*, n_eta_bins>& m_eff_hs_rpt_param_high_pt,
std::array<TEfficiency*, n_eta_bins>& m_eff_pu_rpt_param_low_pt,
std::array<TEfficiency*, n_eta_bins>& m_eff_pu_rpt_param_high_pt,
int category, double pt, double abs_eta) {

  /////compare rpt with subjetrpt rejection
  int eta_bin = m_bin_finder->FindBin(abs_eta) - 1;
  if (eta_bin > n_eta_bins - 1) {eta_bin = eta_bin - 1;}

  bool passed_cut = false;

  for (auto curr_cut : m_eff_rpt_cut) {
    passed_cut =  (rpt >= curr_cut);

    if (category == 0) {
      //split in low and high pt
      if (pt>m_jet_pt_low_cut and pt<m_jet_pt_high_cut) {
        m_eff_hs_rpt_param_low_pt.at(eta_bin)->Fill(passed_cut, curr_cut);
      } if (pt>=m_jet_pt_high_cut and pt<=m_jet_pt_max_cut) {
        m_eff_hs_rpt_param_high_pt.at(eta_bin)->Fill(passed_cut, curr_cut);
      }
    } else if (category == 1 or category == 2 or category == 3) {
      //split in low and high pt
      //split in eta bins
      if (pt>m_jet_pt_low_cut and pt<m_jet_pt_high_cut) {
        m_eff_pu_rpt_param_low_pt.at(eta_bin)->Fill(passed_cut, curr_cut);
      } if (pt>=m_jet_pt_high_cut and pt<=m_jet_pt_max_cut) {
        m_eff_pu_rpt_param_high_pt.at(eta_bin)->Fill(passed_cut, curr_cut);
      }
    }
  }//LOOP over cut values
}

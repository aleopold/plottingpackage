#include "getTChain.cxx"
#include "prepareRocCurves.cxx"

struct EventData {
  int eventnumber = -1;
  int n_tracks = 0;
  int n_hs_tracks = 0;
  int n_hs_tracks_w_time = 0;
  int n_hs_tracks_w_good_time = 0;

  float local_vx_density;

  // jets
  int n_fwd_pu_jets = 0;
  int n_fwd_hs_jets = 0;
  int n_fwd_hard_pu_jets = 0;
  int n_fwd_hard_hs_jets = 0;

  int n_fwd_hard_jets = 0;
  bool hasHighRpTJets = false;
};

struct BDTData {
  vector<int> index;
  vector<float> score;
  vector<float> hs_frac;
};

TGraphErrors *getROCcurve(TEfficiency *eff1, TEfficiency *eff2);

TGraph *getRatioGraph(TGraph *numerator, TGraph *denominator);

TChain *loadChain() {
  std::vector<TString> list_of_files =
      getListOfFiles("INPUT.txt", -1); // this is defined in GetTChain.h
  // skip the files used for training
  int n_files_to_skip = 35;
  // int n_files_to_skip = 200; //FILES FROM TAO
  int end_pos_for_erase = list_of_files.size() - n_files_to_skip;
  list_of_files.erase(list_of_files.begin(),
                      list_of_files.end() - end_pos_for_erase);

  cout << list_of_files.size() << " files left for evaluation\n";

  if (list_of_files.size() == 0) {
    cout << "ERROR: getListOfFiles returned no files to be read" << endl;
    return nullptr;
  }
  TChain *g_file = getChain(list_of_files, "JetTree", -1);
  return g_file;
}

float run(TChain *g_file, float cutvalue) {
  // TGraph* run(TChain* g_file, float cutvalue) {
  //////////  load BDT from XML //////////
  TMVA::Tools::Instance();

  TMVA::Reader *tmva_reader = new TMVA::Reader("!Color:!Silent");

  float m_delta_z;
  float m_z_sigma;
  float m_q_over_p;
  float m_q_over_p_sigma;
  float m_d0;
  float m_d0_sigma;
  float m_delta_z_resunits;
  float m_cluster_sumpt2;
  float m_number_of_tracks;
  float m_z0_stddev;
  float m_z0_mean;
  float m_closest_vx_is_pvx;
  float m_distance_pu_vx;
  float m_n_pu_vertices;
  float m_pt_mod;

  tmva_reader->AddVariable("m_delta_z", &m_delta_z);
  tmva_reader->AddVariable("m_z_sigma", &m_z_sigma);
  tmva_reader->AddVariable("m_q_over_p", &m_q_over_p);
  tmva_reader->AddVariable("m_q_over_p_sigma", &m_q_over_p_sigma);
  tmva_reader->AddVariable("m_delta_z_resunits", &m_delta_z_resunits);
  tmva_reader->AddVariable("m_cluster_sumpt2", &m_cluster_sumpt2);
  tmva_reader->AddVariable("m_d0", &m_d0);
  tmva_reader->AddVariable("m_d0_sigma", &m_d0_sigma);
  // tmva_reader->AddVariable("m_z0_stddev", &m_z0_stddev);
  // tmva_reader->AddVariable("m_closest_vx_is_pvx", &m_closest_vx_is_pvx);
  // tmva_reader->AddVariable("m_distance_pu_vx", &m_distance_pu_vx);
  // tmva_reader->AddVariable("m_n_pu_vertices", &m_n_pu_vertices);
  // tmva_reader->AddVariable("m_pt_mod", &m_pt_mod);
  // tmva_reader->AddVariable("m_number_of_tracks", &m_number_of_tracks);

  TString weightfile = "dataset/weights/TMVAClassification_BDT.weights.xml";
  tmva_reader->BookMVA("BDT", weightfile);
  ////////////////////////////////////////
  cout << "BDT loaded\n";

  initRpTCuts();

  std::vector<TObject *> write;

  std::array<TEfficiency *, 4> m_eff_std_rpt;
  setupRocCurvePlots(m_eff_std_rpt, "StandardRpT", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_std_rpt_variablebin;
  setupRocCurvePlotsVariableBins(m_eff_std_rpt_variablebin, "StandardRpT",
                                 "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_subjet_rpt;
  setupRocCurvePlots(m_eff_subjet_rpt, "SubJetRpT", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_subjet_rpt_variablebin;
  setupRocCurvePlotsVariableBins(m_eff_subjet_rpt_variablebin, "SubJetRpT",
                                 "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_bestTruthHSCluster_rpt;
  setupRocCurvePlots(m_eff_bestTruthHSCluster_rpt, "BestTruthCluster",
                     "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_bestTruthHSClusterSubjetRpT;
  setupRocCurvePlots(m_eff_bestTruthHSClusterSubjetRpT,
                     "BestTruthClusterrSubjetRpT", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_BDTClusterOrStdRpT;
  setupRocCurvePlots(m_eff_BDTClusterOrStdRpT, "BDTClusterOrStdRpT", "RpT cut",
                     write);

  std::array<TEfficiency *, 4> m_eff_BDTClusterOrStdRpT_variablebin;
  setupRocCurvePlotsVariableBins(m_eff_BDTClusterOrStdRpT_variablebin,
                                 "BDTClusterOrStdRpT", "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_BDTClusterOrSubjetRpT;
  setupRocCurvePlots(m_eff_BDTClusterOrSubjetRpT, "BDTClusterOrSubjetRpT",
                     "RpT cut", write);

  std::array<TEfficiency *, 4> m_eff_BDTClusterOrSubjetRpT_variablebin;
  setupRocCurvePlotsVariableBins(m_eff_BDTClusterOrSubjetRpT_variablebin,
                                 "BDTClusterOrSubjetRpT", "RpT cut", write);

  TTreeReader reader(g_file);

  TTreeReaderValue<int> m_tree_eventnumber(reader, "m_tree_eventnumber");
  // TTreeReaderValue<double> m_tree_truth_t0(reader, "m_tree_truth_t0");
  // TTreeReaderValue<double> m_tree_param_vx_density(reader,
  // "m_tree_param_vx_density");
  TTreeReaderValue<double> m_tree_local_vx_density(reader,
                                                   "m_tree_local_vx_density");
  TTreeReaderValue<double> m_tree_local_truthvx_density(
      reader, "m_tree_local_truthvx_density");
  TTreeReaderValue<double> m_tree_deltaz_vx_truthvx(reader,
                                                    "m_tree_deltaz_vx_truthvx");

  TTreeReaderValue<int> m_tree_n_hgtd_tracks(reader, "m_tree_n_hgtd_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks(reader,
                                                "m_tree_n_hgtd_hs_tracks");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_time(
      reader, "m_tree_n_hgtd_hs_tracks_with_time");
  TTreeReaderValue<int> m_tree_n_hgtd_hs_tracks_with_good_time(
      reader, "m_tree_n_hgtd_hs_tracks_with_good_time");

  TTreeReaderArray<double> m_tree_cluster_time(reader, "m_tree_cluster_time");
  TTreeReaderArray<double> m_tree_cluster_time_res(reader,
                                                   "m_tree_cluster_time_res");
  TTreeReaderArray<double> m_tree_cluster_z0(reader, "m_tree_cluster_z0");
  TTreeReaderArray<double> m_tree_cluster_z0_sigma(reader,
                                                   "m_tree_cluster_z0_sigma");
  TTreeReaderArray<double> m_tree_cluster_q_over_p(reader,
                                                   "m_tree_cluster_q_over_p");
  TTreeReaderArray<double> m_tree_cluster_q_over_p_sigma(
      reader, "m_tree_cluster_q_over_p_sigma");
  TTreeReaderArray<double> m_tree_cluster_d0(reader, "m_tree_cluster_d0");
  TTreeReaderArray<double> m_tree_cluster_d0_sigma(reader,
                                                   "m_tree_cluster_d0_sigma");
  TTreeReaderArray<double> m_tree_cluster_hs_fraction(
      reader, "m_tree_cluster_hs_fraction");
  TTreeReaderArray<double> m_tree_cluster_delta_z(reader,
                                                  "m_tree_cluster_delta_z");
  TTreeReaderArray<double> m_tree_cluster_delta_z_resunit(
      reader, "m_tree_cluster_delta_z_resunit");
  TTreeReaderArray<double> m_tree_cluster_sumpt2(reader,
                                                 "m_tree_cluster_sumpt2");
  TTreeReaderArray<double> m_tree_cluster_ntracks(reader,
                                                  "m_tree_cluster_ntracks");
  TTreeReaderArray<double> m_tree_cluster_z0_stddev(reader,
                                                    "m_tree_cluster_z0_stddev");
  TTreeReaderArray<double> m_tree_cluster_z0_mean(reader,
                                                  "m_tree_cluster_z0_mean");
  TTreeReaderArray<double> m_tree_cluster_closest_vx_is_pvx(
      reader, "m_tree_cluster_closest_vx_is_pvx");
  TTreeReaderArray<double> m_tree_cluster_distance_pu_vx(
      reader, "m_tree_cluster_distance_pu_vx");
  TTreeReaderArray<double> m_tree_cluster_n_pu_vertices(
      reader, "m_tree_cluster_n_pu_vertices");
  TTreeReaderArray<double> m_tree_cluster_pt_mod(reader,
                                                 "m_tree_cluster_pt_mod");

  TTreeReaderValue<int> m_tree_jet_category(reader, "m_tree_jet_category");
  TTreeReaderValue<double> m_tree_jet_pt(reader, "m_tree_jet_pt");
  TTreeReaderValue<double> m_tree_jet_eta(reader, "m_tree_jet_eta");
  TTreeReaderValue<double> m_tree_jet_rpt(reader, "m_tree_jet_rpt");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_truth_time(
      reader, "m_tree_jet_hgtd_rpt_trutht0_truth_time");
  TTreeReaderValue<double> m_tree_jet_hgtd_rpt_trutht0_reco_time(
      reader, "m_tree_jet_hgtd_rpt_trutht0_reco_time");
  // TTreeReaderValue<double> m_tree_jet_hgtd_rpt_smearedt0_reco_time(reader,
  // "m_tree_jet_hgtd_rpt_smearedt0_reco_time");
  TTreeReaderValue<double> m_tree_jet_subjet_rpt(reader,
                                                 "m_tree_jet_subjet_rpt");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times_truth_trk(
      reader, "m_tree_cluster_ntracks");
  TTreeReaderArray<double> m_tree_jet_hgtd_rpt_cluster_times(
      reader, "m_tree_jet_hgtd_rpt_cluster_times");

  double jeteta_min = 2.4;
  double jeteta_max = 4.0;

  cout << "starting loop\n";
  while (reader.Next()) {

    // if (*m_tree_deltaz_vx_truthvx>=0.1) {continue;}
    double pt = *m_tree_jet_pt;
    // use only jets over 30GeV
    if (pt < 50.) {
      continue;
    }
    double eta = *m_tree_jet_eta;
    double abs_eta = fabs(eta);
    if (abs_eta < jeteta_min or abs_eta > jeteta_max) {
      continue;
    }
    // retrieve the ITk only RpT variable for this jet
    double std_rpt = *m_tree_jet_rpt;
    int category = *m_tree_jet_category;
    // this is how the jet categories are defined
    bool ishs = (category == 0);
    bool ispu = (category > 0 and category < 4);
    // bool ispu = (category>0);
    fillRpTEffCurves(std_rpt, m_eff_std_rpt, category, pt);
    fillRpTEffCurves(std_rpt, m_eff_std_rpt_variablebin, category, pt);
    /////////////////

    // cout << "get truth cluster\n";
    // get the cluster with the highest HS fraction, but > 0.5
    double current_fraction = 0;
    int truth_cluster_position = -1;
    int n_clusters = m_tree_cluster_hs_fraction.GetSize();
    for (int i = 0; i < n_clusters; i++) {
      if (m_tree_cluster_hs_fraction.At(i) > 0.5 and
          m_tree_cluster_hs_fraction.At(i) > current_fraction and
          m_tree_cluster_ntracks.At(i) >= 3) {
        current_fraction = m_tree_cluster_hs_fraction.At(i);
        truth_cluster_position = i;
      }
    }
    //
    double btc_rpt = -1;
    double subjetRpT = *m_tree_jet_subjet_rpt;
    fillRpTEffCurves(subjetRpT, m_eff_subjet_rpt, category, pt);
    fillRpTEffCurves(subjetRpT, m_eff_subjet_rpt_variablebin, category, pt);

    if (truth_cluster_position > 0) {
      btc_rpt = m_tree_jet_hgtd_rpt_cluster_times.At(truth_cluster_position);
      fillRpTEffCurves(btc_rpt, m_eff_bestTruthHSCluster_rpt, category, pt);
      fillRpTEffCurves(btc_rpt, m_eff_bestTruthHSClusterSubjetRpT, category,
                       pt);

    } else {
      fillRpTEffCurves(std_rpt, m_eff_bestTruthHSCluster_rpt, category, pt);
      fillRpTEffCurves(subjetRpT, m_eff_bestTruthHSClusterSubjetRpT, category,
                       pt);
    }

    // cout << "get BDT cluster\n";
    ////////////////////////////////////////
    // get the cluster from BDT evaluation
    bool cluster_accepted = false;
    int cluster_position = -1;
    double max_bdt_value = -1;
    for (int i = 0; i < n_clusters; i++) {
      m_delta_z = m_tree_cluster_delta_z.At(i);
      m_z_sigma = m_tree_cluster_z0_sigma.At(i);
      m_q_over_p = m_tree_cluster_q_over_p.At(i);
      m_q_over_p_sigma = m_tree_cluster_q_over_p_sigma.At(i);
      m_delta_z_resunits = m_tree_cluster_delta_z_resunit.At(i);
      m_cluster_sumpt2 = m_tree_cluster_sumpt2.At(i);
      m_d0 = m_tree_cluster_d0.At(i);
      m_d0_sigma = m_tree_cluster_d0_sigma.At(i);
      m_number_of_tracks = m_tree_cluster_ntracks.At(i);
      m_z0_stddev = m_tree_cluster_z0_stddev.At(i);
      m_z0_mean = m_tree_cluster_z0_mean.At(i);
      m_closest_vx_is_pvx = m_tree_cluster_closest_vx_is_pvx.At(i);
      m_distance_pu_vx = m_tree_cluster_distance_pu_vx.At(i);
      m_n_pu_vertices = m_tree_cluster_n_pu_vertices.At(i);
      m_pt_mod = m_tree_cluster_pt_mod.At(i);

      float bdt_output = tmva_reader->EvaluateMVA("BDT");

      if (bdt_output > cutvalue and bdt_output > max_bdt_value and
          m_number_of_tracks >= 3) {
        // cout << "found good cluster at " << i << " with BDT op " <<
        // bdt_output << '\n';
        max_bdt_value = bdt_output;
        cluster_position = i;
        cluster_accepted = true;
      }
    } // LOOP over clusters in event

    ////////////////////////////////////////
    // cout << "and into the histos\n";
    if (cluster_accepted) {

      // cout << "curr i " << cluster_position << '\n';
      // cout << "max bdt val " << max_bdt_value << '\n';
      double bdt_selected_cluster_rpt =
          m_tree_jet_hgtd_rpt_cluster_times.At(cluster_position);
      fillRpTEffCurves(bdt_selected_cluster_rpt, m_eff_BDTClusterOrStdRpT,
                       category, pt);
      fillRpTEffCurves(bdt_selected_cluster_rpt,
                       m_eff_BDTClusterOrStdRpT_variablebin, category, pt);

      fillRpTEffCurves(bdt_selected_cluster_rpt, m_eff_BDTClusterOrSubjetRpT,
                       category, pt);
      fillRpTEffCurves(bdt_selected_cluster_rpt,
                       m_eff_BDTClusterOrSubjetRpT_variablebin, category, pt);

    } else {
      fillRpTEffCurves(std_rpt, m_eff_BDTClusterOrStdRpT, category, pt);
      fillRpTEffCurves(std_rpt, m_eff_BDTClusterOrStdRpT_variablebin, category,
                       pt);
      fillRpTEffCurves(subjetRpT, m_eff_BDTClusterOrSubjetRpT, category, pt);
      fillRpTEffCurves(subjetRpT, m_eff_BDTClusterOrSubjetRpT_variablebin,
                       category, pt);
    }

  } // LOOP over jets

  // get the improvement at 85% HS efficiency
  std::vector<TObject *> garbage;
  TString template_name = "m_eff_%s_%s_pt_%s";
  TString pt_range = "high";

  vector<TString> names = {"StandardRpTVarBin", "BDTClusterOrSubjetRpTVarBin"};

  vector<TString> eff1_names;
  vector<TString> eff2_names;
  for (const auto &name : names) {
    eff1_names.push_back(
        Form(template_name, "hs", pt_range.Data(), name.Data()));
    eff2_names.push_back(
        Form(template_name, "pu", pt_range.Data(), name.Data()));
  }

  // get the roc curves for all
  vector<TGraphErrors *> graphs;
  for (size_t i = 0; i < eff1_names.size(); i++) {
    auto eff1 = (TEfficiency *)gDirectory->Get(eff1_names.at(i));
    auto eff2 = (TEfficiency *)gDirectory->Get(eff2_names.at(i));

    auto graph = getROCcurve(eff1, eff2);
    garbage.push_back(graph);
    graphs.push_back(graph);
  }
  TGraph *ratio_graph = getRatioGraph(graphs.at(0), graphs.at(1));
  // garbage.push_back(ratio_graph);

  float improvement = ratio_graph->Eval(0.85);

  for (int i = 0; i < garbage.size(); i++) {
    delete garbage.at(i);
  }

  for (int i = 0; i < write.size(); i++) {
    delete write.at(i);
  }

  return improvement;
  // return ratio_graph;
}

void optimizeBDTCut() {
  auto chain = loadChain();
  float cut_min = 0.3;
  float cut_max = 0.4;
  float cut_stepsize = 0.02;

  vector<float> x;
  vector<float> y;
  for (float cut_val = cut_min; cut_val <= cut_max; cut_val += cut_stepsize) {
    float improvement = run(chain, cut_val);
    cout << "cut " << cut_val << " improvement " << improvement << '\n';
    x.push_back(cut_val);
    y.push_back(improvement);
  }

  TGraph *gr = new TGraph(x.size(), &x[0], &y[0]);
  auto file = TFile::Open("optimizeBDTCut.root", "RECREATE");
  gr->Write();
  file->Close();
}

////////////////////////////////
TGraphErrors *getROCcurve(TEfficiency *eff1, TEfficiency *eff2) {
  std::vector<double> eff;
  std::vector<double> eff_err;
  std::vector<double> rej;
  std::vector<double> rej_err;
  for (int i = 0; i < eff1->CreateGraph()->GetN(); i++) {
    if (eff1->GetEfficiency(i) == 0) {
      continue;
    }
    if (eff2->GetEfficiency(i) == 0) {
      continue;
    }
    eff.push_back(eff1->GetEfficiency(i));
    rej.push_back(1. / eff2->GetEfficiency(i));
    // if (eff1->GetEfficiencyErrorLow(i) != eff1->GetEfficiencyErrorUp(i)) {
    //   cout << "getROCcurve, errors are not the same for up and low\n";
    //   cout << eff1->GetEfficiencyErrorLow(i) << '\n';
    //   cout << eff1->GetEfficiencyErrorUp(i) << '\n';
    // }
    eff_err.push_back(
        max(eff1->GetEfficiencyErrorLow(i), eff1->GetEfficiencyErrorUp(i)));
    rej_err.push_back(
        max(eff2->GetEfficiencyErrorLow(i), eff2->GetEfficiencyErrorUp(i)) *
        fabs(1. / (eff2->GetEfficiency(i) * eff2->GetEfficiency(i))));
  }
  TGraphErrors *graph =
      new TGraphErrors(eff.size(), &eff[0], &rej[0], &eff_err[0], &rej_err[0]);
  return graph;
}

TGraph *getRatioGraph(TGraph *numerator, TGraph *denominator) {
  // int n = graphs.at(0)->GetN();
  int n = 10;
  double step = 0.2 / (double)n;
  TGraph *r = new TGraph();
  r->SetTitle("");
  int skip = 5;
  vector<double> xvals;
  for (int i = 0; i < n; i++) {
    xvals.push_back(0.8 + i * step);
  }

  for (int i = 0; i < n; i++) {
    if (xvals.at(i) > 0.96) {
      continue;
    }
    double y1, y2;
    y1 = numerator->Eval(xvals.at(i));
    y2 = denominator->Eval(xvals.at(i));
    r->SetPoint(i + 1, xvals.at(i), y2 / y1);
  }
  r->GetXaxis()->SetLabelSize(0.075);
  r->GetYaxis()->SetLabelSize(0.075);
  r->SetLineColor(denominator->GetLineColor());
  r->SetLineStyle(denominator->GetLineStyle());
  r->SetLineWidth(denominator->GetLineWidth());
  return r;
}

#include "TFile.h"

#include <fstream>
#include <string>
#include <iostream>

std::vector <TString> getListOfFiles(TString path_to_list) {
  std::vector <TString> files_list; //stores the paths of the accepted rootfiles

  TFile *root_file; //used to check if TFile is Zombie or not
  std::string line;
  TString buff;
  std::ifstream input_file(path_to_list.Data());

  if (input_file.is_open()) {
    while ( getline (input_file,line) )
    {
      //check if files is OK, skip if not
      buff = line.c_str();
      if (not buff.EndsWith(".root")) {continue;}
      root_file = TFile::Open(line.c_str(), "read");
      if (not root_file) {
        std::cout << "GetTChain::getListOfFiles: WARNING - file " << line << " can't be read." << std::endl;
        continue;
      }
      if(root_file->IsZombie()) {
        root_file->Close();
        continue;
      } else {
        root_file->Close();
        files_list.push_back(TString(line.c_str()));
      }
    }
    input_file.close();
  } else {
    std::cout << "GetTChain::getListOfFiles: file " << path_to_list.Data() << " does not exist." << std::endl; //error message if there is an issue with the INPUT_LIST.txt file
  }

  return files_list;
}

std::vector <TString> getListOfFiles(TString path_to_list, int max_files) {
  std::vector <TString> files_list; //stores the paths of the accepted rootfiles
  int count(0);
  TFile *root_file; //used to check if TFile is Zombie or not
  std::string line;
  TString buff;
  std::ifstream input_file(path_to_list.Data());

  if (input_file.is_open()) {
    while ( getline (input_file,line) )
    {
      //check if files is OK, skip if not
      // buff = line.c_str();
      // root_file = TFile::Open(line.c_str(), "read");
      // if (not root_file) {
      //   std::cout << "GetTChain::getListOfFiles: WARNING - file " << line << " can't be read." << std::endl;
      //   continue;
      // }
      // if(root_file->IsZombie()) {
      //   root_file->Close();
      //   continue;
      // } else {
      //   root_file->Close();
      //   files_list.push_back(TString(line.c_str()));
      //   count++;
      //   if (max_files != -1 and count >= max_files) {return files_list;}
      // }
      ///THIS WHEN NO CHECKS
      files_list.push_back(TString(line.c_str()));
      count++;
      if (max_files != -1 and count >= max_files) {return files_list;}
    }
    input_file.close();
  } else {
    std::cout << "GetTChain::getListOfFiles: file " << path_to_list.Data() << " does not exist." << std::endl; //error message if there is an issue with the INPUT_LIST.txt file
  }

  return files_list;
}


TChain * getChain(std::vector <TString> files_list, TString tree_name, const int& n_files) {
  TChain *chain = new TChain(tree_name.Data());
  if (n_files == -1) {
  for (auto file : files_list) {
    chain->Add(file.Data());
  }
} else {
  int count = 0;
  auto itr = files_list.begin();
  while (count < n_files and itr!=files_list.end()) {
    chain->Add((*itr).Data());
    itr++;
    count++;
  }
}

  return chain;
}

TChain * getChain(TString file_name_reg_ex, TString tree_name) {
  TChain *chain = new TChain(tree_name.Data());
  chain->Add(file_name_reg_ex.Data());
  return chain;
}

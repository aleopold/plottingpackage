#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

#include "preparePaths.cxx"

TString g_input_file_path = "default";
TString g_output_plot_path = "default";
int g_skip = 0;
TString g_dataset_name = "default";
TString g_dataset_description = "default";
bool g_print_png = false;
TString g_work_status = "default";
bool g_use_log_scale = false;
bool g_do_zoom = false;
TString g_descr = "default";

TFile* g_file = nullptr;

// TString g_config_file_name = "muon45_mu0.cfg";
// TString g_config_file_name = "pion0p1to5p0_mu0.cfg";
TString g_config_file_name = "VBFinv_mu200.cfg";

TString selected_tracks = "ALL";

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
////////////          H E L P E R          ////////////

TH1F* turnGraphIntoHist(TGraph* graph, TString name, TString label, int bins, double xmin, double xmax, int color);

///////////////////////////////////////////////////////


void efficiencyStackPlotAllCases(TString accessor, TString track_type, TString vs_x, TString vs_x_bin, TString xlabel) {

  TString eff_name = Form("%s/%s_m_eff_vs_%s", accessor.Data(), track_type.Data(), vs_x.Data());
  auto eff = (TEfficiency*) g_file->Get(eff_name);
  if (eff==nullptr) {
    cout << "s_m_eff_vs_ histogram not found" << endl;
    return;
  }
  TGraphAsymmErrors* graph_total = eff->CreateGraph();
  graph_total->SetMarkerStyle(21);
  graph_total->SetMarkerSize(0);
  Color_t marker_color = kBlack;
  graph_total->SetMarkerColor(marker_color);
  graph_total->SetLineColor(marker_color);
  graph_total->SetLineWidth(2);

  TString name_template = "%s/%s_m_eff_vs_%s_primesfrac%s";

  // std::vector<TString> primes_fractions = {"AllPrimes", "MoreThanHalfPrimes", "HalfPrimesHasPrimes", "LessThanHalfPrimes","NoPrimesNoPossiblePrimes"};
  // std::vector<TString> parts_tobe_combined = {"NoPrimes1PossiblePrimes", "NoPrimes2PossiblePrimes", "NoPrimes3PossiblePrimes", "NoPrimes4PossiblePrimes"};
  // std::vector<TString> labels = {"Prime Frac. = 1", "0.5 < Prime Frac. < 1", "Prime Frac. = 0.5", "0 < Prime Frac. < 0.5", "Misassignment", "Confusion"};
  std::vector<TString> primes_fractions = {"AllPrimes", "MoreThanHalfPrimes", "HalfPrimesHasPrimes", "LessThanHalfPrimes","NoPrimesNoPossiblePrimes", "NoPrimes1PossiblePrimes", "NoPrimes2PossiblePrimes", "NoPrimes3PossiblePrimes", "NoPrimes4PossiblePrimes"};
  std::vector<TString> labels = {"Prime Frac. = 1", "0.5 < Prime Frac. < 1", "Prime Frac. = 0.5", "0 < Prime Frac. < 0.5", "Misassignment", "Confusion"};

  std::vector<Color_t> colors = {kTeal+4, kTeal+2, kTeal, kTeal-9, kMagenta, kRed, kRed, kRed, kRed};

  std::vector<TEfficiency*> effs;
  std::vector<TH1F*> hists;

  for(int i=0; i< primes_fractions.size(); i++) {
    TString plotname = Form(name_template, accessor.Data(), track_type.Data(), vs_x.Data(), primes_fractions.at(i).Data());
    auto eff = (TEfficiency*) g_file->Get(plotname);
    if (eff == nullptr) {
      std::cout << "efficiencyAndMistagLinePrimeFractionGT50 ERROR\n";
      std::cout << "could not find " << plotname << '\n';
      return;
    }
    eff->SetFillColor(colors.at(i));
    effs.push_back(eff);
  }

  //add up the misassigned parts
  // std::vector<TEfficiency*> effs_mis;
  // for(int i=0; i< parts_tobe_combined.size(); i++) {
  //   TString plotname = Form(name_template, accessor.Data(), track_type.Data(), vs_x.Data(), parts_tobe_combined.at(i).Data());
  //   auto eff = (TEfficiency*) g_file->Get(plotname);
  //   eff->SetFillColor(kRed);
  //   effs_mis.push_back(eff);
  // }
  // TEfficiency* combined_mis = effs_mis.at(0);
  // cout << "effs_mis.size() " << effs_mis.size() << '\n';
  // for (int i=1; i<effs_mis.size(); i++) {
  //   combined_mis->Add(*effs_mis.at(i));
  // }
  // effs.push_back(combined_mis);



  THStack* stack_hist = new THStack("stack_hist",";|#eta|; time association rate");

  for (int i=0; i<effs.size(); i++) {
    auto graph = effs.at(i)->CreateGraph();
    TH1F* hist = turnGraphIntoHist(graph, graph->GetName()+TString::Format("h%i", i), "", 16, 2.4, 4.0, effs.at(i)->GetFillColor());
    hist->SetFillColor(colors.at(i));
    hist->SetLineColor(colors.at(i));
    hists.push_back(hist);
    stack_hist->Add(hist);
  }

  TCanvas* canvas = new TCanvas();
  canvas->SetTicks(1,1);

  auto hist = new TH1F("axishist", Form(";%s; time association rate", xlabel.Data()), 10, 2.4, 4.0);
  hist->SetAxisRange(0., 1.4, "Y");
  hist->Draw();

  stack_hist->Draw("same");

  graph_total->Draw("same p");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.89, text_color);
  atlas::myText(0.31, 0.89, text_color, g_work_status.Data());

  TString dataset_descr = "VBF H #rightarrow invisible, #LT#mu#GT=200 ";
  if (g_config_file_name == "muon45_mu0.cfg") {
    // dataset_descr = "Single muon, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0";
    dataset_descr = "#mu^{+}, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0 ";
  } else if (g_config_file_name == "pion0p1to5p0_mu0.cfg") {
    // dataset_descr = "Single pion, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0";
    dataset_descr = "#pi^{+}, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0 ";
  }
  // atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  atlas::myText(0.19, 0.84, text_color, dataset_descr.Data(), 0.035);
  // myText(0.19, 0.79, text_color, "VBFinv, #langle#mu#rangle=200 ", 0.04);
  atlas::myText(0.19, 0.79, text_color, "p_{T}^{track} > 1.0 GeV", 0.035);
  atlas::myText(0.19, 0.74, text_color, "Timing scenario \"Initial\"", 0.035);

  auto legend = new TLegend(0.65, 0.73, 0.89, 0.92);

  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.025);

  legend->AddEntry(graph_total, "Total", "lp");
  for (int i=0; i<labels.size(); i++) {
    legend->AddEntry(hists.at(i), labels.at(i), "f");
  }

  legend->Draw("same");

  gPad->RedrawAxis();

  TString plot_name = Form("%s/efficiencyStackPlotAllCases2_%s_%s_%s_%s.pdf",  g_output_plot_path.Data(), track_type.Data(), vs_x.Data(), accessor.Data(),  g_dataset_name.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (g_print_png) canvas->Print(plot_name);

}



void effCurvesVersionsEta2(TString config_file_name = "") {
  if (config_file_name != "") {
    g_config_file_name = config_file_name;
  }
  TEnv env(g_config_file_name);

  g_input_file_path = env.GetValue("input_file_path_TTI", "default");
  g_output_plot_path = env.GetValue("output_plot_path", "default");
  g_dataset_name = env.GetValue("dataset_name", "default");
  g_dataset_description = env.GetValue("dataset_description", "My Sample");
  g_print_png = env.GetValue("print_png", false);
  g_do_zoom = env.GetValue("do_zoom", false);
  g_use_log_scale = env.GetValue("use_log_scale", false);
  g_work_status = env.GetValue("work_status", "default");

  g_file = TFile::Open(g_input_file_path, "READ");

  SetAtlasStyle();

  preparePlottingDir(g_config_file_name);


  efficiencyStackPlotAllCases("ProgressiveFilter_1", selected_tracks, "eta", "eta_0", "|#eta|");
  efficiencyStackPlotAllCases("PFTQv05", selected_tracks, "eta", "eta_0", "|#eta|");


  g_file->Close();
}


////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////


TH1F* turnGraphIntoHist(TGraph* graph, TString name, TString label, int bins, double xmin, double xmax, int color) {
  TH1F* hist = new TH1F(name, label, bins, xmin, xmax);
  for (int point=0; point<graph->GetN(); point++) {
    double x,y;
    graph->GetPoint(point, x, y);
    int bin_n = hist->FindFixBin(x);
    hist->SetBinContent(bin_n, y);
  }
  hist->SetFillColor(color);
  hist->SetLineWidth(0);
  hist->SetMarkerSize(0);
  hist->SetMarkerColor(color);
  return hist;
}

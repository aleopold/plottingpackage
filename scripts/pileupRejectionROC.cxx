#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

#include "preparePaths.cxx"

TString g_input_file_path = "/afs/cern.ch/user/t/tawang/public/PileupRejection_100k.root";
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
TString dataset_name = "VBFinv, mu=200";
TString descr = "pileupRejectionTruthT0_highpt";
TString dataset_description = "";
bool print_png = true;
bool g_use_log_scale = false;
TString work_status = "Internal";
bool g_do_ratioplot = true;
double x_eff_min = 0.80;
double legend_text_size = 0.05;

TFile* g_file = nullptr;

TString g_config_file_name = "pileup.cfg";

TGraphErrors* getROCcurve(TEfficiency* eff1, TEfficiency* eff2);

void decorateGraph(TGraphErrors* graph, int i, vector<int> colors);

TGraph* getRatioGraph(TGraph* numerator, TGraph* denominator);

void SetRatioPlotStyle();

TStyle* RatioPlotStyle();


void plotROC(const std::vector<TString>& names,
             const std::vector<TString>& labels,
             TString descr, double ratioplot_miny, double ratioplot_maxy,
           vector<int> colors = {kBlack, kCyan-3, kAzure-3, kRed-4, kMagenta, kGreen+1, kViolet+10}, vector<int> styles = {}) {

  std::vector<TObject*> garbage;
  TString template_name = "m_eff_%s_%s_pt_%s";

  std::vector<TString> pt_ranges = {"low", "high"};

  for (const auto& pt_range : pt_ranges) {
    vector<TString> eff1_names;
    vector<TString> eff2_names;
    for (const auto& name : names) {
      eff1_names.push_back(Form(template_name, "hs", pt_range.Data(), name.Data()));
      eff2_names.push_back(Form(template_name, "pu", pt_range.Data(), name.Data()));
    }

    //get the roc curves for all
    vector<TGraphErrors*> graphs;
    for(size_t i=0; i<eff1_names.size(); i++) {
      auto eff1 = (TEfficiency*) g_file->Get(eff1_names.at(i));
      auto eff2 = (TEfficiency*) g_file->Get(eff2_names.at(i));
      if (eff1 ==nullptr or eff2 == nullptr) {
        cout << "ERROR: " << eff1_names.at(i) << " does not exist, aborting" << '\n';
        return;
      }
      auto graph = getROCcurve(eff1, eff2);
      garbage.push_back(graph);
      decorateGraph(graph, i, colors);
      graphs.push_back(graph);
    }

    //*************//*************//*************//*************//
    //                    P L O T T I N G                       //
    //*************//*************//*************//*************//

    TCanvas *canvas = new TCanvas();
    garbage.push_back(canvas);
    TPad* p1 = nullptr;
    TPad* p2 = nullptr;
    if (g_do_ratioplot) {
      //lower pad
      p2 = new TPad("p2","p2",0, 0.05, 1, 0.35);
      p2->SetTopMargin(0);
      p2->SetBottomMargin(0.31);
      garbage.push_back(p2);
      // p2->UseCurrentStyle();
      p2->Draw();
      //upper pad
      p1 = new TPad("p1","p1",0.,0.35,1.,1.);
      p1->SetBottomMargin(0);
      garbage.push_back(p1);
      p1->Draw();
      p1->cd();
      if(g_use_log_scale) {
        p1->SetLogy();
      }

    } else {
      if(g_use_log_scale) {
        canvas->SetLogy();
      }

    }

    auto axishisto = new TH1F("", "", 10, x_eff_min, 1.);
    garbage.push_back(axishisto);

    double maxy = -1;
    for (auto graph : graphs) {
      double locy = graph->Eval(0.8);
      if (locy > maxy) {
        maxy = locy;
      }
    }

    if(g_use_log_scale) {
      axishisto->GetYaxis()->SetRangeUser(1, maxy*10.);
    } else {
      axishisto->GetYaxis()->SetRangeUser(0, maxy*1.5);
    }

    axishisto->GetXaxis()->SetTitle("HS efficiency");
    axishisto->GetYaxis()->SetTitle("PU rejection");
    axishisto->GetYaxis()->SetTitleSize (0.07);
    axishisto->GetYaxis()->SetTitleOffset(1.);
    axishisto->GetXaxis()->SetTickLength(0.035);
    axishisto->GetXaxis()->SetNdivisions(505);
    axishisto->Draw();

    axishisto->GetYaxis()->SetLabelSize(0);
    axishisto->GetYaxis()->SetTickLength(0.015);


    int pos = 0;
    for (auto graph : graphs) {
      if (styles.size() == graphs.size()) {
        graph->SetLineStyle(styles.at(pos));
        graph->SetLineWidth(3);
      }
      graph->Draw("LP same");
      pos++;
    }

    int maxy_gaxis;
    int starty_gaxis;
    // if (pt_range == "low") {
    //   maxy_gaxis = 160;
    //   starty_gaxis = 20;
    // } else {
    //   maxy_gaxis = 350;
    //   starty_gaxis = 50;
    // }
    //ttbar
    if (pt_range == "low") {
      maxy_gaxis = 60;
      starty_gaxis = 10;
    } else {
      maxy_gaxis = 120;
      starty_gaxis = 20;
    }

    // TGaxis *axis = new TGaxis( 0.8, 20, 0.8, maxy_gaxis, 20, maxy_gaxis, 510, "S");
    //ttbar
    // TGaxis *axis = nullptr;
    // if (pt_range == "low") {
    //   axis = new TGaxis( 0.8, 10, 0.8, 60, 10, 60, 505, "S");
    // } else {
    //   axis = new TGaxis( 0.8, 20, 0.8, 120, 20, 120, 505, "S");
    // }
    //dijet
    // TGaxis *axis = nullptr;
    // if (pt_range == "low") {
    //   axis = new TGaxis( 0.8, 20, 0.8, 160, 20, 160, 510, "S");
    // } else {
    //   axis = new TGaxis( 0.8, 50, 0.8, 350, 50, 350, 510, "S");
    // }
    //VBFinv
    TGaxis *axis = nullptr;
    if (pt_range == "low") {
      axis = new TGaxis( 0.8, 20, 0.8, 160, 20, 160, 510, "S");
    } else {
      axis = new TGaxis( 0.8, 20, 0.8, 150, 20, 150, 510, "S");
    }



    axis->SetLabelFont(42); //Absolute font size in pixel (precision 3)
    axis->SetTickSize(0);
    axis->SetLabelSize(0.08);
    axis->Draw("same");

    // TLegend* legend = new TLegend(0.65, 0.93, 0.95, 0.75);
    TLegend* legend = new TLegend(0.55, 0.93, 0.95, 0.65);
    garbage.push_back(legend);
    legend->SetBorderSize(0);
    legend->SetTextSize(legend_text_size);
    legend->SetTextFont(42);
    legend->SetFillStyle(0);

    Color_t text_color = kBlack;
    atlas::ATLAS_LABEL(0.19, 0.86, text_color, 1.4);
    atlas::myText(0.3, 0.86, text_color, work_status.Data(), 0.07);

    TString buffer_dataset_description = dataset_description;
    buffer_dataset_description.ReplaceAll("$", "#");
    atlas::myText(0.19, 0.81, text_color, buffer_dataset_description.Data(), 0.06);
    if (pt_range == "low") {
      // atlas::myText(0.19, 0.74, text_color, "30 GeV < p_{T}^{jet} < 50 GeV", 0.05);
      atlas::myText(0.19, 0.74, text_color, "30 GeV < p_{T}^{jet} < 50 GeV", 0.06);
    } else  {
      // atlas::myText(0.19, 0.74, text_color, "50 GeV < p_{T}^{jet} < 100 GeV", 0.05);
      // atlas::myText(0.19, 0.74, text_color, "50 GeV < p_{T}^{jet} < 100 GeV", 0.05);
      atlas::myText(0.19, 0.74, text_color, "p_{T}^{jet} > 50 GeV", 0.06);
    }
    atlas::myText(0.19, 0.67, text_color, "2.4 < |#eta_{jet}| < 4.0", 0.06);

    p1->RedrawAxis();


    for (int i=0; i<graphs.size(); i++) {
      legend->AddEntry(graphs.at(i), labels.at(i), "pl");
    }

    legend->Draw("same");

    if (g_do_ratioplot) {
      p2->cd();
      auto axishisto2 = new TH1F("", ";HS efficiency;ratio ", 10, x_eff_min, 1.);
      garbage.push_back(axishisto2);
      axishisto2->GetYaxis()->SetRangeUser(ratioplot_miny, ratioplot_maxy);
      // axishisto2->GetYaxis()->SetTitle("ratio");
      axishisto2->GetYaxis()->SetNdivisions(502);
      axishisto2->GetYaxis()->SetTitleOffset(0.45);
      axishisto2->GetYaxis()->SetTitleSize(0.16); // labels will be 14 pixels
      axishisto2->GetYaxis()->SetLabelSize(0.15); // labels will be 14 pixels
      axishisto2->GetYaxis()->SetTickLength (0.02);
      // axishisto2->GetXaxis()->SetLabelSize(0.1); // labels will be 14 pixels
      // axishisto2->GetXaxis()->SetTitle("HS efficiency");
      axishisto2->GetXaxis()->SetNdivisions(505);
      axishisto2->GetXaxis()->SetTitleOffset(0.95);
      axishisto2->GetXaxis()->SetTitleSize(0.16); // labels will be 14 pixels
      axishisto2->GetXaxis()->SetLabelSize(0.15); // labels will be 14 pixels
      axishisto2->GetXaxis()->SetTickLength (0.08);
      axishisto2->Draw();

      auto line = new TLine(0.8, 1.0, 1.0, 1.0);
      garbage.push_back(line);
      line->SetLineStyle(2);
      line->Draw("same");

      for (int i=1; i<graphs.size(); i++) {
        TGraph* ratio_graph = getRatioGraph(graphs.at(0), graphs.at(i));
        ratio_graph->Draw("L same");
        garbage.push_back(ratio_graph);
      }
      p2->RedrawAxis();
    }

    TString plot_name = Form("%s/%s_%s_pt_%s.pdf",  output_plot_path.Data(), dataset_name.Data(), pt_range.Data(), descr.Data());
    if (g_use_log_scale) {
      plot_name.ReplaceAll(".pdf", "_logscale.pdf");
    }
    canvas->Print(plot_name);
    plot_name.ReplaceAll(".pdf", ".png");
    if (print_png) canvas->Print(plot_name);

    // for (auto& trash : garbage) {
      // delete trash;
    // }

  }//loop over pt ranges

}

void pileupRejectionROC () {

  TEnv env(g_config_file_name);

  g_input_file_path = env.GetValue("input_file_path", "default");
  g_do_ratioplot = env.GetValue("do_ratioplot", false);
  x_eff_min = env.GetValue("x_eff_min", 0.8);
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  dataset_description = env.GetValue("dataset_description", "default");
  g_use_log_scale = env.GetValue("use_log_scale", false);
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");

  g_file = TFile::Open(g_input_file_path, "READ");

  // SetAtlasStyle();
  SetRatioPlotStyle();

  preparePlottingDir(g_config_file_name);

  // plotROC({"StandardRpT", "TruthVertexTruthTimePrimes"}, {"RpT (ITk)", "HGTDRpT truth t0 + truth track time (ITk+HGTD)"}, "TruthT0StandardVsPerfect", 0.9, 2.5, {kBlack, kCyan-3});
  //
  // //THIS IS FOR PFTQv04!!! naming was not adapted in TruthVertexPFORv02
  // plotROC({"StandardRpT", "TruthVertexPFORv02"}, {"RpT (ITk)", "HGTDRpT truth t0 + reco track time (ITk+HGTD)"}, "TruthT0StandardVsPFQTv04", 0.9, 2.5, {kBlack, kCyan-3});
  //
  // plotROC({"StandardRpT", "TruthVertexProgFilt"}, {"RpT (ITk)", "HGTDRpT truth t0 + reco track time (ITk+HGTD)"}, "TruthT0StandardVsTruthVertexProgFilt", 0.9, 2.5, {kBlack, kCyan-3});
  //
  // plotROC({"TruthVertexPFORv02", "TruthVertexProgFilt"}, {"HGTDRpT truth t0 + cleaned reco track time (ITk+HGTD)", "HGTDRpT truth t0 + default reco track time (ITk+HGTD)"}, "TruthT0PFQTv04VsProgFilt", 0.8, 1.1, {kRed, kGreen});
  //
  // plotROC({"TruthVertexTruthTimePrimes", "TruthVertexPFORv02"}, {"HGTDRpT truth t0 + truth track time (ITk+HGTD)", "HGTDRpT truth t0 + reco track time (ITk+HGTD)"}, "TruthT0PerfectVsPFQTv04", 0.5, 1.1, {kCyan-3, kAzure-3});
  //
  // plotROC({"StandardRpT", "SubJetRpTReco"}, {"RpT (ITk)", "subjetRpT (ITk+HGTD)"}, "StandardVsSubjetRpT", 0.9, 1.4, {kBlack, kMagenta});
  //
  // //THIS IS FOR PFTQv04!!! naming was not adapted in TruthVertexPFORv02
  // plotROC({"StandardRpT", "RecoVertexTimePF", "RecoVertexTimev02PFORv02"}, {"RpT (ITk)", "HGTDRpT, default PF (ITk+HGTD)", "HGTDRpT, cleaned PF (ITk+HGTD)"}, "StandardVsHGTDRpTRecot0", 0.9, 1.4, {kBlack, kRed, kMagenta});

  // plotROC({"StandardRpT", "TruthVertexTruthTimePrimesGoodHSC", "TruthVertexTruthTimePrimesBadHSC"}, {"RpT (ITk)", "truth t0, truth trk time, found HS cluster (ITk+HGTD)", "truth t0, truth trk time, no HS cluster (ITk+HGTD)"}, "StandardVsPerfectHSCluster", 0.9, 10.0, {kBlack, kBlue, kRed});

  // plotROC({"StandardRpTGoodHSC", "TruthVertexTruthTimePrimesGoodHSC"}, {"RpT (ITk)", "truth t0, truth trk time, found HS cluster (ITk+HGTD)"}, "StandardVsPerfectHSClusterGoodHSC", 0.9, 6.0, {kBlack, kBlue});
  //
  // plotROC({"StandardRpTBadHSC", "TruthVertexTruthTimePrimesBadHSC"}, {"RpT (ITk)", "truth t0, truth trk time, no HS cluster (ITk+HGTD)"}, "StandardVsPerfectHSClusterBadHSC", 0.9, 6.0, {kBlack, kRed});

  // plotROC({"StandardRpT", "TruthVertexTruthTimePrimesBadHSC"}, {"RpT (ITk)", "truth t0, truth trk time, no HS cluster (ITk+HGTD)"}, "StandardVsTCt0", 0.9, 1.0, {kBlack, kRed});

  // const static int n_workpoints = 26;
  // std::array<float, n_workpoints> m_hsc_eff{{0.2,  0.3,  0.4,  0.5,  0.6,   0.7,  0.8, 0.8,  0.8, 0.8,  0.8,  0.7, 0.7,  0.7, 0.7,  0.7,  0.6, 0.6,  0.6, 0.6,  0.6, 0.5, 0.5,  0.5, 0.5, 1.0}};
  // std::array<float, n_workpoints> m_hsc_pur {{0.98, 0.97, 0.96, 0.95, 0.925, 0.87, 0.8, 0.85, 0.9, 0.95, 0.98, 0.8, 0.85, 0.9, 0.95, 0.98, 0.8, 0.85, 0.9, 0.95, 0.98, 0.8, 0.85, 0.9, 0.98, 1.0}};

  // const static int n_workpoints = 1;
  // std::array<float, n_workpoints> m_hsc_eff{{1.0}};
  // std::array<float, n_workpoints> m_hsc_pur {{1.0}};

  // std::string plot_name("");
  // for (int i=0; i<n_workpoints; i++) {
  //   plot_name = "m_eff_rpt_hscluster_" + std::to_string(m_hsc_eff.at(i));
  //   plot_name += "_" + std::to_string(m_hsc_pur.at(i));
  //   plotROC({"StandardRpT", TString(plot_name)}, {"RpT (ITk)", "HS cluster, reco trk time (ITk+HGTD)"}, "StandardVs"+TString(plot_name), 0.9, 1.7, {kBlack, kBlue});
  //
  //   plot_name = "m_eff_rpt_hscluster_truth_prime_time_" + std::to_string(m_hsc_eff.at(i));
  //   plot_name += "_" + std::to_string(m_hsc_pur.at(i));
  //   plotROC({"StandardRpT",  TString(plot_name)}, {"RpT (ITk)", "HS cluster, truth trk time (ITk+HGTD)"}, "StandardVs"+TString(plot_name), 0.9, 1.7, {kBlack, kBlue});
  // }

  // plotROC({"StandardRpT", "BestTruthCluster", "BestTruthClusterOrSubjetRpT"}, {"RpT (ITk)", "best truth cluster (ITk+HGTD)", "best truth cluster + subJetRpT (ITk+HGTD)"}, "TruthInfoPUrej", 0.9, 1.5, {kBlack, kBlue, kRed});
  //
  // plotROC({"StandardRpT", "BDTClusterOrStdRpT", "BDTClusterOrSubjetRpT"}, {"RpT (ITk)", "best BDT cluster (ITk+HGTD)", "best BDT cluster + subJetRpT (ITk+HGTD)"}, "BDTPuRejection", 0.9, 1.5, {kBlack, kBlue, kRed});
  //
  //////THIS IS JUST FOR VALDATING OLD RESULTS
  // plotROC({"StandardRpT", "SubJetRpT"}, {"RpT (ITk)", "subJetRpT (ITk+HGTD)"}, "SubJetRpTFULLSTATS", 0.9, 1.4, {kBlack, kGreen});
  //
  // plotROC({"StandardRpT", "BDTClusterOrStdRpT", "BDTClusterOrSubjetRpT", "SubJetRpT"}, {"RpT (ITk)", "best BDT cluster (ITk+HGTD)", "best BDT cluster + subJetRpT (ITk+HGTD)", "subJetRpT (ITk+HGTD)"}, "AllCombinedFULLSTATS", 0.9, 1.5, {kBlack, kBlue, kRed, kGreen});


  ////THIS FOR THE NON TRAINING PART
  // plotROC({"StandardRpT", "BDTClusterOrStdRpT", "BDTClusterOrSubjetRpT", "SubJetRpT"}, {"RpT (ITk)", "best BDT cluster (ITk+HGTD)", "best BDT cluster + subJetRpT (ITk+HGTD)", "subJetRpT (ITk+HGTD)"}, "AllCombinedValidation", 0.9, 1.6, {kBlack, kBlue, kRed, kGreen});

  // plotROC({"StandardRpT", "BDTClusterOrStdRpT", "BDTClusterOrSubjetRpT"}, {"ITk", "ITk+HGTD (t_{0})", "ITk+HGTD (t_{0}+self-tagging)"}, "ShowImprovements", 0.9, 1.6, {kBlack, kBlue, kRed});


  // plotROC({"StandardRpT", "BDTClusterOrStdRpT", "BDTClusterOrSubjetRpT", "SubJetRpT"}, {"ITk", "ITk+HGTD (t_{0})", "ITk+HGTD (t_{0}+self-tagging)", "ITk+HGTD (self-tagging)"}, "ShowImprovementsAndSubjetRpT", 0.9, 2.0, {kBlack, kBlue, kRed, kGreen+2}, {1, 1, 1, 2});

  // plotROC({"StandardRpTVarBinVarBin", "BDTClusterOrStdRpTVarBin", "BDTClusterOrSubjetRpTVarBin"}, {"ITk", "ITk+HGTD (t_{0})", "ITk+HGTD (t_{0}+self-tagging)"}, "ShowImprovementsVarBin", 0.9, 1.6, {kBlack, kBlue, kRed});


  // plotROC({"StandardRpTVarBin", "BDTClusterOrStdRpTVarBin", "BDTClusterOrSubjetRpTVarBin", "SubJetRpTVarBin"}, {"ITk", "ITk+HGTD (t_{0})", "ITk+HGTD (t_{0}+self-tagging)", "ITk+HGTD (self-tagging)"}, "ShowImprovementsAndSubjetRpTVarBin", 0.9, 1.8, {kBlack, kBlue, kRed, kGreen+2}, {1, 1, 1, 2});

  plotROC({"StandardRpTVarBin", "SubJetRpTVarBin", "BDTClusterOrStdRpTVarBin", "BDTClusterOrSubjetRpTVarBin"}, {"ITk", "ITk+HGTD (self-tagging only)", "ITk+HGTD (t_{0} only)", "ITk+HGTD "}, "ShowImprovementsAndSubjetRpTVarBin", 0.9, 1.6, {kBlack, kGreen+2, kBlue, kRed}, {9, 8, 3, 1});
  //
  // plotROC({"StandardRpTVarBin", "TruthT0TruthTrkTimeVarBin", "TruthT0RecoTrkTimeVarBin"}, {"ITk", "ITk+HGTD (truth t_{0}, truth trk)", "ITk+HGTD (truth t_{0}, reco trk)"}, "BestCaseVarBin", 0.9, 6.0, {kBlack, kBlue, kRed, kGreen+2}, {1, 1, 1, 2});
  //
  // plotROC({"StandardRpT", "TruthT0TruthTrkTime", "TruthT0RecoTrkTime"}, {"ITk", "ITk+HGTD (truth t_{0}, truth trk)", "ITk+HGTD (truth t_{0}, reco trk)"}, "BestCase", 0.9, 6.0, {kBlack, kBlue, kRed, kGreen+2}, {1, 1, 1, 2});


  // plotROC({"StandardRpT", "TruthT0TruthTrkTime", "TruthT0RecoTrkTime", "TruthT0RecoTrkTime75eff", "BDTClusterOrSubjetRpT"}, {"ITk", "ITk+HGTD (truth t_{0}, truth trk)", "ITk+HGTD (truth t_{0}, reco trk)", "ITk+HGTD (BDT eff truth t_{0}, reco trk)", "ITk+HGTD (reco t_{0}, reco trk)"}, "ComparisonFromBestToWorst", 0.9, 3.5, {kBlack, kRed, kBlue, kGray, kMagenta+3}, {1, 1, 1, 1, 3});

  // plotROC({"StandardRpT", "TruthT0TruthTrkTime", "TruthT0RecoTrkTime", "TruthT0RecoTrkTime75eff", "SmearedTruthT0RecoTrkTime75eff", "BDTClusterOrSubjetRpT"}, {"ITk", "ITk+HGTD (truth t_{0}, truth trk)", "ITk+HGTD (truth t_{0}, reco trk)", "ITk+HGTD (BDT eff truth t_{0}, reco trk)", "ITk+HGTD (BDT eff smeared truth t_{0}, reco trk)", "ITk+HGTD (reco t_{0}, reco trk)"}, "ComparisonFromBestToWorst2", 0.9, 3.5, {kBlack, kRed, kBlue, kGray, kGreen, kMagenta+3}, {1, 3, 3, 3, 3, 1});

  //
  // plotROC({"StandardRpT", "BDTClusterOrStdRpT", "BDTClusterOrSubjetRpT", "SubJetRpT", "BestTruthCluster", "BestTruthClusterOrSubjetRpT"}, {"RpT (ITk)", "best BDT cluster (ITk+HGTD)", "best BDT cluster + subJetRpT (ITk+HGTD)", "subJetRpT (ITk+HGTD)",  "TruthInfo (ITk+HGTD)", "TruthInfo + subJetRpT (ITk+HGTD)"}, "AllCombinedValidationCompareWithTruth", 0.9, 1.7, {kBlack, kBlue, kRed, kGreen, kGray, kOrange});
  //
  //
  // std::vector<Color_t> colors = {kCyan+3, kCyan+2, kCyan+1, kRed-2, kPink-4};
  // plotROC({"StdRpTtype1", "BDTRpTtype1"}, {"RpT (ITk)", "BDT RpT (ITk+HGTD)"}, "ComparisonType1", 0.9, 5, {kBlack, colors.at(0)});
  // plotROC({"StdRpTtype2", "BDTRpTtype2"}, {"RpT (ITk)", "BDT RpT (ITk+HGTD)"}, "ComparisonType2", 0.9, 5, {kBlack, colors.at(1)});
  // plotROC({"StdRpTtype3", "BDTRpTtype3"}, {"RpT (ITk)", "BDT RpT (ITk+HGTD)"}, "ComparisonType3", 0.9, 5, {kBlack, colors.at(2)});
  // plotROC({"StdRpTtype4", "BDTRpTtype4"}, {"RpT (ITk)", "BDT RpT (ITk+HGTD)"}, "ComparisonType4", 0.1, 1.5, {kBlack, colors.at(3)});
  // plotROC({"StdRpTtype5", "BDTRpTtype5"}, {"RpT (ITk)", "BDT RpT (ITk+HGTD)"}, "ComparisonType5", 0., 1., {kBlack, colors.at(4)});
  // //
  // plotROC({"NoPerfectButBDTStdComparison", "NoPerfectButBDT"}, {"RpT (ITk)", "BDT RpT (ITk+HGTD)"}, "CheckingTheNonPerfectButBDTbin", 0.9, 1.2, {kBlack, kRed});
  //
  // plotROC({"YesPerfectButBDTStdComparison", "YesPerfectButBDT", "YesPerfectButBDTPerfectComparison"}, {"RpT (ITk)", "BDT RpT (ITk+HGTD)",  "TruthInfo (ITk+HGTD)"}, "CheckingThePerfectButBDTbin", 0.9, 1.5, {kBlack, kRed, kMagenta});

  // plotROC({"StandardRpTVarBin"}, {"ITk"}, "VarBins", 0.9, 1.1, {kBlack, kBlue, kRed});

  // plotROC({"StandardRpT", "TruthT0TruthTrkTime"}, {"ITk", "ITk+HGTD (truth t_{0}, truth trk)"}, "BestCaseTruthOnly", 0.9, 6.0, {kBlack, kBlue, kRed, kGreen+2}, {1, 1, 1, 2});

  g_file->Close();
}





////////////////////////////////
TGraphErrors* getROCcurve(TEfficiency* eff1, TEfficiency* eff2) {
  std::vector<double> eff;
  std::vector<double> eff_err;
  std::vector<double> rej;
  std::vector<double> rej_err;
  for (int i=0; i<eff1->CreateGraph()->GetN() ; i++) {
    if (eff1->GetEfficiency(i) == 0) {continue;}
    if (eff2->GetEfficiency(i) == 0) {continue;}
    eff.push_back(eff1->GetEfficiency(i));
    rej.push_back(1./eff2->GetEfficiency(i));
    // if (eff1->GetEfficiencyErrorLow(i) != eff1->GetEfficiencyErrorUp(i)) {
    //   cout << "getROCcurve, errors are not the same for up and low\n";
    //   cout << eff1->GetEfficiencyErrorLow(i) << '\n';
    //   cout << eff1->GetEfficiencyErrorUp(i) << '\n';
    // }
    eff_err.push_back(max(eff1->GetEfficiencyErrorLow(i), eff1->GetEfficiencyErrorUp(i)));
    rej_err.push_back(max(eff2->GetEfficiencyErrorLow(i), eff2->GetEfficiencyErrorUp(i))*fabs(1./(eff2->GetEfficiency(i)*eff2->GetEfficiency(i))));
  }
  TGraphErrors *graph = new TGraphErrors(eff.size() , &eff[0], &rej[0], &eff_err[0], &rej_err[0]);
  return graph;
}

void decorateGraph(TGraphErrors* graph, int i, vector<int> colors) {
  int pos = i;
  if (i>colors.size()) {
    pos = i%colors.size();
  }
  graph->SetMarkerColor(colors.at(pos) + i/colors.size());
  graph->SetMarkerSize(0.5);
  graph->SetLineColor(colors.at(pos) + i/colors.size());
  graph->SetLineWidth(1);
}

// TGraph* getRatioGraph(TGraph* numerator, TGraph* denominator) {
//   // int n = graphs.at(0)->GetN();
//   int n = numerator->GetN();
//   TGraph* r = new TGraph(n);
//   r->SetTitle("");
//   int skip = 5;
//   for (int i=skip; i<n; i++) {
//     double x, y;
//     numerator->GetPoint(i, x, y);
//     // y = graphs.at(ratio_plot_choice)->Eval(x)/y;
//     y = denominator->Eval(x)/y;
//     r->SetPoint(i-skip, x, y);
//     // double r_err =
//     // r->SetPointError(i, Double_t ex, Double_t ey);
//   }
//   r->GetXaxis()->SetLabelSize(0.075);
//   r->GetYaxis()->SetLabelSize(0.075);
//   r->SetLineColor(denominator->GetLineColor());
//   r->SetLineStyle(denominator->GetLineStyle());
//   r->SetLineWidth(denominator->GetLineWidth());
//   return r;
// }

TGraph* getRatioGraph(TGraph* numerator, TGraph* denominator) {
  // int n = graphs.at(0)->GetN();
  int n = 10;
  double step = 0.2/(double)n;
  TGraph* r = new TGraph();
  r->SetTitle("");
  int skip = 5;
  vector<double> xvals;
  for (int i=0; i<n; i++) {
    xvals.push_back(0.8 + i*step);
  }

  for (int i=0; i<n; i++) {
    if (xvals.at(i)>0.96) {continue;}
    double y1, y2;
    y1 = numerator->Eval(xvals.at(i));
    y2 = denominator->Eval(xvals.at(i));
    r->SetPoint(i+1, xvals.at(i), y2/y1);
  }
  // for (int i=skip; i<n; i++) {
  //   double x, y;
  //   numerator->GetPoint(i, x, y);
  //   // y = graphs.at(ratio_plot_choice)->Eval(x)/y;
  //   y = denominator->Eval(x)/y;
  //   r->SetPoint(i-skip, x, y);
    // double r_err =
    // r->SetPointError(i, Double_t ex, Double_t ey);
  // }
  r->GetXaxis()->SetLabelSize(0.075);
  r->GetYaxis()->SetLabelSize(0.075);
  r->SetLineColor(denominator->GetLineColor());
  r->SetLineStyle(denominator->GetLineStyle());
  r->SetLineWidth(denominator->GetLineWidth());
  return r;
}


void SetRatioPlotStyle ()
{
  static TStyle* atlasStyle = 0;
  std::cout << "[SetAtlasStyle] Applying ATLAS style settings...\n";
  if ( atlasStyle==0 ) atlasStyle = RatioPlotStyle();
  gROOT->SetStyle("RatioPlotStyle");
  gStyle->SetOptStat(0);
  gROOT->ForceStyle();
}

TStyle* RatioPlotStyle()
{
  TStyle *atlasStyle = new TStyle("RatioPlotStyle","Atlas style");

  // use plain black on white colors
  Int_t icol=0; // WHITE
  atlasStyle->SetFrameBorderMode(icol);
  atlasStyle->SetFrameFillColor(icol);
  atlasStyle->SetCanvasBorderMode(icol);
  atlasStyle->SetCanvasColor(icol);
  atlasStyle->SetPadBorderMode(icol);
  atlasStyle->SetPadColor(icol);
  atlasStyle->SetStatColor(icol);
  //atlasStyle->SetFillColor(icol); // don't use: white fill color for *all* objects

  // set the paper & margin sizes
  atlasStyle->SetPaperSize(20,26);

  // set margin sizes
  atlasStyle->SetPadTopMargin(0.05);
  atlasStyle->SetPadRightMargin(0.05);
  atlasStyle->SetPadBottomMargin(0.16);
  atlasStyle->SetPadLeftMargin(0.16);

  // set title offsets (for axis label)
  atlasStyle->SetTitleXOffset(1.4);
  atlasStyle->SetTitleYOffset(1.4);

  // use large fonts
  //Int_t font=72; // Helvetica italics
  Int_t font=42; // Helvetica
  Double_t tsize=0.05;
  atlasStyle->SetTextFont(font);

  // atlasStyle->SetTextSize(tsize);
  atlasStyle->SetLabelFont(font,"x");
  atlasStyle->SetTitleFont(font,"x");
  atlasStyle->SetLabelFont(font,"y");
  atlasStyle->SetTitleFont(font,"y");
  atlasStyle->SetLabelFont(font,"z");
  atlasStyle->SetTitleFont(font,"z");
  //
  atlasStyle->SetLabelSize(tsize,"x");
  atlasStyle->SetTitleSize(tsize,"x");
  atlasStyle->SetLabelSize(tsize,"y");
  atlasStyle->SetTitleSize(tsize,"y");
  atlasStyle->SetLabelSize(tsize,"z");
  atlasStyle->SetTitleSize(tsize,"z");

  // use bold lines and markers
  // atlasStyle->SetMarkerStyle(20);
  // atlasStyle->SetMarkerSize(1.2);
  atlasStyle->SetHistLineWidth(2.);
  atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars
  //atlasStyle->SetErrorX(0.001);
  // get rid of error bar caps
  atlasStyle->SetEndErrorSize(0.);

  // do not display any of the standard histogram decorations
  atlasStyle->SetOptTitle(0);
  //atlasStyle->SetOptStat(1111);
  atlasStyle->SetOptStat(0);
  //atlasStyle->SetOptFit(1111);
  atlasStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  atlasStyle->SetPadTickX(1);
  atlasStyle->SetPadTickY(1);

  return atlasStyle;

}


#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

#include "preparePaths.cxx"

TString g_input_file_path = "/afs/cern.ch/user/t/tawang/public/PileupRejection_100k.root";
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
TString dataset_name = "VBFinv, mu=200";
TString descr = "pileupRejectionTruthT0_highpt";
TString dataset_description = "";
bool print_png = true;
TString work_status = "Internal";
bool g_do_ratioplot = true;
double x_eff_min = 0.80;
double legend_text_size = 0.05 ;

TFile* g_file = nullptr;

TString g_config_file_name = "pileup.cfg";

void plot(TString name1, TString name2) {

  auto pu_std = (TH1F*) g_file->Get(name1);
  auto pu_bdt = (TH1F*) g_file->Get(name2);


  auto canvas = new TCanvas();

  auto axishisto = new TH1F("", "", 10, 2.4, 4.0);
  axishisto->GetYaxis()->SetRangeUser(0, 1.7);
  axishisto->GetXaxis()->SetTitle("|#eta|");
  axishisto->GetYaxis()->SetTitle("Relative Pileup Jet Rate");
  axishisto->Draw();

  auto hist = new TH1F("hist", ";|eta|;Relative Pileup Jet Rate",
                       pu_std->GetXaxis()->GetNbins(),
                       pu_std->GetXaxis()->GetXmin(),
                       pu_std->GetXaxis()->GetXmax());

  for(int i=1; i<=pu_std->GetNbinsX(); i++) {
    hist->SetBinContent(i,(float) pu_bdt->GetBinContent(i)/(float) pu_std->GetBinContent(i));
  }
  hist->SetLineWidth(3);
  hist->SetLineStyle(1);
  hist->Draw("same");

  auto line = new TLine(2.4, 1.0, 4.0, 1.0);
  line->SetLineStyle(9);
  line->SetLineWidth(3);
  line->Draw("same");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.315, 0.88, text_color, work_status.Data(), 0.05);
  atlas::myText(0.19, 0.8, text_color, "85% HS efficiency", 0.05);

  TLegend* legend = new TLegend(0.66, 0.93, 0.95, 0.73);
  legend->SetBorderSize(0);
  legend->SetTextSize(legend_text_size);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);

  legend->AddEntry(line, "ITk", "l");
  legend->AddEntry(hist, "ITk + HGTD", "l");

  legend->Draw("same");


  TString plot_name = Form("%s/puRateVsEta_%s_%s.pdf",  output_plot_path.Data(), name1.Data(), name2.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (print_png) canvas->Print(plot_name);

}

void plot2(TString name1, TString name2, int version=0) {

  auto pu_std = (TH1F*) g_file->Get(name1);
  auto pu_bdt = (TH1F*) g_file->Get(name2);

  auto pu_std_highpt = (TH1F*) g_file->Get(name1+"_highpt");
  auto pu_bdt_highpt = (TH1F*) g_file->Get(name2+"_highpt");


  auto canvas = new TCanvas();

  auto axishisto = new TH1F("", "", 10, 2.4, 4.0);
  axishisto->GetYaxis()->SetRangeUser(0, 1.7);
  axishisto->GetXaxis()->SetTitle("|#eta|");
  axishisto->GetYaxis()->SetTitle("Relative Pileup Jet Rate");
  axishisto->Draw();

  auto hist = new TH1F("hist", ";|eta|;Relative Pileup Jet Rate",
                       pu_std->GetXaxis()->GetNbins(),
                       pu_std->GetXaxis()->GetXmin(),
                       pu_std->GetXaxis()->GetXmax());

  auto hist_highpt = new TH1F("hist_highpt", ";|eta|;Relative Pileup Jet Rate",
                      pu_std->GetXaxis()->GetNbins(),
                      pu_std->GetXaxis()->GetXmin(),
                      pu_std->GetXaxis()->GetXmax());

  for(int i=1; i<=pu_std->GetNbinsX(); i++) {
    hist->SetBinContent(i,(float) pu_bdt->GetBinContent(i)/(float) pu_std->GetBinContent(i));

    hist_highpt->SetBinContent(i,(float) pu_bdt_highpt->GetBinContent(i)/(float) pu_std_highpt->GetBinContent(i));
  }

  hist->SetLineWidth(3);
  hist_highpt->SetLineWidth(3);
  hist->SetLineStyle(1);


  if (version == 0) {
    hist->SetLineColor(kRed);
    hist->Draw("same");
  } else if (version == 1) {
    hist_highpt->SetLineColor(kRed);
    hist_highpt->Draw("same");
  } else if (version == 2) {
    hist->SetLineColor(kRed);
    hist->Draw("same");
    hist_highpt->SetLineStyle(2);
    hist_highpt->SetLineColor(kRed);
    hist_highpt->Draw("same");
  }

  auto line = new TLine(2.4, 1.0, 4.0, 1.0);
  line->SetLineWidth(3);
  line->SetLineStyle(9);
  line->Draw("same");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.315, 0.88, text_color, work_status.Data(), 0.05);
  atlas::myText(0.19, 0.82, text_color, "85% HS efficiency", 0.05);

  atlas::myText(0.19, 0.75, text_color, "2.4 < |#eta_{jet}| < 4.0", 0.05);
  if (version == 0) {
    atlas::myText(0.19, 0.68, text_color, "30 < p_{T}^{jet} < 50GeV", 0.05);
  } else if (version == 1) {
    atlas::myText(0.19, 0.68, text_color, "p_{T}^{jet} > 50GeV", 0.05);
  } else if (version == 2) {
  }



  TLegend* legend;
  if(version == 2) {
    legend = new TLegend(0.45, 0.93, 0.95, 0.73);
  } else {
    legend = new TLegend(0.65, 0.93, 0.95, 0.73);
  }
  legend->SetBorderSize(0);
  legend->SetTextSize(0.04);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);

  legend->AddEntry(line, "ITk", "l");

  if (version == 0) {
    legend->AddEntry(hist, "ITk + HGTD", "l");
  } else if (version == 1) {
    legend->AddEntry(hist_highpt, "ITk + HGTD", "l");
  } else if (version == 2) {
    legend->AddEntry(hist, "ITk + HGTD, 30 < p_{T}^{jet} < 50GeV", "l");
    legend->AddEntry(hist_highpt, "ITk + HGTD, p_{T}^{jet} > 50GeV", "l");
  }

  legend->Draw("same");


  // TString plot_name = Form("%s/puRateVsEtaOverlay_%s_%s.pdf",  output_plot_path.Data(), name1.Data(), name2.Data());

  TString plot_name = "";
  if (version == 0) {
    plot_name = Form("%s/puRateVsEtaOverlayLow_%s_%s.pdf",  output_plot_path.Data(), name1.Data(), name2.Data());
  } else if (version == 1) {
    plot_name = Form("%s/puRateVsEtaOverlayHigh_%s_%s.pdf",  output_plot_path.Data(), name1.Data(), name2.Data());
  } else if (version == 2) {
    plot_name = Form("%s/puRateVsEtaOverlayBoth_%s_%s.pdf",  output_plot_path.Data(), name1.Data(), name2.Data());
  }
  // TString plot_name = Form("%s/puRateVsEtaOverlayHigh_%s_%s.pdf",  output_plot_path.Data(), name1.Data(), name2.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (print_png) canvas->Print(plot_name);

}

void puRateVsEta() {

  TEnv env(g_config_file_name);

  g_input_file_path = env.GetValue("input_file_path", "default");
  g_do_ratioplot = env.GetValue("do_ratioplot", false);
  x_eff_min = env.GetValue("x_eff_min", 0.8);
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  dataset_description = env.GetValue("dataset_description", "default");
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");

  g_file = TFile::Open(g_input_file_path, "READ");

  SetAtlasStyle();

  preparePlottingDir(g_config_file_name);

  // plot("m_hist_jeteta_rpt", "m_hist_jeteta_btrpt");
  // plot("m_hist_jeteta_rpt_1bin", "m_hist_jeteta_btrpt_1bin");

  plot2("m_hist_jeteta_rpt", "m_hist_jeteta_btrpt", 0);
  plot2("m_hist_jeteta_rpt", "m_hist_jeteta_btrpt", 1);
  plot2("m_hist_jeteta_rpt", "m_hist_jeteta_btrpt", 2);

}

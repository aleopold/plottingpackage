


void preparePlottingDir(TString g_config_file_name) {
  TEnv env(g_config_file_name);

  TString output_plot_path = env.GetValue("output_plot_path", "default");

  if (output_plot_path.Contains("www")) {
    gSystem->Exec(Form("mkdir -p %s", output_plot_path.Data()));
    gSystem->Exec(Form("cp /afs/cern.ch/user/a/aleopold/public/utilities/php_template/*.php %s", output_plot_path.Data()));
  } else {
    gSystem->Exec(Form("mkdir -p %s", output_plot_path.Data()));
  }
}

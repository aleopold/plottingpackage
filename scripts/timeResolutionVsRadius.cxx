#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

const double m_td_fitmin = -0.05;
const double m_td_fitmax = 0.5;

double* fitGaussianCore(TH1D* hist, TF1*& func) {
  // double m_td_fitmin = -0.05;
  // double m_td_fitmax = 0.05;
  func = new TF1("func","gaus(0)",m_td_fitmin-1., m_td_fitmax+1.);

  hist->Fit(func, "0", "", m_td_fitmin, m_td_fitmax);
  // hist->Fit(func, "", "", m_td_fitmin, m_td_fitmax);

  double mu = func->GetParameter(1);
  double sig = func->GetParameter(2);
  double sig_err = func->GetParError (2);

  double* params = new double[3];
  params[0] = mu;
  params[1] = sig;
  params[2] = sig_err;

  // delete func;
  return params;
}


void plotting(bool logscale, TString track_type_name, TString postfix, TString associator) {

  TString work_status = "Internal";
  TString output_plot_path = "~/www/hgtd/meeting_20191216_controlPlots/";

  /////////  O P E N   R O O T F I L E  /////////
  TString path = "../data/AccessorPerformanc.20068742.MERGED.truthEvent.root";

  //open file
  TFile* f = TFile::Open(path, "READ");
  if (not f) {
    cout << "file not found" << endl;
    return;
  }

  //get 2dhist
  auto hist2d = (TH2D*) f->Get(track_type_name + "/m_2dhist_overall_res_pfor_v2_vs_radius");
  //extract 1d histograms
  int n_bins = hist2d->GetNbinsX();
  float x_min = hist2d->GetXaxis()->GetXmin();
  float x_max = hist2d->GetXaxis()->GetXmax();
  vector<TH1D*> hists1d;
  hists1d.reserve(n_bins);
  for (int i=1; i<=n_bins; i++) {
    TString name = Form("bin%i", i);
    auto hist = hist2d->ProjectionY(name.Data(), i, i);
    hist->Scale(1./hist->Integral(0, hist->GetNbinsX()+1));
    // hist->Draw();
    // break;
    hists1d.push_back(hist);
  }



  // return;
  // cout << hists1d.size() << '\n';
  auto output_hist = new TH1F("hist", ";r [mm]; #sigma per track [ps]", n_bins, x_min, x_max);
  //do the fit for each
  for (int i=0; i<hists1d.size(); i++) {
    TF1* func = nullptr;
    double* params = fitGaussianCore(hists1d.at(i), func);
    output_hist->SetBinContent(i, params[1]*1000);
    output_hist->SetBinError(i, params[2]*1000);
    TCanvas* canv = new TCanvas();
    canv->SetLogy();
    hists1d.at(i)->Draw();
    func->SetNpx(10000);
    func->SetLineWidth(1);
    func->SetLineColor(kRed);
    func->Draw("same");
    TString pn = Form("%s/controlFit_%i_%s.pdf",  output_plot_path.Data(), i, postfix.Data());
    canv->Print(pn);
    pn.ReplaceAll(".pdf", ".png");
    if (true) canv->Print(pn);
    delete canv;
    delete func;
    delete params;
  }

  //----- P L O T T I N G -----
  TCanvas* canvas = new TCanvas();
  if (logscale) {
    canvas->SetLogy();
    output_hist->SetAxisRange(0.0001, 3*output_hist->GetMaximum(), "Y");
    // output_hist->SetAxisRange(0.0001, 3*output_hist->GetMaximum(), "Y");
  } else {
    // output_hist->SetAxisRange(0., 2.*output_hist->GetMaximum(), "Y");
    output_hist->SetAxisRange(0., 60, "Y");
  }

  double marker_size = 0.8;

  output_hist->SetMarkerSize(marker_size);
  output_hist->SetMarkerStyle(20);
  output_hist->SetMarkerColor(kBlack);
  output_hist->Draw("hist E2");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.89, text_color);
  atlas::myText(0.31, 0.89, text_color, work_status.Data());
  TString dataset_buff = "VBFinv, mu=200, Step 3.1";
  dataset_buff.ReplaceAll("mu", "<#mu>");
  dataset_buff.ReplaceAll("_", ", ");
  atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  atlas::myText(0.19, 0.79, text_color, dataset_buff.Data(), 0.04);
  // myText(0.19, 0.79, text_color, "VBFinv, #langle#mu#rangle=200, ITk Layout", 0.04);
  atlas::myText(0.19, 0.74, text_color, "p_{T}^{track} > 1.0 GeV", 0.035);
  atlas::myText(0.19, 0.69, text_color, "Timing scenario \"Initial\"", 0.035);
  atlas::myText(0.19, 0.64, text_color, associator.Data(), 0.035);

  TString plot_name;
  if (logscale) {
    // plot_name = Form("%s/timeResCombLogz0.pdf",  output_plot_path.Data());
    plot_name = Form("%s/timeResVsRadiusLog_%s.pdf",  output_plot_path.Data(), postfix.Data());
  } else {
    plot_name = Form("%s/timeResVsRadius_%s.pdf",  output_plot_path.Data(), postfix.Data());
  }
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (true) canvas->Print(plot_name);

  f->Close();
  delete canvas;
}

void timeResolutionVsRadius() {

  SetAtlasStyle();


  plotting(false, "ALL", "ALL", "PFORv02");

  plotting(false, "HS", "HS", "PFORv02");

}

#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

TString g_input_file_path = "default";
bool g_do_ratioplot = true;
// double x_eff_min = 0.80;
TString g_output_plot_path = "default/path";
int skip = 0;
TString g_dataset_name = "SomeSample";
TString g_dataset_description = "Some Sample";
bool print_png = true;
TString g_work_status = "Work Status";

TFile* g_file = nullptr;

std::vector<Color_t> colors = {kBlue, kRed};

void plot(TString h1_name, TString h2_name, TString plot_name, TString label1, TString label2) {

  auto h1 = (TH1F*) g_file->Get(h1_name);
  h1->Scale(1./h1->Integral(0, h1->GetNbinsX()+1));
  h1->SetLineColor(colors.at(0));
  h1->SetLineWidth(2.);
  auto h2 = (TH1F*) g_file->Get(h2_name);
  h2->Scale(1./h2->Integral(0, h2->GetNbinsX()+1));
  h2->SetLineColor(colors.at(1));
  h2->SetLineWidth(2.);

  auto canvas = new TCanvas();

  auto axishisto = new TH1F("", "", h1->GetNbinsX(), 0, 1.5);
  axishisto->GetYaxis()->SetRangeUser(0, 1.4*std::max(h1->GetMaximum(), h2->GetMaximum()));
  axishisto->GetXaxis()->SetTitle("RpT");
  axishisto->GetYaxis()->SetTitle("a.u.");
  axishisto->Draw();

  h1->Draw("same hist");
  h2->Draw("same hist");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.31, 0.88, text_color, g_work_status.Data());
  atlas::myText(0.19, 0.83, text_color, g_dataset_description.Data());

  TLegend* legend = new TLegend(0.7, 0.8, 0.95, 0.93);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.03);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);

  legend->AddEntry(h1, label1.Data(), "l");
  legend->AddEntry(h2, label2.Data(), "l");
  legend->Draw("same");

  TString plot_name_string = Form("%s/%s_%s.pdf",  g_output_plot_path.Data(), g_dataset_name.Data(), plot_name.Data());
  canvas->Print(plot_name_string);
  plot_name_string.Form("%s/%s_%s.png",  g_output_plot_path.Data(), g_dataset_name.Data(), plot_name.Data());
  if (print_png) canvas->Print(plot_name_string);

  delete canvas;
  delete legend;

}


void rptDistros() {
  TEnv env("pileup.cfg");

  g_input_file_path = env.GetValue("input_file_path", "default");
  g_do_ratioplot = env.GetValue("do_ratioplot", false);
  // x_eff_min = env.GetValue("x_eff_min", 0.8);
  g_output_plot_path = env.GetValue("output_plot_path", "default");
  g_dataset_name = env.GetValue("dataset_name", "default");
  g_dataset_description = env.GetValue("dataset_description", "My Sample");
  print_png = env.GetValue("print_png", false);
  g_work_status = env.GetValue("work_status", "default");

  g_file = TFile::Open(g_input_file_path, "READ");
  SetAtlasStyle();

  plot("m_hist_std_rpt_hs", "m_hist_std_rpt_pu", "DefaultRpTDist", "HS jets", "PU jets");

  plot("m_hist_default_hgtd_rpt_hs", "m_hist_default_hgtd_rpt_pu", "DefaultHGTDRpTDist", "HS jets", "PU jets");

  plot("m_hist_v01_hgtd_rpt_hs", "m_hist_v01_hgtd_rpt_pu", "TCv01RpTDist", "HS jets", "PU jets");

  plot("m_hist_v02_hgtd_rpt_hs", "m_hist_v02_hgtd_rpt_pu", "TCv02RpTDist", "HS jets", "PU jets");

  plot("m_hist_v03_hgtd_rpt_hs", "m_hist_v03_hgtd_rpt_pu", "TCv03RpTDist", "HS jets", "PU jets");

  plot("m_hist_v04_hgtd_rpt_hs", "m_hist_v04_hgtd_rpt_pu", "TCv04RpTDist", "HS jets", "PU jets");

  plot("m_hist_std_rpt_hs", "m_hist_default_hgtd_rpt_hs", "RpTComparisonStdTCv04HS", "HS jets Default", "HS jets TCv04");

  plot("m_hist_default_hgtd_rpt_hs", "m_hist_v04_hgtd_rpt_hs", "RpTComparisonDefaultTCv04HS", "HS jets Default", "HS jets TCv04");

  plot("m_hist_std_rpt_pu", "m_hist_default_hgtd_rpt_pu", "RpTComparisonStdTCv04PU", "PU jets Default", "PU jets TCv04");

  plot("m_hist_default_hgtd_rpt_pu", "m_hist_v04_hgtd_rpt_pu", "RpTComparisonDefaultTCv04PU", "PU jets Default", "PU jets TCv04");
}

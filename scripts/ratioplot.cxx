#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

void CanvasPartition(TCanvas *C, const Int_t Nx = 2, const Int_t Ny = 2,
                     Float_t lMargin = 0.15, Float_t rMargin = 0.05,
                     Float_t bMargin = 0.15, Float_t tMargin = 0.05);

std::pair<TPad *, TPad *> RatioPlotPartition(TCanvas *C, Float_t lMargin = 0.15,
                                             Float_t rMargin = 0.05,
                                             Float_t bMargin = 0.15,
                                             Float_t tMargin = 0.05);

void preparedPlot() {
  gStyle->SetOptStat(0);
  TCanvas *C = (TCanvas *)gROOT->FindObject("C");
  if (C)
    delete C;
  C = new TCanvas("C", "canvas", 1024, 640);
  C->SetFillColor(5);
  // C->SetFillStyle(4000);
  // Number of PADS
  const Int_t Nx = 2;
  const Int_t Ny = 2;
  // Margins
  // Float_t lMargin = 0.12;
  // Float_t rMargin = 0.05;
  // Float_t bMargin = 0.15;
  // Float_t tMargin = 0.05;
  auto style = gROOT->GetStyle("ATLAS");
  // float lMargin = 0.12;
  // float rMargin = 0.05;
  // float bMargin = 0.15;
  // float tMargin = 0.05;
  float lMargin = style->GetPadLeftMargin();
  float rMargin = style->GetPadRightMargin();
  float bMargin = style->GetPadBottomMargin();
  float tMargin = style->GetPadTopMargin();
  // Canvas setup
  CanvasPartition(C, Nx, Ny, lMargin, rMargin, bMargin, tMargin);
  // Dummy histogram.
  TH1F *h = (TH1F *)gROOT->FindObject("histo");
  if (h)
    delete h;
  h = new TH1F("histo", ";x axis;y axis", 100, -5.0, 5.0);
  h->FillRandom("gaus", 10000);
  h->GetXaxis()->SetTitle("x axis");
  h->GetYaxis()->SetTitle("y axis");
  TPad *pad[Nx][Ny];
  for (Int_t i = 0; i < Nx; i++) {
    for (Int_t j = 0; j < Ny; j++) {
      C->cd(0);
      // Get the pads previously created.
      char pname[16];
      sprintf(pname, "pad_%i_%i", i, j);
      pad[i][j] = (TPad *)gROOT->FindObject(pname);

      pad[i][j]->Draw();
      pad[i][j]->SetFillStyle(4000);
      pad[i][j]->SetFrameFillStyle(4000);
      pad[i][j]->SetFillColor(10 + i * j);
      pad[i][j]->SetFrameFillColor(10 + i * j);
      // pad[i][j]->GetFrame()->SetFillColor(10 + i * j);
      pad[i][j]->cd();
      // Size factors
      Float_t xFactor = pad[0][0]->GetAbsWNDC() / pad[i][j]->GetAbsWNDC();
      Float_t yFactor = pad[0][0]->GetAbsHNDC() / pad[i][j]->GetAbsHNDC();
      char hname[16];
      sprintf(hname, "h_%i_%i", i, j);
      TH1F *hFrame = (TH1F *)h->Clone(hname);
      hFrame->Reset();
      hFrame->Draw();
      // y axis range
      hFrame->GetYaxis()->SetRangeUser(0.0001, 1.2 * h->GetMaximum());
      // Format for y axis
      hFrame->GetYaxis()->SetLabelFont(43);
      hFrame->GetYaxis()->SetLabelSize(16);
      hFrame->GetYaxis()->SetLabelOffset(0.02);
      hFrame->GetYaxis()->SetTitleFont(43);
      hFrame->GetYaxis()->SetTitleSize(16);
      hFrame->GetYaxis()->SetTitleOffset(5);
      hFrame->GetYaxis()->CenterTitle();
      hFrame->GetYaxis()->SetNdivisions(505);
      // TICKS Y Axis
      hFrame->GetYaxis()->SetTickLength(xFactor * 0.04 / yFactor);
      // Format for x axis
      hFrame->GetXaxis()->SetLabelFont(43);
      hFrame->GetXaxis()->SetLabelSize(16);
      hFrame->GetXaxis()->SetLabelOffset(0.02);
      hFrame->GetXaxis()->SetTitleFont(43);
      hFrame->GetXaxis()->SetTitleSize(16);
      hFrame->GetXaxis()->SetTitleOffset(5);
      hFrame->GetXaxis()->CenterTitle();
      hFrame->GetXaxis()->SetNdivisions(505);
      // TICKS X Axis
      hFrame->GetXaxis()->SetTickLength(yFactor * 0.06 / xFactor);
      h->Draw("same");
    }
  }
  C->cd();
  C->Print("/afs/cern.ch/user/a/aleopold/www/hgtd/"
           "tdr_plots_2020-02-29_tta_test/canvas2.pdf");
  C->Print("/afs/cern.ch/user/a/aleopold/www/hgtd/"
           "tdr_plots_2020-02-29_tta_test/canvas2.png");
}

// void ratioplot() {
//   SetAtlasStyle();
//   // preparedPlot();
//   // auto canvas = new TCanvas("", "", 800, 600);
//   auto canvas = new TCanvas();
//
//   auto pad = new TPad("pad", "", 0.0, 0.0, 1, 1);
//   // auto pad = new TPad("pad", "", 0.0, 0.3, 1, 1);
//   pad->Draw();
//   // pad->SetFillColor(3);
//   // pad->SetFrameFillColor(41);
//   // pad->SetBottomMargin(0);
//
//   double pad1W = pad->GetWw() * pad->GetAbsWNDC();
//   cout << pad->GetWw() << '\n';
//   cout << pad->GetAbsWNDC() << '\n';
//   cout << pad1W << '\n';
//   double pad1H = pad->GetWh() * pad->GetAbsHNDC();
//   double tickScaleX = (pad->GetUxmax() - pad->GetUxmin()) /
//                       (pad->GetX2() - pad->GetX1()) * pad1H;
//
//   cout << tickScaleX << '\n';
//   double tickScaleY = (pad->GetUymax() - pad->GetUymin()) /
//                       (pad->GetY2() - pad->GetY1()) * pad1W;
//   //
//   // double sizeTick = 0.02;
//   // // histo->GetXaxis()->SetTickLength(sizeTick / tickScaleX);
//   // // histo->GetYaxis()->SetTickLength(sizeTick / tickScaleY);
//   //
//   // pad->cd();
//   //
//   // float pad_width = pad->GetAbsWNDC();
//   // float pad_height = pad->GetAbsHNDC();
//   //
//   // cout << pad_width << '\n';
//   // cout << pad_height << '\n';
//   //
//   // cout << pad_width / pad_height << '\n';
//   //
//   auto histo = new TH1F("histo", ";x axis;y axis", 10, 0, 10);
//   double sizeTick = 0.03;
//   // histo->GetXaxis()->SetTickLength(sizeTick / tickScaleX);
//   // histo->GetYaxis()->SetTickLength(sizeTick / tickScaleY);
//   // // histo->GetYaxis()->SetTickLength(0.03);
//   // // histo->GetXaxis()->SetTickLength(0.03 * pad_height / pad_width);
//   histo->Draw();
//
//   canvas->cd();
//   // auto pad2 = new TPad("pad", "", 0.0, 0.0, 1, 0.3);
//   // pad2->Draw();
//   // pad2->SetFillColor(5);
//   // pad2->SetFrameFillColor(50);
//   // pad2->SetTopMargin(0);
//   // pad2->cd();
//   //
//   // float xFactor = pad->GetAbsWNDC() / pad2->GetAbsWNDC();
//   // float yFactor = pad->GetAbsHNDC() / pad2->GetAbsHNDC();
//   //
//   // // histo->GetYaxis()->SetTickLength(xFactor * 0.04 / yFactor);
//   // cout << xFactor * 0.04 / yFactor << '\n';
//   // // histo->GetYaxis()->SetTickLength(0.03);
//   // // histo->GetXaxis()->SetTickLength(0.03);
//   // histo->Draw();
//
//   canvas->Print("/afs/cern.ch/user/a/aleopold/www/hgtd/"
//                 "tdr_plots_2020-02-29_tta_test/ratioplot.pdf");
//   canvas->Print("/afs/cern.ch/user/a/aleopold/www/hgtd/"
//                 "tdr_plots_2020-02-29_tta_test/ratioplot.png");
// }

// void ratioplot() {
//   SetAtlasStyle();
//   // preparedPlot();
//   // auto canvas = new TCanvas("", "", 800, 600);
//   auto canvas = new TCanvas();
//
//   // auto pad = new TPad("pad", "", 0.0, 0.0, 1, 1);
//   auto pad = new TPad("pad", "", 0.0, 0.3, 1, 1);
//   pad->Draw();
//   pad->cd();
//   // pad->SetFillColor(3);
//   // pad->SetFrameFillColor(41);
//   pad->SetBottomMargin(0);
//   //
//   // double pad1W = pad->GetWw() * pad->GetAbsWNDC();
//   // // cout << pad->GetWw() << '\n';
//   // // cout << pad->GetAbsWNDC() << '\n';
//   // cout << pad1W << '\n';
//   // // double pad1H = pad->GetWh() * pad->GetAbsHNDC();
//   // // double tickScaleX = (pad->GetUxmax() - pad->GetUxmin()) /
//   // //                     (pad->GetX2() - pad->GetX1()) * pad1H;
//   // //
//   // // cout << tickScaleX << '\n';
//   // cout << pad->GetUymax() << '\n';
//   // cout << pad->GetUymin() << '\n';
//   // cout << pad->GetY2() << '\n';
//   // cout << pad->GetY1() << '\n';
//   // double tickScaleY = (pad->GetUymax() - pad->GetUymin()) /
//   //                     (pad->GetY2() - pad->GetY1()) * pad1W;
//   // cout << tickScaleY << '\n';
//   // //
//   // double sizeTick = 0.02;
//   // // histo->GetXaxis()->SetTickLength(sizeTick / tickScaleX);
//   // // histo->GetYaxis()->SetTickLength(sizeTick / tickScaleY);
//   //
//   // pad->cd();
//   //
//   // float pad_width = pad->GetAbsWNDC();
//   // float pad_height = pad->GetAbsHNDC();
//   //
//   // cout << pad_width << '\n';
//   // cout << pad_height << '\n';
//   //
//   // cout << pad_width / pad_height << '\n';
//
//   //
//   auto histo = new TH1F("histo", ";x axis;y axis", 10, 0, 10);
//   // double sizeTick = histo->GetYaxis()->GetTickLength();
//   // cout << "sizeTick " << sizeTick << '\n';
//   // double sizeTick = 0.03;
//   // histo->GetXaxis()->SetTickLength(sizeTick / tickScaleX);
//   // histo->GetYaxis()->SetTickLength(sizeTick / tickScaleY);
//   // // histo->GetYaxis()->SetTickLength(0.03);
//   // // histo->GetXaxis()->SetTickLength(0.03 * pad_height / pad_width);
//   histo->Draw();
//
//   canvas->cd();
//   auto pad2 = new TPad("pad2", "", 0.0, 0.0, 1, 0.3);
//   pad2->Draw();
//   pad2->SetTopMargin(0);
//   pad2->SetBottomMargin(2 * pad2->GetBottomMargin());
//   pad2->cd();
//   auto histo2 = new TH1F("histo2", ";x axis;ratio", 10, 0, 10);
//
//   // double padratio = pad->GetAbsHNDC() / pad2->GetAbsHNDC();
//   // double padratio = pad2->GetAbsHNDC() / pad->GetAbsHNDC();
//   //
//   // double sizeTick2 = histo2->GetYaxis()->GetTickLength();
//   // cout << "sizeTick2 " << sizeTick2 << '\n';
//
//   // histo2->GetXaxis()->SetTickLength(sizeTick * padratio);
//   // histo2->GetYaxis()->SetTickLength(sizeTick * padratio);
//   // histo2->GetYaxis()->SetTickLength(sizeTick / tickScaleY);
//   histo2->Draw();
//
//   canvas->Print("/afs/cern.ch/user/a/aleopold/www/hgtd/"
//                 "tdr_plots_2020-02-29_tta_test/ratioplot.pdf");
//   canvas->Print("/afs/cern.ch/user/a/aleopold/www/hgtd/"
//                 "tdr_plots_2020-02-29_tta_test/ratioplot.png");
// }

void ratioplot() {
  SetAtlasStyle();

  ///////////////////////////////////////////////////////
  ///////////      C R E A T E   D A T A      ///////////
  TRandom3 rand;
  vector<TH1 *> hists;
  vector<Color_t> cols = {kBlack, kRed, kBlue, kGreen};
  for (int i = 0; i < 4; i++) {
    auto h = new TH1F(Form("h%i", i + 1), ";x value; y value", 10, 0, 10);
    h->SetLineColor(cols.at(i));
    for (int i = 1; i <= 10; i++) {
      h->SetBinContent(i, rand.Gaus(1, 0.5));
      h->SetBinError(i, 0.01);
    }
    hists.push_back(h);
  }
  ///////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////

  std::vector<TRatioPlot *> ratio_plots;
  std::vector<TCanvas *> dummy_canvases;

  TPad *upper_pad = nullptr;
  TPad *lower_pad = nullptr;
  auto canvas = new TCanvas();
  canvas->cd();

  int bins = hists.at(0)->GetXaxis()->GetNbins();
  double min = hists.at(0)->GetXaxis()->GetXmin();
  double max = hists.at(0)->GetXaxis()->GetXmax();
  auto dummyh1 = new TH1F("dummyh1", "", bins, min, max);
  dummyh1->SetBinContent(1, 1);
  dummyh1->SetLineColor(0);
  dummyh1->SetMarkerColor(0);
  dummyh1->GetXaxis()->SetTitle(hists.at(0)->GetXaxis()->GetTitle());
  dummyh1->GetYaxis()->SetTitle(hists.at(0)->GetYaxis()->GetTitle());
  auto dummyh2 = new TH1F("dummyh2", "", bins, min, max);
  dummyh2->SetBinContent(1, 1);
  dummyh2->SetLineColor(0);
  dummyh2->SetMarkerColor(0);

  auto rp = new TRatioPlot(dummyh1, dummyh2);
  ratio_plots.push_back(rp);
  rp->Draw();
  auto dummy_graph = rp->GetLowerRefGraph();
  dummy_graph->SetLineColor(0);
  dummy_graph->SetMarkerColor(0);
  upper_pad = rp->GetUpperPad();
  lower_pad = rp->GetLowerPad();
  for (auto &&obj : *lower_pad->GetListOfPrimitives()) {
    // if (obj->GetName() == "TLine") {
    if (std::strncmp(obj->GetName(), "TLine", 5) == 0) {
      cout << obj->GetName() << '\n';
      delete obj;
    }
  }

  rp->SetLowBottomMargin(0.47);
  rp->SetLowTopMargin(0.);
  rp->SetUpBottomMargin(0.);
  rp->SetUpTopMargin(0.05);

  for (int i = 1; i < hists.size(); i++) {
    auto dummy = new TCanvas();
    dummy->cd();
    dummy_canvases.push_back(dummy);
    // auto rp1 = new TRatioPlot(hists.at(0), hists.at(i));
    auto rp1 = new TRatioPlot(hists.at(i), hists.at(0));

    rp1->Draw();
    ratio_plots.push_back(rp1);
  }

  // plot the histograms
  upper_pad->cd();
  double histmax = 0;
  for (int i = 0; i < hists.size(); i++) {
    hists.at(i)->Draw("same hist");
    if (hists.at(i)->GetMaximum() > histmax) {
      histmax = hists.at(i)->GetMaximum();
    }
  }

  /// plot the ratio graphs
  lower_pad->cd();
  for (int i = 1; i < ratio_plots.size(); i++) {
    auto graph = ratio_plots.at(i)->GetLowerRefGraph();
    graph->SetLineColor(hists.at(i)->GetLineColor());
    graph->SetMarkerColor(hists.at(i)->GetLineColor());
    graph->Draw("same LP");
  }

  ratio_plots.at(0)->GetUpperRefYaxis()->SetRangeUser(0, histmax * 1.4);

  ratio_plots.at(0)->GetLowerRefYaxis()->SetTitle("ratio");
  ratio_plots.at(0)->GetLowerRefYaxis()->SetRangeUser(0.8, 2.0);

  // ratio_plots.at(0)->GetLowerRefGraph()->SetMinimum(0.5);
  // ratio_plots.at(0)->GetLowerRefGraph()->SetMaximum(3.);

  upper_pad->SetRightMargin(0.05);
  upper_pad->SetLeftMargin(0.16);
  upper_pad->RedrawAxis();

  lower_pad->SetRightMargin(0.05);
  lower_pad->SetLeftMargin(0.16);
  // lower_pad->Update();

  // canvas->Update();

  // auto rp1 = new TRatioPlot(h1, h2);
  //
  // auto upper_pad = rp1->GetUpperPad();
  // upper_pad->SetRightMargin(0.05);
  // upper_pad->SetLeftMargin(0.16);
  // // upper_pad->SetBottomMargin(0.0)
  // auto lower_pad = rp1->GetLowerPad();
  // lower_pad->SetRightMargin(0.05);
  // lower_pad->SetLeftMargin(0.16);
  // // lower_pad->SetTopMargin(0.0);
  //
  // rp1->Draw();
  // rp1->GetLowerRefYaxis()->SetTitle("ratio");
  // rp1->GetUpperRefYaxis()->SetTitle("entries");

  canvas->Print("/afs/cern.ch/user/a/aleopold/www/hgtd/"
                "tdr_plots_2020-02-29_tta_test/ratioplot.pdf");
  canvas->Print("/afs/cern.ch/user/a/aleopold/www/hgtd/"
                "tdr_plots_2020-02-29_tta_test/ratioplot.png");
}

void CanvasPartition(TCanvas *C, const Int_t Nx, const Int_t Ny,
                     Float_t lMargin, Float_t rMargin, Float_t bMargin,
                     Float_t tMargin) {
  if (!C)
    return;
  // Setup Pad layout:
  Float_t vSpacing = 0.0;
  Float_t vStep = (1. - bMargin - tMargin - (Ny - 1) * vSpacing) / Ny;
  cout << "vStep " << vStep << '\n';
  Float_t hSpacing = 0.0;
  Float_t hStep = (1. - lMargin - rMargin - (Nx - 1) * hSpacing) / Nx;
  cout << "hStep " << hStep << '\n';
  Float_t vposd, vposu, vmard, vmaru, vfactor;
  Float_t hposl, hposr, hmarl, hmarr, hfactor;
  for (Int_t i = 0; i < Nx; i++) {
    if (i == 0) {
      hposl = 0.0;
      hposr = lMargin + hStep;
      hfactor = hposr - hposl;
      hmarl = lMargin / hfactor;
      hmarr = 0.0;
    } else if (i == Nx - 1) {
      hposl = hposr + hSpacing;
      hposr = hposl + hStep + rMargin;
      hfactor = hposr - hposl;
      hmarl = 0.0;
      hmarr = rMargin / (hposr - hposl);
    } else {
      hposl = hposr + hSpacing;
      hposr = hposl + hStep;
      hfactor = hposr - hposl;
      hmarl = 0.0;
      hmarr = 0.0;
    }
    for (Int_t j = 0; j < Ny; j++) {
      if (j == 0) {
        vposd = 0.0;
        vposu = bMargin + vStep;
        vfactor = vposu - vposd;
        vmard = bMargin / vfactor;
        vmaru = 0.0;
      } else if (j == Ny - 1) {
        vposd = vposu + vSpacing;
        vposu = vposd + vStep + tMargin;
        vfactor = vposu - vposd;
        vmard = 0.0;
        vmaru = tMargin / (vposu - vposd);
      } else {
        vposd = vposu + vSpacing;
        vposu = vposd + vStep;
        vfactor = vposu - vposd;
        vmard = 0.0;
        vmaru = 0.0;
      }
      C->cd(0);
      char name[16];
      sprintf(name, "pad_%i_%i", i, j);
      TPad *pad = (TPad *)gROOT->FindObject(name);
      if (pad)
        delete pad;
      cout << "hposl " << hposl << '\n';
      cout << "vposd " << vposd << '\n';
      cout << "hposr " << hposr << '\n';
      cout << "vposu " << vposu << '\n';
      pad = new TPad(name, "", hposl, vposd, hposr, vposu);
      cout << "hmarl " << hmarl << '\n';
      cout << "hmarr " << hmarr << '\n';
      cout << "vmard " << vmard << '\n';
      cout << "vmaru " << vmaru << '\n';
      cout << "===================\n";
      pad->SetLeftMargin(hmarl);
      pad->SetRightMargin(hmarr);
      pad->SetBottomMargin(vmard);
      pad->SetTopMargin(vmaru);
      pad->SetFrameBorderMode(0);
      pad->SetBorderMode(0);
      pad->SetBorderSize(0);
      pad->Draw();
    }
  }
}

std::pair<TPad *, TPad *> RatioPlotPartition(TCanvas *C, Float_t lMargin = 0.15,
                                             Float_t rMargin = 0.05,
                                             Float_t bMargin = 0.15,
                                             Float_t tMargin = 0.05) {
  if (!C)
    return {nullptr, nullptr};
  int Ny = 2;
  int Nx = 1;
  // Setup Pad layout:
  Float_t vSpacing = 0.0;
  Float_t vStep = (1. - bMargin - tMargin - (Ny - 1) * vSpacing) / Ny;
  cout << "vStep " << vStep << '\n';
  Float_t hSpacing = 0.0;
  Float_t hStep = (1. - lMargin - rMargin - (Nx - 1) * hSpacing) / Nx;
  cout << "hStep " << hStep << '\n';
  Float_t vposd, vposu, vmard, vmaru, vfactor;
  Float_t hposl, hposr, hmarl, hmarr, hfactor;
  for (Int_t i = 0; i < Nx; i++) {
    if (i == 0) {
      hposl = 0.0;
      hposr = lMargin + hStep;
      hfactor = hposr - hposl;
      hmarl = lMargin / hfactor;
      hmarr = 0.0;
    } else if (i == Nx - 1) {
      hposl = hposr + hSpacing;
      hposr = hposl + hStep + rMargin;
      hfactor = hposr - hposl;
      hmarl = 0.0;
      hmarr = rMargin / (hposr - hposl);
    } else {
      hposl = hposr + hSpacing;
      hposr = hposl + hStep;
      hfactor = hposr - hposl;
      hmarl = 0.0;
      hmarr = 0.0;
    }
    for (Int_t j = 0; j < Ny; j++) {
      if (j == 0) {
        vposd = 0.0;
        vposu = bMargin + vStep;
        vfactor = vposu - vposd;
        vmard = bMargin / vfactor;
        vmaru = 0.0;
      } else if (j == Ny - 1) {
        vposd = vposu + vSpacing;
        vposu = vposd + vStep + tMargin;
        vfactor = vposu - vposd;
        vmard = 0.0;
        vmaru = tMargin / (vposu - vposd);
      } else {
        vposd = vposu + vSpacing;
        vposu = vposd + vStep;
        vfactor = vposu - vposd;
        vmard = 0.0;
        vmaru = 0.0;
      }
      C->cd(0);
      char name[16];
      sprintf(name, "pad_%i_%i", i, j);
      TPad *pad = (TPad *)gROOT->FindObject(name);
      if (pad)
        delete pad;
      cout << "hposl " << hposl << '\n';
      cout << "vposd " << vposd << '\n';
      cout << "hposr " << hposr << '\n';
      cout << "vposu " << vposu << '\n';
      pad = new TPad(name, "", hposl, vposd, hposr, vposu);
      cout << "hmarl " << hmarl << '\n';
      cout << "hmarr " << hmarr << '\n';
      cout << "vmard " << vmard << '\n';
      cout << "vmaru " << vmaru << '\n';
      pad->SetLeftMargin(hmarl);
      pad->SetRightMargin(hmarr);
      pad->SetBottomMargin(vmard);
      pad->SetTopMargin(vmaru);
      pad->SetFrameBorderMode(0);
      pad->SetBorderMode(0);
      pad->SetBorderSize(0);
      pad->Draw();
    }
  }
  return {nullptr, nullptr};
}

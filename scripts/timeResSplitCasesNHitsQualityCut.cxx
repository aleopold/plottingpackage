#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

TString g_input_file_path = "/afs/cern.ch/work/a/aleopold/private/hgtd/newAnalysis/run/TrackTimeAccessors_Output_test.root";
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
TString dataset_description = "";
int skip = 0;
TString dataset_name = "VBFinv, mu=200";
bool print_png = true;
TString work_status = "Internal";
TString descr = "OutlierStackPlotGoodZ0_pforv02";
bool do_zoom = false;
// TString descr = "OutlierStackPlotBadZ0";

std::vector<Color_t> colors = {kTeal+4, kTeal+2, kTeal, kTeal-9, kMagenta, kRed, kRed-7, kRed+2, kRed+3};

TFile* g_file = nullptr;

void doPlot(std::vector<TString> histnames) {



  std::vector<TH1F*> hists;

  for (const auto& histname : histnames) {
    auto hist = (TH1F*) g_file->Get(histname);
    if (hist == nullptr) {
      cout << "timeResSplitCasesNHits:doplot\n";
      cout << "  cant' find histogram " << histname << '\n';
      return;
    }
    hists.push_back(hist);
  }

  auto overall_hist = new TH1F("overall_hist", ";t_{reco} - t_{truth} [ns]; number of tracks", 200, -.4, 0.4);
  for (const auto& h : hists) {
    overall_hist->Add(h);
  }

  THStack* stack_hist = new THStack("stack_hist",";t_{reco} - t_{truth} [ns]; a.u.");

  //std::vector<int> colors = {42, 30, 46, 38, 28};
  int ntotal = 0;
  for (size_t i=0; i<hists.size(); i++) {
    hists.at(i)->SetFillColor(colors.at(i));
    hists.at(i)->SetLineWidth(0.);
    ntotal += hists.at(i)->Integral(0, hists.at(i)->GetNbinsX()+1);
    stack_hist->Add(hists.at(i));
  }

  stack_hist->SetMaximum(stack_hist->GetMaximum()*1.7);
  if(do_zoom) {
    stack_hist->SetMaximum(100);
  }

  TCanvas* c = new TCanvas();
  c->SetTicks(1,1);
  // c->SetLogy();


  stack_hist->Draw();

  overall_hist->SetMarkerColor(kBlack);
  overall_hist->SetMarkerStyle(20);
  overall_hist->SetMarkerSize(0.5);
  overall_hist->Draw("same p");
  // overall_hist->Draw();

  c->Update();

  cout << "total " << ntotal << '\n';
  for (size_t i=0; i<hists.size(); i++) {
    // cout << "hist " << i << " " <<  (float) hists.at(i)->Integral(0, hists.at(i)->GetNbinsX()+1)/(float) ntotal << '\n';
    cout << "hist " << i << " " <<  (float) hists.at(i)->Integral(0, hists.at(i)->GetNbinsX()+1) << '\n';
  }

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.31, 0.88, text_color, work_status);
  atlas::myText(0.19, 0.83, text_color, dataset_description.Data());

  auto legend = new TLegend(0.47, 0.65, 0.85, 0.92);

  legend->SetTextFont(42);
  // legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.035);


  legend->AddEntry(hists.at(0), Form("All primes, %.1f%%", 100 * (float) hists.at(0)->Integral(0, hists.at(0)->GetNbinsX()+1)/(float) ntotal), "f");

  legend->AddEntry(hists.at(1), Form("More than half primes, %.1f%%", 100*(float) hists.at(1)->Integral(0, hists.at(1)->GetNbinsX()+1)/(float) ntotal), "f");

  legend->AddEntry(hists.at(2), Form("Half primes, %.1f%%", 100*(float) hists.at(2)->Integral(0, hists.at(2)->GetNbinsX()+1)/(float) ntotal), "f");

  legend->AddEntry(hists.at(3), Form("Less than half primes, %.1f%%", 100*(float) hists.at(3)->Integral(0, hists.at(3)->GetNbinsX()+1)/(float) ntotal), "f");

  legend->AddEntry(hists.at(4), Form("No primes, no possible primes, %.1f%%", 100*(float) hists.at(4)->Integral(0, hists.at(4)->GetNbinsX()+1)/(float) ntotal), "f");

  legend->AddEntry(hists.at(5), Form("No primes, 1 possible prime, %.1f%%", 100*(float) hists.at(5)->Integral(0, hists.at(5)->GetNbinsX()+1)/(float) ntotal), "f");

  legend->AddEntry(hists.at(6), Form("No primes, 2 possible primes, %.1f%%", 100*(float) hists.at(6)->Integral(0, hists.at(6)->GetNbinsX()+1)/(float) ntotal), "f");

  legend->AddEntry(hists.at(7), Form("No primes, 3 possible primes, %.1f%%", 100*(float) hists.at(7)->Integral(0, hists.at(7)->GetNbinsX()+1)/(float) ntotal), "f");

  legend->AddEntry(hists.at(8), Form("No primes, 4 possible primes, %.1f%%", 100*(float) hists.at(8)->Integral(0, hists.at(8)->GetNbinsX()+1)/(float) ntotal), "f");

  legend->Draw("same");

  TString plot_name = Form("%s/%s_%s.pdf",  output_plot_path.Data(), dataset_name.ReplaceAll(" ", "").ReplaceAll(",", "_").Data(), descr.Data());
  c->Print(plot_name);
  if (print_png) c->Print(plot_name.ReplaceAll(".pdf", ".png"));

  // std::for_each(cleanup.begin(), cleanup.end(), [](TObject* obj) {delete obj;});

}

void timeResSplitCasesNHitsQualityCut() {

  TEnv env("pileup.cfg");

  g_input_file_path = env.GetValue("input_file_path_TTI", "default");
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  dataset_description = env.GetValue("dataset_description", "My Sample");
  print_png = env.GetValue("print_png", false);
  do_zoom = env.GetValue("do_zoom", false);
  work_status = env.GetValue("work_status", "default");

  g_file = TFile::Open(g_input_file_path, "READ");

  SetAtlasStyle();

  for (int i=1; i<=4; i++) {
    if(do_zoom) {
      descr = Form("OutlierStackPlotGoodZ0QualityCut_PFORv02_%iHits_Zoom", i);
    } else {
      descr = Form("OutlierStackPlotGoodZ0QualityCut_PFORv02_%iHits", i);
    }

    doPlot({Form("m_hist_timeres_nhits_outlierchecks_isolatedGoodZ0AllPrimes_nhits_%i", i),
    Form("m_hist_timeres_nhits_outlierchecks_isolatedGoodZ0MoreThanHalfPrimes_nhits_%i", i),
    Form("m_hist_timeres_nhits_outlierchecks_isolatedGoodZ0HalfPrimesHasPrimes_nhits_%i", i),
    Form("m_hist_timeres_nhits_outlierchecks_isolatedGoodZ0LessThanHalfPrimes_nhits_%i", i),
    Form("m_hist_timeres_nhits_outlierchecks_isolatedGoodZ0NoPrimesNoPossiblePrimes_nhits_%i", i),
    Form("m_hist_timeres_nhits_outlierchecks_isolatedGoodZ0NoPrimes1PossiblePrimes_nhits_%i", i),
    Form("m_hist_timeres_nhits_outlierchecks_isolatedGoodZ0NoPrimes2PossiblePrimes_nhits_%i", i),
    Form("m_hist_timeres_nhits_outlierchecks_isolatedGoodZ0NoPrimes3PossiblePrimes_nhits_%i", i),
    Form("m_hist_timeres_nhits_outlierchecks_isolatedGoodZ0NoPrimes4PossiblePrimes_nhits_%i", i),});

}



}

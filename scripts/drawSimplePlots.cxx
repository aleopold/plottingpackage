#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

#include "preparePaths.cxx"


TString g_input_file_path = "/afs/cern.ch/user/t/tawang/public/PileupRejection_100k.root";
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
TString dataset_name = "VBFinv, mu=200";
TString descr = "pileupRejectionTruthT0_highpt";
TString dataset_description = "";
bool print_png = true;
TString work_status = "Internal";
bool g_do_ratioplot = true;
double x_eff_min = 0.80;
double legend_text_size = 0.02;

TFile* g_file = nullptr;

TString g_config_file_name = "pileup.cfg";

void plotClusterCategories(TString name, double ymax=0.7) {
  auto hist = (TH1F*) g_file->Get(name);

  hist->Scale(1./hist->Integral());
  hist->GetYaxis()->SetRangeUser(0.0, ymax);
  hist->GetXaxis()->SetTitle("");

  std::vector<TObject*> garbage;

  TCanvas *canvas = new TCanvas();
  garbage.push_back(canvas);

  gStyle->SetPaintTextFormat("2.2f");
  // hist->Draw("hist text90");
  hist->Draw("hist");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.315, 0.88, text_color, work_status.Data(), 0.05);

  TString buffer_dataset_description = dataset_description;
  buffer_dataset_description.ReplaceAll("$", "#");
  atlas::myText(0.19, 0.82, text_color, buffer_dataset_description.Data(), 0.05);

  TString plot_name = Form("%s/%s.pdf",  output_plot_path.Data(), name.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (print_png) canvas->Print(plot_name);

  for (auto& trash : garbage) {
    delete trash;
  }
}

void plotBDTScore() {
  auto hist1 = (TH1F*) g_file->Get("hist_bdt_out_sig");
  auto hist2 = (TH1F*) g_file->Get("hist_bdt_out_bg");

  hist1->Scale(1./hist1->Integral());
  hist1->SetFillStyle(3345);
  hist1->SetFillColor(kBlue);
  hist1->SetLineColor(kBlue);
  hist1->Rebin(2);


  hist2->Scale(1./hist2->Integral());
  hist2->SetFillStyle(3354);
  hist2->SetFillColor(kRed);
  hist2->SetLineColor(kRed);
  hist2->Rebin(2);

  auto canvas = new TCanvas();

  auto axishisto = new TH1F("", "", 10, -1., 1.);
  axishisto->GetXaxis()->SetTitle("BDT output");
  axishisto->GetYaxis()->SetTitle("a.u.");
  axishisto->GetYaxis()->SetRangeUser(0, 0.25);

  axishisto->Draw();

  hist1->Draw("same hist");
  hist2->Draw("same hist");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.315, 0.88, text_color, work_status.Data(), 0.05);

  TString buffer_dataset_description = dataset_description;
  buffer_dataset_description.ReplaceAll("$", "#");
  atlas::myText(0.19, 0.82, text_color, buffer_dataset_description.Data(), 0.05);

  auto legend = new TLegend(0.65, 0.7, 0.9, 0.92);

  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.025);
  legend->SetFillStyle(0);
  legend->AddEntry(hist1, "Signal", "f");
  legend->AddEntry(hist2, "Background", "f");

  legend->Draw("same");

  gPad->RedrawAxis();

  TString plot_name = Form("%s/%s_BDTScore.pdf",  output_plot_path.Data(), dataset_name.ReplaceAll(" ", "").ReplaceAll(",", "_").Data());

  canvas->Print(plot_name);
  if (print_png) canvas->Print(plot_name.ReplaceAll(".pdf", ".png"));

}



void drawSimplePlots() {

  TEnv env(g_config_file_name);

  g_input_file_path = env.GetValue("bdt_control_input_file_path", "default");
  g_do_ratioplot = env.GetValue("do_ratioplot", false);
  x_eff_min = env.GetValue("x_eff_min", 0.8);
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  dataset_description = env.GetValue("dataset_description", "default");
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");

  g_file = TFile::Open(g_input_file_path, "READ");

  SetAtlasStyle();

  preparePlottingDir(g_config_file_name);

  plotClusterCategories("hist_chosen_cluster_type", 0.9);

  // plotClusterCategories("hist_no_perfect_butbdt");
  // plotClusterCategories("hist_compare_bdt_with_perfect_choice", 1.0);

  plotBDTScore();

}

import os, sys
import subprocess

from glob import glob

import ROOT

#path_to_files = '/eos/user/a/aleopold/hgtd/ESD/single_muon_Pt45.ITk_step3p0.v02.segfind/'
#path_to_files = '/eos/user/a/aleopold/hgtd/ESD/singlepi1.v01.segfind/'
#path_to_files = '/eos/user/a/aleopold/hgtd/ESD/singlemu45.step3p0.v02.segfind/'
#path_to_files = '/eos/user/a/aleopold/hgtd/ESD/singlemu45.step3p0.v02.segfind3/'
# path_to_files = '/eos/user/a/aleopold/hgtd/ESD/user.aleopold.301399.VBFinv.mu200.MasterControlPlot.20191109_1859_TrackTimes/'
# path_to_files = '/eos/user/a/aleopold/hgtd/ESD/user.aleopold.301399.VBFinv.mu200.MasterControlPlot.20191109_1859_PileupRejection/'
# path_to_files = '/eos/user/a/aleopold/hgtd/ESD/occupancy_3.0/'
# path_to_files = '/eos/user/a/aleopold/hgtd/ESD/occupancy_3.1/'
# path_to_files = '/eos/user/a/aleopold/hgtd/results/user.aleopold.VBFinv.mu200.AccPerf.20191127_2256root_AccessorPerformance/'
# path_to_files = '/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_AccPerf.2019_12_04_2333_AccessorPerformance/' #19993183
# path_to_files = '/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_truthEvent.20191206_2341_AccessorPerformance/' #19993232
# path_to_files = '/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_AccPerf.2019_12_16_2120_AccessorPerformance/' #20068742
# path_to_files = '/eos/user/a/aleopold/hgtd/ESD/VBFmu200/'
# path_to_files = '/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_AccPerf.2020_01_18_0116_AccessorPerformance/'
# path_to_files = '/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_PileupRejection.20200118_2058_PileupRejection/'
#path_to_files = '/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_PileupRejection.20200118_2257_PileupRejection/'
# path_to_files = '/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_AccPerf_2HitHighEta.2020_01_24_1855_AccessorPerformance/'
# path_to_files = '/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_SinglePion_mu0_step3p1_AccPerf_PP1Plus20GlobalPlus10.2020_01_24_1904_AccessorPerformance/'
# path_to_files = '/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_AccPerf.2020_02_03_1434_AccessorPerformance/'
path_to_files = '/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_VBFinv_mu200_step3p1_AccPerf.2020_03_02_2306_AccessorPerformance/'

# file_wildcard = 'HGTD_SegFind_Plots.*.root'
# file_wildcard = 'user.aleopold.19727222.TrackTimes.*.root'
# file_wildcard = 'HitControlPlots.*.root'
# file_wildcard = 'TrackTimesControlPlots_Output.*.root'
# file_wildcard = 'user.aleopold.20340234.PileupRejection.*.root'
file_wildcard = 'user.aleopold.20714825.AccessorPerformance.*.root'
# file_wildcard = 'user.aleopold.20585471.Jets.*.root'

#output_name = 'HGTD_SegFind_Plots_singlemu45.step3p0.v02.v3p1.root'
# output_name = 'aleopold.301399.VBFinv.mu200.MasterControlPlot.20191109_1859_TrackTimes.MERGED.root'
# output_name = 'aleopold.301399.VBFinv.mu200.MasterControlPlot.20191109_1859_PileupRejection.MERGED.root'
# output_name = 'HitControlPlots.MERGED.root'
# output_name = 'TrackTimesControlPlots_Output.3.0.MERGED.root'
# output_name = 'TrackTimesControlPlots_Output.3.1.MERGED.root'
# output_name = 'AccessorPerformanc.20094673.MERGED.truthEvent.root'
output_name = 'AccessorPerformanc.20714825.MERGED.root'
# output_name = 'PileupRejection.20340234.MERGED.root'
# output_name = 'Jets.20585471.MERGED.root'
# output_name = 'AccessorPerformanc.20404452.MERGED.PP1Plus20GlobalPlus10.root'
# output_name = 'PileupRejection.20409375.MERGED.root'
# output_name = 'PileupRejection.20418562.MERGED.root'

def main():

    # cmd = 'hadd ../data/{0} {1}{2}'.format(output_name, path_to_files, file_wildcard)
    cmd = 'hadd /eos/user/a/aleopold/hgtd/results/merged/{0} {1}{2}'.format(output_name, path_to_files, file_wildcard)
    print cmd
    subprocess.call(cmd, shell=True)


if __name__ == "__main__":
    main()

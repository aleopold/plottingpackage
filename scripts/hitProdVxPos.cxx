#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

#include "preparePaths.cxx"

TString g_input_file_path = "default";
TString g_output_plot_path = "default";
int g_skip = 0;
TString g_dataset_name = "default";
TString g_dataset_description = "default";
bool g_print_png = false;
TString g_work_status = "default";
bool g_use_log_scale = false;
bool g_do_zoom = false;
TString g_descr = "default";

TFile* g_file = nullptr;

TString g_config_file_name = "Hits.cfg";


void plot(int version) {

  //read from file
  auto hist = (TH2D*) g_file->Get("m_pileup_vx_radius_vs_z");
  hist->GetZaxis()->SetTitle("number of production vertices");
  //canvas
  auto canvas = new TCanvas("canvas", "canvas", 1000, 700);
  TPad* p1 = nullptr;
  p1 = new TPad("p1","p1",0.,0.,1.,1.);
  p1->SetRightMargin(0.2);
  // if (version == 2) {
  //   p1->SetTopMargin(0.07);
  // }
  p1->Draw();
  p1->cd();
  p1->SetLogz();

  gStyle->SetPalette(kRainBow);

  //axeshistogram
  auto axishist = new TH1F("", ";z [mm]; radius [mm]", 10, 0, 3500);
  axishist->SetAxisRange(0., 620, "Y");
  axishist->Draw();

  hist->Draw("same colz");

  gPad->RedrawAxis();
  //labels
  if (version == 1) {
    Color_t text_color = kBlack;
    atlas::ATLAS_LABEL(0.19, 0.89, text_color);
    atlas::myText(0.31, 0.89, text_color, g_work_status.Data());
    TString buffer_dataset_description = g_dataset_description;
    buffer_dataset_description.ReplaceAll("$", "#");
    atlas::myText(0.19, 0.82, text_color, buffer_dataset_description.Data(), 0.05);
  } else if (version == 2) {
    Color_t text_color = kBlack;
    atlas::ATLAS_LABEL(0.16, 0.96, text_color);
    atlas::myText(0.28, 0.96, text_color, g_work_status.Data());
    TString buffer_dataset_description = g_dataset_description;
    buffer_dataset_description.ReplaceAll("$", "#");
    atlas::myText(0.55, 0.96, text_color, buffer_dataset_description.Data(), 0.03);
  } else if (version == 3) {
    Color_t text_color = kBlack;
    TString buffer_dataset_description = g_dataset_description;
    buffer_dataset_description.ReplaceAll("$", "#");
    atlas::myText(0.16, 0.96, text_color, buffer_dataset_description.Data(), 0.05);
  }






  //printing
  TString plot_name = Form("%s/hitProdVxPos_v%i.pdf",  g_output_plot_path.Data(), version);
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  if (g_print_png) canvas->Print(plot_name);
}


void hitProdVxPos() {
  TEnv env(g_config_file_name);

  g_input_file_path = env.GetValue("hit_input_path", "default");
  g_output_plot_path = env.GetValue("output_plot_path", "default");
  g_dataset_name = env.GetValue("dataset_name", "default");
  g_dataset_description = env.GetValue("dataset_description", "My Sample");
  g_print_png = env.GetValue("print_png", false);
  g_do_zoom = env.GetValue("do_zoom", false);
  g_use_log_scale = env.GetValue("use_log_scale", false);
  g_work_status = env.GetValue("work_status", "default");

  g_file = TFile::Open(g_input_file_path, "READ");

  SetAtlasStyle();

  preparePlottingDir(g_config_file_name);

  plot(1);
  plot(2);
  plot(3);

}

#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

#include "preparePaths.cxx"

TString g_input_file_path = "/afs/cern.ch/work/a/aleopold/private/hgtd/newAnalysis/run/TrackTimeAccessors_Output_test.root";
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
int skip = 0;
TString dataset_name = "VBFinv, mu=200";
TString dataset_description = "";
bool print_png = true;
TString work_status = "Internal";
bool g_use_log_scale = false;
bool do_zoom = false;
// TString descr = "OutlierStackPlotGoodZ0_pforv02";
TString descr = "OutlierStackPlotAllZ0_PF";
// TString descr = "OutlierStackPlotBadZ0";

// TString g_config_file_name = "muon45_mu0.cfg";
// TString g_config_file_name = "pion0p1to5p0_mu0.cfg";
TString g_config_file_name = "VBFinv_mu200.cfg";

TFile* file = nullptr;

void plot(TString accessor, TString track_type) {

  // std::vector<Color_t> colors = {kTeal+4, kTeal+2, kTeal, kTeal-9, kMagenta, kRed, kRed-7, kRed+2, kRed+3};
  std::vector<Color_t> colors = {kTeal+4, kTeal+2, kTeal, kTeal-9, kMagenta, kRed, kRed, kRed, kRed};

  std::vector<TH1F*> hists;

  // auto hist1 = (TH1F*) file->Get("m_hist_timeres_outlierchecks_pforv02GoodZ0AllPrimes");
  // hists.push_back(hist1);
  // auto hist2 = (TH1F*) file->Get("m_hist_timeres_outlierchecks_pforv02GoodZ0MoreThanHalfPrimes");
  // hists.push_back(hist2);
  // auto hist3 = (TH1F*) file->Get("m_hist_timeres_outlierchecks_pforv02GoodZ0HalfPrimesHasPrimes");
  // hists.push_back(hist3);
  // auto hist4 = (TH1F*) file->Get("m_hist_timeres_outlierchecks_pforv02GoodZ0LessThanHalfPrimes");
  // hists.push_back(hist4);
  // auto hist5 = (TH1F*) file->Get("m_hist_timeres_outlierchecks_pforv02GoodZ0NoPrimesNoPossiblePrimes");
  // hists.push_back(hist5);
  // auto hist6 = (TH1F*) file->Get("m_hist_timeres_outlierchecks_pforv02GoodZ0NoPrimes1PossiblePrimes");
  // hists.push_back(hist6);
  // auto hist7 = (TH1F*) file->Get("m_hist_timeres_outlierchecks_pforv02GoodZ0NoPrimes2PossiblePrimes");
  // hists.push_back(hist7);
  // auto hist8 = (TH1F*) file->Get("m_hist_timeres_outlierchecks_pforv02GoodZ0NoPrimes3PossiblePrimes");
  // hists.push_back(hist8);
  // auto hist9 = (TH1F*) file->Get("m_hist_timeres_outlierchecks_pforv02GoodZ0NoPrimes4PossiblePrimes");
  // hists.push_back(hist9);

  auto hist1 = (TH1F*) file->Get(Form("%s/%s_m_hist_timeres_outlier_casesAllPrimes", accessor.Data(), track_type.Data()));
  hists.push_back(hist1);
  auto hist2 = (TH1F*) file->Get(Form("%s/%s_m_hist_timeres_outlier_casesMoreThanHalfPrimes", accessor.Data(), track_type.Data()));
  hists.push_back(hist2);
  auto hist3 = (TH1F*) file->Get(Form("%s/%s_m_hist_timeres_outlier_casesHalfPrimesHasPrimes", accessor.Data(), track_type.Data()));
  hists.push_back(hist3);
  auto hist4 = (TH1F*) file->Get(Form("%s/%s_m_hist_timeres_outlier_casesLessThanHalfPrimes", accessor.Data(), track_type.Data()));
  hists.push_back(hist4);
  auto hist5 = (TH1F*) file->Get(Form("%s/%s_m_hist_timeres_outlier_casesNoPrimesNoPossiblePrimes", accessor.Data(), track_type.Data()));
  hists.push_back(hist5);
  auto hist6 = (TH1F*) file->Get(Form("%s/%s_m_hist_timeres_outlier_casesNoPrimes1PossiblePrimes", accessor.Data(), track_type.Data()));
  hists.push_back(hist6);
  auto hist7 = (TH1F*) file->Get(Form("%s/%s_m_hist_timeres_outlier_casesNoPrimes2PossiblePrimes", accessor.Data(), track_type.Data()));
  hists.push_back(hist7);
  auto hist8 = (TH1F*) file->Get(Form("%s/%s_m_hist_timeres_outlier_casesNoPrimes3PossiblePrimes", accessor.Data(), track_type.Data()));
  hists.push_back(hist8);
  auto hist9 = (TH1F*) file->Get(Form("%s/%s_m_hist_timeres_outlier_casesNoPrimes4PossiblePrimes", accessor.Data(), track_type.Data()));
  hists.push_back(hist9);

  //scale to total n
  double total = 0;
  for (auto& hist : hists) {
    total += hist->Integral(0, hist->GetNbinsX()+1);
    cout << "total: " << total << '\n';
  }
  for (auto& hist : hists) {
    hist->Scale(1./total);
  }


  auto overall_hist = new TH1F("overall_hist", ";t_{reco} - t_{truth} [ns]; number of tracks", 200, -.4, 0.4);
  for (const auto& h : hists) {
    overall_hist->Add(h);
  }

  THStack* stack_hist = new THStack("stack_hist",";t_{reco} - t_{truth} [ns]; a.u.");

  //std::vector<int> colors = {42, 30, 46, 38, 28};
  int ntotal = 0;
  for (size_t i=0; i<hists.size(); i++) {
    hists.at(i)->SetFillColor(colors.at(i));
    hists.at(i)->SetLineWidth(0.);
    ntotal += hists.at(i)->Integral(0, hists.at(i)->GetNbinsX()+1);
    stack_hist->Add(hists.at(i));
  }

  stack_hist->SetMaximum(stack_hist->GetMaximum()*1.7);

  if(do_zoom) {
    stack_hist->SetMaximum(100);
  }

  TCanvas* c = new TCanvas();
  c->SetTicks(1,1);
  if (g_use_log_scale) {
    c->SetLogy();
  }

  auto hist = new TH1F("axishist", ";t_{reco} - t_{truth} [ns]; a.u.", 10, -0.4, 0.4);
  if (g_use_log_scale) {
    hist->SetAxisRange(1.e-5, 0.4, "Y");
  } else {
    hist->SetAxisRange(0, 0.08, "Y");
  }

  hist->Draw();

  stack_hist->Draw("hist same");
  if (g_use_log_scale) {
    // stack_hist->SetMaximum(stack_hist->GetMaximum()*4.0);
    // stack_hist->SetAxisRange(1.e-4, 0.3, "Y"); //for m200
    // stack_hist->GetYaxis()->SetLimits(1.e-4, 0.3);
  }

  overall_hist->SetMarkerColor(kBlack);
  overall_hist->SetLineWidth(0);
  overall_hist->SetMarkerStyle(20);
  overall_hist->SetMarkerSize(0);
  // overall_hist->Draw("same p");
  // overall_hist->Draw();

  c->Update();

  // cout << "total " << ntotal << '\n';
  // for (size_t i=0; i<hists.size(); i++) {
  //   cout << "hist " << i << " " <<  (float) hists.at(i)->Integral(0, hists.at(i)->GetNbinsX()+1)/(float) ntotal << '\n';
  //   cout << "hist " << i << " " <<  (float) hists.at(i)->Integral(0, hists.at(i)->GetNbinsX()+1)/(float) 1 << '\n';
  // }

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.31, 0.88, text_color, work_status);
  TString dataset_descr = "VBF H #rightarrow invisible, #LT#mu#GT=200 ";
  if (g_config_file_name == "muon45_mu0.cfg") {
    // dataset_descr = "Single muon, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0";
    dataset_descr = "#mu^{+}, #it{p}_{#it{T}} = 45GeV, #LT#mu#GT=0 ";
  } else if (g_config_file_name == "pion0p1to5p0_mu0.cfg") {
    // dataset_descr = "Single pion, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0";
    dataset_descr = "#pi^{+}, 0.1 < #it{p}_{#it{T}} < 5 GeV, #LT#mu#GT=0 ";
  } else if (g_config_file_name == "VBFinv_mu200.cfg") {
    dataset_descr = "VBF H #rightarrow invisible, #LT#mu#GT=200 ";
  }
  // atlas::myText(0.19, 0.84, text_color, "HGTD Full Simulation", 0.04);
  atlas::myText(0.19, 0.84, text_color, dataset_descr.Data(), 0.035);
  atlas::myText(0.19, 0.79, text_color, "p_{T}^{track} > 1.0 GeV", 0.035);
  atlas::myText(0.19, 0.74, text_color, "Timing scenario \"Initial\"", 0.035);

  // auto legend = new TLegend(0.47, 0.65, 0.85, 0.92);

  auto legend = new TLegend(0.62, 0.65, 0.9, 0.92);

  legend->SetTextFont(42);
  // legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.025);


  // // ntotal = 1.;
  // legend->AddEntry(hists.at(0), Form("100%% primaries, %.1f%%", 100 * (float) hists.at(0)->Integral(0, hists.at(0)->GetNbinsX()+1)/(float) ntotal), "f");
  // legend->AddEntry(hists.at(1), Form(">50%% primaries, %.1f%%", 100*(float) hists.at(1)->Integral(0, hists.at(1)->GetNbinsX()+1)/(float) ntotal), "f");
  // legend->AddEntry(hists.at(2), Form("50%% primaries, %.1f%%", 100*(float) hists.at(2)->Integral(0, hists.at(2)->GetNbinsX()+1)/(float) ntotal), "f");
  // legend->AddEntry(hists.at(3), Form("<50%% primaries, %.1f%%", 100*(float) hists.at(3)->Integral(0, hists.at(3)->GetNbinsX()+1)/(float) ntotal), "f");
  // legend->AddEntry(hists.at(4), Form("0%% primaries, 0 in HGTD, %.1f%%", 100*(float) hists.at(4)->Integral(0, hists.at(4)->GetNbinsX()+1)/(float) ntotal), "f");
  // legend->AddEntry(hists.at(5), Form("0%% primaries, 1 in HGTD, %.1f%%", 100*(float) hists.at(5)->Integral(0, hists.at(5)->GetNbinsX()+1)/(float) ntotal), "f");
  // legend->AddEntry(hists.at(6), Form("0%% primaries, 2 in HGTD, %.1f%%", 100*(float) hists.at(6)->Integral(0, hists.at(6)->GetNbinsX()+1)/(float) ntotal), "f");
  // legend->AddEntry(hists.at(7), Form("0%% primaries, 3 in HGTD, %.1f%%", 100*(float) hists.at(7)->Integral(0, hists.at(7)->GetNbinsX()+1)/(float) ntotal), "f");
  // legend->AddEntry(hists.at(8), Form("0%% primaries, 4 in HGTD, %.1f%%", 100*(float) hists.at(8)->Integral(0, hists.at(8)->GetNbinsX()+1)/(float) ntotal), "f");

  std::vector<TString> labels = {"Prime Frac. = 1", "0.5 < Prime Frac. < 1", "Prime Frac. = 0.5", "0 < Prime Frac. < 0.5", "Misassignment", "Confusion"};
  for (int i=0; i<labels.size(); i++) {
    legend->AddEntry(hists.at(i), labels.at(i), "f");
  }


  // legend->AddEntry(hists.at(0), "100%% primaries", "f");
  // legend->AddEntry(hists.at(1), ">50%% primaries", "f");
  // legend->AddEntry(hists.at(2), "50%% primaries", "f");
  // legend->AddEntry(hists.at(3), "<50%% primaries", "f");
  // legend->AddEntry(hists.at(4), "0%% primaries, 0 in HGTD", "f");
  // legend->AddEntry(hists.at(5), "0%% primaries, 1 in HGTD", "f");
  // legend->AddEntry(hists.at(6), "0%% primaries, 2 in HGTD", "f");
  // legend->AddEntry(hists.at(7), "0%% primaries, 3 in HGTD", "f");
  // legend->AddEntry(hists.at(8), "0%% primaries, 4 in HGTD", "f");

  legend->Draw("same");

  TString plot_name = Form("%s/%s_%s_%s_%s.pdf",  output_plot_path.Data(), dataset_name.ReplaceAll(" ", "").ReplaceAll(",", "_").Data(), descr.Data(), accessor.Data(), track_type.Data());
  if (g_use_log_scale) {
    plot_name.ReplaceAll(".pdf", "LogScale.pdf");
  }
  c->Print(plot_name);
  if (print_png) c->Print(plot_name.ReplaceAll(".pdf", ".png"));

  // std::for_each(cleanup.begin(), cleanup.end(), [](TObject* obj) {delete obj;});

}


void timeResSplitCases(TString config_file_name = "") {
  if (config_file_name != "") {
    g_config_file_name = config_file_name;
  }

  TEnv env(g_config_file_name);

  g_input_file_path = env.GetValue("input_file_path_TTI", "default");
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  dataset_description = env.GetValue("dataset_description", "My Sample");
  print_png = env.GetValue("print_png", false);
  do_zoom = env.GetValue("do_zoom", false);
  g_use_log_scale = env.GetValue("use_log_scale", false);
  work_status = env.GetValue("work_status", "default");

  file = TFile::Open(g_input_file_path, "READ");

  SetAtlasStyle();


  // std::vector<TString> algo_types = {"ProgressiveFilter_1", "PFTQv03", "PFTQv04"};
  // std::vector<TString> track_types = {"ALL", "HS"};
  std::vector<TString> algo_types = {"ProgressiveFilter_1", "PFTQv05"};
  std::vector<TString> track_types = {"ALL"};

  for (const auto& acc : algo_types) {
    for (const auto& track_type : track_types ) {
      plot(acc, track_type);
    }
  }


}

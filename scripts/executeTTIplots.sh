# root -l -q -b effCurvesVersionsEta.cxx
# root -l -q -b effCurvesVersionsTruthPt.cxx
# root -l -q -b timeResSplitCases.cxx
# root -l -q -b timeResolutionPlot.cxx


filenames="VBFinv_mu200.cfg pion0p1to5p0_mu0.cfg muon45_mu0.cfg"

for filename in $filenames; do

  # root -l -b -q effCurvesVersionsEta.cxx'("'$filename'")' > /dev/null 2>&1 &
  root -l -b -q effCurvesVersionsEta.cxx'("'$filename'")' > /dev/null 2>&1 &
  root -l -q -b timeResSplitCases.cxx'("'$filename'")' > /dev/null 2>&1 &
  root -l -q -b timeResolutionPlot.cxx'("'$filename'")' > /dev/null 2>&1 &
done

root -l -q -b effCurvesVersionsTruthPt.cxx'("'VBFinv_mu200.cfg'")'  > /dev/null 2>&1 &

#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

TString g_input_file_path = "/afs/cern.ch/user/t/tawang/public/PileupRejection_100k.root";
bool g_do_ratioplot = true;
double x_eff_min = 0.80;
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
int skip = 0;
TString dataset_name = "VBFinv, mu=200";
TString descr = "pileupRejectionVsDensityFixedHSEff";
bool print_png = true;
TString work_status = "Internal";

const double density_min = 0;
const double density_max = 1.6;
const int bins = 16;
const double bin_step = (density_max - density_min)/ (double) bins;

TFile* g_file = nullptr;

TGraph* getRatioGraph(TGraph* numerator, TGraph* denominator) {
  // int n = graphs.at(0)->GetN();
  int n = numerator->GetN();
  TGraph* r = new TGraph(n);
  r->SetTitle("");
  for (int i=0; i<n; i++) {
    double x, y;
    numerator->GetPoint(i, x, y);
    // y = graphs.at(ratio_plot_choice)->Eval(x)/y;
    y = denominator->Eval(x)/y;
    r->SetPoint(i, x, y);
  }
  r->GetXaxis()->SetLabelSize(0.075);
  r->GetYaxis()->SetLabelSize(0.075);
  r->SetLineColor(denominator->GetLineColor());
  r->SetLineStyle(denominator->GetLineStyle());
  return r;
}

double findCutForGivenEff(TGraph* graph, double eff);

TGraph* getGraph(TString eff_name_pattern, double target_pu_eff);

void PlotGraph(TString rpt_name_tmplt_ghost,
               TString rpt_name_tmplt_deltar,
               double target_hs_eff,
               TString plot_name) {

  auto graph1 = getGraph(rpt_name_tmplt_ghost, target_hs_eff);
  auto graph2 = getGraph(rpt_name_tmplt_deltar, target_hs_eff);

  auto canvas = new TCanvas();
  TPad* p1 = nullptr;
  TPad* p2 = nullptr;
  if (g_do_ratioplot) {
    p2 = new TPad("p2","p2",0.,0.02,1.,0.3);
    p2->UseCurrentStyle();
    p2->Draw();
    p1 = new TPad("p1","p1",0.,0.3,1.,1.);
    p1->Draw();
    p1->cd();
  }

  auto axishisto = new TH1F("", "", bins, density_min, density_max);
  axishisto->GetYaxis()->SetRangeUser(0., 200);
  // if (plot_name.Contains("0p5")){
  //   axishisto->GetYaxis()->SetRangeUser(0., 0.1);
  // } else if (plot_name.Contains("1p0")) {
  //   axishisto->GetYaxis()->SetRangeUser(0., 0.1);
  // } else {
  //   axishisto->GetYaxis()->SetRangeUser(0.5, 1.2);
  // }

  axishisto->GetXaxis()->SetTitle("Vertex density [1/mm]");
  axishisto->GetYaxis()->SetTitle("PU rejection");
  axishisto->Draw();

  graph1->Draw("PL same");

  graph2->SetMarkerColor(kTeal-7);
  graph2->SetLineColor(kTeal-7);
  graph2->Draw("PL same");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.31, 0.88, text_color, "Simulation Internal");
  atlas::myText(0.19, 0.83, text_color, "VBFinv, mu200, Step 3.1");
  if (plot_name.Contains("low")) {
    TString info = Form("30 < #it{p}^{jet}_{#it{T}} < 50 GeV, #epsilon(HS)=%.1f %%", target_hs_eff*100);
    atlas::myText(0.19, 0.77, text_color, info.Data());
  } else {
    TString info = Form("#it{p}^{jet}_{#it{T}} > 50 GeV, #epsilon(HS)=%.1f %%", target_hs_eff*100);
    atlas::myText(0.19, 0.77, text_color, info.Data());
  }

  TLegend* legend = new TLegend(0.22, 0.2, 0.45, 0.4);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.04);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->AddEntry(graph1, "RpT (ITk)", "pl");
  // legend->AddEntry(graph2,"RpT (ITk, #DeltaR=0.4 assoc.)", "pl");
  legend->AddEntry(graph2,"Subjet RpT (ITk+HGTD)", "pl");
  legend->Draw("same");

  if (g_do_ratioplot) {
    p2->cd();
    auto axishisto2 = new TH1F("", "", 10, density_min, density_max);
    axishisto2->GetYaxis()->SetRangeUser(0.8, 1.3);
    axishisto2->GetYaxis()->SetTitle("ratio");
    axishisto2->GetYaxis()->SetNdivisions(505);
    axishisto2->GetYaxis()->SetTitleOffset(0.55);
    axishisto2->GetYaxis()->SetTitleSize(0.13); // labels will be 14 pixels
    axishisto2->GetYaxis()->SetLabelSize(0.1); // labels will be 14 pixels
    axishisto2->GetXaxis()->SetLabelSize(0.1); // labels will be 14 pixels
    axishisto2->Draw();

    auto line = new TLine(density_min, 1.0, density_max, 1.0);
    line->SetLineStyle(2);
    line->Draw("same");
    auto line2 = new TLine(density_min, 1.2, density_max, 1.2);
    line2->SetLineStyle(2);
    line2->Draw("same");
    // vector<int> chosen_pos = {1, 2, 3, 4, 5};
    // for (auto pos : chosen_pos) {
    TGraph* ratio_graph = getRatioGraph(graph1, graph2);
    ratio_graph->Draw("L same");
    // }
  }

  TString plot_name_string = Form("%s/%s_%s_%s.pdf",  output_plot_path.Data(), dataset_name.ReplaceAll(" ", "").ReplaceAll(",", "_").Data(), descr.Data(), plot_name.Data());
  canvas->Print(plot_name_string);
  plot_name_string.Form("%s/%s_%s_%s.png",  output_plot_path.Data(), dataset_name.Data(), descr.Data(), plot_name.Data());
  if (print_png) canvas->Print(plot_name_string);

  delete canvas;
  delete graph1;
  delete graph2;
  delete axishisto;
  delete legend;
}

TGraph* getGraph(TString eff_name_pattern, double target_pu_eff) {
  auto graph = new TGraph();
  graph->SetName(eff_name_pattern.Data());

  int point_i=0;
  for(int i=0; i<bins; i++) {
    TString pu_name = Form(eff_name_pattern, i);
    auto pu_eff = (TEfficiency*) g_file->Get(pu_name);
    if (pu_eff == nullptr) {cout << pu_name << "does not exist.\n";}
    auto pu_graph = pu_eff->CreateGraph();
    double cut_val = findCutForGivenEff(pu_graph, target_pu_eff);
    // cout << "cut val: " << cut_val << '\n';
    //get the corresponding hs eff
    // pu_name.ReplaceAll("pu", "hs");
    pu_name.ReplaceAll("hs", "pu");
    auto hs_eff = (TEfficiency*) g_file->Get(pu_name);
    auto hs_graph = hs_eff->CreateGraph();
    double hs_eff_val = hs_graph->Eval(cut_val);
    cout << " add point " << i+1 << " " << density_min + i*bin_step << " " << hs_eff_val << '\n';
    if (hs_eff_val>0) {
      graph->SetPoint(point_i, density_min + i*bin_step + bin_step/2., 1./hs_eff_val);
      point_i++;
    }
    delete pu_graph;
    delete hs_graph;
  }
  return graph;
}

double findCutForGivenEff(TGraph* graph, double eff) {
  //from min value
  int steps = 1000;
  double xmin = graph->GetXaxis()->GetXmin();
  double xmax = graph->GetXaxis()->GetXmax();
  double stepsize = (xmax - xmin)/(double)steps;

  for(int i=0; i<steps; i++) {
    double x = xmin + ((double)i)*stepsize;
    double y = graph->Eval(x);
    if (y<=eff) {
      cout << "found cut value: " << x << " at eff " << y << '\n';
      return x;
    }
  }
  cout << "WARNING, EFF DOES NOT FALL BELOW DEFINED VALUE!!" << '\n';
  cout << "lowest value: " << graph->Eval(xmax) << '\n';
  return -1;
}

void pileupRejectionRejVsDensAtFixedEff() {
  TEnv env("pileup.cfg");

  g_input_file_path = env.GetValue("input_file_path", "default");
  g_do_ratioplot = env.GetValue("do_ratioplot", false);
  x_eff_min = env.GetValue("x_eff_min", 0.8);
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");


  g_file = TFile::Open(g_input_file_path, "READ");
  SetAtlasStyle();

  // PlotGraph("m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_deltar0p4_density_bin_%i", 0.95, "highpt_95");
  //
  // PlotGraph("m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_deltar0p4_density_bin_%i", 0.95, "lowpt_95");
  //
  // PlotGraph("m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_deltar0p4_density_bin_%i", 0.90, "highpt_90");
  //
  // PlotGraph("m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_deltar0p4_density_bin_%i", 0.90, "lowpt_90");
  //
  // PlotGraph("m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_deltar0p4_density_bin_%i", 0.85, "highpt_85");
  //
  // PlotGraph("m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_deltar0p4_density_bin_%i", 0.85, "lowpt_85");
  //
  // PlotGraph("m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_deltar0p4_density_bin_%i", 0.80, "highpt_80");
  //
  // PlotGraph("m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_deltar0p4_density_bin_%i", 0.80, "lowpt_80");

  PlotGraph("m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_subjetrpt_density_bin_%i", 0.95, "highpt_95");

  PlotGraph("m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_subjetrpt_density_bin_%i", 0.95, "lowpt_95");

  PlotGraph("m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_subjetrpt_density_bin_%i", 0.90, "highpt_90");

  PlotGraph("m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_subjetrpt_density_bin_%i", 0.90, "lowpt_90");

  PlotGraph("m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_subjetrpt_density_bin_%i", 0.85, "highpt_85");

  PlotGraph("m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_subjetrpt_density_bin_%i", 0.85, "lowpt_85");

  PlotGraph("m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_subjetrpt_density_bin_%i", 0.80, "highpt_80");

  PlotGraph("m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_subjetrpt_density_bin_%i", 0.80, "lowpt_80");
}

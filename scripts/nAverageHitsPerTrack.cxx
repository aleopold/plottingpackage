#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

TString g_config_file_name = "muon45_mu0.cfg";
// TString g_config_file_name = "pion0p1to5p0_mu0.cfg";

TFile* g_file = nullptr;
TString work_status =  "Work in progress";
TString dataset_name =  "VBFinv, mu=200, ITk Layout";
TString output_plot_path = "Plots/";
TString g_input_file_path = "default";
TString g_dataset_description = "default";
bool print_png;
float g_xaxis_max = 660;
int g_nbins = 27;

void plottingPrimaryHitsVsRadius() {

  auto profile = (TProfile*) g_file->Get("HS_m_prof_primaryhits_vs_radius");
  if (profile==nullptr) {cout << "nAverageHitsPerTrack histogram not found" << endl; return;}
  profile->Sumw2();
  profile->SetMarkerStyle(21);
  profile->SetMarkerSize(2.);
  Color_t marker_color = kBlack;
  profile->SetMarkerColor(marker_color);
  profile->SetLineColor(marker_color);

  //plotting part
  TCanvas *c = new TCanvas();
  gPad->SetTickx(0);
  c->SetTicks(1,1);

  auto hist = new TH1F("axishist", ";r [mm];<N_{hits}> per track / 20 mm", 10, 120, g_xaxis_max);
  hist->SetAxisRange(0., 4, "Y");

  Color_t text_color = kBlack;

  hist->Draw();
  profile->Draw("same");


  //get scale on top
  TF1 *f2 =new TF1("f2","TMath::Tan(2*TMath::ATan(TMath::Exp(-x)))*3500",2.420,4.0);
  gPad->SetTickx(0);

  // f2 =TF1("f2","TMath::Tan(2*TMath::ATan(TMath::Exp(-x)))*3500",2.420,4.0);
  // axis_eta = TGaxis(120,4.5,660,4.5,"f2",510,"-");

  TGaxis *axis_eta = new TGaxis(120, 10, g_xaxis_max, 10,"f2",310,"-");
  axis_eta->SetTitle("|#eta|");
  axis_eta->SetTitleSize(0.05);
  axis_eta->SetTitleOffset(0.35);
  // axis_eta->Draw("same");
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.32, 0.88, text_color, work_status.Data());
  atlas::myText(0.19, 0.85, text_color, g_dataset_description.Data(), 0.03);
  atlas::myText(0.19, 0.81, text_color, "p_{T}^{track} > 1.0 [GeV]", 0.03);

  TString plot_name = Form("%s/nAverageHitsPerTrack_%s.pdf",  output_plot_path.Data(), dataset_name.Data());
  c->Print(plot_name);
  plot_name.Form("%s/nAverageHitsPerTrack_%s.png",  output_plot_path.Data(), dataset_name.Data());
  if (print_png) c->Print(plot_name);

  // delete legend;
  // delete t;
  // delete c;

  // file->Close();

}



void nAverageHitsPerTrack() {
  TEnv env(g_config_file_name);

  g_input_file_path = env.GetValue("input_file_path_TTI", "default");
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");
  g_dataset_description = env.GetValue("dataset_description", "default");

  g_file = TFile::Open(g_input_file_path, "READ");
  SetAtlasStyle();

  plottingPrimaryHitsVsRadius();

  g_file->Close();
}

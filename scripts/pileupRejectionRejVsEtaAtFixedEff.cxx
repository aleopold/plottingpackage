#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

TString g_input_file_path = "/afs/cern.ch/user/t/tawang/public/PileupRejection_100k.root";
bool g_do_ratioplot = true;
double x_eff_min = 0.80;
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
int skip = 0;
TString dataset_name = "VBFinv, mu=200";
TString descr = "pileupRejectionVsEtaFixedHSEff";
bool print_png = true;
TString work_status = "Internal";

const double eta_min = 0;
const double eta_max = 4.;
const int bins = 10;
const double bin_step = (eta_max - eta_min)/ (double) bins;

TFile* g_file = nullptr;

//to get the y value from x I can use Eval, but the other way around
//does not exist. So implement a recursive function that evals at two points,
//and closes in
double findCutForGivenEff(TGraph* graph, double eff);

TGraph* getGraph(TString eff_name_pattern, double target_pu_eff);

void PlotGraph(TString rpt_name_tmplt_ghost,
               TString rpt_name_tmplt_deltar,
               double target_hs_eff,
               TString plot_name) {

  auto graph_rpt_ghosttracks = getGraph(rpt_name_tmplt_ghost, target_hs_eff);
  auto graph_rpt_deltar = getGraph(rpt_name_tmplt_deltar, target_hs_eff);

  auto canvas = new TCanvas();

  auto axishisto = new TH1F("", "", bins, eta_min, eta_max);
  axishisto->GetYaxis()->SetRangeUser(0., 200);
  // if (plot_name.Contains("0p5")){
  //   axishisto->GetYaxis()->SetRangeUser(0., 0.1);
  // } else if (plot_name.Contains("1p0")) {
  //   axishisto->GetYaxis()->SetRangeUser(0., 0.1);
  // } else {
  //   axishisto->GetYaxis()->SetRangeUser(0.5, 1.2);
  // }

  axishisto->GetXaxis()->SetTitle("|#eta|");
  axishisto->GetYaxis()->SetTitle("PU rejection");
  axishisto->Draw();

  graph_rpt_ghosttracks->Draw("PL same");

  graph_rpt_deltar->SetMarkerColor(kTeal-7);
  graph_rpt_deltar->SetLineColor(kTeal-7);
  graph_rpt_deltar->Draw("PL same");

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.31, 0.88, text_color, "Simulation Internal");
  atlas::myText(0.19, 0.83, text_color, "VBFinv, mu200, Step 3.1");
  if (plot_name.Contains("low")) {
    TString info = Form("30 < #it{p}^{jet}_{#it{T}} < 50 GeV, #epsilon(HS)=%.1f %%", target_hs_eff*100);
    atlas::myText(0.19, 0.77, text_color, info.Data());
  } else {
    TString info = Form("#it{p}^{jet}_{#it{T}} > 50 GeV, #epsilon(HS)=%.1f %%", target_hs_eff*100);
    atlas::myText(0.19, 0.77, text_color, info.Data());
  }

  TLegend* legend = new TLegend(0.22, 0.2, 0.45, 0.4);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.04);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->AddEntry(graph_rpt_ghosttracks, "ITk", "pl");
  legend->AddEntry(graph_rpt_deltar,"ITk + HGTD", "pl");
  legend->Draw("same");

  TString plot_name_string = Form("%s/%s_%s_%s.pdf",  output_plot_path.Data(), dataset_name.ReplaceAll(" ", "").ReplaceAll(",", "_").Data(), descr.Data(), plot_name.Data());
  canvas->Print(plot_name_string);
  plot_name_string.Form("%s/%s_%s_%s.png",  output_plot_path.Data(), dataset_name.Data(), descr.Data(), plot_name.Data());
  if (print_png) canvas->Print(plot_name_string);

  delete canvas;
  delete graph_rpt_ghosttracks;
  delete graph_rpt_deltar;
  delete axishisto;
  delete legend;
}

TGraph* getGraph(TString eff_name_pattern, double target_pu_eff) {
  auto graph = new TGraph();
  graph->SetName(eff_name_pattern.Data());

  int point_i=0;
  for(int i=0; i<bins; i++) {
    TString pu_name = Form(eff_name_pattern, i);
    auto pu_eff = (TEfficiency*) g_file->Get(pu_name);
    if (pu_eff == nullptr) {cout << pu_name << "does not exist.\n";}
    auto pu_graph = pu_eff->CreateGraph();
    double cut_val = findCutForGivenEff(pu_graph, target_pu_eff);
    // cout << "cut val: " << cut_val << '\n';
    //get the corresponding hs eff
    // pu_name.ReplaceAll("pu", "hs");
    pu_name.ReplaceAll("hs", "pu");
    auto hs_eff = (TEfficiency*) g_file->Get(pu_name);
    auto hs_graph = hs_eff->CreateGraph();
    double hs_eff_val = hs_graph->Eval(cut_val);
    cout << " add point " << i+1 << " " << eta_min + i*bin_step << " " << hs_eff_val << '\n';
    if (hs_eff_val>0) {
      graph->SetPoint(point_i, eta_min + i*bin_step + bin_step/2., 1./hs_eff_val);
      point_i++;
    }
    delete pu_graph;
    delete hs_graph;
  }
  return graph;
}

double findCutForGivenEff(TGraph* graph, double eff) {
  //from min value
  int steps = 1000;
  double xmin = graph->GetXaxis()->GetXmin();
  double xmax = graph->GetXaxis()->GetXmax();
  double stepsize = (xmax - xmin)/(double)steps;

  for(int i=0; i<steps; i++) {
    double x = xmin + ((double)i)*stepsize;
    double y = graph->Eval(x);
    if (y<=eff) {
      cout << "found cut value: " << x << " at eff " << y << '\n';
      return x;
    }
  }
  cout << "WARNING, EFF DOES NOT FALL BELOW DEFINED VALUE!!" << '\n';
  cout << "lowest value: " << graph->Eval(xmax) << '\n';
  return -1;
}

void pileupRejectionRejVsEtaAtFixedEff() {
  TEnv env("pileup.cfg");

  g_input_file_path = env.GetValue("input_file_path", "default");
  g_do_ratioplot = env.GetValue("do_ratioplot", false);
  x_eff_min = env.GetValue("x_eff_min", 0.8);
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");


  g_file = TFile::Open(g_input_file_path, "READ");
  SetAtlasStyle();

  // PlotGraph("m_eff_hs_rpt_param_high_pt_etabin_%i", "m_eff_hs_subjetrpt_param_high_pt_etabin_%i", 0.95, "highpt_95");
  //
  // PlotGraph("m_eff_hs_rpt_param_low_pt_etabin_%i", "m_eff_hs_subjetrpt_param_low_pt_etabin_%i", 0.95, "lowpt_95");
  //
  // PlotGraph("m_eff_hs_rpt_param_high_pt_etabin_%i", "m_eff_hs_subjetrpt_param_high_pt_etabin_%i", 0.90, "highpt_90");
  //
  // PlotGraph("m_eff_hs_rpt_param_low_pt_etabin_%i", "m_eff_hs_subjetrpt_param_low_pt_etabin_%i", 0.90, "lowpt_90");
  //
  // PlotGraph("m_eff_hs_rpt_param_high_pt_etabin_%i", "m_eff_hs_subjetrpt_param_high_pt_etabin_%i", 0.85, "highpt_85");
  //
  // PlotGraph("m_eff_hs_rpt_param_low_pt_etabin_%i", "m_eff_hs_subjetrpt_param_low_pt_etabin_%i", 0.85, "lowpt_85");
  //
  // PlotGraph("m_eff_hs_rpt_param_high_pt_etabin_%i", "m_eff_hs_subjetrpt_param_low_pt_etabin_%i", 0.80, "highpt_80");
  //
  // PlotGraph("m_eff_hs_rpt_param_low_pt_etabin_%i", "m_eff_hs_subjetrpt_param_low_pt_etabin_%i", 0.80, "lowpt_80");

  PlotGraph("stdm_eff_hs_rpt_low_pt_etabin_%i", "bdtm_eff_hs_rpt_low_pt_etabin_%i", 0.85, "lowpt_85");

  PlotGraph("stdm_eff_hs_rpt_high_pt_etabin_%i", "bdtm_eff_hs_rpt_high_pt_etabin_%i", 0.85, "highpt_85");
}


#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

TString g_input_file_path = "default";
bool g_do_ratioplot = true;
// double x_eff_min = 0.80;
TString output_plot_path = "default/path";
int skip = 0;
TString dataset_name = "SomeSample";
TString dataset_description = "Some Sample";
bool print_png = true;
TString work_status = "Work Status";

TFile* g_file = nullptr;

std::vector<Color_t> colors = {kBlack, kRed, kBlue, kGreen, kTeal-7};

TGraph* getGraph(TString eff_plot_name_template, double target_eff, bool doinverse, TString from, TString to, float xmin, float binstep, int n_bins);
TGraph* getRatioGraph(TGraph* numerator, TGraph* denominator);
double findCutForGivenEff(TGraph* graph, double eff);

void PlotGraph(std::vector<TString> eff_plot_name_templates, std::vector<TString> legends,
               double target_eff, TString from, TString to, bool doinverse,
               TString plot_name, int bins, float xmin, float xmax,
               float ymin, float ymax, float ratioymin, float ratioymax,
               vector<int> chosen_ratio_pos, TString xtitle, TString ytitle) {

  std::vector<TGraph*> graphs;
  double bin_step = (xmax - xmin)/ (double) bins;
  for (const auto& name : eff_plot_name_templates) {
    auto graph = getGraph(name, target_eff, doinverse, from, to, xmin, bin_step, bins);
    graphs.push_back(graph);
  }

  auto canvas = new TCanvas();

  TPad* p1 = nullptr;
  TPad* p2 = nullptr;
  if (g_do_ratioplot) {
    p2 = new TPad("p2","p2",0.,0.02,1.,0.3);
    p2->UseCurrentStyle();
    p2->Draw();
    p1 = new TPad("p1","p1",0.,0.3,1.,1.);
    p1->Draw();
    p1->cd();
  }

  auto axishisto = new TH1F("", "", bins, xmin, xmax);
  axishisto->GetYaxis()->SetRangeUser(ymin, ymax);
  axishisto->GetXaxis()->SetTitle(xtitle);
  axishisto->GetYaxis()->SetTitle(ytitle);
  axishisto->Draw();

  int color_i = 0;
  for (const auto& graph : graphs) {
    graph->SetMarkerColor(colors.at(color_i));
    graph->SetLineColor(colors.at(color_i));
    graph->Draw("PL same");
    color_i++;
  }

  Color_t text_color = kBlack;
  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.31, 0.88, text_color, work_status.Data());
  atlas::myText(0.19, 0.83, text_color, dataset_description.Data());
  if (plot_name.Contains("low")) {
    TString info = Form("30 < #it{p}^{jet}_{#it{T}} < 50 GeV, #epsilon=%.1f %%", target_eff*100);
    atlas::myText(0.19, 0.77, text_color, info.Data());
  } else {
    TString info = Form("#it{p}^{jet}_{#it{T}} > 50 GeV, #epsilon=%.1f %%", target_eff*100);
    atlas::myText(0.19, 0.77, text_color, info.Data());
  }

  // TLegend* legend = new TLegend(0.22, 0.2, 0.45, 0.4);
  // TLegend* legend = new TLegend(0.65, 0.8, 0.95, 0.93);
  TLegend* legend = new TLegend(0.55, 0.8, 0.95, 0.93);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.03);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  int leg_i = 0;
  for (const auto& leg : legends) {
    legend->AddEntry(graphs.at(leg_i), leg.Data(), "pl");
    leg_i++;
  }
  legend->Draw("same");

  if (g_do_ratioplot) {
    p2->cd();
    auto axishisto2 = new TH1F("", "", 10, xmin, xmax);
    axishisto2->GetYaxis()->SetRangeUser(ratioymin, ratioymax);
    axishisto2->GetYaxis()->SetTitle("ratio");
    axishisto2->GetYaxis()->SetNdivisions(505);
    axishisto2->GetYaxis()->SetTitleOffset(0.55);
    axishisto2->GetYaxis()->SetTitleSize(0.13); // labels will be 14 pixels
    axishisto2->GetYaxis()->SetLabelSize(0.1); // labels will be 14 pixels
    axishisto2->GetXaxis()->SetLabelSize(0.1); // labels will be 14 pixels
    axishisto2->Draw();

    auto line = new TLine(xmin, 1.0, xmax, 1.0);
    line->SetLineStyle(2);
    line->Draw("same");
    for (auto pos : chosen_ratio_pos) {
      TGraph* ratio_graph = getRatioGraph(graphs.at(0), graphs.at(pos));
      ratio_graph->Draw("L same");
    }
  }

  TString plot_name_string = Form("%s/%s_%s.pdf",  output_plot_path.Data(), dataset_name.Data(), plot_name.Data());
  canvas->Print(plot_name_string);
  plot_name_string.Form("%s/%s_%s.png",  output_plot_path.Data(), dataset_name.Data(), plot_name.Data());
  if (print_png) canvas->Print(plot_name_string);

  delete canvas;
  for(int i=0; i<graphs.size(); i++) {
    delete graphs.at(i);
  }
  delete axishisto;
  delete legend;
}

TGraph* getGraph(TString eff_plot_name_template, double target_eff, bool doinverse, TString from, TString to, float xmin, float binstep, int n_bins) {
  auto graph = new TGraph();
  graph->SetName(eff_plot_name_template.Data());

  int point_i=0;
  for(int i=0; i<n_bins; i++) {
    TString pu_name = Form(eff_plot_name_template, i);
    auto pu_eff = (TEfficiency*) g_file->Get(pu_name);
    if (pu_eff == nullptr) {cout << pu_name << "does not exist.\n";}
    auto pu_graph = pu_eff->CreateGraph();
    double cut_val = findCutForGivenEff(pu_graph, target_eff);
    pu_name.ReplaceAll(from, to);
    auto hs_eff = (TEfficiency*) g_file->Get(pu_name);
    auto hs_graph = hs_eff->CreateGraph();
    double hs_eff_val = hs_graph->Eval(cut_val);
    if (doinverse) {
      if (hs_eff_val>0) {
        graph->SetPoint(point_i, xmin + i*binstep + binstep/2., 1./hs_eff_val);
        point_i++;
      } else {
        cout << "WARNING: point " << i << " missing in " << eff_plot_name_template << '\n';
      }
    } else {
      graph->SetPoint(point_i, xmin + i*binstep + binstep/2., hs_eff_val);
      point_i++;
    }

    delete pu_graph;
    delete hs_graph;
  }
  return graph;
}

TGraph* getRatioGraph(TGraph* numerator, TGraph* denominator) {
  // int n = graphs.at(0)->GetN();
  int n = numerator->GetN();
  TGraph* r = new TGraph(n);
  r->SetTitle("");
  for (int i=0; i<n; i++) {
    double x, y;
    numerator->GetPoint(i, x, y);
    // y = graphs.at(ratio_plot_choice)->Eval(x)/y;
    y = denominator->Eval(x)/y;
    r->SetPoint(i, x, y);
  }
  r->GetXaxis()->SetLabelSize(0.075);
  r->GetYaxis()->SetLabelSize(0.075);
  r->SetLineColor(denominator->GetLineColor());
  r->SetLineStyle(denominator->GetLineStyle());
  r->SetLineWidth(2.0);
  return r;
}

double findCutForGivenEff(TGraph* graph, double eff) {
  //from min value
  int steps = 1000;
  double xmin = graph->GetXaxis()->GetXmin();
  double xmax = graph->GetXaxis()->GetXmax();
  double stepsize = (xmax - xmin)/(double)steps;

  for(int i=0; i<steps; i++) {
    double x = xmin + ((double)i)*stepsize;
    double y = graph->Eval(x);
    if (y<=eff) {
      // cout << "found cut value: " << x << " at eff " << y << '\n';
      return x;
    }
  }
  cout << "WARNING, EFF DOES NOT FALL BELOW DEFINED VALUE!!" << '\n';
  cout << "lowest value: " << graph->Eval(xmax) << '\n';
  return -1;
}

void pilupRocVsX() {
  TEnv env("pileup.cfg");

  g_input_file_path = env.GetValue("input_file_path", "default");
  g_do_ratioplot = env.GetValue("do_ratioplot", false);
  // x_eff_min = env.GetValue("x_eff_min", 0.8);
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  dataset_description = env.GetValue("dataset_description", "My Sample");
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");

  g_file = TFile::Open(g_input_file_path, "READ");
  SetAtlasStyle();

  //PU rejection vs PU density, high pt

  PlotGraph({"m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_subjetrpt_density_bin_%i", "m_eff_hs_high_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_hs_high_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.95, "hs", "pu", true,
  "PURejVsPUDensity_highpt_95", 16, 0.0, 1.6, 0., 400., 0.9, 1.4, {1, 2, 3}, "Vertex density [1/mm]", "PU rejection");

  PlotGraph({"m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_subjetrpt_density_bin_%i", "m_eff_hs_high_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_hs_high_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.9, "hs", "pu", true,
  "PURejVsPUDensity_highpt_90", 16, 0.0, 1.6, 0., 400., 0.9, 1.4, {1, 2, 3}, "Vertex density [1/mm]", "PU rejection");

  PlotGraph({"m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_subjetrpt_density_bin_%i", "m_eff_hs_high_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_hs_high_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.85, "hs", "pu", true,
  "PURejVsPUDensity_highpt_85", 16, 0.0, 1.6, 0., 400., 0.9, 1.4, {1, 2, 3}, "Vertex density [1/mm]", "PU rejection");

  PlotGraph({"m_eff_hs_high_pt_ghost_density_bin_%i", "m_eff_hs_high_pt_subjetrpt_density_bin_%i", "m_eff_hs_high_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_hs_high_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.8, "hs", "pu", true,
  "PURejVsPUDensity_highpt_80", 16, 0.0, 1.6, 0., 400., 0.9, 1.4, {1, 2, 3}, "Vertex density [1/mm]", "PU rejection");

  //PU rejection vs PU density, low pt
  //
  PlotGraph({"m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_subjetrpt_density_bin_%i", "m_eff_hs_low_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_hs_low_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.95, "hs", "pu", true,
  "PURejVsPUDensity_lowpt_95", 16, 0.0, 1.6, 0., 200., 0.8, 1.3, {1, 2, 3}, "Vertex density [1/mm]", "PU rejection");
  //
  PlotGraph({"m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_subjetrpt_density_bin_%i", "m_eff_hs_low_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_hs_low_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.9, "hs", "pu", true,
  "PURejVsPUDensity_lowpt_90", 16, 0.0, 1.6, 0., 200., 0.8, 1.3, {1, 2, 3}, "Vertex density [1/mm]", "PU rejection");

  PlotGraph({"m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_subjetrpt_density_bin_%i", "m_eff_hs_low_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_hs_low_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.85, "hs", "pu", true,
  "PURejVsPUDensity_lowpt_85", 16, 0.0, 1.6, 0., 200., 0.8, 1.3, {1, 2, 3}, "Vertex density [1/mm]", "PU rejection");

  PlotGraph({"m_eff_hs_low_pt_ghost_density_bin_%i", "m_eff_hs_low_pt_subjetrpt_density_bin_%i", "m_eff_hs_low_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_hs_low_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.8, "hs", "pu", true,
  "PURejVsPUDensity_lowpt_80", 16, 0.0, 1.6, 0., 200., 0.8, 1.3, {1, 2, 3}, "Vertex density [1/mm]", "PU rejection");
  //
  // //HS efficiency vs PU density, high pt
  //
  PlotGraph({"m_eff_pu_high_pt_ghost_density_bin_%i", "m_eff_pu_high_pt_subjetrpt_density_bin_%i", "m_eff_pu_high_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_pu_high_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.01, "pu", "hs", false,
  "HSEffVsPUDensity_highpt_1", 16, 0.0, 1.6, 0., 1.3, 0.95, 1.1, {1, 2, 3}, "Vertex density [1/mm]", "HS efficiency");

  PlotGraph({"m_eff_pu_high_pt_ghost_density_bin_%i", "m_eff_pu_high_pt_subjetrpt_density_bin_%i", "m_eff_pu_high_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_pu_high_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.02, "pu", "hs", false,
  "HSEffVsPUDensity_highpt_2", 16, 0.0, 1.6, 0., 1.3, 0.95, 1.1, {1, 2, 3}, "Vertex density [1/mm]", "HS efficiency");

  PlotGraph({"m_eff_pu_high_pt_ghost_density_bin_%i", "m_eff_pu_high_pt_subjetrpt_density_bin_%i", "m_eff_pu_high_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_pu_high_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.03, "pu", "hs", false,
  "HSEffVsPUDensity_highpt_3", 16, 0.0, 1.6, 0., 1.3, 0.95, 1.1, {1, 2, 3}, "Vertex density [1/mm]", "HS efficiency");

  PlotGraph({"m_eff_pu_high_pt_ghost_density_bin_%i", "m_eff_pu_high_pt_subjetrpt_density_bin_%i", "m_eff_pu_high_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_pu_high_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.05, "pu", "hs", false,
  "HSEffVsPUDensity_highpt_5", 16, 0.0, 1.6, 0., 1.3, 0.95, 1.1, {1, 2, 3}, "Vertex density [1/mm]", "HS efficiency");

  //HS efficiency vs PU density, low pt

  PlotGraph({"m_eff_pu_low_pt_ghost_density_bin_%i", "m_eff_pu_low_pt_subjetrpt_density_bin_%i", "m_eff_pu_low_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_pu_low_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.01, "pu", "hs", false,
  "HSEffVsPUDensity_lowpt_1", 16, 0.0, 1.6, 0., 1.3, 0.95, 1.1, {1, 2, 3}, "Vertex density [1/mm]", "HS efficiency");

  PlotGraph({"m_eff_pu_low_pt_ghost_density_bin_%i", "m_eff_pu_low_pt_subjetrpt_density_bin_%i", "m_eff_pu_low_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_pu_low_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.02, "pu", "hs", false,
  "HSEffVsPUDensity_lowpt_2", 16, 0.0, 1.6, 0., 1.3, 0.95, 1.1, {1, 2, 3}, "Vertex density [1/mm]", "HS efficiency");

  PlotGraph({"m_eff_pu_low_pt_ghost_density_bin_%i", "m_eff_pu_low_pt_subjetrpt_density_bin_%i", "m_eff_pu_low_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_pu_low_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.03, "pu", "hs", false,
  "HSEffVsPUDensity_lowpt_3", 16, 0.0, 1.6, 0., 1.3, 0.95, 1.1, {1, 2, 3}, "Vertex density [1/mm]", "HS efficiency");

  PlotGraph({"m_eff_pu_low_pt_ghost_density_bin_%i", "m_eff_pu_low_pt_subjetrpt_density_bin_%i", "m_eff_pu_low_pt_hgtdrpt_defaultpf_density_bin_%i", "m_eff_pu_low_pt_hgtdrpt_tcv05pforv02_density_bin_%i" }, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.05, "pu", "hs", false,
  "HSEffVsPUDensity_lowpt_5", 16, 0.0, 1.6, 0., 1.3, 0.95, 1.1, {1, 2, 3}, "Vertex density [1/mm]", "HS efficiency");

  // //PU rejection vs eta, high pt

  PlotGraph({"m_eff_hs_high_pt_ghost_eta_bin_%i", "m_eff_hs_high_pt_subjetrpt_eta_bin_%i", "m_eff_hs_high_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_hs_high_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.95, "hs", "pu", true,
  "PURejVsEta_highpt_95", 20, 0.0, 4, 0., 200, 0.9, 1.3, {1, 2, 3}, "|#eta|", "PU Rejection");

  PlotGraph({"m_eff_hs_high_pt_ghost_eta_bin_%i", "m_eff_hs_high_pt_subjetrpt_eta_bin_%i", "m_eff_hs_high_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_hs_high_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.90, "hs", "pu", true,
  "PURejVsEta_highpt_90", 20, 0.0, 4, 0., 200, 0.9, 1.3, {1, 2, 3}, "|#eta|", "PU Rejection");

  PlotGraph({"m_eff_hs_high_pt_ghost_eta_bin_%i", "m_eff_hs_high_pt_subjetrpt_eta_bin_%i", "m_eff_hs_high_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_hs_high_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.85, "hs", "pu", true,
  "PURejVsEta_highpt_85", 20, 0.0, 4, 0., 200, 0.9, 1.3, {1, 2, 3}, "|#eta|", "PU Rejection");

  PlotGraph({"m_eff_hs_high_pt_ghost_eta_bin_%i", "m_eff_hs_high_pt_subjetrpt_eta_bin_%i", "m_eff_hs_high_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_hs_high_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.80, "hs", "pu", true,
  "PURejVsEta_highpt_80", 20, 0.0, 4, 0., 200, 0.9, 1.3, {1, 2, 3}, "|#eta|", "PU Rejection");


  //PU rejection vs eta, low pt

  PlotGraph({"m_eff_hs_low_pt_ghost_eta_bin_%i", "m_eff_hs_low_pt_subjetrpt_eta_bin_%i", "m_eff_hs_low_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_hs_low_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.95, "hs", "pu", true,
  "PURejVsEta_lowpt_95", 20, 0.0, 4, 0., 200, 0.9, 1.3, {1, 2, 3}, "|#eta|", "PU Rejection");

  PlotGraph({"m_eff_hs_low_pt_ghost_eta_bin_%i", "m_eff_hs_low_pt_subjetrpt_eta_bin_%i", "m_eff_hs_low_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_hs_low_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.90, "hs", "pu", true,
  "PURejVsEta_lowpt_90", 20, 0.0, 4, 0., 200, 0.9, 1.3, {1, 2, 3}, "|#eta|", "PU Rejection");

  PlotGraph({"m_eff_hs_low_pt_ghost_eta_bin_%i", "m_eff_hs_low_pt_subjetrpt_eta_bin_%i", "m_eff_hs_low_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_hs_low_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.85, "hs", "pu", true,
  "PURejVsEta_lowpt_85", 20, 0.0, 4, 0., 200, 0.9, 1.3, {1, 2, 3}, "|#eta|", "PU Rejection");

  PlotGraph({"m_eff_hs_low_pt_ghost_eta_bin_%i", "m_eff_hs_low_pt_subjetrpt_eta_bin_%i", "m_eff_hs_low_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_hs_low_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.80, "hs", "pu", true,
  "PURejVsEta_lowpt_80", 20, 0.0, 4, 0., 200, 0.9, 1.3, {1, 2, 3}, "|#eta|", "PU Rejection");

  // //HS efficiency vs eta, high pt

  PlotGraph({"m_eff_pu_high_pt_ghost_eta_bin_%i", "m_eff_pu_high_pt_subjetrpt_eta_bin_%i", "m_eff_pu_high_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_pu_high_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.01, "pu", "hs", false,
  "HSEffVsEta_highpt_1", 20, 0.0, 4, 0., 1.3, 0.95, 1.05, {1, 2, 3}, "|#eta|", "HS efficiency");

  PlotGraph({"m_eff_pu_high_pt_ghost_eta_bin_%i", "m_eff_pu_high_pt_subjetrpt_eta_bin_%i", "m_eff_pu_high_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_pu_high_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.02, "pu", "hs", false,
  "HSEffVsEta_highpt_2", 20, 0.0, 4, 0., 1.3, 0.95, 1.05, {1, 2, 3}, "|#eta|", "HS efficiency");

  PlotGraph({"m_eff_pu_high_pt_ghost_eta_bin_%i", "m_eff_pu_high_pt_subjetrpt_eta_bin_%i", "m_eff_pu_high_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_pu_high_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.03, "pu", "hs", false,
  "HSEffVsEta_highpt_3", 20, 0.0, 4, 0., 1.3, 0.95, 1.05, {1, 2, 3}, "|#eta|", "HS efficiency");

  PlotGraph({"m_eff_pu_high_pt_ghost_eta_bin_%i", "m_eff_pu_high_pt_subjetrpt_eta_bin_%i", "m_eff_pu_high_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_pu_high_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.05, "pu", "hs", false,
  "HSEffVsEta_highpt_5", 20, 0.0, 4, 0., 1.3, 0.95, 1.05, {1, 2, 3}, "|#eta|", "HS efficiency");

  // //HS efficiency vs eta, low pt
  //
  PlotGraph({"m_eff_pu_low_pt_ghost_eta_bin_%i", "m_eff_pu_low_pt_subjetrpt_eta_bin_%i", "m_eff_pu_low_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_pu_low_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.01, "pu", "hs", false,
  "HSEffVsEta_lowpt_1", 20, 0.0, 4, 0., 1.3, 0.95, 1.05, {1, 2, 3}, "|#eta|", "HS efficiency");

  PlotGraph({"m_eff_pu_low_pt_ghost_eta_bin_%i", "m_eff_pu_low_pt_subjetrpt_eta_bin_%i", "m_eff_pu_low_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_pu_low_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.02, "pu", "hs", false,
  "HSEffVsEta_lowpt_2", 20, 0.0, 4, 0., 1.3, 0.95, 1.05, {1, 2, 3}, "|#eta|", "HS efficiency");

  PlotGraph({"m_eff_pu_low_pt_ghost_eta_bin_%i", "m_eff_pu_low_pt_subjetrpt_eta_bin_%i", "m_eff_pu_low_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_pu_low_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.03, "pu", "hs", false,
  "HSEffVsEta_lowpt_3", 20, 0.0, 4, 0., 1.3, 0.95, 1.05, {1, 2, 3}, "|#eta|", "HS efficiency");

  PlotGraph({"m_eff_pu_low_pt_ghost_eta_bin_%i", "m_eff_pu_low_pt_subjetrpt_eta_bin_%i", "m_eff_pu_low_pt_hgtdrpt_defaultpf_eta_bin_%i", "m_eff_pu_low_pt_hgtdrpt_tcv05pforv02_eta_bin_%i"}, {"RpT (ITk)", "Subjet RpT (ITk + HGTD)", "HGTDRpT, default t0, default PF (ITk + HGTD)", "HGTDRpT, TCv05 t0, PFORv02 (ITk + HGTD)"}, 0.05, "pu", "hs", false,
  "HSEffVsEta_lowpt_5", 20, 0.0, 4, 0., 1.3, 0.95, 1.05, {1, 2, 3}, "|#eta|", "HS efficiency");

}

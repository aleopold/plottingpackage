#include "../inc/AtlasStyle.h"
#include "../inc/AtlasUtils.h"
#include "../src/AtlasStyle.cxx"
#include "../src/AtlasUtils.cxx"

TString g_input_file_path = "/afs/cern.ch/user/t/tawang/public/PileupRejection_100k.root";
TString output_plot_path = "~/www/hgtd/tdr_plots_2020-01-09";
TString dataset_name = "VBFinv, mu=200";
TString descr = "pileupRejectionTruthT0_highpt";
bool print_png = true;
TString work_status = "Internal";
bool g_do_ratioplot = true;
double x_eff_min = 0.80;
double legend_text_size = 0.02;


TGraph* getROCcurve(TEfficiency* eff1, TEfficiency* eff2);

// vector<int> colors = {kGreen+2, kCyan-3, kAzure-3, kRed+2, kRed-4, kOrange+1, kMagenta+4, kMagenta+3, kMagenta+2, kMagenta+1, kMagenta};
// vector<int> colors = {kGreen+2, kCyan-3, kAzure-3, kRed+2, kRed-4, kOrange+1, kGreen+4, kGreen+3, kGreen+2, kGreen+1, kGreen};
// vector<int> colors = {kGreen+2, kCyan-3, kAzure-3, kRed+2, kRed-4, kOrange+1, kGreen+4, kGreen+3, kGray+2, kGreen+1, kGreen, kSpring+10, kOrange-9, kOrange-8, kOrange-7, kOrange-6, kOrange-5, kOrange-4, kOrange-3, kOrange-2, kOrange-1, kOrange};
// vector<int> colors = {kGreen+2, kCyan-3, kAzure-3, kRed+2, kRed-4, kOrange+1, kGreen+4, kGreen+3, kGray+2, kGreen+1, kGreen, kSpring+10, kMagenta};
vector<int> colors = {kBlack, kCyan-3, kAzure-3, kRed-4, kMagenta, kGreen+1, kViolet+10};
void decorateGraph(TGraph* graph, int i);

// vector<int> chosen_pos = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
// vector<int> chosen_pos = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
// vector<int> chosen_pos = {1, 2, 3, 4, 5};
vector<int> chosen_pos = {3, 4, 5};
TGraph* getRatioGraph(TGraph* numerator, TGraph* denominator);

void combineMultipleROCcurves() {

  TFile* file = TFile::Open(g_input_file_path, "READ");

  //m_eff_hs_high_pt_TruthVertexPFORv02
  //m_eff_hs_high_pt_TruthVertexTruthTimePrimes
  //get standard RpT curve
  vector<TString> eff1_names;
  vector<TString> eff2_names;


  eff1_names.push_back("m_eff_hs_high_pt_StandardRpT");
  eff2_names.push_back("m_eff_pu_high_pt_StandardRpT");

  eff1_names.push_back("m_eff_hs_high_pt_TruthVertexPFORv02");
  eff2_names.push_back("m_eff_pu_high_pt_TruthVertexPFORv02");


  eff1_names.push_back("m_eff_hs_high_pt_TruthVertexTruthTimePrimes");
  eff2_names.push_back("m_eff_pu_high_pt_TruthVertexTruthTimePrimes");

  eff1_names.push_back("m_eff_hs_high_pt_SubJetRpTReco");
  eff2_names.push_back("m_eff_pu_high_pt_SubJetRpTReco");

  // eff1_names.push_back("m_eff_hs_high_pt_SubJetRpTTruth");
  // eff2_names.push_back("m_eff_pu_high_pt_SubJetRpTTruth");
  //
  // eff1_names.push_back("m_eff_hs_high_pt_SubJetRpTTruthTDRresolution");
  // eff2_names.push_back("m_eff_pu_high_pt_SubJetRpTTruthTDRresolution");

  eff1_names.push_back("m_eff_hs_high_pt_RecoVertexTimePF");
  eff2_names.push_back("m_eff_pu_high_pt_RecoVertexTimePF");

  // eff1_names.push_back("m_eff_hs_high_pt_RecoVertexTimev02PFORv02");
  // eff2_names.push_back("m_eff_pu_high_pt_RecoVertexTimev02PFORv02");
  //
  // eff1_names.push_back("m_eff_hs_high_pt_RecoVertexTimev02TruthTrackTime");
  // eff2_names.push_back("m_eff_pu_high_pt_RecoVertexTimev02TruthTrackTime");
  //
  // eff1_names.push_back("m_eff_hs_high_pt_RecoVertexTimev03PFORv02");
  // eff2_names.push_back("m_eff_pu_high_pt_RecoVertexTimev03PFORv02");
  //
  // eff1_names.push_back("m_eff_hs_high_pt_RecoVertexTimev04PFORv02");
  // eff2_names.push_back("m_eff_pu_high_pt_RecoVertexTimev04PFORv02");
  //
  // eff1_names.push_back("m_eff_hs_high_pt_RecoVertexTimev05PFORv02");
  // eff2_names.push_back("m_eff_pu_high_pt_RecoVertexTimev05PFORv02");

  // for(int i=1; i<=10; i++) {
  //   eff1_names.push_back(Form("m_eff_hs_high_pt_RecoVertexTimev05PFORv02_Cut_%i", i));
  //   eff2_names.push_back(Form("m_eff_pu_high_pt_RecoVertexTimev05PFORv02_Cut_%i", i));
  // }
  eff1_names.push_back(Form("m_eff_hs_high_pt_RecoVertexTimev05PFORv02_Cut_%i", 10));
  eff2_names.push_back(Form("m_eff_pu_high_pt_RecoVertexTimev05PFORv02_Cut_%i", 10));

  //get the roc curves for all
  vector<TGraph*> graphs;
  for(size_t i=0; i<eff1_names.size(); i++) {
    auto eff1 = (TEfficiency*) file->Get(eff1_names.at(i));
    auto eff2 = (TEfficiency*) file->Get(eff2_names.at(i));
    if (eff1 ==nullptr or eff2 == nullptr) {
      cout << "ERROR: " << eff1_names.at(i) << " does not exist, aborting" << '\n';
      return;
    }
    TGraph* graph = getROCcurve(eff1, eff2);
    decorateGraph(graph, i);
    graphs.push_back(graph);
  }
  graphs.at(0)->SetLineColor(kBlack);
  graphs.at(0)->SetMarkerColor(kBlack);

  //*************//*************//*************//*************//
  //                    P L O T T I N G                       //
  //*************//*************//*************//*************//

  std::vector<TObject*> garbage;

  TCanvas *c = new TCanvas();
  garbage.push_back(c);
  TPad* p1 = nullptr;
  TPad* p2 = nullptr;
  if (g_do_ratioplot) {
    p2 = new TPad("p2","p2",0.,0.02,1.,0.3);
    garbage.push_back(p2);
    p2->UseCurrentStyle();
    p2->Draw();
    p1 = new TPad("p1","p1",0.,0.3,1.,1.);
    garbage.push_back(p1);
    p1->Draw();
    p1->cd();
    p1->SetLogy();
  } else {
    c->SetLogy();
  }


  auto axishisto = new TH1F("", "", 10, x_eff_min, 1.);
  garbage.push_back(axishisto);
  axishisto->GetYaxis()->SetRangeUser(1, 500);
  axishisto->GetXaxis()->SetTitle("HS efficiency");
  axishisto->GetYaxis()->SetTitle("PU rejection");
  axishisto->Draw();

  int iteratori=0;
  for (auto graph : graphs) {
    if (iteratori==1 or iteratori==2) {
      iteratori++;
      continue;
    }
    graph->Draw("LP same");
    iteratori++;
  }

  Color_t text_color = kBlack;

  TLegend* legend = new TLegend(0.55, 0.93, 0.95, 0.7);
  garbage.push_back(legend);
  legend->SetBorderSize(0);
  legend->SetTextSize(legend_text_size);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->SetTextColor(text_color);

  int cut_int = 1;


  // legend->AddEntry(graphs.at(0),"RpT (ITk)", "pl");
  // legend->AddEntry(graphs.at(1),"TruthVertex t0 PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(2),"TruthVertex t0 TruthTimePrimes (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(3),"SubJetRpT PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(4),"SubJetRpT TruthTimePrimes (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(5),"SubJetRpT TruthTimePrimes (25ps) (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(6),"HGTDRpT RecoVertexTimePF (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(7),"HGTDRpT RecoVertexTimev02PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(8),"HGTDRpT RecoVertexTimev02TruthTrackTime (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(9),"HGTDRpT RecoVertexTimev03PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(10),"HGTDRpT RecoVertexTimev04PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(11),"HGTDRpT RecoVertexTimev05PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(12),Form("HGTDRpT RecoVertexTimev05PFORv02 Cut %i (ITk+HGTD)", 10), "pl");

  legend->AddEntry(graphs.at(0),"RpT (ITk)", "pl");
  // legend->AddEntry(graphs.at(2),"TruthVertex t0 TruthTimePrimes (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(1),"TruthVertex t0 PFORv02 (ITk+HGTD)", "pl");
  legend->AddEntry(graphs.at(3),"SubJetRpT PFORv02 (ITk+HGTD)", "pl");
  legend->AddEntry(graphs.at(4),"HGTDRpT RecoVertexTimePF (ITk+HGTD)", "pl");
  legend->AddEntry(graphs.at(5),Form("HGTDRpT RecoVertexTimev04PFORv02 Cut %i (ITk+HGTD)", 10), "pl");

  legend->Draw("same");

  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.27, 0.88, text_color, work_status.Data(), 0.05);

  TString dataset_buff = dataset_name;
  dataset_buff.ReplaceAll("mu", "<#mu>");
  dataset_buff.ReplaceAll("_", ", ");
  atlas::myText(0.19, 0.85, text_color, dataset_buff.Data(), 0.03);
  atlas::myText(0.19, 0.80, text_color, "p_{T}^{jet} > 50 GeV", 0.03);

  std::cout<<"do ratio"<<'\n';
  if (g_do_ratioplot) {
    p2->cd();
    auto axishisto2 = new TH1F("", "", 10, x_eff_min, 1.);
    garbage.push_back(axishisto2);
    axishisto2->GetYaxis()->SetRangeUser(0.8, 1.3);
    axishisto2->GetYaxis()->SetTitle("ratio");
    axishisto2->GetYaxis()->SetNdivisions(505);
    axishisto2->GetYaxis()->SetTitleOffset(0.55);
    axishisto2->GetYaxis()->SetTitleSize(0.13); // labels will be 14 pixels
    axishisto2->GetYaxis()->SetLabelSize(0.1); // labels will be 14 pixels
    axishisto2->GetXaxis()->SetLabelSize(0.1); // labels will be 14 pixels
    axishisto2->Draw();

    auto line = new TLine(0.8, 1.0, 1.0, 1.0);
    garbage.push_back(line);
    line->SetLineStyle(2);
    line->Draw("same");
    auto line2 = new TLine(0.8, 1.2, 1.0, 1.2);
    garbage.push_back(line2);
    line2->SetLineStyle(2);
    line2->Draw("same");
    // vector<int> chosen_pos = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    for (auto pos : chosen_pos) {
      TGraph* ratio_graph = getRatioGraph(graphs.at(0), graphs.at(pos));
      ratio_graph->Draw("L same");
      garbage.push_back(ratio_graph);
    }
  }


  TString plot_name = Form("%s/%s_%s.pdf",  output_plot_path.Data(), dataset_name.ReplaceAll(" ", "").ReplaceAll(",", "_").Data(), descr.Data());
  c->Print(plot_name);
  plot_name.Form("%s/%s_%s.png",  output_plot_path.Data(), dataset_name.Data(), descr.Data());
  if (print_png) c->Print(plot_name);

  for (auto& graph : graphs) {
    delete graph;
  }
  file->Close();
}

void combineMultipleROCcurvesLowPt() {

  TFile* file = TFile::Open(g_input_file_path, "READ");

  //m_eff_hs_high_pt_TruthVertexPFORv02
  //m_eff_hs_high_pt_TruthVertexTruthTimePrimes
  //get standard RpT curve
  vector<TString> eff1_names;
  vector<TString> eff2_names;


  eff1_names.push_back("m_eff_hs_low_pt_StandardRpT");
  eff2_names.push_back("m_eff_pu_low_pt_StandardRpT");

  eff1_names.push_back("m_eff_hs_low_pt_TruthVertexPFORv02");
  eff2_names.push_back("m_eff_pu_low_pt_TruthVertexPFORv02");


  eff1_names.push_back("m_eff_hs_low_pt_TruthVertexTruthTimePrimes");
  eff2_names.push_back("m_eff_pu_low_pt_TruthVertexTruthTimePrimes");

  eff1_names.push_back("m_eff_hs_low_pt_SubJetRpTReco");
  eff2_names.push_back("m_eff_pu_low_pt_SubJetRpTReco");

  // eff1_names.push_back("m_eff_hs_low_pt_SubJetRpTTruth");
  // eff2_names.push_back("m_eff_pu_low_pt_SubJetRpTTruth");
  //
  // eff1_names.push_back("m_eff_hs_low_pt_SubJetRpTTruthTDRresolution");
  // eff2_names.push_back("m_eff_pu_low_pt_SubJetRpTTruthTDRresolution");

  eff1_names.push_back("m_eff_hs_low_pt_RecoVertexTimePF");
  eff2_names.push_back("m_eff_pu_low_pt_RecoVertexTimePF");

  // eff1_names.push_back("m_eff_hs_low_pt_RecoVertexTimev02PFORv02");
  // eff2_names.push_back("m_eff_pu_low_pt_RecoVertexTimev02PFORv02");
  //
  // eff1_names.push_back("m_eff_hs_low_pt_RecoVertexTimev02TruthTrackTime");
  // eff2_names.push_back("m_eff_pu_low_pt_RecoVertexTimev02TruthTrackTime");
  //
  // eff1_names.push_back("m_eff_hs_low_pt_RecoVertexTimev03PFORv02");
  // eff2_names.push_back("m_eff_pu_low_pt_RecoVertexTimev03PFORv02");
  //
  // eff1_names.push_back("m_eff_hs_low_pt_RecoVertexTimev04PFORv02");
  // eff2_names.push_back("m_eff_pu_low_pt_RecoVertexTimev04PFORv02");
  //
  // eff1_names.push_back("m_eff_hs_low_pt_RecoVertexTimev05PFORv02");
  // eff2_names.push_back("m_eff_pu_low_pt_RecoVertexTimev05PFORv02");

  // for(int i=1; i<=10; i++) {
  //   eff1_names.push_back(Form("m_eff_hs_low_pt_RecoVertexTimev05PFORv02_Cut_%i", i));
  //   eff2_names.push_back(Form("m_eff_pu_low_pt_RecoVertexTimev05PFORv02_Cut_%i", i));
  // }

  eff1_names.push_back(Form("m_eff_hs_low_pt_RecoVertexTimev05PFORv02_Cut_%i", 10));
  eff2_names.push_back(Form("m_eff_pu_low_pt_RecoVertexTimev05PFORv02_Cut_%i", 10));

  //get the roc curves for all
  vector<TGraph*> graphs;
  for(size_t i=0; i<eff1_names.size(); i++) {
    auto eff1 = (TEfficiency*) file->Get(eff1_names.at(i));
    auto eff2 = (TEfficiency*) file->Get(eff2_names.at(i));
    if (eff1 ==nullptr or eff2 == nullptr) {
      cout << "ERROR: " << eff1_names.at(i) << " does not exist, aborting" << '\n';
      return;
    }
    TGraph* graph = getROCcurve(eff1, eff2);
    decorateGraph(graph, i);
    graphs.push_back(graph);
  }
  graphs.at(0)->SetLineColor(kBlack);
  graphs.at(0)->SetMarkerColor(kBlack);

  //*************//*************//*************//*************//
  //                    P L O T T I N G                       //
  //*************//*************//*************//*************//

  std::vector<TObject*> garbage;

  TCanvas *c = new TCanvas();
  garbage.push_back(c);
  TPad* p1 = nullptr;
  TPad* p2 = nullptr;
  if (g_do_ratioplot) {
    p2 = new TPad("p2","p2",0.,0.02,1.,0.3);
    garbage.push_back(p2);
    p2->UseCurrentStyle();
    p2->Draw();
    p1 = new TPad("p1","p1",0.,0.3,1.,1.);
    garbage.push_back(p1);
    p1->Draw();
    p1->cd();
    p1->SetLogy();
  } else {
    c->SetLogy();
  }


  auto axishisto = new TH1F("", "", 10, x_eff_min, 1.);
  garbage.push_back(axishisto);
  axishisto->GetYaxis()->SetRangeUser(1, 500);
  axishisto->GetXaxis()->SetTitle("HS efficiency");
  axishisto->GetYaxis()->SetTitle("PU rejection");
  axishisto->Draw();

  int iteratori=0;
  for (auto graph : graphs) {
    if (iteratori==1 or iteratori==2) {
      iteratori++;
      continue;
    }
    graph->Draw("LP same");
    iteratori++;
  }

  Color_t text_color = kBlack;

  TLegend* legend = new TLegend(0.55, 0.93, 0.95, 0.7);
  garbage.push_back(legend);
  legend->SetBorderSize(0);
  legend->SetTextSize(legend_text_size);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);

  int cut_int = 1;


  // legend->AddEntry(graphs.at(0),"RpT (ITk)", "pl");
  // legend->AddEntry(graphs.at(1),"TruthVertex t0 PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(2),"TruthVertex t0 TruthTimePrimes (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(3),"SubJetRpT PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(4),"SubJetRpT TruthTimePrimes (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(5),"SubJetRpT TruthTimePrimes (25ps) (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(6),"HGTDRpT RecoVertexTimePF (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(7),"HGTDRpT RecoVertexTimev02PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(8),"HGTDRpT RecoVertexTimev02TruthTrackTime (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(9),"HGTDRpT RecoVertexTimev03PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(10),"HGTDRpT RecoVertexTimev04PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(11),"HGTDRpT RecoVertexTimev05PFORv02 (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(12),Form("HGTDRpT RecoVertexTimev05PFORv02 Cut %i (ITk+HGTD)", 10), "pl");

  legend->AddEntry(graphs.at(0),"RpT (ITk)", "pl");
  // legend->AddEntry(graphs.at(2),"TruthVertex t0 TruthTimePrimes (ITk+HGTD)", "pl");
  // legend->AddEntry(graphs.at(1),"TruthVertex t0 PFORv02 (ITk+HGTD)", "pl");
  legend->AddEntry(graphs.at(3),"SubJetRpT PFORv02 (ITk+HGTD)", "pl");
  legend->AddEntry(graphs.at(4),"HGTDRpT RecoVertexTimePF (ITk+HGTD)", "pl");
  legend->AddEntry(graphs.at(5),Form("HGTDRpT RecoVertexTimev04PFORv02 Cut %i (ITk+HGTD)", 10), "pl");

  legend->SetTextColor(text_color);
  legend->Draw("same");

  atlas::ATLAS_LABEL(0.19, 0.88, text_color);
  atlas::myText(0.27, 0.88, text_color, work_status.Data(), 0.05);

  TString dataset_buff = dataset_name;
  dataset_buff.ReplaceAll("mu", "<#mu>");
  dataset_buff.ReplaceAll("_", ", ");
  atlas::myText(0.19, 0.85, text_color, dataset_buff.Data(), 0.03);
  atlas::myText(0.19, 0.80, text_color, "30 GeV < p_{T}^{jet} < 50 GeV", 0.03);

  std::cout<<"do ratio"<<'\n';
  if (g_do_ratioplot) {
    p2->cd();
    auto axishisto2 = new TH1F("", "", 10, x_eff_min, 1.);
    garbage.push_back(axishisto2);
    axishisto2->GetYaxis()->SetRangeUser(0.8, 1.3);
    axishisto2->GetYaxis()->SetTitle("ratio");
    axishisto2->GetYaxis()->SetNdivisions(505);
    axishisto2->GetYaxis()->SetTitleOffset(0.55);
    axishisto2->GetYaxis()->SetTitleSize(0.13); // labels will be 14 pixels
    axishisto2->GetYaxis()->SetLabelSize(0.1); // labels will be 14 pixels
    axishisto2->GetXaxis()->SetLabelSize(0.1); // labels will be 14 pixels
    axishisto2->Draw();

    auto line = new TLine(0.8, 1.0, 1.0, 1.0);
    garbage.push_back(line);
    line->SetLineStyle(2);
    line->Draw("same");
    auto line2 = new TLine(0.8, 1.2, 1.0, 1.2);
    garbage.push_back(line2);
    line2->SetLineStyle(2);
    line2->Draw("same");
    // vector<int> chosen_pos = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    for (auto pos : chosen_pos) {
      TGraph* ratio_graph = getRatioGraph(graphs.at(0), graphs.at(pos));
      ratio_graph->Draw("L same");
      garbage.push_back(ratio_graph);
    }
  }


  TString plot_name = Form("%s/%s_%s.pdf",  output_plot_path.Data(), dataset_name.ReplaceAll(" ", "").ReplaceAll(",", "_").Data(), descr.Data());
  c->Print(plot_name);
  plot_name.Form("%s/%s_%s.png",  output_plot_path.Data(), dataset_name.Data(), descr.Data());
  if (print_png) c->Print(plot_name);

  for (auto& graph : graphs) {
    delete graph;
  }
  file->Close();
}

//*************//*************//*************//*************//
//             H E L P E R   F U N C T I O N S              //
//*************//*************//*************//*************//


void decorateGraph(TGraph* graph, int i) {
  int pos = i;
  if (i>colors.size()) {
    pos = i%colors.size();
  }
  graph->SetMarkerColor(colors.at(pos) + i/colors.size());
  graph->SetMarkerSize(0.5);
  graph->SetLineColor(colors.at(pos) + i/colors.size());
  graph->SetLineWidth(1);
}


TGraph* getRatioGraph(TGraph* numerator, TGraph* denominator) {
  // int n = graphs.at(0)->GetN();
  int n = numerator->GetN();
  TGraph* r = new TGraph(n);
  r->SetTitle("");
  for (int i=0; i<n; i++) {
    double x, y;
    numerator->GetPoint(i, x, y);
    // y = graphs.at(ratio_plot_choice)->Eval(x)/y;
    y = denominator->Eval(x)/y;
    r->SetPoint(i, x, y);
  }
  r->GetXaxis()->SetLabelSize(0.075);
  r->GetYaxis()->SetLabelSize(0.075);
  r->SetLineColor(denominator->GetLineColor());
  r->SetLineStyle(denominator->GetLineStyle());
  return r;
}


TGraph* getROCcurve(TEfficiency* eff1, TEfficiency* eff2) {
  std::vector<double> eff;
  std::vector<double> rej;
  for (int i=0; i<eff1->CreateGraph()->GetN() ; i++) {
    if (eff1->GetEfficiency(i) == 0) {continue;}
    if (eff2->GetEfficiency(i) == 0) {continue;}
    eff.push_back(eff1->GetEfficiency(i));
    rej.push_back(1./eff2->GetEfficiency(i));
  }
  TGraph *graph = new TGraph(eff.size() , &eff[0], &rej[0]);
  return graph;
}

void pileupRejectionTruthT0() {
  TEnv env("pileup.cfg");

  g_input_file_path = env.GetValue("input_file_path", "default");
  g_do_ratioplot = env.GetValue("do_ratioplot", false);
  x_eff_min = env.GetValue("x_eff_min", 0.8);
  output_plot_path = env.GetValue("output_plot_path", "default");
  dataset_name = env.GetValue("dataset_name", "default");
  print_png = env.GetValue("print_png", false);
  work_status = env.GetValue("work_status", "default");

  SetAtlasStyle();
  combineMultipleROCcurves();

  descr = "pileupRejectionTruthT0_lowpt";
  combineMultipleROCcurvesLowPt();
}

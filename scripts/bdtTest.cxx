

void bdtTest() {

  TMVA::Tools::Instance();
  auto inputFile = TFile::Open("/afs/cern.ch/work/a/aleopold/private/hgtd/analysis_pkg/run/VertexControlPlots_20200127_v01.root");
  auto outputFile = TFile::Open("TMVAOutputCV.root", "RECREATE");

  TMVA::Factory factory("TMVAClassification", outputFile, "!V:ROC:!Correlations:!Silent:Color:!DrawProgressBar:AnalysisType=Classification" );

  TMVA::DataLoader loader("dataset");

  loader.AddVariable("m_delta_z");
  loader.AddVariable("m_z_sigma");
  loader.AddVariable("m_delta_z_resunits");
  loader.AddVariable("m_cluster_sumpt2");
  loader.AddVariable("m_d0");
  loader.AddVariable("m_d0_sigma");
  loader.AddVariable("m_q_over_p");
  loader.AddVariable("m_q_over_p_sigma");

  TTree *tsignal, *tbackground;
  inputFile->GetObject("SignalTree", tsignal);
  inputFile->GetObject("BackgroundTree", tbackground);

  TCut mycuts = "";
  TCut mycutb = "";
  // TCut mycuts = "m_number_of_tracks>3";
  // TCut mycutb = "m_number_of_tracks>3";

  loader.AddSignalTree    (tsignal,     4.5);   //signal weight  = 1
  loader.AddBackgroundTree(tbackground, 1.0);   //background weight = 1
  loader.PrepareTrainingAndTestTree(mycuts, mycutb,
  "nTrain_Signal=1000:nTrain_Background=1000:SplitMode=Random:NormMode=NumEvents:!V" );


  factory.BookMethod(&loader,TMVA::Types::kBDT, "BDT",
  "!V:NTrees=200:MinNodeSize=2.5%:MaxDepth=2:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20" );
  // factory.BookMethod(&loader, TMVA::Types::kMLP, "MLP",
                   // "!H:!V:NeuronType=tanh:VarTransform=N:NCycles=100:HiddenLayers=N+5:TestRate=5:!UseRegulator" );

  factory.TrainAllMethods();

  factory.TestAllMethods();
  factory.EvaluateAllMethods();

  outputFile->Close();

  auto c1 = factory.GetROCCurve(&loader);
  c1->Draw();
  if (!gROOT->IsBatch()) TMVA::TMVAGui( "TMVAOutputCV.root" );
}

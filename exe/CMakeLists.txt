
add_executable(main "${CMAKE_CURRENT_SOURCE_DIR}/main.cxx")

target_link_libraries(main LINK_PUBLIC
            "${PROJECT_NAME}Lib"
            ${ROOT_LIBRARIES}
            ${Boost_LIBRARIES}
            )

# install(TARGETS main DESTINATION ${INSTALL_DIR})

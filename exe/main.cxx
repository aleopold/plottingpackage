
#include <cstdlib>
#include <iostream>

#include "Configurator.h"
#include "PathHandler.h"
#include "Plotter.h"

#include "TError.h"

int main(int argc, char *argv[]) {

  gErrorIgnoreLevel = kError;

  std::cout<< '\n';
  std::cout << "*************************" << '\n';
  std::cout << "****  P L O T T E R  ****" << '\n';
  std::cout << "*************************" << '\n';
  std::cout<< '\n';

  const std::string root_folder_path(std::getenv("ROOTFOLDERPATH"));

  std::string config_file_name("");

  const std::string config_path = root_folder_path + "/cfg";

  PathHandler& path_handler(PathHandler::getInstance());

  //if no config file is given, list available ones and abort
  if (argc <= 2) {
    std::cout << "================================" << '\n';
    std::cout << "ERROR: No config file defined" << '\n';
    std::cout << "available config files are:" << '\n';
    std::cout << '\n';
    //print available ones
    for (const auto& file_name : path_handler.getFilesInDir(config_path)) {
      //gives full path, I only want to show filename
      std::size_t found = file_name.find_last_of("/");
      std::cout << "   - " << file_name.substr(found+1) << '\n';
    }
    std::cout << '\n';
    std::cout << "usage:" << '\n';
    std::cout << "./<path_to_main>/main -c <config_filename>" << '\n';
    std::cout << '\n';
    std::cout << "aborting..." << '\n';
    return -1;
  }

  bool list_objects_in_rootfile = false;
  //read inputs from the command line
  for (int i=0; i<argc; ++i) {
    if (std::string(argv[i]) == "-c" && i+1<argc) {
      config_file_name = argv[i+1];
    }
    if (std::string(argv[i]) == "-l") {
      list_objects_in_rootfile = true;
    }
  }

  std::cout << "[main] load configuration ..." << '\n';
  Configurator configurator(config_path, config_file_name);

  Plotter plotter;

  std::cout << "[main] configure plotter ..." << '\n';
  plotter.configure(configurator);

  if (list_objects_in_rootfile) {
  std::cout<< '\n';
    plotter.listObjects();
    return 0;
  }

  std::cout << "[main] plot ..." << '\n';

  plotter.plot();

  std::cout<< '\n';
  std::cout << "[main] done" << '\n';
  std::cout<< '\n';
  return 0;

}

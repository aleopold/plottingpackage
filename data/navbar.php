<ul id="menu-list">
  <li><a href="../../index.php">Home</a></li>
  <li><a href="../../about.php">About</a></li>
  <li><a href="../../project-overview.php">Projects  <img id="down-arrow" src="../../img/down-arrow.png" alt="down" style="height:13px; width: 13px;"> </a>
      <ul>
<!--	  <li><a href="project_template.php">Project Template</a></li> -->
	       <li><a href="../../project_crosstalk.php">Crosstalk</a></li>
	       <li><a href="../../project_hgtd.php">Qualif. Task</a></li>
         <li><a href="../../project_validation.php">Rel21 validation</a></li>
         <li><a href="../../project_susyre.php">SUSY reinterpretation</a></li>
      </ul>
  </li>
  <li><a href="../../utilities.php">Tips&amp;Tricks</a></li>
  <li><a href="../../blog_overview.php">Blog</a></li>
</ul>

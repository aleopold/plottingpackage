#include "PathHandler.h"
#include <string>
#include <iostream>
#include <cassert>

const std::string root_folder_path(std::getenv("ROOTFOLDERPATH"));
const std::string file_name("CMakeLists.txt");

const std::string path = root_folder_path + "/" + file_name;

/*
* check if a file that is known to exist can be found
*/
void testFileFinding(const PathHandler& path_handler) {
  std::cout << "testing good file" << '\n';
  std::cout << "with path: " << path << '\n';
  assert(path_handler.fileExists(path) == true);

  std::cout << "testing bad file" << '\n';
  assert(path_handler.fileExists("/aaa/bbb/ccc") == false);

}

void testPathCombining(const PathHandler& path_handler) {

  std::string combined_path = path_handler.combinePaths(root_folder_path, file_name);

  assert(path_handler.fileExists(path) == true);

}



int main () {
  std::cout << "start test" << '\n';

  PathHandler& path_handler(PathHandler::getInstance());
  std::cout << "instance retrieved" << '\n';

  std::cout << "testFileFinding" << '\n';
  testFileFinding(path_handler);
  std::cout << "testPathCombining" << '\n';
  testPathCombining(path_handler);

  std::cout << "end test" << '\n';
  return 0;
}

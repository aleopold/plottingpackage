#include "Configurator.h"
#include <string>
#include <iostream>
#include <cassert>

const std::string root_folder_path(std::getenv("ROOTFOLDERPATH"));
const std::string file_name("Main.cfg");

const std::string path = root_folder_path + "/cfg";


int main () {

  std::cout << "starting test_Configurator" << '\n';

  Configurator configurator(path, file_name);

  return 0;
}

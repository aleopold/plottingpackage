cmake_minimum_required (VERSION 3.10)

# set(CMAKE_C_COMPILER "gcc")
# set(CMAKE_CXX_COMPILER "g++")

project (PlottingPackage)

# set(INSTALL_DIR "${CMAKE_CURRENT_SOURCE_DIR}/dist/${CMAKE_BUILD_TYPE}/bin/")

##### You need to tell CMake where to find the ROOT installation.
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

# set(CMAKE_CXX_STANDARD 17)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ROOT_CXX_FLAGS} -lTreePlayer -O3 -std=c++17")

message( ${CMAKE_CXX_FLAGS} )

find_package(ROOT REQUIRED COMPONENTS RIO Net Hist Graf Graf3d Gpad Tree Rint Postscript Matrix Physics MathCore Thread Proof)
#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
include(${ROOT_USE_FILE})
include_directories(${ROOT_INCLUDE_DIR})

##### include boost
# set(Boost_USE_STATIC_LIBS OFF)
# set(Boost_USE_MULTITHREADED OFF)
# set(Boost_USE_STATIC_RUNTIME OFF)
# find_package(Boost 1.71.0 COMPONENTS filesystem)
# /cvmfs/sft.cern.ch/lcg/releases/LCG_94/Boost/1.66.0/x86_64-slc6-gcc62-opt

set(Boost_INCLUDE_DIRS /cvmfs/sft.cern.ch/lcg/releases/LCG_94/Boost/1.66.0/x86_64-slc6-gcc62-opt/include)
set(Boost_LIBRARY_DIRS /cvmfs/sft.cern.ch/lcg/releases/LCG_94/Boost/1.66.0/x86_64-slc6-gcc62-opt/lib)
find_package(Boost 1.66.0 EXACT COMPONENTS filesystem)

if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})
else()
    find_package(Boost COMPONENTS filesystem)
    if(Boost_FOUND)
        include_directories(${Boost_INCLUDE_DIRS})
    endif()
endif()

#Bring the headers into the project
include_directories(inc)

link_libraries(stdc++fs)

#To include a directory, that contains a CMakeLists file do:
add_subdirectory(src)
add_subdirectory(exe)

### testing
enable_testing()
add_subdirectory(test)

template<typename T>
T* RootFileLoader::extractTObject(std::string name, TFile* file)  {

  T* temp = dynamic_cast<T*>(file->Get(name.c_str()));

  if (temp == nullptr) {
    std::cout << "[RootFileLoader::extractTObject()] WARNING Couldn't find object "
              << name << '\n';
    return nullptr;
  }

  bool status = T::AddDirectoryStatus();
  T::AddDirectory(false);
  name = "cloned_" + name;
  T *hist = dynamic_cast<T*>(temp->Clone(name.c_str()));
  T::AddDirectory(status);
  return hist;
}


template<>
inline TEfficiency* RootFileLoader::extractTObject<TEfficiency>(std::string name, TFile* file) {
  TEfficiency *temp = dynamic_cast<TEfficiency*>(file->Get(name.c_str()));

  if (temp == nullptr) {
    std::cout << "[RootFileLoader::extractTObject()] WARNING Couldn't find efficiency "
              << name<< std::endl;
    return nullptr;
  }

  bool status = TEfficiency::GetObjectStat();
  TEfficiency::SetObjectStat(false);
  name = "cloned_" + name;
  TEfficiency *eff = dynamic_cast<TEfficiency*>(temp->Clone(name.c_str()));
  TEfficiency::SetObjectStat(status);
  return eff;
}



template <typename T>
std::vector<T *> Plotter::convertObjects(const std::vector<TObject *> objs) {
  std::vector<T *> derived;
  derived.reserve(objs.size());
  for (auto &obj : objs) {
    derived.push_back((T *)obj);
  }
  return derived;
}

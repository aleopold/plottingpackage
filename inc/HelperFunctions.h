/**
* @Author Alexander Leopold (alexander.leopold@cern.ch)
*
* @brief General useful helper functions. Use ROOT libraries often.
*/


#ifndef  HELPERFUNCTIONS_H
#define HELPERFUNCTIONS_H

#include "TString.h"

namespace helper {
  
  std::vector<std::string> vectorize(TString str, TString sep);

  std::vector<int> vectorizeToInt(TString str, TString sep);

  std::string removeWhiteSpace(std::string str);
}

#endif // HELPERFUNCTIONS_H

/**
 * @Author Alexander Leopold (alexander.leopold@cern.ch)
 *
 * @brief  Utilities for plotting of histograms, efficiency curves, etc.
 *
 * can do:
 * - plot content of a root file (histograms, graphs, etc.)
 * - compare different root files with same content
 * -
 *
 * is configurable via text files
 */

#ifndef PLOTTER_H
#define PLOTTER_H

#include "Configurator.h"
#include "RootFileLoader.h"

#include <memory>
#include <string>
#include <vector>

// ROOT includes
class TCanvas;
class TFile;
class TGraph;
class TH1;
class TH1F;
class TH2D;
class TPad;
class TProfile;

class Plotter {

  using object_store_t = ObjectStore;

public:
  Plotter();
  ~Plotter();
  void configure(const Configurator &configurator);

  void listObjects();

  void setObjectList(const std::string &file_name);

  void plot();

  void plotHistograms(std::string name, const std::vector<TH1 *> &hist_vec,
                      const std::vector<std::string> &labels,
                      const std::vector<int> &colors,
                      const std::vector<int> &styles);

  void plotEfficiency(std::string name,
                      const std::vector<TEfficiency *> &eff_vec,
                      const std::vector<std::string> &labels,
                      const std::vector<int> &colors,
                      const std::vector<int> &styles);

  void plotProfile(std::string name, const std::vector<TProfile *> &prof_vec,
                   const std::vector<std::string> &labels,
                   const std::vector<int> &colors,
                   const std::vector<int> &styles);

  // this one plots only the first 2D histogram. Overlay not possible
  void plot2DHistogram(std::string name, const std::vector<TH2D *> &hist_vec,
                       const std::vector<std::string> &labels,
                       const std::vector<int> &colors,
                       const std::vector<int> &styles);

  TGraph *getRatioGraph(TGraph *numerator, TGraph *denominator);

private:
  void scaleLineX(TH2D *hist, int bin_x_max, int bin_y_max);

  void releaseInputFiles();

  void printAdditionalLabels();

  TH1F *transformEffToHis(TEfficiency *eff);

  // void CanvasPartition(std::string name, TCanvas *canvas, const int Nx,
  // const int Ny, float lMargin, float rMargin,
  // float bMargin, float tMargin);

  std::vector<TPad *> CanvasPartition(std::string name, TCanvas *canvas,
                                      const int Nx, const int Ny, float lMargin,
                                      float rMargin, float bMargin,
                                      float tMargin);

  template <typename T>
  std::vector<T *> convertObjects(const std::vector<TObject *> objs);

private:
  PathHandler &m_path_handler;
  std::unique_ptr<RootFileLoader> m_file_loader;

  bool m_use_object_list = false;

  std::vector<std::string> m_label_names;

  std::vector<int> m_colors;
  std::vector<int> m_line_style;

  std::vector<std::string> m_object_names;

  std::string m_output_plot_path;

  std::string m_work_status;

  std::string m_description_text;

  bool m_print_png = true;

  bool m_run_overlay_mode = false;

  bool m_use_log_scale = false;
  float m_log_scale_minimum = 1.e-4;
  float m_y_scale_scalefactor = 4.;

  bool m_do_ratio_plot = false;

  bool m_2d_scale_global = false;

  // settings for in-file comparison
  bool m_run_comparison = false;
  std::string m_comparison_cfg_file;
  // std::vector<std::vector<std::string>>
  // m_comparison_names;
  std::vector<std::vector<ComparisonData>> m_comparison_objects;

  void debugLine(const std::string &line) {
    if (m_debug)
      std::cout << line << '\n';
  }
  bool m_debug = false;
};

#endif

#include "Plotter.hpp"

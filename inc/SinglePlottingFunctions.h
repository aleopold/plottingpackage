#ifndef  SINGLEPLOTTINGFUNCTIONS_H
#define SINGLEPLOTTINGFUNCTIONS_H

#include "TFile.h"
#include "TString.h"
#include "TGraph.h"
#include "TEnv.h"


//get a bunch of tefficiency objects, and combine them to add up

void plotEffCutFlow(TFile * file, TEnv& config_reader);

TH1F* turnGraphIntoHist(TGraph* graph, TString name, TString label, int bins, double xmin, double xmax, int color);


#endif // SINGLEPLOTTINGFUNCTIONS_H

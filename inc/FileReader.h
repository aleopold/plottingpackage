/**
 * @Author Alexander Leopold (alexander.leopold@cern.ch)
 *
 * @brief  Loads and interprets text files.
 *
 *
 */

#ifndef FILEREADER_H
#define FILEREADER_H

#include <string>
#include <vector>

class FileReader {
 public:
  using stringVec_t = std::vector<std::string>;

  //returns the lines of the textfiles as a string per line in the vector;
  std::vector<std::string> readFileLines(const std::string& path);

  //
  std::vector<stringVec_t> getVectorizedLines(const std::string& path,
                                              const std::string& delim);
 private:

};

#endif //FILEREADER_H

/**
 * @Author Alexander Leopold (alexander.leopold@cern.ch)
 *
 * @brief  Sets the configurations for the plotter, e.g input/output paths
 * plot descriptions, plotting options
 *
 *
 */

#ifndef CONFIGURATOR_H
#define CONFIGURATOR_H

#include "PathHandler.h"

// STL includes
#include <map>
#include <string>
#include <vector>

//ROOT includes
#include "TEnv.h"

class Configurator {

 public:
  Configurator(const std::string& path_name, const std::string& main_config_name);

  const TEnv& config() const {return m_main_env;}

  std::string getPathToConfigs() const;

 private:
  int readConfigFile();

 private:

  std::string m_path_to_config; //path to where config files are stored
  std::string m_main_cfg_file_name; //name of the main config file (is required)
  PathHandler& m_path_handler;

  TEnv m_main_env; //stores the instructions from the main config file

};

#endif //CONFIGURATOR_H

/**
 * @Author Alexander Leopold (alexander.leopold@cern.ch)
 *
 * @brief
 *
 */

#ifndef ROOTFILELOADER_H
#define ROOTFILELOADER_H

#include "PathHandler.h"

// STL includes
#include <iostream>
#include <map>
#include <string>
#include <vector>


#include "TEfficiency.h"
#include "TFile.h"

class TObject;

struct ObjectStore {
  //in each file, the name should be the same to allow comparison
  std::string name;
  std::string class_name;
  //vector retrieved objects form all the files, allows overlay
  std::vector<TObject*> object_vec;
};

struct ComparisonData {
  std::string name;
  int color;
  int style;
  std::string label;
};

class RootFileLoader {

  using object_store_t = ObjectStore;

 public:
  RootFileLoader();
  ~RootFileLoader();

  //the string filenames can contain more than one name, these get
  //unpacked into a vector of strings, assuming space delimiter
  RootFileLoader(std::string path, std::string filenames);



  std::vector<TFile*> getFiles();

  size_t getNfiles();

  //get all TObject names from the first file
  std::vector<std::string> getObjectNames();

  //get the object collections
  std::vector<object_store_t> getObjects();

  size_t nObjects();

  object_store_t getObject(const std::string& obj_name);

  //this works only if only one input file is defined!!
  object_store_t getObjectComparison(const std::vector<std::string>& obj_names);

  object_store_t getObjectComparison(const std::vector<ComparisonData>& obj_names);

  void setDebugMode(bool mode) {m_debug = mode;}

private:

  template<typename T>
  T* extractTObject(std::string name, TFile* file);

  template<typename T>
  std::vector<T*> getTObjectsVector(const std::string& histname);

  void cacheObjects();

  std::string interpretPathToRootFile(const std::string& input_path);
  std::vector<std::string> unpackFileNames(const std::string& filenames);

  void openRootFiles();
  void addRootFile(const std::string& path_to_file);

  void releaseInputFiles();
  void releaseObjects();

 private:
  PathHandler& m_path_handler;

  std::string m_path_to_files;
  std::vector<std::string> m_file_names;

  std::vector<TFile*> m_files;
  std::vector<std::string> m_object_names;

  std::vector<object_store_t> m_objects;

  bool m_debug = false;
};

#include "RootFileLoader.ipp"


#endif //ROOTFILELOADER_H

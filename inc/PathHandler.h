/**
* @Author Alexander LEOPOLD (alexander.leopold@cern.ch)
* @brief  Describe the class here.
*
**/

#ifndef PATHHANDLER_H
#define PATHHANDLER_H

// STL includes
#include <string>
#include <vector>

class PathHandler {

 public:
  static PathHandler& getInstance()
  {
     static PathHandler instance;
     return instance;
  }

 public:
  PathHandler(PathHandler const&) = delete;
  void operator=(PathHandler const&) = delete;

 private:
  PathHandler() {}

 public:

  bool fileExists(const std::string& filename) const;
  bool directoryExists(const std::string& filename) const;

  std::string combinePaths(const std::string& p1, const std::string& p2) const;

  bool createDirectory(const std::string& path) const;

  void copyFile(const std::string& origin, const std::string& target) const;

  std::vector<std::string> getFilesInDir(const std::string& path) const;

 private:
   void findAndReplaceAll(std::string& input, const std::string& to_replace,
                          const std::string& replacement) const;
};


#endif //PATHHANDLER_H

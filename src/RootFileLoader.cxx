#include "RootFileLoader.h"

#include "HelperFunctions.h"

#include "TCollection.h"
#include "TDirectory.h"
#include "TH1.h"
#include "TH2D.h"
#include "TKey.h"
#include "TObject.h"
#include "TProfile.h"

#include <algorithm>
#include <iostream>
#include <utility>

RootFileLoader::RootFileLoader() :
  m_path_handler(PathHandler::getInstance()) {
}
RootFileLoader::RootFileLoader(std::string path, std::string filenames) :
 RootFileLoader() {
  m_path_to_files = interpretPathToRootFile(path);
  m_file_names = unpackFileNames(filenames);

  //set m_files
  openRootFiles();

  //set m_object_names
  getObjectNames();

  //get TObjects into m_objects
  cacheObjects();
}

std::vector<TFile*> RootFileLoader::getFiles() {
  return m_files;
}

size_t RootFileLoader::getNfiles() {
  return m_files.size();
}

std::vector<RootFileLoader::object_store_t> RootFileLoader::getObjects() {
  return m_objects;
}

size_t RootFileLoader::nObjects() {
  return m_objects.size();
}

RootFileLoader::object_store_t RootFileLoader::getObject(
    const std::string& obj_name) {
  auto res = std::find_if(m_objects.begin(), m_objects.end(),
                  [&](const object_store_t& obj){return obj.name == obj_name;});
  if(res != m_objects.end()) {
    return *res;
  } else {
    //warn and return dummy
    std::cout << "[RootFileLoader::getObject] WARNING object \"" << obj_name <<  "\" not found" << '\n';
    object_store_t dummy;
    return dummy;
  }
}

RootFileLoader::object_store_t RootFileLoader::getObjectComparison(
    const std::vector<std::string>& obj_names) {
  //create a new object
  object_store_t new_obj;
  if (m_files.size() != 1) {
    std::cout << "[RootFileLoader::getObjectComparison] WARNING only ";
    std::cout << "uses first input for comparison" << '\n';
    // return new_obj;
  }
  std::string new_name = "compare_";
  // new_name+=obj_names.at(0).name;
  std::string new_class_name;
  for(auto& obj_name : obj_names) {
    auto obj = getObject(obj_name);
    new_class_name = obj.class_name;
    new_name += "_" + obj.name;
    new_obj.object_vec.push_back(obj.object_vec.at(0));
  }
  new_obj.class_name = new_class_name;
  new_obj.name = new_name;
  return new_obj;
}

RootFileLoader::object_store_t RootFileLoader::getObjectComparison(
    const std::vector<ComparisonData>& obj_names) {
  //create a new object
  object_store_t new_obj;
  if (m_files.size() != 1) {
    std::cout << "[RootFileLoader::getObjectComparison] WARNING only ";
    std::cout << "uses first input for comparison" << '\n';
    // return new_obj;
  }
  std::string new_name = "compare_";
  // new_name+=obj_names.at(0).name;
  std::string new_class_name;

  for(auto& obj_name : obj_names) {
    if (m_debug) std::cout << "comp obj name: " << obj_name.name << '\n';
    auto obj = getObject(obj_name.name);
    new_class_name = obj.class_name;
    new_name += "_" + obj.name;
    new_obj.object_vec.push_back(obj.object_vec.at(0));
  }
  new_obj.class_name = new_class_name;
  new_obj.name = new_name;
  return new_obj;
}

std::vector<std::string> RootFileLoader::getObjectNames() {
  //if these are already extracted, don't do it again
  //TODO this is not solid yet
  if (m_object_names.size() > 0) {return m_object_names;}

  if (m_files.size() == 0) {
    std::cout << "[RootFileLoader::getObjectNames] WARNING no files loaded" << '\n';
    return m_object_names;
  }

  auto first_file = m_files.at(0);
  auto list_of_keys = first_file->GetListOfKeys();
  int n_entries = list_of_keys->GetEntries();
  m_object_names.reserve(n_entries);

  TIter next(list_of_keys);
  TKey* key;
  while ((key = (TKey*) next())) {
    std::string object_name(key->GetName());
    //if it is a directory, open the directory
    //NOTE goes only one level down!

    auto obj = m_files.at(0)->Get(object_name.c_str());
    TString class_name = obj->ClassName();
    if (class_name == "TDirectoryFile") {
      TDirectory* dir = first_file->GetDirectory(object_name.c_str());
      auto dir_keys = dir->GetListOfKeys();
      TIter inner_next(dir_keys);
      TKey* iner_key;
      while ((iner_key = (TKey*) inner_next())) {
        std::string object_name_inner(iner_key->GetName());
        object_name_inner = object_name + "/" + object_name_inner;
        m_object_names.push_back(object_name_inner);
      }
    }
    if (object_name == "") {continue;} //need instructions from object title
    m_object_names.push_back(object_name);
  }
  return m_object_names;
}

void RootFileLoader::cacheObjects() {
  if (m_files.size() == 0) {
    std::cout <<"[Plotter::cacheObjects]: WARNING no input files" << '\n';
    return;
  }
  for (auto object_name : m_object_names) {
    auto obj = m_files.at(0)->Get(object_name.c_str());
    TString class_name = TString(obj->ClassName());
    if (m_debug) std::cout<< "[Plotter::cacheObjects]: obj name: " << object_name << '\n';
    if (class_name == "TH1F") {
      std::vector<TH1*> histo_vec = getTObjectsVector<TH1>(object_name);

      object_store_t object;
      object.name = object_name;
      object.class_name = class_name;
      object.object_vec = std::vector<TObject*>(histo_vec.begin(), histo_vec.end());

      m_objects.push_back(object);
    } else if (class_name == "TProfile") {
      std::vector<TProfile*> histo_vec = getTObjectsVector<TProfile>(object_name);

      object_store_t object;
      object.name = object_name;
      object.class_name = class_name;
      object.object_vec = std::vector<TObject*>(histo_vec.begin(), histo_vec.end());

      m_objects.push_back(object);
    } else if (class_name == "TEfficiency") {
      std::vector<TEfficiency*> histo_vec = getTObjectsVector<TEfficiency>(object_name);

      object_store_t object;
      object.name = object_name;
      object.class_name = class_name;
      object.object_vec = std::vector<TObject*>(histo_vec.begin(), histo_vec.end());

      m_objects.push_back(object);
    } else if (class_name == "TH2D") {
      std::vector<TH2D*> histo_vec = getTObjectsVector<TH2D>(object_name);

      object_store_t object;
      object.name = object_name;
      object.class_name = class_name;
      object.object_vec = std::vector<TObject*>(histo_vec.begin(), histo_vec.end());

      m_objects.push_back(object);
    }
  }
}

std::string RootFileLoader::interpretPathToRootFile(const std::string& input_path) {
  //if this input path starts with a '/', it is treated as a absolute path
  //else we assume the path starts in our root directory
  if (input_path.size() == 0) {
    std::string err_msg = "[RootFileLoader]: ERROR invalid root file path";
    throw std::invalid_argument(err_msg);
  }
  if (input_path[0] != '/') {
    //path is a relative path, start from the root dir of this project
    std::string root_path(std::getenv("ROOTFOLDERPATH"));
    if (root_path.size() == 0) {
      std::string err_msg = "[RootFileLoader]: ERROR environmental variables not set";
      throw std::runtime_error(err_msg);
    }
    return m_path_handler.combinePaths(root_path, input_path);
  } else {
    //path is absolute
    return input_path;
  }
}

std::vector<std::string> RootFileLoader::unpackFileNames(const std::string& filenames) {
  return helper::vectorize(filenames, " ");
}

void RootFileLoader::openRootFiles() {
  for (const auto& file_name : m_file_names) {
    std::string path(m_path_handler.combinePaths(m_path_to_files, file_name));
    if (m_path_handler.fileExists(path)) {
      addRootFile(path);
    }
  }
}


void RootFileLoader::addRootFile(const std::string& path_to_file){
  auto file = TFile::Open(path_to_file.c_str());
  if (not file) {
    releaseInputFiles();
    std::string err_msg = "[RootFileLoader::addRootFile]: Input file does not exist.";
    throw std::invalid_argument(err_msg);
  }
  if (file->IsZombie()) {
    releaseInputFiles();
    std::string err_msg = "[RootFileLoader::addRootFile]: Input file is in zombie state.";
    throw std::invalid_argument(err_msg);
  }
  if (m_debug) {
    std::cout << "[RootFileLoader::addRootFile]: adding " << file->GetName();
    std::cout << '\n';
  }
  m_files.push_back(file);
}


void RootFileLoader::releaseInputFiles() {
  for (auto file : m_files) {
    if (file) {
      file->Close();
    }
  }
}

void RootFileLoader::releaseObjects() {
  for (auto& object : m_objects) {
    for (auto& object_element : object.object_vec) {
      if (object_element) {
        delete object_element;
      }
    }
  }
}

RootFileLoader::~RootFileLoader() {
  releaseObjects();
  releaseInputFiles();
}


template<typename T>
std::vector<T*> RootFileLoader::getTObjectsVector(const std::string& histname) {
  std::vector<T*> vec;
  for (auto file : m_files) {
    auto histo = extractTObject<T>(histname, file);
    vec.push_back(histo);
  }
  return vec;
}

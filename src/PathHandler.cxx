#include "PathHandler.h"

#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

bool PathHandler::fileExists(const std::string& name) const {
  return fs::exists(name);
}

bool PathHandler::directoryExists(const std::string& name) const {
  return fileExists(name) and fs::is_directory(name);
}

std::string PathHandler::combinePaths(const std::string& p1,
                                      const std::string& p2) const {
  std::string combined_path = p1 + "/" + p2;
  findAndReplaceAll(combined_path, "//", "/");
  return combined_path;
}

void PathHandler::findAndReplaceAll(std::string& input,
                                    const std::string& to_replace,
                                    const std::string& replacement) const {
  // Get the first occurrence
  size_t pos = input.find(to_replace);

  // Repeat till end is reached
  while( pos != std::string::npos)
    {
     // Replace this occurrence of Sub String
     input.replace(pos, to_replace.size(), replacement);
     // Get the next occurrence from the current position
     pos = input.find(to_replace, pos + replacement.size());
    }
}

bool PathHandler::createDirectory(const std::string& path) const {
  if (fs::exists(path)) {
    return false;
  }
  fs::path b_path(path);
  return fs::create_directory(b_path);
}

void PathHandler::copyFile(const std::string& origin, const std::string& target) const {
  fs::copy(origin, target);
}

std::vector<std::string> PathHandler::getFilesInDir(const std::string& path) const {
  std::vector<std::string> filenames;
  for(auto& file: fs::directory_iterator(path)) {
    filenames.push_back(file.path().string());
  }
  return filenames;
}

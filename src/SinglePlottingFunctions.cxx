#include "SinglePlottingFunctions.h"

#include "AtlasUtils.h"
#include "AtlasStyle.h"

#include "TAxis.h"
#include "TCanvas.h"
#include "TEfficiency.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TH2D.h"
#include "TCollection.h"
#include "TKey.h"
#include "TLegend.h"
#include "TLine.h"
#include "TProfile.h"
#include "TString.h"
#include "TGraphAsymmErrors.h"
#include "THStack.h"

#include <vector>
#include <algorithm>
#include <iostream>

void plotEffCutFlow(TFile* file, TEnv& config_reader) {
  if (not file) {
    std::cout << "[plotEffCutFlow]: no file" << std::endl;
    return;
  }

  TString dataset_name = config_reader.GetValue("dataset_name", "DataSetName_DEFAULT");
  bool print_png = config_reader.GetValue("print_png", false);
  TString work_status = config_reader.GetValue("work_status", "Internal");
  TString output_plot_path = config_reader.GetValue("output_plot_path", "PLOTS/");


  std::vector<Color_t> colors = {kTeal-6, kRed-4, kAzure+6, kOrange-3, kPink-9, kMagenta-3, kViolet+4};

  std::vector<TObject*> cleanup;
  std::vector<TEfficiency*> effs;
  std::vector<TH1F*> hists;

  auto eff1 = (TEfficiency*) file->Get("m_eff_vs_eta_has_assigned_segment_good_chi2");
  eff1->SetFillColor(colors.at(0));
  effs.push_back(eff1);
  auto eff2 = (TEfficiency*) file->Get("m_eff_vs_eta_no_hits_in_cone");
  eff2->SetFillColor(colors.at(1));
  effs.push_back(eff2);
  auto eff3 = (TEfficiency*) file->Get("m_eff_vs_eta_no_seeds");
  eff3->SetFillColor(colors.at(2));
  effs.push_back(eff3);
  auto eff4 = (TEfficiency*) file->Get("m_eff_vs_eta_no_working_fit");
  eff4->SetFillColor(colors.at(3));
  effs.push_back(eff4);
  auto eff5 = (TEfficiency*) file->Get("m_eff_vs_eta_no_segment_accepted");
  eff5->SetFillColor(colors.at(4));
  effs.push_back(eff5);

  THStack* stack_hist = new THStack("stack_hist",";|#eta|;efficiency");
  cleanup.push_back(stack_hist);

  //std::vector<int> colors = {42, 30, 46, 38, 28};
  for (size_t i=0; i<effs.size(); i++) {
    auto graph = effs.at(i)->CreateGraph();
    TH1F* hist = turnGraphIntoHist(graph, graph->GetName()+TString::Format("h%zu", i), "", 16, 2.4, 4.0, effs.at(i)->GetFillColor());
    hists.push_back(hist);
    stack_hist->Add(hist);
  }

  stack_hist->SetMaximum(stack_hist->GetMaximum()*1.4);

  TCanvas* c = new TCanvas();
  cleanup.push_back(c);
  c->SetTicks(1,1);

  stack_hist->Draw();

  atlas::ATLAS_LABEL(0.19, 0.88, kBlack);
  atlas::myText(0.31, 0.88, kBlack, work_status);

  auto legend = new TLegend(0.6, 0.73, 0.85, 0.92);

  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.035);


  legend->AddEntry(hists.at(0), "Segment found", "f");
  legend->AddEntry(hists.at(1), "No hits in cone", "f");
  legend->AddEntry(hists.at(2), "No seed found", "f");
  legend->AddEntry(hists.at(3), "No working fit", "f");
  legend->AddEntry(hists.at(4), "No segment passed cut", "f");

  legend->Draw("same");

  TString plot_name = Form("%s/%s_StackPlot.pdf",  output_plot_path.Data(), dataset_name.ReplaceAll(" ", "").ReplaceAll(",", "_").Data());
  c->Print(plot_name);
  if (print_png) c->Print(plot_name.ReplaceAll(".pdf", ".png"));

  std::for_each(cleanup.begin(), cleanup.end(), [](TObject* obj) {delete obj;});

}

TH1F* turnGraphIntoHist(TGraph* graph, TString name, TString label, int bins, double xmin, double xmax, int color) {
  TH1F* hist = new TH1F(name, label, bins, xmin, xmax);
  for (int point=0; point<graph->GetN(); point++) {
    double x,y;
    graph->GetPoint(point, x, y);
    int bin_n = hist->FindFixBin(x);
    hist->SetBinContent(bin_n, y);
  }
  hist->SetFillColor(color);
  hist->SetMarkerSize(3);
  hist->SetMarkerColor(color);
  return hist;
}

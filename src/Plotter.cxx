#include "Plotter.h"

#include "AtlasStyle.h"
#include "AtlasUtils.h"
#include "FileReader.h"
#include "HelperFunctions.h"

#include <string>

// ROOT includes
#include "TCanvas.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "TH1.h"
#include "TH2D.h"
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"
#include "TProfile.h"
#include "TROOT.h"
#include "TRatioPlot.h"

#include <algorithm>
#include <iostream>

Plotter::Plotter() : m_path_handler(PathHandler::getInstance()) {

  SetAtlasStyle();
}

void Plotter::configure(const Configurator &configurator) {

  std::cout << "[Plotter::configure] configuration: " << '\n';

  // get the input root file(s)
  const TEnv &config_reader = configurator.config();

  std::string input_path = config_reader.GetValue("input_path", "data");
  std::string raw_input_file_names =
      config_reader.GetValue("input_file_names", "");

  // put it into the reader, which gives us the pointers to the files
  m_file_loader =
      std::make_unique<RootFileLoader>(input_path, raw_input_file_names);

  m_use_object_list = config_reader.GetValue("plot_object_list", false);
  if (m_use_object_list) {
    setObjectList(config_reader.GetValue("object_list_file", ""));
  }

  // get the label names(TString str, TString sep)
  m_label_names =
      helper::vectorize(config_reader.GetValue("input_file_labels", ""), " ");

  size_t n_input_file = m_file_loader->getNfiles();
  if (n_input_file == 0) {
    std::string err_msg = "[Plotter::configure]: ERROR no input files found";
    throw std::invalid_argument(err_msg);
  }

  std::cout << "[Plotter::configure] " << n_input_file << " input files"
            << '\n';

  m_run_overlay_mode = n_input_file > 1;
  if (n_input_file > 1 and n_input_file != m_label_names.size()) {
    std::string err_msg = "[Plotter::configure]: ERROR invalid number of "
                          "label names defined in configuration";
    throw std::invalid_argument(err_msg);
  }

  m_output_plot_path = config_reader.GetValue("output_path", "");
  std::cout << "[Plotter::configure] output path: " << m_output_plot_path
            << '\n';
  if (not m_path_handler.directoryExists(m_output_plot_path)) {
    const std::string root_folder_path(std::getenv("ROOTFOLDERPATH"));
    m_path_handler.createDirectory(m_output_plot_path);
    m_path_handler.copyFile(
        m_path_handler.combinePaths(root_folder_path, "data/index.php"),
        m_output_plot_path);
    m_path_handler.copyFile(
        m_path_handler.combinePaths(root_folder_path, "data/navbar.php"),
        m_output_plot_path);
    m_path_handler.copyFile(
        m_path_handler.combinePaths(root_folder_path, "data/quickbar.php"),
        m_output_plot_path);
  }

  // which colors should be used for overlaps
  // m_colors = {kTeal-6, kRed-4,kOrange-3,  kAzure+6, kPink-9, kMagenta-3,
  // kViolet+4};
  std::string line_colors_string = config_reader.GetValue("line_colors", "1");
  m_colors = helper::vectorizeToInt(line_colors_string, " ");

  std::cout << "[Plotter::configure] " << m_colors.size()
            << " line colors defined" << '\n';
  // the line style used for each file
  std::string line_styles_string = config_reader.GetValue("line_styles", "1");
  m_line_style = helper::vectorizeToInt(line_styles_string, " ");

  std::cout << "[Plotter::configure] " << m_line_style.size()
            << " line styles defined" << '\n';

  m_work_status = config_reader.GetValue("work_status", "Internal");

  m_description_text = config_reader.GetValue("description_text", "");

  m_run_comparison = config_reader.GetValue("run_comparison", false);

  if (m_run_comparison) {
    m_comparison_cfg_file =
        config_reader.GetValue("comparison_config_file", "default");
    std::cout << "[Plotter::configure] using comparison file: "
              << m_comparison_cfg_file << '\n';
    std::string complete_cfg_filename = m_path_handler.combinePaths(
        configurator.getPathToConfigs(), m_comparison_cfg_file);
    if (m_path_handler.fileExists(complete_cfg_filename)) {
      FileReader file_reader;
      auto raw_comparison_stringvec =
          file_reader.getVectorizedLines(complete_cfg_filename, ";");
      std::cout << "[Plotter::configure] number of comparison plots: "
                << raw_comparison_stringvec.size() << '\n';
      for (auto &raw_comparison_string_line : raw_comparison_stringvec) {
        std::vector<ComparisonData> comparison_line;
        for (auto &raw_comparison_string : raw_comparison_string_line) {
          auto parts = helper::vectorize(raw_comparison_string, ",");
          if (parts.size() != 4) {
            std::string err_msg =
                "[Plotter::configure]: ERROR can't read "
                "comparison file format, "
                "expects: <hist_name>, <color_int>, <style_int>, <label_name>;";
            throw std::invalid_argument(err_msg);
          }
          ComparisonData comparison_obj;
          comparison_obj.name = helper::removeWhiteSpace(parts.at(0));
          comparison_obj.color = std::stoi(parts.at(1));
          comparison_obj.style = std::stoi(parts.at(2));
          comparison_obj.label = parts.at(3);
          comparison_line.push_back(comparison_obj);
        }
        m_comparison_objects.push_back(comparison_line);
      }
    } else {
      std::cout << "[Plotter::configure] WARNING comparison input not found"
                << '\n';
    }
  }

  m_use_log_scale = config_reader.GetValue("use_log_scale", false);
  m_log_scale_minimum = config_reader.GetValue("log_scale_minimum", 1.e-4);
  m_y_scale_scalefactor = config_reader.GetValue("y_axis_scalefactor", 1.3);

  m_do_ratio_plot = config_reader.GetValue("do_ratio_plot", false);

  m_debug = config_reader.GetValue("debug_plotter", false);
  ; // TODO
}

Plotter::~Plotter() {
  // releaseInputFiles();
}

void Plotter::printAdditionalLabels() {
  Color_t text_color = kBlack;
  double ypos = 0.82;
  double step = 0.05;
  double text_size = 0.04;

  if (m_do_ratio_plot) {
    text_size = 0.06;
  }

  // split the string into lines
  auto lines = helper::vectorize(m_description_text, "\\");

  for (auto line : lines) {
    std::replace(line.begin(), line.end(), '$', '#');
    atlas::myText(0.19, ypos, text_color, line.c_str(), text_size);
    ypos -= step;
  }
}

void Plotter::listObjects() {
  auto object_stores = m_file_loader->getObjects();
  for (const auto &object_store : object_stores) {
    std::cout << object_store.name << '\n';
  }
}

void Plotter::setObjectList(const std::string &file_name) {
  FileReader reader;
  const std::string root_folder_path(std::getenv("ROOTCFGPATH"));
  auto path = m_path_handler.combinePaths(root_folder_path, file_name);
  m_object_names = reader.readFileLines(path);
}

void Plotter::plot() {

  debugLine("[Plotter::plot] start");
  // get the objects from the RootFileLoader
  std::vector<object_store_t> object_stores;
  object_stores.reserve(m_file_loader->nObjects());
  if (m_use_object_list) {
    for (const auto &obj : m_object_names) {
      object_stores.push_back(m_file_loader->getObject(obj));
    }
  } else {
    object_stores = m_file_loader->getObjects();
  }

  debugLine("[Plotter::plot] getObjects done");
  // pass them to the corresponding functions depending on class name
  for (const auto &object_store : object_stores) {

    if (m_debug)
      std::cout << "[Plotter::plot] obj name: " << object_store.name << '\n';
    if (m_debug)
      std::cout << "[Plotter::plot] class name: " << object_store.class_name
                << '\n';
    if (object_store.class_name == "TH1F") {
      auto histos = convertObjects<TH1>(object_store.object_vec);
      plotHistograms(object_store.name, histos, m_label_names, m_colors,
                     m_line_style);
    } else if (object_store.class_name == "TEfficiency") {
      auto histos = convertObjects<TEfficiency>(object_store.object_vec);
      plotEfficiency(object_store.name, histos, m_label_names, m_colors,
                     m_line_style);
    } else if (object_store.class_name == "TProfile") {
      auto histos = convertObjects<TProfile>(object_store.object_vec);
      plotProfile(object_store.name, histos, m_label_names, m_colors,
                  m_line_style);
    } else if (object_store.class_name == "TH2D") {
      auto histos = convertObjects<TH2D>(object_store.object_vec);
      plot2DHistogram(object_store.name, histos, m_label_names, m_colors,
                      m_line_style);
    }
  }

  if (m_run_comparison) {
    m_run_overlay_mode = true;
    debugLine("[Plotter::plot] run comparison");
    for (const auto &comparison_vec : m_comparison_objects) {
      auto comparison_object =
          m_file_loader->getObjectComparison(comparison_vec);
      if (comparison_object.class_name == "TH1F") {
        auto histos = convertObjects<TH1>(comparison_object.object_vec);
        std::vector<std::string> labels;
        std::vector<int> colors;
        std::vector<int> styles;
        for (auto &comparison : comparison_vec) {
          labels.push_back(comparison.label);
          colors.push_back(comparison.color);
          styles.push_back(comparison.style);
        }
        plotHistograms(comparison_object.name, histos, labels, colors, styles);
      } else if (comparison_object.class_name == "TEfficiency") {
        auto histos = convertObjects<TEfficiency>(comparison_object.object_vec);
        std::vector<std::string> labels;
        std::vector<int> colors;
        std::vector<int> styles;
        for (auto &comparison : comparison_vec) {
          labels.push_back(comparison.label);
          colors.push_back(comparison.color);
          styles.push_back(comparison.style);
        }
        plotEfficiency(comparison_object.name, histos, labels, colors, styles);
      } else if (comparison_object.class_name == "TProfile") {
        auto histos = convertObjects<TProfile>(comparison_object.object_vec);
        std::vector<std::string> labels;
        std::vector<int> colors;
        std::vector<int> styles;
        for (auto &comparison : comparison_vec) {
          labels.push_back(comparison.label);
          colors.push_back(comparison.color);
          styles.push_back(comparison.style);
        }
        plotProfile(comparison_object.name, histos, labels, colors, styles);
      }
    }
  }
}

void Plotter::plotHistograms(std::string name,
                             const std::vector<TH1 *> &hist_vec,
                             const std::vector<std::string> &labels,
                             const std::vector<int> &colors,
                             const std::vector<int> &styles) {
  debugLine("[Plotter::plotHistograms] start");
  for (auto hist : hist_vec) {
    if (not hist) {
      std::cout << "Plotter::plotHistograms: " << name
                << " does not exist in one of the files." << '\n';
      return;
    }
  }

  std::vector<TObject *> dummy_canvases;

  TCanvas *c = new TCanvas(name.c_str());
  c->SetTicks(1, 1);

  if (m_use_log_scale) {
    c->SetLogy();
  }

  // find the max in y
  double max_y = -1;
  for (auto &hist : hist_vec) {
    hist->Scale(1. / hist->Integral(0, hist->GetNbinsX() + 1));
    if (hist->GetMaximum() > max_y) {
      max_y = hist->GetMaximum();
    }
  }

  auto legend = new TLegend(0.65, 0.75, 0.9, 0.9);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextFont(72);
  // legend->SetTextSize(0);

  if (m_do_ratio_plot) {
    std::vector<TRatioPlot *> ratio_plots;

    TPad *upper_pad = nullptr;
    TPad *lower_pad = nullptr;

    int bins = hist_vec.at(0)->GetXaxis()->GetNbins();
    double min = hist_vec.at(0)->GetXaxis()->GetXmin();
    double max = hist_vec.at(0)->GetXaxis()->GetXmax();
    auto dummyh1 = new TH1F("dummyh1", "", bins, min, max);
    dummyh1->SetBinContent(1, 1);
    dummyh1->SetLineColor(0);
    dummyh1->SetMarkerColor(0);
    dummyh1->GetXaxis()->SetTitle(hist_vec.at(0)->GetXaxis()->GetTitle());
    dummyh1->GetYaxis()->SetTitle(hist_vec.at(0)->GetYaxis()->GetTitle());
    auto dummyh2 = new TH1F("dummyh2", "", bins, min, max);
    dummyh2->SetBinContent(1, 1);
    dummyh2->SetLineColor(0);
    dummyh2->SetMarkerColor(0);

    auto canvas_rp = new TRatioPlot(dummyh1, dummyh2);
    ratio_plots.push_back(canvas_rp);
    canvas_rp->Draw();
    auto dummy_graph = canvas_rp->GetLowerRefGraph();
    dummy_graph->SetLineColor(0);
    dummy_graph->SetMarkerColor(0);
    upper_pad = canvas_rp->GetUpperPad();
    lower_pad = canvas_rp->GetLowerPad();

    canvas_rp->SetLowBottomMargin(0.47);
    canvas_rp->SetLowTopMargin(0.);
    canvas_rp->SetUpBottomMargin(0.);
    canvas_rp->SetUpTopMargin(0.05);

    for (int i = 1; i < hist_vec.size(); i++) {
      auto dummy = new TCanvas();
      dummy->cd();
      dummy_canvases.push_back(dummy);
      auto rp = new TRatioPlot(hist_vec.at(i), hist_vec.at(0));
      rp->Draw();
      ratio_plots.push_back(rp);
    }

    for (size_t i = 0; i < hist_vec.size(); i++) {
      hist_vec.at(i)->SetLineColor(colors.at(i));
      hist_vec.at(i)->SetMarkerColor(colors.at(i));
      hist_vec.at(i)->SetMarkerSize(1.0);
      hist_vec.at(i)->SetMarkerStyle(21);
      hist_vec.at(i)->SetLineStyle(styles.at(i));
      hist_vec.at(i)->SetLineWidth(4);
      std::string label_title = labels.at(i);
      std::replace(label_title.begin(), label_title.end(), '$', '#');
      std::replace(label_title.begin(), label_title.end(), '\\', ' ');
      legend->AddEntry(hist_vec.at(i), label_title.c_str(), "l");
    }
    // plot the histograms
    upper_pad->cd();
    double histmax = 0;
    for (int i = 0; i < hist_vec.size(); i++) {
      hist_vec.at(i)->Draw("same hist");
      if (hist_vec.at(i)->GetMaximum() > histmax) {
        histmax = hist_vec.at(i)->GetMaximum();
      }
    }
    legend->Draw();
    debugLine("[Plotter::plotEfficiency] draw text labels");
    atlas::ATLAS_LABEL(0.19, 0.87, kBlack, 1.1);
    atlas::myText(0.29, 0.87, kBlack, m_work_status.c_str());
    printAdditionalLabels();
    /// plot the ratio graphs
    lower_pad->cd();
    double graph_low = 10;
    double graph_high = -10;
    for (int i = 1; i < ratio_plots.size(); i++) {
      auto graph = ratio_plots.at(i)->GetLowerRefGraph();
      for (int point = 1; point <= graph->GetN(); point++) {
        double x, y;
        graph->GetPoint(point, x, y);
        if (y < graph_low) {
          graph_low = y;
        }
        if (y > graph_high) {
          graph_high = y;
        }
      }

      graph->SetLineColor(hist_vec.at(i)->GetLineColor());
      graph->SetMarkerColor(hist_vec.at(i)->GetLineColor());
      graph->Draw("same LP");
    }

    double low_distance_from_one = fabs(graph_low - 1);
    double high_distance_from_one = fabs(1 - graph_high);
    // eff_vec.at(0)->CreateGraph()->GetYaxis()->GetTitle()
    // ratio_plots.at(0)->GetUpperRefYaxis()->SetTitle(
    //     histograms.at(0)->GetYaxis()->GetTitle());

    ratio_plots.at(0)->GetLowerRefYaxis()->SetRangeUser(
        1 - 1.2 * low_distance_from_one, 1 + 1.2 * high_distance_from_one);
    ratio_plots.at(0)->GetUpperRefYaxis()->SetRangeUser(
        0, max_y * m_y_scale_scalefactor);
    ratio_plots.at(0)->GetLowerRefYaxis()->SetTitle("ratio");

    upper_pad->SetRightMargin(0.05);
    upper_pad->SetLeftMargin(0.16);
    lower_pad->SetRightMargin(0.05);
    lower_pad->SetLeftMargin(0.16);
  } else {

    bool first = true;
    int iter = 0;
    for (auto &hist : hist_vec) {
      if (m_use_log_scale) {
        hist->SetAxisRange(m_log_scale_minimum, max_y * m_y_scale_scalefactor,
                           "Y");
      } else {
        hist->SetAxisRange(0., max_y * m_y_scale_scalefactor, "Y");
      }

      hist->GetYaxis()->SetTitle("a.u.");
      hist->SetLineColor(colors.at(iter));
      hist->SetMarkerColor(colors.at(iter));
      hist->SetLineStyle(styles.at(iter));
      hist->SetLineWidth(4);
      if (first) {
        hist->Draw("hist");
        first = false;
      } else {
        hist->Draw("hist same");
      }
      if (m_run_overlay_mode) {
        std::string label_title = labels.at(iter);
        std::replace(label_title.begin(), label_title.end(), '$', '#');
        std::replace(label_title.begin(), label_title.end(), '\\', ' ');
        legend->AddEntry(hist, label_title.c_str(), "l");
      }
      iter++;
    }

    if (m_run_overlay_mode) {
      legend->Draw("same");
    }

    atlas::ATLAS_LABEL(0.19, 0.88, kBlack);
    atlas::myText(0.31, 0.88, kBlack, m_work_status.c_str());
    printAdditionalLabels();

    gPad->RedrawAxis();
  }

  std::replace(name.begin(), name.end(), '/', '_');
  TString plot_name = Form("%s/plotHistograms_%s.pdf",
                           m_output_plot_path.c_str(), name.c_str());
  c->Print(plot_name);
  if (m_print_png)
    c->Print(plot_name.ReplaceAll(".pdf", ".png"));
  // delete hist_clone;
  delete c;
  delete legend;
}

void Plotter::plotEfficiency(std::string name,
                             const std::vector<TEfficiency *> &eff_vec,
                             const std::vector<std::string> &labels,
                             const std::vector<int> &colors,
                             const std::vector<int> &styles) {
  debugLine("[Plotter::plotEfficiency] start");

  TCanvas *canvas = new TCanvas(name.c_str());

  std::vector<TGraphAsymmErrors *> graphs;
  std::vector<TH1F *> histograms;
  std::vector<TObject *> dummy_canvases;

  canvas->SetTicks(1, 1);

  auto legend = new TLegend(0.65, 0.80, 0.9, 0.9);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);

  bool first = true;
  debugLine("[Plotter::plotEfficiency] draw graphs");
  if (m_do_ratio_plot) {

    for (auto eff : eff_vec) {
      TGraphAsymmErrors *graph = eff->CreateGraph();
      graphs.push_back(graph);
      if (not eff) {
        std::cout << "Plotter::plotEfficiency: " << name
                  << " does not exist in one of the files." << '\n';
        return;
      }
    }

    for (auto &eff : eff_vec) {
      auto hist = transformEffToHis(eff);
      histograms.push_back(hist);
      if (not hist) {
        std::cout << "Plotter::plotEfficiency: " << name
                  << " does not exist in one of the files." << '\n';
        return;
      }
    }

    std::vector<TRatioPlot *> ratio_plots;

    TPad *upper_pad = nullptr;
    TPad *lower_pad = nullptr;

    int bins = histograms.at(0)->GetXaxis()->GetNbins();
    double min = histograms.at(0)->GetXaxis()->GetXmin();
    double max = histograms.at(0)->GetXaxis()->GetXmax();
    auto dummyh1 = new TH1F("dummyh1", "", bins, min, max);
    dummyh1->SetBinContent(1, 1);
    dummyh1->SetLineColor(0);
    dummyh1->SetMarkerColor(0);
    dummyh1->GetXaxis()->SetTitle(
        eff_vec.at(0)->CreateGraph()->GetXaxis()->GetTitle());
    dummyh1->GetYaxis()->SetTitle(
        eff_vec.at(0)->CreateGraph()->GetYaxis()->GetTitle());
    auto dummyh2 = new TH1F("dummyh2", "", bins, min, max);
    dummyh2->SetBinContent(1, 1);
    dummyh2->SetLineColor(0);
    dummyh2->SetMarkerColor(0);

    auto canvas_rp = new TRatioPlot(dummyh1, dummyh2);
    ratio_plots.push_back(canvas_rp);
    canvas_rp->Draw();
    auto dummy_graph = canvas_rp->GetLowerRefGraph();
    dummy_graph->SetLineColor(0);
    dummy_graph->SetMarkerColor(0);
    upper_pad = canvas_rp->GetUpperPad();
    lower_pad = canvas_rp->GetLowerPad();

    canvas_rp->SetLowBottomMargin(0.47);
    canvas_rp->SetLowTopMargin(0.);
    canvas_rp->SetUpBottomMargin(0.);
    canvas_rp->SetUpTopMargin(0.05);

    for (int i = 1; i < histograms.size(); i++) {
      auto dummy = new TCanvas();
      dummy->cd();
      dummy_canvases.push_back(dummy);
      auto rp = new TRatioPlot(histograms.at(i), histograms.at(0));
      rp->Draw();
      ratio_plots.push_back(rp);
    }

    for (size_t i = 0; i < histograms.size(); i++) {
      histograms.at(i)->GetYaxis()->SetRangeUser(0., 1.35);
      histograms.at(i)->SetLineColor(colors.at(i));
      histograms.at(i)->SetMarkerColor(colors.at(i));
      histograms.at(i)->SetMarkerSize(1.0);
      histograms.at(i)->SetMarkerStyle(21);
      histograms.at(i)->SetLineStyle(styles.at(i));
      histograms.at(i)->SetLineWidth(4);
      std::string label_title = labels.at(i);
      std::replace(label_title.begin(), label_title.end(), '$', '#');
      std::replace(label_title.begin(), label_title.end(), '\\', ' ');
      legend->AddEntry(histograms.at(i), label_title.c_str(), "l");
    }

    // plot the histograms
    upper_pad->cd();
    double histmax = 0;
    for (int i = 0; i < histograms.size(); i++) {
      histograms.at(i)->Draw("same hist");
      if (histograms.at(i)->GetMaximum() > histmax) {
        histmax = histograms.at(i)->GetMaximum();
      }
    }
    legend->Draw();
    debugLine("[Plotter::plotEfficiency] draw text labels");
    atlas::ATLAS_LABEL(0.19, 0.87, kBlack, 1.1);
    atlas::myText(0.29, 0.87, kBlack, m_work_status.c_str());
    printAdditionalLabels();
    /// plot the ratio graphs
    lower_pad->cd();
    double graph_low = 10;
    double graph_high = -10;
    for (int i = 1; i < ratio_plots.size(); i++) {
      auto graph = ratio_plots.at(i)->GetLowerRefGraph();
      for (int point = 1; point <= graph->GetN(); point++) {
        double x, y;
        graph->GetPoint(point, x, y);
        if (y < graph_low) {
          graph_low = y;
        }
        if (y > graph_high) {
          graph_high = y;
        }
      }

      graph->SetLineColor(histograms.at(i)->GetLineColor());
      graph->SetMarkerColor(histograms.at(i)->GetLineColor());
      graph->Draw("same LP");
    }

    // ratio_plots.at(0)->GetLowerRefYaxis()->SetRangeUser(0.8, 2.0);
    double low_distance_from_one = fabs(graph_low - 1);
    double high_distance_from_one = fabs(1 - graph_high);
    // eff_vec.at(0)->CreateGraph()->GetYaxis()->GetTitle()
    // ratio_plots.at(0)->GetUpperRefYaxis()->SetTitle(
    //     histograms.at(0)->GetYaxis()->GetTitle());
    ratio_plots.at(0)->GetUpperRefYaxis()->SetRangeUser(0, 1.3);

    ratio_plots.at(0)->GetLowerRefYaxis()->SetRangeUser(
        1 - 1.2 * low_distance_from_one, 1 + 1.2 * high_distance_from_one);
    ratio_plots.at(0)->GetLowerRefYaxis()->SetTitle("ratio");

    upper_pad->SetRightMargin(0.05);
    upper_pad->SetLeftMargin(0.16);
    lower_pad->SetRightMargin(0.05);
    lower_pad->SetLeftMargin(0.16);

  } else {

    for (auto eff : eff_vec) {
      TGraphAsymmErrors *graph = eff->CreateGraph();
      graphs.push_back(graph);
      if (not eff) {
        std::cout << "Plotter::plotEfficiency: " << name
                  << " does not exist in one of the files." << '\n';
        return;
      }
    }

    for (size_t i = 0; i < graphs.size(); i++) {
      graphs.at(i)->GetYaxis()->SetRangeUser(0., 1.35);
      graphs.at(i)->SetLineColor(colors.at(i));
      graphs.at(i)->SetMarkerColor(colors.at(i));
      graphs.at(i)->SetMarkerSize(1.0);
      graphs.at(i)->SetMarkerStyle(21);
      graphs.at(i)->SetLineStyle(styles.at(i));
      graphs.at(i)->SetLineWidth(4);
      if (first) {
        graphs.at(i)->Draw("AP");
        eff_vec.at(i)->Draw("same P");
        first = false;
      } else {
        graphs.at(i)->Draw("same P");
        eff_vec.at(i)->Draw("same P");
      }
      if (m_run_overlay_mode) {
        std::string label_title = labels.at(i);
        std::replace(label_title.begin(), label_title.end(), '$', '#');
        std::replace(label_title.begin(), label_title.end(), '\\', ' ');
        legend->AddEntry(graphs.at(i), label_title.c_str(), "l");
        // legend->AddEntry(graphs.at(i), labels.at(i).c_str(), "l");
      }
    }
    debugLine("[Plotter::plotEfficiency] draw lines");
    TLine line1 = TLine(graphs.at(0)->GetXaxis()->GetXmin(), 1,
                        graphs.at(0)->GetXaxis()->GetXmax(), 1);
    line1.SetLineStyle(7);
    line1.SetLineColor(kGray);
    line1.Draw();
    TLine line2 = TLine(graphs.at(0)->GetXaxis()->GetXmin(), .5,
                        graphs.at(0)->GetXaxis()->GetXmax(), .5);
    line2.SetLineStyle(7);
    line2.SetLineColor(kGray);
    line2.Draw();

    if (m_run_overlay_mode) {
      legend->Draw("same");
    }

    debugLine("[Plotter::plotEfficiency] draw text labels");
    atlas::ATLAS_LABEL(0.19, 0.88, kBlack);
    atlas::myText(0.31, 0.88, kBlack, m_work_status.c_str());
    printAdditionalLabels();

  } // don't do ratio plot

  std::replace(name.begin(), name.end(), '/', '_');
  TString plot_name = Form("%s/plotEfficiency_%s.pdf",
                           m_output_plot_path.c_str(), name.c_str());
  canvas->Print(plot_name);
  if (m_print_png) {
    canvas->Print(plot_name.ReplaceAll(".pdf", ".png"));
  }
  for (auto &graph : graphs) {
    delete graph;
  }

  for (auto &dummy_canvas : dummy_canvases) {
    delete dummy_canvas;
  }

  delete canvas;
  delete legend;
}

void Plotter::plotProfile(std::string name,
                          const std::vector<TProfile *> &prof_vec,
                          const std::vector<std::string> &labels,
                          const std::vector<int> &colors,
                          const std::vector<int> &styles) {
  debugLine("[Plotter::plotProfile] start");
  for (auto hist : prof_vec) {
    if (not hist) {
      std::cout << "Plotter::plotProfile: " << name
                << " does not exist in one of the files." << '\n';
      return;
    }
  }

  TCanvas *c = new TCanvas(name.c_str());
  c->SetTicks(1, 1);

  // find the max in y
  double max_y = -1;
  for (auto &hist : prof_vec) {
    // hist->Scale(1./hist->Integral());
    if (hist->GetMaximum() > max_y) {
      max_y = hist->GetMaximum();
    }
  }

  auto legend = new TLegend(0.65, 0.80, 0.9, 0.9);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);

  bool first = true;
  int iter = 0;
  for (auto &hist : prof_vec) {
    hist->SetAxisRange(0., max_y * 1.5, "Y");
    hist->SetLineColor(colors.at(iter));
    hist->SetMarkerColor(colors.at(iter));
    hist->SetLineStyle(styles.at(iter));
    hist->SetLineWidth(4);
    if (first) {
      hist->Draw("el p");
      // hist->Draw("hist");
      first = false;
    } else {
      hist->Draw("el p same");
      // hist->Draw("hist same");
    }
    if (m_run_overlay_mode) {
      std::string label_title = labels.at(iter);
      std::replace(label_title.begin(), label_title.end(), '$', '#');
      std::replace(label_title.begin(), label_title.end(), '\\', ' ');
      legend->AddEntry(hist, label_title.c_str(), "lp");
      // legend->AddEntry(hist, labels.at(iter).c_str(), "l");
    }
    iter++;
  }

  if (m_run_overlay_mode) {
    legend->Draw("same");
  }

  atlas::ATLAS_LABEL(0.19, 0.88, kBlack);
  atlas::myText(0.31, 0.88, kBlack, m_work_status.c_str());
  printAdditionalLabels();

  gPad->RedrawAxis();

  std::replace(name.begin(), name.end(), '/', '_');
  TString plot_name =
      Form("%s/plotProfile_%s.pdf", m_output_plot_path.c_str(), name.c_str());
  c->Print(plot_name);
  if (m_print_png)
    c->Print(plot_name.ReplaceAll(".pdf", ".png"));
  // delete hist_clone;
  delete c;
  delete legend;
}

void Plotter::plot2DHistogram(std::string name,
                              const std::vector<TH2D *> &hist_vec,
                              const std::vector<std::string> &labels,
                              const std::vector<int> &colors,
                              const std::vector<int> &styles) {
  debugLine("[Plotter::plot2DHistogram] start");
  auto hist = hist_vec.at(0);
  if (not hist) {
    std::cout << "[Plotter::plot2DHistogram]: " << name
              << " does not exist in one of the files." << '\n';
    return;
  }

  TCanvas *c = new TCanvas(name.c_str());
  c->SetTicks(1, 1);

  if (m_use_log_scale) {
    c->SetLogz();
  }

  if (m_2d_scale_global) {
    hist->Scale(1. / hist->Integral(0, hist->GetNbinsX() + 1, 0,
                                    hist->GetNbinsY() + 1));
  } else {
    scaleLineX(hist, hist->GetNbinsX(), hist->GetNbinsY());
  }

  double max_y = hist->GetMaximum();
  if (m_use_log_scale) {
    hist->SetAxisRange(m_log_scale_minimum, max_y * m_y_scale_scalefactor, "Z");
  }

  int iter = 0;
  hist->SetLineColor(colors.at(iter));
  hist->SetMarkerColor(colors.at(iter));
  hist->SetLineStyle(styles.at(iter));
  hist->SetLineWidth(4);
  hist->Draw("colz");

  atlas::ATLAS_LABEL(0.19, 0.88, kBlack);
  atlas::myText(0.31, 0.88, kBlack, m_work_status.c_str());
  printAdditionalLabels();

  gPad->RedrawAxis();

  std::replace(name.begin(), name.end(), '/', '_');
  TString plot_name = Form("%s/plot2DHistogram_%s.pdf",
                           m_output_plot_path.c_str(), name.c_str());
  c->Print(plot_name);
  if (m_print_png)
    c->Print(plot_name.ReplaceAll(".pdf", ".png"));
  // delete hist_clone;
  delete c;
}

void Plotter::scaleLineX(TH2D *hist, int bin_x_max, int bin_y_max) {
  double integral_line = 0;
  for (int x = 0; x <= bin_x_max; x++) {
    integral_line = hist->Integral(x, x, 0, bin_y_max);
    if (integral_line > 0) {
      for (int y = 1; y <= bin_y_max; y++) {
        double bincontent = hist->GetBinContent(x, y);
        double binerror = hist->GetBinError(x, y);
        hist->SetBinContent(x, y, bincontent / integral_line);
        hist->SetBinError(x, y, binerror / integral_line);
      }
    }
  }
}

TGraph *Plotter::getRatioGraph(TGraph *numerator, TGraph *denominator) {
  int n = std::max(numerator->GetN(), denominator->GetN());

  TGraph *r = new TGraph();
  r->SetTitle("");

  std::vector<double> xvals;
  for (int i = 1; i < n; i++) {
    double x, y;
    numerator->GetPoint(i, x, y);
    xvals.push_back(x);
  }

  for (int i = 1; i < n; i++) {
    double y1, y2;
    y1 = numerator->Eval(xvals.at(i - 1));
    y2 = denominator->Eval(xvals.at(i - 1));
    r->SetPoint(i + 1, xvals.at(i - 1), y2 / y1);
  }
  // for (int i=skip; i<n; i++) {
  //   double x, y;
  //   numerator->GetPoint(i, x, y);
  //   // y = graphs.at(ratio_plot_choice)->Eval(x)/y;
  //   y = denominator->Eval(x)/y;
  //   r->SetPoint(i-skip, x, y);
  // double r_err =
  // r->SetPointError(i, Double_t ex, Double_t ey);
  // }
  // r->GetXaxis()->SetLabelSize(0.075);
  // r->GetYaxis()->SetLabelSize(0.075);
  r->SetLineColor(denominator->GetLineColor());
  r->SetLineStyle(denominator->GetLineStyle());
  r->SetLineWidth(denominator->GetLineWidth());
  return r;
}

// void Plotter::CanvasPartition(std::string name, TCanvas *canvas, const int
// Nx,
//                               const int Ny, float lMargin, float rMargin,
//                               float bMargin, float tMargin) {
//   debugLine("Plotter::CanvasPartition");
//   if (!canvas)
//     return;
//   // Setup Pad layout:
//   float vSpacing = 0.0;
//   float vStep = (1. - bMargin - tMargin - (Ny - 1) * vSpacing) / Ny;
//   float hSpacing = 0.0;
//   float hStep = (1. - lMargin - rMargin - (Nx - 1) * hSpacing) / Nx;
//   float vposd, vposu, vmard, vmaru, vfactor;
//   float hposl, hposr, hmarl, hmarr, hfactor;
//   for (int i = 0; i < Nx; i++) {
//     if (i == 0) {
//       hposl = 0.0;
//       hposr = lMargin + hStep;
//       hfactor = hposr - hposl;
//       hmarl = lMargin / hfactor;
//       hmarr = 0.0;
//     } else if (i == Nx - 1) {
//       hposl = hposr + hSpacing;
//       hposr = hposl + hStep + rMargin;
//       hfactor = hposr - hposl;
//       hmarl = 0.0;
//       hmarr = rMargin / (hposr - hposl);
//     } else {
//       hposl = hposr + hSpacing;
//       hposr = hposl + hStep;
//       hfactor = hposr - hposl;
//       hmarl = 0.0;
//       hmarr = 0.0;
//     }
//     for (int j = 0; j < Ny; j++) {
//       if (j == 0) {
//         vposd = 0.0;
//         vposu = bMargin + vStep;
//         vfactor = vposu - vposd;
//         vmard = bMargin / vfactor;
//         vmaru = 0.0;
//       } else if (j == Ny - 1) {
//         vposd = vposu + vSpacing;
//         vposu = vposd + vStep + tMargin;
//         vfactor = vposu - vposd;
//         vmard = 0.0;
//         vmaru = tMargin / (vposu - vposd);
//       } else {
//         vposd = vposu + vSpacing;
//         vposu = vposd + vStep;
//         vfactor = vposu - vposd;
//         vmard = 0.0;
//         vmaru = 0.0;
//       }
//       debugLine("Plotter::CanvasPartition values set");
//       canvas->cd(0);
//       char padname[16];
//       sprintf(padname, "pad_%i_%i", i, j);
//       std::cout << "pad name: " << (name + padname).c_str() << '\n';
//       TPad *pad = (TPad *)gROOT->FindObject((name + padname).c_str());
//       if (pad)
//         delete pad;
//       pad = new TPad((name + padname).c_str(), "", hposl, vposd, hposr,
//       vposu); pad->SetLeftMargin(hmarl); pad->SetRightMargin(hmarr);
//       pad->SetBottomMargin(vmard);
//       pad->SetTopMargin(vmaru);
//       pad->SetFrameBorderMode(0);
//       pad->SetBorderMode(0);
//       pad->SetBorderSize(0);
//       pad->Draw();
//       debugLine("Plotter::CanvasPartition pad drawn");
//     }
//   }
// }

std::vector<TPad *> Plotter::CanvasPartition(std::string name, TCanvas *canvas,
                                             const int Nx, const int Ny,
                                             float lMargin, float rMargin,
                                             float bMargin, float tMargin) {
  debugLine("Plotter::CanvasPartition");

  std::vector<TPad *> pads;

  if (!canvas)
    return pads;

  // Setup Pad layout:
  float vSpacing = 0.0;
  float vStep = (1. - bMargin - tMargin - (Ny - 1) * vSpacing) / Ny;
  float hSpacing = 0.0;
  float hStep = (1. - lMargin - rMargin - (Nx - 1) * hSpacing) / Nx;
  float vposd, vposu, vmard, vmaru, vfactor;
  float hposl, hposr, hmarl, hmarr, hfactor;
  for (int i = 0; i < Nx; i++) {
    if (i == 0) {
      hposl = 0.0;
      hposr = lMargin + hStep;
      hfactor = hposr - hposl;
      hmarl = lMargin / hfactor;
      hmarr = 0.0;
    } else if (i == Nx - 1) {
      hposl = hposr + hSpacing;
      hposr = hposl + hStep + rMargin;
      hfactor = hposr - hposl;
      hmarl = 0.0;
      hmarr = rMargin / (hposr - hposl);
    } else {
      hposl = hposr + hSpacing;
      hposr = hposl + hStep;
      hfactor = hposr - hposl;
      hmarl = 0.0;
      hmarr = 0.0;
    }
    for (int j = 0; j < Ny; j++) {
      if (j == 0) {
        vposd = 0.0;
        vposu = bMargin + vStep;
        vfactor = vposu - vposd;
        vmard = bMargin / vfactor;
        vmaru = 0.0;
      } else if (j == Ny - 1) {
        vposd = vposu + vSpacing;
        vposu = vposd + vStep + tMargin;
        vfactor = vposu - vposd;
        vmard = 0.0;
        vmaru = tMargin / (vposu - vposd);
      } else {
        vposd = vposu + vSpacing;
        vposu = vposd + vStep;
        vfactor = vposu - vposd;
        vmard = 0.0;
        vmaru = 0.0;
      }
      debugLine("Plotter::CanvasPartition values set");
      canvas->cd(0);
      char padname[16];
      sprintf(padname, "pad_%i_%i", i, j);
      std::cout << "pad name: " << (name + padname).c_str() << '\n';
      TPad *pad = (TPad *)gROOT->FindObject((name + padname).c_str());
      if (pad)
        delete pad;
      pad = new TPad((name + padname).c_str(), "", hposl, vposd, hposr, vposu);
      pad->SetLeftMargin(hmarl);
      pad->SetRightMargin(hmarr);
      pad->SetBottomMargin(vmard);
      pad->SetTopMargin(vmaru);
      pad->SetFrameBorderMode(0);
      pad->SetBorderMode(0);
      pad->SetBorderSize(0);
      pad->Draw();
      pads.push_back(pad);
      debugLine("Plotter::CanvasPartition pad drawn");
    }
  }

  return pads;
}

TH1F *Plotter::transformEffToHis(TEfficiency *eff) {
  auto total_hist = eff->GetTotalHistogram();
  int bins = total_hist->GetXaxis()->GetNbins();
  double min = total_hist->GetXaxis()->GetXmin();
  double max = total_hist->GetXaxis()->GetXmax();
  TH1F *histogram = new TH1F(eff->GetName(), eff->GetTitle(), bins, min, max);
  for (int i = 1; i <= bins; i++) {
    histogram->SetBinContent(i, eff->GetEfficiency(i));
    histogram->SetBinError(i, std::max(eff->GetEfficiencyErrorLow(i),
                                       eff->GetEfficiencyErrorUp(i)));
    // histogram->SetTitle(eff->GetTitle());
  }
  return histogram;
}

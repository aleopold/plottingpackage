#include "FileReader.h"
#include "HelperFunctions.h"
#include <fstream>


std::vector<std::string> FileReader::readFileLines(const std::string& path) {
  //per line, read multiple strings into a vector of strings
  // Open the File
  std::ifstream input_text_file(path);
  std::vector<std::string> lines;
  std::string line;
  // Read the next line from File untill it reaches the end.
  while (std::getline(input_text_file, line))
  {
  	if(line.size() > 0) {
      if (line.at(0) == '#') {continue;}
      lines.push_back(line);
    }
  }
  return lines;
}



std::vector<FileReader::stringVec_t> FileReader::getVectorizedLines(
      const std::string& path, const std::string& delim) {

  std::vector<FileReader::stringVec_t> comparison_names;
  auto lines = readFileLines(path);
  for (auto& line : lines) {

    auto vector_line = helper::vectorize(line, delim);
    comparison_names.push_back(vector_line);
  }
  return comparison_names;
}

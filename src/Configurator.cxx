#include "Configurator.h"

#include <stdexcept>

Configurator::Configurator(const std::string& path_name, const std::string& main_config_name) :
    m_path_to_config(path_name),
    m_main_cfg_file_name(main_config_name),
    m_path_handler(PathHandler::getInstance()) {

  //check that the main config file exists
  auto file_path = m_path_handler.combinePaths(path_name, main_config_name);
  if (not m_path_handler.fileExists(file_path)) {
    //since this is needed, program needs to exit
    std::string err_msg = "[Configurator]: ERROR main config file path invalid.";
    throw std::invalid_argument(err_msg);
  }

  if (readConfigFile() != 0) {
    std::string err_msg = "[Configurator]: ERROR main config file could not be read.";
    throw std::runtime_error(err_msg);
  }

  //read text files that have to be read
}

int Configurator::readConfigFile() {
  auto file_path = m_path_handler.combinePaths(m_path_to_config, m_main_cfg_file_name);
  //returns 0 if successful
  int ret = m_main_env.ReadFile(file_path.c_str(), EEnvLevel::kEnvUser);
  return ret;
}


std::string Configurator::getPathToConfigs() const {
  return m_path_to_config;
}

#include "HelperFunctions.h"

#include <algorithm>
#include <string>

#include "TCollection.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TROOT.h"
#include "TSystem.h"


std::vector<std::string> helper::vectorize(TString str, TString sep) {
  std::vector<std::string> result;
  TObjArray *strings = str.Tokenize(sep.Data());

  if (strings->GetEntries() == 0) {
    delete strings;
    return result;
  }

  TIter istr(strings);

  while (TObjString *os = (TObjString *)istr()) {
    // the number sign and everything after is treated as a comment
    if (os->GetString()[0] == '#') { break; }

    result.push_back(std::string(os->GetString().Data()));
  }

  delete strings;
  return result;
}

std::vector<int> helper::vectorizeToInt(TString str, TString sep) {
  std::vector<int> result;
  TObjArray *strings = str.Tokenize(sep.Data());

  if (strings->GetEntries() == 0) {
    delete strings;
    return result;
  }

  TIter istr(strings);

  while (TObjString *os = (TObjString *)istr()) {
    // the number sign and everything after is treated as a comment
    if (os->GetString()[0] == '#') { break; }

    result.push_back((os->GetString().Atoi()));
  }

  delete strings;
  return result;
}

std::string helper::removeWhiteSpace(std::string str) {
  std::string::iterator end_pos = std::remove(str.begin(), str.end(), ' ');
  str.erase(end_pos, str.end());
  return str;
}
